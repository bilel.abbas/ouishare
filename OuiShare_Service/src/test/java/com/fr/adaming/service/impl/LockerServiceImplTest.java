package com.fr.adaming.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.adaming.OuiShareServiceApplication;
import com.fr.adaming.entity.Locker;
import com.fr.adaming.service.ILockerService;

@SpringBootTest(classes = OuiShareServiceApplication.class)
public class LockerServiceImplTest {

	@Autowired
	private ILockerService service;

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Test
	@Sql(statements = "delete from locker where adress like 'adress'", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createValidLocker_shouldReturnGoodEntity() {
		// Preparer les entitées

		Locker l = new Locker("adress", 69000L, "city", 15L, "name", false);

		// invoquer la methode
		Locker result = service.create(l);

		// Verifier le résultat
		assertNotNull(result);
		assertNotNull(result.getId());
		assertEquals("name", result.getName());
		assertEquals(69000L, result.getZipCode());
		assertEquals("city", result.getCity());
		assertEquals(15L, result.getSlot());
		assertEquals("adress", result.getAdress());
	}

	@Test
	@Sql(statements = "insert into locker(id,adress,zip_code,city,slot,name,deleted)values(123,'adress1',123456789,'lyon',123,'pierre',false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id =123", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createNonValidLocker_shouldReturnError() {
		// Preparer les entitées

		Locker locker = new Locker( "adress", 69000L, "city", 15L, "pierre", false);

		// invoquer la methode
		Locker result = service.create(locker);

		// Verifier le résultat
		assertNull(result);
	}

	@Test
	@Sql(statements = "insert into locker(id,adress,zip_code,city,slot,name,deleted)values(125,'adress1',123456789,'lyon',123,'jacqo',false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id =125", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createEmptyLocker_shouldReturnError() {
		// Preparer les entitées

		Locker locker = new Locker();

		// invoquer la methode
		Locker result = service.create(locker);

		// Verifier le résultat
		assertNull(result);
	}

	@Test
	@Sql(statements = "insert into locker(id,adress,zip_code,city,slot,name,deleted)values(456,'adress2',123456789,'lyon',123,'Michel',false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into locker(id,adress,zip_code,city,slot,name,deleted)values(457,'adress3',123456789,'lyon',123,'jc',false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id =456", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "delete from locker where id =457", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findAllLockersWhenTheyExist_shouldReturnList() {
		List<Locker> list = service.readAll();
		assertNotNull(list);
		assertThat(list).asList().hasSize(2);
	}

	@Test
	public void findAllLockerIfNotExist_shouldReturnEmptyList() {
		assertTrue(service.readAll().isEmpty());
	}

	@Test
	@Sql(statements = "insert into locker(id,adress,zip_code,city,slot,name,deleted)values(555,'adress3',123456789,'lyon',123,'jean',false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id =555", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateLocker_shouldReturnTrue() {
		Locker locker = new Locker(555L, "adressmodif", 987654321L, "city", 123L, "pierre", false);

		Locker retour = service.update(locker);

		assertEquals("adressmodif", retour.getAdress());
		assertEquals(987654321L, retour.getZipCode());
	}

	@Test
	@Sql(statements = "insert into locker(id,adress,zip_code,city,slot,name,deleted)values(555,'adress3',123456789,'lyon',123,'jean',false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id =555", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateLockerwithWrongtId_shouldReturnError() {
		Locker locker = new Locker(556L, "adressmodif", 987654321L, "city", 123L, "pierre", false);

		exception.expect(InvalidDataAccessApiUsageException.class);
		Locker retour = service.update(locker);
		
		assertNull(retour);
	}

	@Test
	@Sql(statements = "insert into locker(id,adress,zip_code,city,slot,name,deleted)values(666,'adress3',123456789,'lyon',123,'patou',false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id =666", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void fakeDeleteOfLocker_shouldReturnTrue() {

		assertTrue(service.setDeletedTrue(666L));
	}

	@Test
	@Sql(statements = "insert into locker(id,adress,zip_code,city,slot,name,deleted)values(667,'adress3',123456789,'lyon',123,'patou',false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id =667", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "delete from locker where id =668", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void fakeDeleteOfLockerWithWrongId_shouldReturnError() {

		assertFalse(service.setDeletedTrue(668L));
	}

	@Test
	@Sql(statements = "insert into locker(id,adress,zip_code,city,slot,name,deleted)values(777,'adress3',123456789,'lyon',123,'patou',false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id =777", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void realDeleteGoodLocker_shouldReturnNothing() {

		Locker l = new Locker(777L, "adress", 69000L, "city", 15L, "name", false);

		// invoquer la methode
		boolean result = service.deleteById(l.getId());

		// Verifier le résultat
		assertTrue(result);
	}
 
	@Test
	@Sql(statements = "delete from locker where id =778", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "delete from locker where id =779", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "insert into locker(id,adress,zip_code,city,slot,name,deleted)values(778,'adress3',123456789,'lyon',123,'patou',false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void realDeleteLockerwithWrongId_shouldReturnFalse() {

		Locker l = new Locker(779L, "adress", 69000L, "city", 15L, "name", false);

		// invoquer la methode
		boolean result = service.deleteById(l.getId());

		// Verifier le résultat
		assertFalse(result);
	}
	
	@Test
	@Sql(statements = "insert into locker(id,adress,zip_code,city,slot,name,deleted)values(456,'adress2',123456789,'lyon',123,'Michel',true)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into locker(id,adress,zip_code,city,slot,name,deleted)values(457,'adress3',123456789,'lyon',123,'jc',true)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id =456", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "delete from locker where id =457", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findDeleteLockersWhenTheyExist_shouldReturnList() {
		List<Locker> list = service.readDelete();
		assertNotNull(list);
		assertThat(list).asList().hasSize(2);
	}

	@Test
	@Sql(statements = "insert into locker(id,adress,zip_code,city,slot,name,deleted)values(1,'adress1',123456789,'lyon',123,'pierre',false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Insert into slot (id, dimensions, number,deleted) values (1,'10x10x10',45676,false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Insert into type (id, name, stock,deleted) values (1,'Aspirateur',45,false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD )
	@Sql(statements = "INSERT INTO device (id,brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) VALUES (1,'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = {"UPDATE Slot SET id_device=1 where id=1","UPDATE Slot SET id_locker=1 where id=1"," update device set id_type=1 where id=1","update device set id_slot=1 where id=1" },executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = {"UPDATE Slot SET id_device=null where id=1","UPDATE Slot SET id_locker=null where id=1"," update device set id_type=null where id=1","update device set id_slot=null where id=1" },executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = {"delete from locker where id =1","delete from slot where id =1","delete from type where id =1","delete from device where id =1"},executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllTypesAvailableWithValidLockerId_shouldReturnOk() {

		List<String> listTypes = service.allType(1L);

		assertEquals(1,listTypes.size());
		assertThat(listTypes).hasOnlyElementsOfType(String.class);
	}

	@Test
	public void findDeleteLockerIfNotExist_shouldReturnEmptyList() {
		assertTrue(service.readDelete().isEmpty());
	}
	
	@Test
	@Sql(statements = "insert into locker(id,adress,zip_code,city,slot,name,deleted)values(456,'adress2',123456789,'lyon',123,'Michel',false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into locker(id,adress,zip_code,city,slot,name,deleted)values(457,'adress3',123456789,'lyon',123,'jc',false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id =456", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "delete from locker where id =457", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findRealLockersWhenTheyExist_shouldReturnList() {
		List<Locker> list = service.readAllReal();
		assertNotNull(list);
		assertThat(list).asList().hasSize(2);
	}

	@Test
	@Sql(statements = "insert into locker(id,adress,zip_code,city,slot,name,deleted)values(1,'adress1',123456789,'lyon',123,'pierre',false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Insert into slot (id, dimensions, number,deleted) values (1,'10x10x10',45676,false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "Insert into Type (id, name, stock,deleted) values (1,'Aspirateur',45,false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD )
	@Sql(statements = "INSERT INTO device (id,brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) VALUES (1,'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = {"UPDATE Slot SET id_device=1 where id=1","UPDATE Slot SET id_locker=1 where id=1"," update device set id_type=1 where id=1","update device set id_slot=1 where id=1" },executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = {"UPDATE Slot SET id_device=null where id=1","UPDATE Slot SET id_locker=null where id=1"," update device set id_type=null where id=1","update device set id_slot=null where id=1" },executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = {"delete from locker where id =1","delete from slot where id =1","delete from type where id =1","delete from device where id =1"},executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllTypesAvailableWithValidLockerIdNull_shouldReturnError() {

		List<String> listTypes = service.allType(null);

		assertTrue(listTypes.isEmpty());
		assertThat(listTypes).hasOnlyElementsOfType(String.class);
	}

	@Test
	public void findRealLockerIfNotExist_shouldReturnEmptyList() {
		assertTrue(service.readAllReal().isEmpty());
	}
}