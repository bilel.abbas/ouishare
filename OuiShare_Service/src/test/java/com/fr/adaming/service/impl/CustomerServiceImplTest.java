package com.fr.adaming.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.adaming.OuiShareServiceApplication;
import com.fr.adaming.entity.Customer;
import com.fr.adaming.service.ICustomerService;

/**
 * @author CORNELOUP Theo
 *
 */
@SpringBootTest(classes = OuiShareServiceApplication.class)
public class CustomerServiceImplTest {

	@Autowired
	private ICustomerService service;

	@Test
	@Sql(statements = "delete from customer where id=1", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveValideCustomerExample_shouldReturnNewCustomerWithIdNotNull() {

		Customer c = new Customer();

		// prepare inputs
		c.setId(1L);
		c.setEmail("a@a.fr");
		c.setLastName("Murray");
		c.setFirstName("billy");
		c.setAdress("adresse");
		c.setZipCode(0123);
		c.setCity("city");
		c.setPwd("azerty");
		c.setPhone("012345678");
		c.setDeleted(false);

		// invoke method
		Customer result = service.create(c);

		// verify result
		assertNotNull(result);
		assertNotNull(result.getId());
		assertEquals("a@a.fr", result.getEmail());
		assertEquals("Murray", result.getLastName());
		assertEquals("billy", result.getFirstName());
		assertEquals("adresse", result.getAdress());
		assertEquals(0123, result.getZipCode());
		assertEquals("city", result.getCity());
		assertEquals("azerty", result.getPwd());
		assertEquals("012345678", result.getPhone());
	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveExistingCustomer_shouldReturnEntityWithNotNullId() {
		// prepare inputs
		Customer c = new Customer();
		c.setId(1L);
		c.setEmail("a@a.fr");
		c.setLastName("Murray");
		c.setAdress("adresse");
		c.setZipCode(123);
		c.setCity("city");
		c.setPwd("azerty");
		c.setDeleted(false);

		// invoke method
		Customer result = service.create(c);

		assertNull(result);

	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 0123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateExistingCustomer_shouldReturnEntityWithNotNullId() {

		Customer c = new Customer();
		c.setId(1L);
		c.setEmail("de@de.fr");
		c.setLastName("billy");
		c.setAdress("rue");
		c.setZipCode(0456);
		c.setCity("lyon");
		c.setPwd("aqwzsx");
		c.setDeleted(false);

		// invoke method
		Customer result = service.update(c);

		assertNotNull(result);
		assertNotNull(result.getId());
		assertEquals("de@de.fr", result.getEmail());
		assertEquals("billy", result.getLastName());

	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 0123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateDifferentIdCustomer_shouldNotReturnEntityWithIdNUll() {

		Customer c = new Customer();
		c.setId(45L);
		c.setEmail("de@de.fr");
		c.setLastName("billy");
		c.setAdress("rue");
		c.setZipCode(0456);
		c.setCity("lyon");
		c.setPwd("aqwzsx");
		c.setDeleted(false);

		// invoke method
		Customer result = service.update(c);

		assertNull(result);
	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 0123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteValidCustomer_shouldReturnTrue() {

		Customer c = new Customer();
		c.setId(1L);

		assertTrue(service.deleteById(c.getId()));
	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'v@v.fr','Murray', 'adresse', 0123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteNotExistingCustomer_shouldReturnTrue() {

		Customer c = new Customer();
		c.setId(45L);
		c.setEmail("p@p.fr");

		assertFalse(service.deleteById(c.getId()));

	}

	@Test
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void EmptyListCustomer_shouldReturnEmptyList() {

		List<Customer> list = service.readAll();
		assertTrue(list.isEmpty());

	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 0123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void ListValidCustomer_shouldNotReturnEmptyList() {

		List<Customer> list = service.readAll();
		assertFalse(list.isEmpty());

	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 0123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void setDeletedTrueExistingCustomer_shouldReturnTrue() {

		Customer c = new Customer();
		c.setId(1L);

		assertTrue(service.setDeletedTrue(c.getId()));

	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 0123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void setDeletedTrueNotExistingCustomer_shouldReturnFalse() {

		Customer c = new Customer();
		c.setId(45L);

		assertFalse(service.setDeletedTrue(c.getId()));

	}

	@Test
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void setDeletedNotExistingCustomer_shouldReturnFalse() {

		Customer c = new Customer();
		c.setId(100L);

		assertFalse(service.setDeletedTrue(c.getId()));
	}

	@Test
	@Sql(statements = { "delete from customer",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 0123, 'city', 'azerty', false)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (2, 'b@b.fr','Murray', 'adresse', 0123, 'city', 'azerty', true)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (3, 'c@c.fr','Murray', 'adresse', 0123, 'city', 'azerty', true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void List2SetDeletedTrueAnd1SetDeletedFalse_shouldReturnAListWith2Elements() {

		List<Customer> list = service.readDelete();
		assertTrue(list.size() == 2);
	}

	@Test
	@Sql(statements = { "delete from customer",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 0123, 'city', 'azerty', false)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (2, 'b@b.fr','Murray', 'adresse', 0123, 'city', 'azerty', true)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (3, 'c@c.fr','Murray', 'adresse', 0123, 'city', 'azerty', true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void List2SetDeletedTrueAnd1SetDeletedFalse_shouldReturnAListWith1Element() {

		List<Customer> list = service.readAll();
		assertTrue(list.size() == 1);
	}

	@Test
	@Sql(statements = { "delete from customer",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'rue', 0123, 'city', 'azerty', false)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (2, 'b@b.fr','Murray', 'rue', 0123, 'city', 'azerty', true)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (3, 'c@c.fr','Jean', 'adresse', 0123, 'city', 'azerty', true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void ListOfCustomer_shouldReturnListOfCustomerByTheirLastName() {

		List<Customer> list = service.findByLastName("Murray");
		assertTrue(list.size() == 2);
	}

	@Test
	@Sql(statements = { "delete from customer",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'rue', 0123, 'city', 'azerty', false)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (2, 'b@b.fr','Murray', 'rue', 0123, 'city', 'azerty', true)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (3, 'c@c.fr','Murray', 'adresse', 145, 'city', 'azerty', true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void ListOfCustomer_shouldReturnListOfCustomerByTheirZipCode() {

		List<Customer> list = service.findByZipCode(145);
		assertTrue(list.size() == 1);
	}

	@Test
	@Sql(statements = { "delete from customer",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'rue', 0123, 'city', 'azerty', false)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (2, 'b@b.fr','Murray', 'rue', 0123, 'city', 'azerty', true)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (3, 'c@c.fr','Murray', 'adresse', 0123, 'Lyon', 'azerty', true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void ListOfCustomer_shouldReturnListOfCustomerByTheirCity() {

		List<Customer> list = service.findByCity("Lyon");
		assertTrue(list.size() == 1);
	}

	@Test
	@Sql(statements = { "delete from customer",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'rue', 0123, 'city', 'azerty', false)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (2, 'b@b.fr','Murray', 'rue', 0123, 'city', 'azerty', true)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (3, 'c@c.fr','Murray', 'adresse', 0123, 'city', 'azerty', true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void ListOfCustomer_shouldReturnListOfCustomerByTheirAdress() {

		List<Customer> list = service.findByAdress("rue");
		assertTrue(list.size() == 2);
	}

	@Test
	@Sql(statements = "delete from customer where id=1", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveCustomerWithNullEmail_shouldReturnFail() {

		Customer c = new Customer();

		// prepare inputs
		c.setId(1L);
		c.setEmail(null);
		c.setLastName("Murray");
		c.setFirstName("billy");
		c.setAdress("adresse");
		c.setZipCode(0123);
		c.setCity("city");
		c.setPwd("azerty");
		c.setPhone("012345678");
		c.setDeleted(false);

		assertThrows(DataIntegrityViolationException.class, () -> {
			service.create(c);
		});
	}

	
}
