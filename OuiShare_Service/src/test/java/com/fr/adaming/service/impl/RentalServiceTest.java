package com.fr.adaming.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.adaming.OuiShareServiceApplication;
import com.fr.adaming.entity.Customer;
import com.fr.adaming.entity.Device;
import com.fr.adaming.entity.Rental;

/**
 * @author Nicolas RUFFIER
 *
 */
@SpringBootTest(classes = OuiShareServiceApplication.class)
public class RentalServiceTest {

	@Autowired
	private RentalServiceImpl service;

	/**************************** TEST CREATE *****************/

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 30000101, 30000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createValidRental_shouldReturnRentalWithIdNotNull() {
		Device device = new Device("a", "a", "a", 1D, 1D, 1D, 1D, "a", "a", 1L, 1D, "a");
		device.setId(1L);
		Customer customer = new Customer();
		customer.setId(1L);
//		customer.setEmail("a");
//		customer.setLastName("a");
//		customer.setAdress("a");
//		customer.setZipCode(1);
//		customer.setCity("a");
//		customer.setPwd("a");
//		customer.setPhone("1");
//		customer.setDeleted(false);
		LocalDateTime start = LocalDateTime.now().plusHours(1);
		LocalDateTime end = LocalDateTime.now().plusHours(2);
		Rental rental = new Rental(start, end, false, device, customer);
		Rental out = service.create(rental);

		assertNotNull(out);
		assertNotNull(out.getId());
		assertNotNull(out.getStartDate());
		assertNotNull(out.getEndDateTheoritical());
		assertNotNull(out.getDevice());
		assertNotNull(out.getCustomer());
		assertEquals(out.getStartDate(), start);
		assertEquals(out.getEndDateTheoritical(), end);
		assertEquals(out.getDevice(), device);
		assertEquals(out.getCustomer(), customer);
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 30000101, 30000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createRentalWithNoAvaibility_shouldReturnRentalNull() {
		Device device = new Device("a", "a", "a", 1D, 1D, 1D, 1D, "a", "a", 1L, 1D, "a");
		device.setId(1L);
		Customer customer = new Customer();
		customer.setId(1L);
		customer.setEmail("a");
		customer.setLastName("a");
		customer.setAdress("a");
		customer.setZipCode(1);
		customer.setCity("a");
		customer.setPwd("a");
		customer.setPhone("1");
		customer.setDeleted(false);
		LocalDateTime start = LocalDateTime.of(2999, 12, 31, 00, 00);
		LocalDateTime end = LocalDateTime.of(3000, 01, 01, 12, 00);
		Rental rental = new Rental(start, end, false, device, customer);
		Rental out = service.create(rental);

		assertNull(out);
	}

	
	/**************************** TEST UPDATE *****************/

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateWithValidRental_ShouldReturnUpdatedRentalNotNull() {
		Rental rental = service.readById(1L);
		rental.setEndDateReal(LocalDateTime.now());
		Rental out = service.update(rental);

		assertNotNull(out);
		assertNotNull(out.getId());
		assertNotNull(out.getStartDate());
		assertNotNull(out.getEndDateTheoritical());
		assertNotNull(out.getDevice());
		assertNotNull(out.getCustomer());
		assertEquals(1L, out.getId());
		assertEquals(LocalDateTime.of(2000, 01, 01, 00, 00), out.getStartDate());
		assertEquals(LocalDateTime.of(2000, 01, 31, 00, 00), out.getEndDateTheoritical());

	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateRentalWithIdNull_ShouldReturnNull() {
		Rental rental = service.readById(1L);
		rental.setId(null);
		rental.setEndDateReal(LocalDateTime.now());
		Rental out = service.update(rental);

		assertNull(out);
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateRentalWithIdDontExist_ShouldReturnNull() {
		Rental rental = service.readById(1L);
		rental.setId(2L);
		rental.setEndDateReal(LocalDateTime.now());
		Rental out = service.update(rental);

		assertNull(out);
	}

	/**************************** TEST READYBYID *****************/

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readByIdWithValidId_shouldReturnRentalNotNullWithId() {
		Rental out = service.readById(1L);

		assertNotNull(out);
		assertNotNull(out.getId());
		assertNotNull(out.getStartDate());
		assertNotNull(out.getEndDateTheoritical());
		assertNotNull(out.getDevice());
		assertNotNull(out.getCustomer());
		assertEquals(1L, out.getId());
		assertFalse(out.isDeleted());
		assertEquals(out.getStartDate(), LocalDateTime.of(2000, 01, 01, 00, 00));
		assertEquals(out.getEndDateTheoritical(), LocalDateTime.of(2000, 01, 31, 00, 00));
		assertFalse(out.isSubscription());
		assertSame(1L, out.getDevice().getId());
		assertSame(1L, out.getCustomer().getId());
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readByIdWithInvalidId_shouldReturnNull() {
		Rental out = service.readById(2L);

		assertNull(out);
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readByIdWithIdNull_shouldReturnInvalidDataAccessApiUsageException() {
		assertThrows(InvalidDataAccessApiUsageException.class, () -> {
			service.readById(null);
		});
	}

	/**************************** TEST READALL *****************/

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAll_shouldReturnListWith1Entry() {
		List<Rental> list = service.readAll();

		assertEquals(1, list.size());
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(2, 1, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllWithDeletedRental_shouldReturnListWith1Entry() {
		List<Rental> list = service.readAll();

		assertEquals(1, list.size());
	}

	/**************************** TEST READDELETE *****************/

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 1, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readDeleted_shouldReturnListWith1Entry() {
		List<Rental> list = service.readDelete();

		assertEquals(1, list.size());
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(2, 1, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readDeletedWithNotDeletedRental_shouldReturnListWith1Entry() {
		List<Rental> list = service.readDelete();

		assertEquals(1, list.size());
	}

	/**************************** TEST READALLREAL *****************/

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(2, 1, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllReal_shouldReturnListWith2Entry() {
		List<Rental> list = service.readAllReal();

		assertEquals(2, list.size());
	}

	/**************************** TEST DELETEBYID *****************/

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteByIdWithValidId_shouldReturnTrue() {
		assertTrue(service.deleteById(1L));
		assertNull(service.readById(1L));
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteByIdWithInvalidId_shouldReturnFalse() {
		assertFalse(service.deleteById(2L));
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteByIdWithIdNull_shouldReturnFalse() {
		assertFalse(service.deleteById(null));
	}

	/**************************** TEST SETDELETEDTRUE *****************/

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void setDeletedTrueWithValidId_shouldReturnTrue() {
		assertTrue(service.setDeletedTrue(1L));
		assertTrue(service.readById(1L).isDeleted());
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void setDeletedTrueWithInvalidId_shouldReturnFalse() {
		assertFalse(service.setDeletedTrue(2L));
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void setDeletedTrueWithIdNull_shouldReturnFalse() {
		assertFalse(service.setDeletedTrue(null));
	}

	/**************************** TEST AVAILABILITY *****************/

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void availabilityWithRentalAfterNow_shouldReturnListWith1Entry() {
		Device device = new Device();
		device.setId(1L);
		Customer customer = new Customer();
		customer.setId(1L);
		LocalDateTime start = LocalDateTime.now().plusHours(1);
		LocalDateTime end = LocalDateTime.now().plusHours(2);
		Rental rental = new Rental(start, end, false, device, customer);
		service.create(rental);

		List<Rental> list = service.availability(device);
		assertEquals(1, list.size());

	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void availabilityWithRentalBeforeNow_shouldReturnEmptyList() {
		Device device = new Device();
		device.setId(1L);
		Customer customer = new Customer();
		customer.setId(1L);
		LocalDateTime start = LocalDateTime.now().minusHours(2);
		LocalDateTime end = LocalDateTime.now().minusHours(1);
		Rental rental = new Rental(start, end, false, device, customer);
		service.create(rental);

		List<Rental> list = service.availability(device);
		assertEquals(0, list.size());

	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void availabilityWithDeviceNull_shouldReturnEmptyList() {
		Device device = new Device();
		device.setId(1L);
		Customer customer = new Customer();
		customer.setId(1L);
		LocalDateTime start = LocalDateTime.now().minusHours(2);
		LocalDateTime end = LocalDateTime.now().minusHours(1);
		Rental rental = new Rental(start, end, false, device, customer);
		service.create(rental);

		List<Rental> list = service.availability(null);
		assertTrue(list.isEmpty());
	}

	/**************************** TEST ADDREVIEW *****************/

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER", "DELETE FROM REVIEW",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20200101, 20200131, 0, 1, 1)",
			"insert into review (id, deleted, rating) "
					+ "values(1, 0, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"DELETE FROM REVIEW" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addReview_shouldReturnTrue() {
		assertTrue(service.addReview(1L, 1L));
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER", "DELETE FROM REVIEW",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20200101, 20200131, 0, 1, 1)",
			"insert into review (id, deleted, rating) "
					+ "values(1, 0, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"DELETE FROM REVIEW" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addReviewWithInvalidRentalId_shouldReturnFalse() {
		assertFalse(service.addReview(2L, 1L));
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER", "DELETE FROM REVIEW",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20200101, 20200131, 0, 1, 1)",
			"insert into review (id, deleted, rating) "
					+ "values(1, 0, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"DELETE FROM REVIEW" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addReviewWithInvalidReviewId_shouldReturnFalse() {
		assertFalse(service.addReview(1L, 2L));
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER", "DELETE FROM REVIEW",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into review (id, deleted, rating) " + "values(1, 0, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id, review_id) "
					+ "values(1, 0, 20200101, 20200131, 0, 1, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"DELETE FROM REVIEW" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addReviewWithRentalWithReviewAlready_shouldReturnFalse() {
		assertFalse(service.addReview(1L, 1L));
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER", "DELETE FROM REVIEW",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into review (id, deleted, rating) " + "values(1, 0, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id, review_id) "
					+ "values(1, 0, 20200101, 20200131, 0, 1, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"DELETE FROM REVIEW" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addReviewWithRentalWithReviewAlreadyAndInvalidReviewId_shouldReturnFalse() {
		assertFalse(service.addReview(1L, 2L));
	}

	/**************************** TEST ADDENDDATEREAL *****************/

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER", "DELETE FROM REVIEW",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"DELETE FROM REVIEW" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addEndDateRealEndDateRealBeforeEndDatetheoritical_shouldReturn0() {
		Device device = new Device();
		device.setId(1L);
		Customer customer = new Customer();
		customer.setId(1L);
		LocalDateTime start = LocalDateTime.now().minusHours(1);
		LocalDateTime end = LocalDateTime.now().plusHours(1);
		Rental rental = new Rental(start, end, false, device, customer);
		rental.setId(1L);
		Rental out = service.create(rental);

		assertEquals(0L, service.addEndDateReal(out.getId()));
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER", "DELETE FROM REVIEW",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20200101, 20200131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"DELETE FROM REVIEW" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addEndDateRealWithInvalidId_shouldReturnNull() {
		assertNull(service.addEndDateReal(2L));
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER", "DELETE FROM REVIEW",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20200101, 20200131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"DELETE FROM REVIEW" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addEndDateRealWithIdNull_shouldReturnNull() {
		assertThrows(InvalidDataAccessApiUsageException.class, () -> {
			service.addEndDateReal(null);
		});
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER", "DELETE FROM REVIEW",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, end_date_real, subscription, device_id, customer_id) "
					+ "values(1, 0, 20200101, 20200131, 20200130, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"DELETE FROM REVIEW" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addEndDateRealWithEndDateRealAlready_shouldReturnNull() {
		assertNull(service.addEndDateReal(1L));
	}

	@Test
	@Sql(statements = { "DELETE FROM DEVICE", "DELETE FROM CUSTOMER", "DELETE FROM RENTAL", "DELETE FROM REVIEW",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"DELETE FROM REVIEW" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addEndDateRealWithEndDateRealBeforeStartDate_shouldReturnNull() {
		Device device = new Device();
		device.setId(1L);
		Customer customer = new Customer();
		customer.setId(1L);
		LocalDateTime start = LocalDateTime.now().plusHours(1);
		LocalDateTime end = LocalDateTime.now().plusHours(2);
		Rental rental = new Rental(start, end, false, device, customer);
		Rental out = service.create(rental);

		assertNull(service.addEndDateReal(out.getId()));
	}

	@Test
	@Sql(statements = { "DELETE FROM DEVICE", "DELETE FROM CUSTOMER", "DELETE FROM RENTAL", "DELETE FROM REVIEW",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"DELETE FROM REVIEW" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addEndDateRealWithEndDateReal15MinutesAfterEndDateTheoritical_shouldReturn15L() {
		Device device = new Device();
		device.setId(1L);
		Customer customer = new Customer();
		customer.setId(1L);
		LocalDateTime start = LocalDateTime.now().minusMinutes(75);
		LocalDateTime end = LocalDateTime.now().minusMinutes(15);
		Rental rental = new Rental(start, end, false, device, customer);
		Rental out = service.create(rental);

		assertEquals(15L, service.addEndDateReal(out.getId()));
	}

	@Test
	@Sql(statements = { "DELETE FROM DEVICE", "DELETE FROM CUSTOMER", "DELETE FROM RENTAL", "DELETE FROM REVIEW",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"DELETE FROM REVIEW" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addEndDateRealWithEndDateReal1DayAfterEndDateTheoritical_shouldReturn1440L() {
		Device device = new Device();
		device.setId(1L);
		Customer customer = new Customer();
		customer.setId(1L);
		LocalDateTime start = LocalDateTime.now().minusDays(1).minusHours(1);
		LocalDateTime end = LocalDateTime.now().minusDays(1);
		Rental rental = new Rental(start, end, false, device, customer);
		Rental out = service.create(rental);

		assertEquals(1440L, service.addEndDateReal(out.getId()));
	}

	@Test
	@Sql(statements = { "DELETE FROM DEVICE", "DELETE FROM CUSTOMER", "DELETE FROM RENTAL", "DELETE FROM REVIEW",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"DELETE FROM REVIEW" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addEndDateRealWithEndDateReal50SecondsAfterEndDateTheoritical_shouldReturn0L() {
		Device device = new Device();
		device.setId(1L);
		Customer customer = new Customer();
		customer.setId(1L);
		LocalDateTime start = LocalDateTime.now().minusHours(1);
		LocalDateTime end = LocalDateTime.now().minusSeconds(50);
		Rental rental = new Rental(start, end, false, device, customer);
		Rental out = service.create(rental);

		assertEquals(0L, service.addEndDateReal(out.getId()));
	}

	@Test
	@Sql(statements = { "DELETE FROM DEVICE", "DELETE FROM CUSTOMER", "DELETE FROM RENTAL", "DELETE FROM REVIEW",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"DELETE FROM REVIEW" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addEndDateRealWithEndDateReal61SecondsAfterEndDateTheoritical_shouldReturn1L() {
		Device device = new Device();
		device.setId(1L);
		Customer customer = new Customer();
		customer.setId(1L);
		LocalDateTime start = LocalDateTime.now().minusHours(1);
		LocalDateTime end = LocalDateTime.now().minusSeconds(61);
		Rental rental = new Rental(start, end, false, device, customer);
		Rental out = service.create(rental);

		assertEquals(1L, service.addEndDateReal(out.getId()));
	}

	/**************************** TEST VALIDATECREATE *****************/

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void validateCreateValidRental_shouldReturnTrue() {
		Device device = new Device("a", "a", "a", 1D, 1D, 1D, 1D, "a", "a", 1L, 1D, "a");
		device.setId(1L);
		Customer customer = new Customer();
		customer.setId(1L);
		customer.setEmail("a");
		customer.setLastName("a");
		customer.setAdress("a");
		customer.setZipCode(1);
		customer.setCity("a");
		customer.setPwd("a");
		customer.setPhone("1");
		customer.setDeleted(false);
		LocalDateTime start = LocalDateTime.now().plusHours(1);
		LocalDateTime end = LocalDateTime.now().plusHours(2);
		Rental rental = new Rental(start, end, false, device, customer);
		assertTrue(service.validateCreate(rental));
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void validateCreateRentalNull_shouldReturnFalse() {
		assertFalse(service.validateCreate(null));
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void validateCreateRentalWithDeviceNull_shouldReturnFalse() {
		Customer customer = new Customer();
		customer.setId(1L);
		customer.setEmail("a");
		customer.setLastName("a");
		customer.setAdress("a");
		customer.setZipCode(1);
		customer.setCity("a");
		customer.setPwd("a");
		customer.setPhone("1");
		customer.setDeleted(false);
		LocalDateTime start = LocalDateTime.now().plusHours(1);
		LocalDateTime end = LocalDateTime.now().plusHours(2);
		Rental rental = new Rental(start, end, false, null, customer);
		assertFalse(service.validateCreate(rental));
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void validateCreateRentalWithCustomerNull_shouldReturnFalse() {
		Device device = new Device("a", "a", "a", 1D, 1D, 1D, 1D, "a", "a", 1L, 1D, "a");
		device.setId(1L);
		LocalDateTime start = LocalDateTime.now().plusHours(1);
		LocalDateTime end = LocalDateTime.now().plusHours(2);
		Rental rental = new Rental(start, end, false, device, null);
		assertFalse(service.validateCreate(rental));
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void validateCreateRentalWithCustomerAndDeviceNull_shouldReturnFalse() {
		LocalDateTime start = LocalDateTime.now().plusHours(1);
		LocalDateTime end = LocalDateTime.now().plusHours(2);
		Rental rental = new Rental(start, end, false, null, null);
		assertFalse(service.validateCreate(rental));
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void validateCreateRentalWithDeviceIdNull_shouldReturnNull() {
		Device device = new Device("a", "a", "a", 1D, 1D, 1D, 1D, "a", "a", 1L, 1D, "a");
		Customer customer = new Customer();
		customer.setId(1L);
		customer.setEmail("a");
		customer.setLastName("a");
		customer.setAdress("a");
		customer.setZipCode(1);
		customer.setCity("a");
		customer.setPwd("a");
		customer.setPhone("1");
		customer.setDeleted(false);
		LocalDateTime start = LocalDateTime.now().plusHours(1);
		LocalDateTime end = LocalDateTime.now().plusHours(2);
		Rental rental = new Rental(start, end, false, device, customer);
		assertFalse(service.validateCreate(rental));
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void validateCreateRentalWithCustomerIdNull_shouldReturnFalse() {
		Device device = new Device("a", "a", "a", 1D, 1D, 1D, 1D, "a", "a", 1L, 1D, "a");
		device.setId(1L);
		Customer customer = new Customer();
		customer.setEmail("a");
		customer.setLastName("a");
		customer.setAdress("a");
		customer.setZipCode(1);
		customer.setCity("a");
		customer.setPwd("a");
		customer.setPhone("1");
		customer.setDeleted(false);
		LocalDateTime start = LocalDateTime.now().plusHours(1);
		LocalDateTime end = LocalDateTime.now().plusHours(2);
		Rental rental = new Rental(start, end, false, device, null);
		assertFalse(service.validateCreate(rental));
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void validateCreateRentalWithCustomerIdAndDeviceIdNull_shouldReturnFalse() {
		Device device = new Device("a", "a", "a", 1D, 1D, 1D, 1D, "a", "a", 1L, 1D, "a");
		Customer customer = new Customer();
		customer.setEmail("a");
		customer.setLastName("a");
		customer.setAdress("a");
		customer.setZipCode(1);
		customer.setCity("a");
		customer.setPwd("a");
		customer.setPhone("1");
		customer.setDeleted(false);
		LocalDateTime start = LocalDateTime.now().plusHours(1);
		LocalDateTime end = LocalDateTime.now().plusHours(2);
		Rental rental = new Rental(start, end, false, device, null);
		assertFalse(service.validateCreate(rental));
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void validateCreateRentalWithDeviceAndCustomerIdNull_shouldReturnFalse() {
		Customer customer = new Customer();
		customer.setEmail("a");
		customer.setLastName("a");
		customer.setAdress("a");
		customer.setZipCode(1);
		customer.setCity("a");
		customer.setPwd("a");
		customer.setPhone("1");
		customer.setDeleted(false);
		LocalDateTime start = LocalDateTime.now().plusHours(1);
		LocalDateTime end = LocalDateTime.now().plusHours(2);
		Rental rental = new Rental(start, end, false, null, customer);
		assertFalse(service.validateCreate(rental));
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void validateCreateRentalWithCustomerAndDeviceIdNull_shouldReturnFalse() {
		Device device = new Device("a", "a", "a", 1D, 1D, 1D, 1D, "a", "a", 1L, 1D, "a");
		LocalDateTime start = LocalDateTime.now().plusHours(1);
		LocalDateTime end = LocalDateTime.now().plusHours(2);
		Rental rental = new Rental(start, end, false, device, null);
		assertFalse(service.validateCreate(rental));
	}
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void validateCreateRentalWithStartDateAfterEndDateTheoritical_shouldReturnFalse() {
		Device device = new Device("a", "a", "a", 1D, 1D, 1D, 1D, "a", "a", 1L, 1D, "a");
		LocalDateTime start = LocalDateTime.now().plusHours(2);
		LocalDateTime end = LocalDateTime.now().plusHours(1);
		Rental rental = new Rental(start, end, false, device, null);
		assertFalse(service.validateCreate(rental));
	}

}
