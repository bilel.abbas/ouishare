package com.fr.adaming.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.adaming.OuiShareServiceApplication;
import com.fr.adaming.entity.Customer;
import com.fr.adaming.entity.Subscription;
import com.fr.adaming.service.ISubscriptionService;

/**
 * @author Thibaud, Guillaume V.
 *
 */
@SpringBootTest(classes = OuiShareServiceApplication.class)
public class SubscriptionServiceTest {

	@Autowired
	ISubscriptionService service;

	@Test
	public void createSubscriptionTestValid_ShouldReturnSubscriptionObject() {
		Subscription subscription = new Subscription();
		LocalDate startDate = LocalDate.now();
		subscription.setStartDate(startDate);
		subscription.setEndDate(startDate.plusMonths(1));

		Subscription retour = service.create(subscription);

		assertNotNull(retour);
		assertEquals(retour.getEndDate(), startDate.plusMonths(1));
	}

	@Test
	public void createSubscriptionTestNotValid_ShouldReturnNull() {
		Subscription subscription = new Subscription();
		Subscription retour = service.create(subscription);
		assertNull(retour);
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"insert into subscription(id, start_date, end_date, deleted, renew) VALUES (2, '2018-11-24', '2018-11-26', false, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from subscription", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createSubscriptionServiceTestExistant_ShouldReturnNull() {
		Subscription subscription = service.readById(2L);
		assertNull(service.create(subscription));
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (2, 20181124, 20181126, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void updateSubscriptionTestValid_ShouldReturnSubscriptionObject() {
		Subscription subscription = new Subscription();
		subscription.setId(2L);
		subscription.setStartDate(LocalDate.now());
		subscription.setEndDate(LocalDate.now().plusMonths(1));
		Subscription retour = service.update(subscription);
		assertNotNull(retour);
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (2, 20181124, 20181126, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void updateSubscriptionTestIdNull_ShouldReturnNull() {
		Subscription subscription = new Subscription();
		subscription.setId(null);
		subscription.setStartDate(LocalDate.now());
		subscription.setEndDate(LocalDate.now().plusMonths(1));
		Subscription retour = service.update(subscription);
		assertNull(retour);
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (2, 20181124, 20181126, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void updateSubscriptionTestSubscriptionNull_ShouldReturnNull() {
		Subscription subscription = null;
		Subscription retour = service.update(subscription);
		assertNull(retour);
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (2, 20181124, 20181126, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void updateSubscriptionTestSubscriptionNotValid_ShouldReturnNull() {
		Subscription subscription = new Subscription();
		subscription.setId(2L);
		subscription.setStartDate(null);
		Subscription retour = service.update(subscription);
		assertNull(retour);
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (2, 20181124, 20181126, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void deleteValidSubscription_shouldReturnTrue() {
		assertTrue(service.deleteById(2L));
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (2, 20181124, 20181126, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void deleteUnknownSubscription_shouldReturnFalse() {
		assertFalse(service.deleteById(344L));
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (2, 20181124, 20181126, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void deleteSubscriptionIdNull_shouldReturnFalse() {
		Long id = null;
		assertFalse(service.setDeletedTrue(id));
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (3, 20181124, 20181126, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void setDeletedValidSubscription_shouldReturnTrue() {
		assertTrue(service.setDeletedTrue(3L));
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (2, 20181124, 20181126, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void setDeletedUnknownSubscription_shouldReturnFalse() {
		assertFalse(service.setDeletedTrue(3L));
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (2, 20181124, 20181126, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void setDeletedSubscriptionIdNull_shouldReturnFalse() {
		Long id = null;
		assertFalse(service.setDeletedTrue(id));
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (2, 20181124, 20181126, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void readValidSubscriptionById_shouldReturnThisSubscription() {
		Subscription subscription = service.readById(2L);
		assertEquals(subscription.getStartDate(), LocalDate.of(2018, 11, 24));
		assertEquals(subscription.getEndDate(), LocalDate.of(2018, 11, 26));
		assertEquals(subscription.isDeleted(), true);
		assertEquals(subscription.isRenew(), true);
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (3, 20170221, 20170321, false, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void readUnknownSubscriptionById_shouldReturnNull() {
		Subscription subscription = service.readById(9L);
		assertNull(subscription);
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (3, 20170221, 20170321, false, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void readSubscriptionByIdWithIdNull_ShouldReturnNull() {
		Long id = null;
		Subscription subscription = service.readById(id);
		assertNull(subscription);
	}

	@Test
	@Sql(statements = "DELETE FROM subscription", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void readAllSubscriptionEmptyList_shouldReturnEmptyList() {
		List<Subscription> listAll = service.readAll();

		assertEquals(listAll.size(), 0);
	}

	@Test
	@Sql(statements = { "DELETE FROM subscription",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (3, 20170221, 20170321, false, true)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (9, 20190221, 20190321, false, true)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20170221, 20170321, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void readAllSubscriptionWithData_shouldReturnSizeList() {
		List<Subscription> listAll = service.readAll();

		assertEquals(listAll.size(), 2);
	}

	@Test
	@Sql(statements = { "DELETE FROM subscription",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (3, 20170221, 20170321, false, true)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (9, 20180314, 20180414, false, true)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20170221, 20170321, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void readDeletedSubscription_shouldReturnSizeList() {
		List<Subscription> listNotDeleted = service.readDelete();

		assertEquals(listNotDeleted.size(), 1);
	}

	@Test
	@Sql(statements = { "DELETE FROM subscription",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (3, 20170221, 20170321, false, true)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (9, 20180314, 20180414, false, true)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20170221, 20170321, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void readAllRealSubscription_shouldReturnSizeList() {
		List<Subscription> listNotDeleted = service.readAllReal();

		assertEquals(listNotDeleted.size(), 3);
	}

	@Test
	@Sql(statements = { "DELETE FROM subscription", "DELETE FROM customer",
			"Insert INTO customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 123, 'city', 'azerty', false)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew, id_customer) VALUES (3, 20170221, 20170321, false, true, 1)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew, id_customer) VALUES (9, 20180314, 20180414, false, true, 1)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew, id_customer) VALUES (5, 20170221, 20170321, true, true, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void findByCustomer_ShouldReturn3Subscriptions() {
		Customer customer = new Customer();
		customer.setId(1L);
		List<Subscription> listByCustomer = service.findByCustomer(customer);

		assertEquals(listByCustomer.size(), 3);
	}
	
	@Test
	@Sql(statements = { "DELETE FROM subscription", "DELETE FROM customer",
			"Insert INTO customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 123, 'city', 'azerty', false)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew, id_customer) VALUES (3, 20170221, 20170321, false, true, 1)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew, id_customer) VALUES (9, 20180314, 20180414, false, true, 1)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew, id_customer) VALUES (5, 20170221, 20170321, true, true, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void findByCustomer_ShouldReturnNull() {
		Customer customer = new Customer();
		customer.setId(3L);
		List<Subscription> listByCustomer = service.findByCustomer(customer);

		assertTrue(listByCustomer.isEmpty());
	}

	@Test
	@Sql(statements = { "DELETE FROM subscription",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (3, 20170221, 20191121, false, true)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (9, 20180314, 20191114, false, true)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20170221, 20191102, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void renewingTestRenewInputNormal() {
		service.renewing();
		List<Subscription> subscriptions = service.readAll();
		for (Subscription s : subscriptions) {
			assertTrue(s.getEndDate().isAfter(LocalDate.now()));
		}
	}

	@Test
	@Sql(statements = { "DELETE FROM subscription",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (3, 20170221, 20191021, false, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void renewingTestInputToOld() {
		service.renewing();
		Subscription subscription = service.readById(3L);

		assertEquals(subscription.getEndDate(), LocalDate.of(2019, 11, 21));
	}

	@Test
	@Sql(statements = { "DELETE FROM subscription",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (3, 20170221, 20191121, false, false)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (9, 20180314, 20191114, false, false)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20170221, 20191102, true, false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void renewingTestRenewInputNormalRenewFalse() {
		service.renewing();
		List<Subscription> subscriptions = service.readAll();
		for (Subscription s : subscriptions) {
			assertTrue(s.getEndDate().isBefore(LocalDate.now()));
		}
	}
}
