package com.fr.adaming.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.adaming.OuiShareServiceApplication;
import com.fr.adaming.service.ISubscriptionService;

@SpringBootTest(classes = OuiShareServiceApplication.class)
public class SubscriptionServiceTestNewMethods {

	@Autowired
	ISubscriptionService service;
	
	@Test
	@Sql(statements = { "Delete FROM subscription",
	"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (2, 20181124, 20181126, false, false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void reactiveSubscriptionTestValid_ShouldReturnReactivatedSubscription() {
		assertTrue(service.reactiveSubscription(2L));
	}
	
	@Test
	@Sql(statements = { "Delete FROM subscription",
	"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (2, 20181124, 20181126, false, false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void reactiveSubscriptionTestWithFalseId_ShouldNotReturnNReactivatedSubscription() {
		assertFalse(service.reactiveSubscription(3L));
	}
	
	@Test
	@Sql(statements = { "Delete FROM subscription",
	"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (2, 20181124, 20181126, true, false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void reactiveSubscriptionTestWithDeletedSub_ShouldNotReturnNReactivatedSubscription() {
		assertFalse(service.reactiveSubscription(2L));
	}
	
	@Test
	@Sql(statements = { "Delete FROM subscription",
	"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (2, 20181124, 20181126, false, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void reactiveSubscriptionTestWithAlreadyRenewSub_ShouldNotReturnNReactivatedSubscription() {
		assertFalse(service.reactiveSubscription(3L));
	}
	
	@Test
	@Sql(statements = { "Delete FROM subscription",
	"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (2, 20181124, 20181126, false, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void desactiveSubscriptionTestValid_ShouldReturnReactivatedSubscription() {
		assertTrue(service.desactiveSubscription(2L));
	}
	
	@Test
	@Sql(statements = { "Delete FROM subscription",
	"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (2, 20181124, 20181126, false, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void desactiveSubscriptionTestWithFalseId_ShouldNotReturnReactivatedSubscription() {
		assertFalse(service.desactiveSubscription(45L));
	}
	
	@Test
	@Sql(statements = { "Delete FROM subscription",
	"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (2, 20181124, 20181126, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void desactiveSubscriptionTestWithDeletedSub_ShouldNotReturnReactivatedSubscription() {
		assertFalse(service.desactiveSubscription(2L));
	}
	
	@Test
	@Sql(statements = { "Delete FROM subscription",
	"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (2, 20181124, 20181126, false, false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void desactiveSubscriptionTestWithNotRenewSub_ShouldNotReturnReactivatedSubscription() {
		assertFalse(service.desactiveSubscription(2L));
	}
}
