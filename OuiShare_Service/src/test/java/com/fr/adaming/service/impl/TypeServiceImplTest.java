package com.fr.adaming.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.adaming.OuiShareServiceApplication;
import com.fr.adaming.entity.Type;
import com.fr.adaming.service.ITypeService;

/**
 * @author Aurélien
 *
 */
@SpringBootTest(classes = OuiShareServiceApplication.class)
public class TypeServiceImplTest{

	@Autowired
	private ITypeService service;
	
	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	@Test
	public void saveValidType_shouldReturnEntityWithNotNullId() {
			
		Type type = new Type(123L,"Aspirateur",1,false);
		
		Type savedType=service.create(type);
		
		assertNotNull(savedType);
		assertNotNull(savedType.getId());
		
		assertEquals("Aspirateur", savedType.getName());
		assertEquals(1,savedType.getStock());	
	}
	
	@Test
	public void saveNullType_shouldReturnNull() {
		
		assertNull(service.create(null));
	}
	
	@Test
	@Sql(statements = {"Delete from Type","Insert into Type (id, name, stock,deleted) values (20,'Aspirateur',45,false)"},executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void saveAlreadyExistingNameType_shouldThrowException() {
		assertThrows(DataIntegrityViolationException.class, () -> {
			service.create(new Type(12L,"Aspirateur",20));
	    });
	}
	
	@Test
	@Sql(statements = {"Delete from Type","Insert into Type (id, name, stock,deleted) values (20,'Aspirateur',45,false)"},executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void findByIdValidType_shouldReturnType() {
		
		assertNotNull(service.readById(20L));
		assertEquals("Aspirateur",service.readById(20L).getName());
	}

	@Test
	@Sql(statements = "Delete from Type",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void findByIdMissingType_shouldReturnNull() {
		assertNull(service.readById(10L));
	}
		
	@Test
	@Sql(statements = {"Delete from Type","Insert into Type (id, name, stock,deleted) values (20,'Aspirateur',45,false)"},executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void updateValidType_shouldReturnEntity() {
			
		Type type = new Type(20L,"Perceuse",30);
		
		Type updatedType=service.update(type);
		
		assertNotNull(updatedType);
		
		assertEquals(30, updatedType.getStock());
		assertEquals("Perceuse",updatedType.getName());	
	}
	
	@Test
	@Sql(statements = "Delete from Type",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void updateNonExistingType_shouldReturnNull() {
			
		Type type = new Type(20L,"Perceuse",30);
		
		assertNull(service.update(type));
	}
	
	@Test
	public void updateNullType_shouldReturnNull() {
						
		assertNull(service.update(null));
	}
	
	@Test
	public void updateNullIdType_shouldReturnNull() {				
		assertNull(service.update(new Type(null,"Perceuse",30)));
	}
	
	@Test
	@Sql(statements = {"Delete from Type","Insert into Type (id, name, stock,deleted) values (20,'Aspirateur',45,false)"},executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void deleteValidType_shouldReturnTrue() {
					
		assertTrue(service.deleteById(20L));
	}
	
	@Test
	@Sql(statements = "Delete from Type",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void deleteNonExistingType_shouldReturnFalse() {
			
		assertFalse(service.deleteById(20L));
	}	
	
	@Test
	@Sql(statements = {"Delete from Type","Insert into Type (id, name, stock,deleted) values (20,'Aspirateur',45,false)"},executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void fakeDeleteValidType_shouldReturnTrue() {
			
		assertTrue(service.setDeletedTrue(20L));		
		assertTrue(service.readById(20L).isDeleted());
	}
	
	@Test
	@Sql(statements = "Delete from Slot",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void fakeDeleteNonExistingType_shouldReturnFalse() {		
		assertFalse(service.setDeletedTrue(20L));		
	}
}
