package com.fr.adaming.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.adaming.OuiShareServiceApplication;
import com.fr.adaming.entity.Slot;
import com.fr.adaming.service.ISlotService;

/**
 * @author Aurélien
 *
 */

@SpringBootTest(classes = OuiShareServiceApplication.class)
public class SlotServiceImplTest{

	@Autowired
	private ISlotService service;
	
	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	@Test
	public void saveValidSlot_shouldReturnEntityWithNotNullId() {
			
		Slot slot = new Slot(45676L,"10x10x10");
		slot.setId(12L);
		
		Slot savedSlot=service.create(slot);
		
		assertNotNull(savedSlot);
		assertNotNull(savedSlot.getId());
		
		assertEquals(45676L, savedSlot.getNumber());
		assertEquals("10x10x10",savedSlot.getDimensions());	
	}
	
	@Test
	@Sql(statements = {"Delete from Slot","Insert into Slot (id, dimensions, number,deleted) values (20,'10x10x10',45676,false)"},executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void saveAlreadyExistingSlot_shouldReturnNull() {
			
		Slot slot = new Slot(20L,45676L,"10x10x10",false);
		assertNull(service.create(slot));
	}
	
	@Test
	public void saveNullSlot_shouldReturnNull() {
		
		assertNull(service.create(null));
	}
	
	@Test
	@Sql(statements = {"Delete from Slot","Insert into Slot (id, dimensions, number,deleted) values (20,'10x10x10',45676,false)"},executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void findByIdValidSlot_shouldReturnSlot() {
		
		assertNotNull(service.readById(20L));
		assertEquals("10x10x10",service.readById(20L).getDimensions());
	}

	@Test
	@Sql(statements = "Delete from Slot",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void findByIdMissingSlot_shouldReturnNull() {
		assertNull(service.readById(10L));
	}
		
	@Test
	@Sql(statements = {"Delete from Slot","Insert into Slot (id, dimensions, number,deleted) values (20,'10x10x10',45676,false)"},executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void updateValidSlot_shouldReturnEntity() {
			
		Slot slot = new Slot(20L,45677L,"20x20x20",false);
		
		Slot updatedSlot=service.update(slot);
		
		assertNotNull(updatedSlot);
		
		assertEquals(45677L, updatedSlot.getNumber());
		assertEquals("20x20x20",updatedSlot.getDimensions());	
	}
	
	@Test
	@Sql(statements = "Delete from Slot",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void updateNonExistingSlot_shouldReturnNull() {
			
		Slot slot = new Slot(20L,45677L,"20x20x20",false);
		
		Slot updatedSlot=service.update(slot);
		
		assertNull(updatedSlot);
	}
	
	@Test
	public void updateNullSlot_shouldReturnNull() {
						
		assertNull(service.update(null));
	}
	
	@Test
	public void updateNullIdSlot_shouldReturnNull() {				
		assertNull(service.update(new Slot(20L,"20x20x20")));
	}
	
	@Test
	@Sql(statements = {"Delete from Slot","Insert into Slot (id, dimensions, number,deleted) values (20,'10x10x10',45676,false)"},executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void deleteValidSlot_shouldReturnTrue() {
					
		assertTrue(service.deleteById(20L));
	}
	
	@Test
	@Sql(statements = "Delete from Slot",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void deleteNonExistingSlot_shouldReturnFalse() {
			
		assertFalse(service.deleteById(20L));
	}	
	
	@Test
	@Sql(statements = {"Delete from Slot","Insert into Slot (id, dimensions, number,deleted) values (20,'10x10x10',45676,false)"},executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void fakeDeleteValidSlot_shouldReturnTrue() {
			
		assertTrue(service.setDeletedTrue(20L));		
		assertTrue(service.readById(20L).isDeleted());
	}
	
	@Test
	@Sql(statements = "Delete from Slot",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void fakeDeleteNonExistingSlot_shouldReturnFalse() {
			
		assertFalse(service.setDeletedTrue(20L));		
	}
	
	
	
}