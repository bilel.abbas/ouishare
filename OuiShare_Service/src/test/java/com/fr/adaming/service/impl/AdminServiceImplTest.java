package com.fr.adaming.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.adaming.OuiShareServiceApplication;
import com.fr.adaming.entity.Admin;
import com.fr.adaming.service.IAdminService;

/**
 * @author bilel
 *
 */
@SpringBootTest(classes = OuiShareServiceApplication.class)
public class AdminServiceImplTest {

	@Autowired
	private IAdminService service;

	// Methode Create

	@Test
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveValidAdmin_shouldReturnAdminWithNotNullId() {
		// prepare inputs
		Admin admin = new Admin();
		admin.setUserName("bilel-abbas");
		admin.setEmail("bilel.abbas@gmail.com");
		admin.setPwd("azerty");
		admin.setDeleted(true);

		// invoke method
		Admin result = service.create(admin);

		// verify result
		assertNotNull(result);
		assertNotNull(result.getId());
		assertEquals("bilel-abbas", result.getUserName());
		assertEquals("bilel.abbas@gmail.com", result.getEmail());
		assertEquals("azerty", result.getPwd());
		assertTrue(admin.isDeleted());

	}

	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel.abbas@gmail.com','azerty',0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveAdminWithExistingAdmin_shouldReturnNull() {
//		Admin admin = new Admin(1L, "bilel-abbas", "bilel.abbas@gmail.com", "azerty");
		Admin admin = new Admin();
		admin.setEmail("bilel.abbas@gmail.com");
		Admin result = service.create(admin);

		assertNull(result);
	}

	@Test
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveInValidAdmin_withUserNameNull_shouldThrowDataIntegrityViolationException() {
		// prepare inputs
		Admin admin = new Admin();
		admin.setUserName(null);
		admin.setEmail("bilel.abbas@gmail.com");
		admin.setPwd("azerty");
		admin.setDeleted(true);

		// invoke method
		Exception exception = assertThrows(DataIntegrityViolationException.class, () -> {
			service.create(admin);
		});

		// verify result

		assertEquals(
				"not-null property references a null or transient value : com.fr.adaming.entity.Admin.userName; nested exception is org.hibernate.PropertyValueException: not-null property references a null or transient value : com.fr.adaming.entity.Admin.userName",
				exception.getMessage());

	}

	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (user_name, email, pwd, deleted) values('bilel-abbas', 'bilel-abbas@gmail.com','azerty',0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveAdminWithExistingEmail_shouldThrowDataIntegrityViolationException() {

		Admin admin = new Admin(5L, "bilel-abbas", "bilel-abbas@gmail.com", "azerty");

		Exception exception = assertThrows(DataIntegrityViolationException.class, () -> {
			service.create(admin);
		});

		assertEquals(342, exception.getMessage().length());

	}

	// Methode Update
	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel-abbas@gmail.com','azerty',0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateAdmin_shouldReturnAdmin() {

		Admin admin = new Admin(1L, "bilel-abbas", "bilel-abbas@gmail.com", "azerty");
		admin.setUserName("admin-adaming");
		Admin result = service.update(admin);

		assertNotNull(result);
		assertEquals("admin-adaming", result.getUserName());
	}

	@Test
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateAdminNotExist_shouldReturnNull() {
		Admin admin = new Admin(1L, "bilel-abbas", "bilel-abbas@gmail.com", "azerty");
		Admin result = service.update(admin);

		assertNull(result);
	}

	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel-abbas@gmail.com','azerty',0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateInvalidAdminWithUserNameNull_shouldThrowDataIntegrityViolationException() {

		Admin admin = new Admin(1L, "bilel-abbas", "bilel-abbas@gmail.com", "azerty");
		admin.setUserName(null);

		// invoke method
		Exception exception = assertThrows(DataIntegrityViolationException.class, () -> {
			service.update(admin);
		});

		// verify result

		assertEquals(
				"not-null property references a null or transient value : com.fr.adaming.entity.Admin.userName; nested exception is org.hibernate.PropertyValueException: not-null property references a null or transient value : com.fr.adaming.entity.Admin.userName",
				exception.getMessage());

	}

	// Methode ReadById

	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel.abbas@gmail.com','azerty',1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readByIdAdmin_shouldReturnAdmin() {

		Admin result = service.readById(1l);

		assertNotNull(result);
		assertNotNull(result.getId());
		assertEquals(1l, result.getId());
		assertEquals("bilel-abbas", result.getUserName());
		assertEquals("bilel.abbas@gmail.com", result.getEmail());
		assertEquals("azerty", result.getPwd());
		assertTrue(result.isDeleted());
	}

	@Test
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readByIdNotExistAdmin_shouldReturnNull() {

		Admin result = service.readById(1l);

		assertNull(result);

	}

	// Methode Delete

	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel.abbas@gmail.com','azerty',1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteByIdAdmin_shouldReturnTrue() {

		boolean result = service.deleteById(1L);
		assertTrue(result);

	}

	@Test
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteByIdInexistAdmin_shouldReturnFalse() {

		boolean result = service.deleteById(1L);

		assertFalse(result);
	}

//	// Methode SetDeletedTrue
//
//	@Test
//	@Sql(statements = { "delete from admin",
//			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel.abbas@gmail.com','azerty',0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
//	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
//	public void setdeleteTrueAdminExist_shouldReturnTrue() {
//
//		boolean result = service.setDeletedTrue(1L);
//
//		assertTrue(result);
//	}
//
//	@Test
//	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
//	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
//	public void setdeleteTrueAdminInExist_shouldReturnFalse() {
//
//		boolean result = service.setDeletedTrue(1L);
//
//		assertFalse(result);
//	}

//	// Methode ReadByEmail
//
//	@Test
//	@Sql(statements = { "delete from admin",
//			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel.abbas@gmail.com','azerty',0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
//	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
//	public void readByEmailAdminExist_shouldReturnAdmin() {
//
//		Admin result = service.readByEmail("bilel.abbas@gmail.com");
//
//		assertNotNull(result);
//		assertNotNull(result.getId());
//		assertEquals(1l, result.getId());
//		assertEquals("bilel-abbas", result.getUserName());
//		assertEquals("bilel.abbas@gmail.com", result.getEmail());
//		assertEquals("azerty", result.getPwd());
//		assertFalse(result.isDeleted());
//	}
//
//	@Test
//	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
//	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
//	public void readByEmailAdminInExist_shouldReturnNull() {
//
//		Admin result = service.readByEmail("bilel.abbas@gmail.com");
//
//		assertNull(result);
//	}

	// Methode Login

	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel.abbas@gmail.com','azerty',0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void LoginAdminExist_shouldReturnAdmin() {

		Admin result = service.login("bilel.abbas@gmail.com", "azerty");

		assertNotNull(result);
		assertNotNull(result.getId());
		assertEquals(1l, result.getId());
		assertEquals("bilel-abbas", result.getUserName());
		assertEquals("bilel.abbas@gmail.com", result.getEmail());
		assertEquals("azerty", result.getPwd());
		assertFalse(result.isDeleted());
	}

	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel.abbas@gmail.com','azerty',0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void loginAdminExist_emailGood_pwdFalse_shouldReturnNull() {

		Admin result = service.login("bilel.abbas@gmail.com", "inti");
		assertNull(result);
	}

	@Test
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void loginEmailAdminInExist_shouldReturnNull() {

		Admin result = service.login("admin@adming.fr", "inti");
		assertNull(result);
	}

	// Methode Fake Delete
	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel.abbas@gmail.com','azerty',0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void fakeDeletedAdminExist_ShouldReturnTrue() {

		boolean result = service.setDeletedTrue(1L);
		assertTrue(result);
	}

	@Test
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void fakeDeletedAdminInexist_ShouldReturnFalse() {

		boolean result = service.setDeletedTrue(1L);
		assertFalse(result);
	}

	// Methode findByDeletedTrue
	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel.abbas@gmail.com','azerty',1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByDeletedTrue_ShouldReturnListAdminDeleted() {

//		service.readDelete();
		List<Admin> admin = service.readDelete();

		assertNotNull(admin);
		assertEquals(1, admin.get(0).getId().intValue());
		assertEquals("bilel-abbas", admin.get(0).getUserName());
		assertEquals("bilel.abbas@gmail.com", admin.get(0).getEmail());
		assertEquals("azerty", admin.get(0).getPwd());
	}

	// Methode findByDeletedFalse
	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel.abbas@gmail.com','azerty',0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByDeletedFalse_ShouldReturnListAdminNotDeleted() {

		List<Admin> admin = service.readAll();

		assertNotNull(admin);
		assertEquals(1, admin.get(0).getId().intValue());
		assertEquals("bilel-abbas", admin.get(0).getUserName());
		assertEquals("bilel.abbas@gmail.com", admin.get(0).getEmail());
		assertEquals("azerty", admin.get(0).getPwd());

	}

	// Methode readAllReal
	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel.abbas@gmail.com','azerty',1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findAllAdmin_ShouldReturnListAllAdmin() {

		List<Admin> admin = service.readAllReal();

		assertNotNull(admin);
		assertEquals(1, admin.get(0).getId().intValue());
		assertEquals("bilel-abbas", admin.get(0).getUserName());
		assertEquals("bilel.abbas@gmail.com", admin.get(0).getEmail());
		assertEquals("azerty", admin.get(0).getPwd());
	}

}
