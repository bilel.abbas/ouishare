package com.fr.adaming.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.adaming.OuiShareServiceApplication;
import com.fr.adaming.entity.Device;
import com.fr.adaming.entity.Slot;
import com.fr.adaming.entity.Type;
import com.fr.adaming.service.IDeviceService;
import com.fr.adaming.service.ISlotService;
import com.fr.adaming.service.ITypeService;

/**
 * @author Julie Noterman et Jean-Baptiste Jamet
 *
 */
@SpringBootTest(classes = OuiShareServiceApplication.class)
public class DeviceServiceImplTest {

	@Autowired
	private IDeviceService service;

	@Autowired
	private ISlotService slotService;

	@Autowired
	private ITypeService typeService;

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Test
	@Sql(statements = "delete from device where id = 123456", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveValidDevice_shouldReturnEntityWithNotNullId() {
		// prepare inputs
		Device d = new Device();
		d.setId(123456L);
		d.setBrand("dyson");
		d.setChip(132456L);
		d.setDeposit(350D);
		d.setDescription("Ceci est un aspirateur");
		d.setName("Aspirateur");
		d.setPriceExtra(10D);
		d.setPricePonctual(5D);
		d.setPriceSub(10D);
		d.setRentalDurationInHours(1D);
		d.setRules("Pas de regles");
		d.setDeleted(false);
		d.setUrlImage("url");

		// invoke method
		Device result = service.create(d);

		// verify result
		assertNotNull(result);
		assertNotNull(result.getId());
		assertEquals("dyson", result.getBrand());
		assertEquals("Ceci est un aspirateur", result.getDescription());
		assertEquals(10D, result.getPriceExtra());
		assertEquals(5D, result.getPricePonctual());
		assertEquals(10D, result.getPriceSub());
		assertEquals(1D, result.getRentalDurationInHours());

	}
	
	@Test
	@Sql(statements = "INSERT INTO device(id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, deleted) VALUES"
			+ "(123456, 'dyson', 132456, 350, 'ceci est un aspirateur', 'aspirateur', 10, 5, 10, 1, 'regle', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from device where id = 123456", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveExistingdDevice_shouldReturnNull() {
		// prepare inputs
		Device d = new Device();
		d.setId(123456L);
		d.setBrand("dyson");
		d.setChip(132456L);
		d.setDeposit(350D);
		d.setDescription("ceci est un aspirateur");
		d.setName("aspirateur");
		d.setPriceExtra(10D);
		d.setPricePonctual(5D);
		d.setPriceSub(10D);
		d.setRentalDurationInHours(1D);
		d.setRules("regle");
		d.setDeleted(false);
		

		// invoke method
		Device result = service.create(d);

		// verify result
		assertNull(result);
		

	}

	@Test
	@Sql(statements = { "delete from device", "delete from slot " }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveValidDeviceAssociateWithSlot_shouldReturnEntityWithNotNullId() {
		// prepare inputs
		Device d = new Device();
		d.setId(123456L);
		d.setBrand("dyson");
		d.setChip(132456L);
		d.setDeposit(350D);
		d.setDescription("Ceci est un aspirateur");
		d.setName("Aspirateur");
		d.setPriceExtra(10D);
		d.setPricePonctual(5D);
		d.setPriceSub(10D);
		d.setRentalDurationInHours(1D);
		d.setRules("Pas de regles");
		d.setDeleted(false);
		d.setUrlImage("url");

		Slot s = new Slot();
		s.setId(121478L);
		s.setNumber(12L);
		s.setDimensions("la dimension");
		Slot slot = slotService.create(s);

		d.setSlot(slot);

		// invoke method
		Device result = service.create(d);

		// verify result
		assertNotNull(result);
		assertNotNull(result.getId());
		assertEquals("dyson", result.getBrand());
		assertEquals("Ceci est un aspirateur", result.getDescription());
		assertEquals(10D, result.getPriceExtra());
		assertEquals(5D, result.getPricePonctual());
		assertEquals(10D, result.getPriceSub());
		assertEquals(1D, result.getRentalDurationInHours());
		assertThat(result.getSlot()).hasFieldOrPropertyWithValue("dimensions", "la dimension");
		assertThat(result.getSlot()).hasFieldOrPropertyWithValue("number", 12L);

	}

	@Test
	@Sql(statements = { "delete from device", "delete from type " }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveValidDeviceAssociateWithType_shouldReturnEntityWithNotNullId() {
		// prepare inputs
		Device d = new Device();
		d.setId(123456L);
		d.setBrand("dyson");
		d.setChip(132456L);
		d.setDeposit(350D);
		d.setDescription("Ceci est un aspirateur");
		d.setName("Aspirateur");
		d.setPriceExtra(10D);
		d.setPricePonctual(5D);
		d.setPriceSub(10D);
		d.setRentalDurationInHours(1D);
		d.setRules("Pas de regles");
		d.setDeleted(false);
		d.setUrlImage("url");

		Type t = new Type();
		t.setId(121478L);
		t.setName("name");
		t.setStock(5);
		t.setDeleted(false);
		Type type = typeService.create(t);

		d.setType(type);

		// invoke method
		Device result = service.create(d);

		// verify result
		assertNotNull(result);
		assertNotNull(result.getId());
		assertEquals("dyson", result.getBrand());
		assertEquals("Ceci est un aspirateur", result.getDescription());
		assertEquals(10D, result.getPriceExtra());
		assertEquals(5D, result.getPricePonctual());
		assertEquals(10D, result.getPriceSub());
		assertEquals(1D, result.getRentalDurationInHours());
		assertThat(result.getType()).hasFieldOrPropertyWithValue("name", "name");
		assertThat(result.getType()).hasFieldOrPropertyWithValue("stock", 5);
		
	}

	@Test
	public void saveNullDevice_shouldReturnNull() {
		// preparer les inputs
		Device d = null;

		// invoke method
		Device result = service.create(d);

		assertNull(result);
	}

	@Test
	@Sql(statements = "INSERT INTO device(id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, deleted) VALUES"
			+ "(123456, 'dyson', 132456, 350, 'ceci est un aspirateur', 'aspirateur', 10, 5, 10, 1, 'regle', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteExistingDeviceById_shouldReturnTrue() {

		// invoquer la methode
		assertTrue(service.deleteById(123456L));

	}

	@Test
	@Sql(statements = "delete from device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteNotExistingDeviceById_shouldReturnFalse() {

		// invoquer la methode
		assertFalse(service.deleteById(123456L));
	}

	@Test
	@Sql(statements = "INSERT INTO device(id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, deleted) VALUES"
			+ "(123456, 'dyson', 132456, 350, 'ceci est un aspirateur', 'aspirateur', 10, 5, 10, 1, 'regle', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void setdeleteTrueExistingDevice_shouldReturnTrue() {

		// invoquer la methode
		assertTrue(service.setDeletedTrue(123456L));

	}

	@Test
	@Sql(statements = "INSERT INTO device(id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, deleted) VALUES"
			+ "(123456, 'dyson', 132456, 350, 'ceci est un aspirateur', 'aspirateur', 10, 5, 10, 1, 'regle', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void setdeleteTrueNotExistingDevice_shouldReturnFalse() {

		// invoquer la methode
		assertFalse(service.setDeletedTrue(14596L));

	}

	@Test
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (11111, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateValidDevice_shouldReturnUpdatedDevice() {

		// prepare inputs
		Device d = new Device();
		d.setId(11111L);
		d.setBrand("samsung");
		d.setChip(222222L);
		d.setDeposit(300D);
		d.setDescription("Ceci est un aspirateur");
		d.setName("Objet qui aspire");
		d.setPriceExtra(10D);
		d.setPricePonctual(10D);
		d.setPriceSub(10D);
		d.setRentalDurationInHours(2D);
		d.setRules("No rules");
		d.setState("Cassé");
		d.setStateComment("Je viens de casser ce bel aspirateur");
		d.setDeleted(false);
		d.setUrlImage("url");

		// invoke method
		Device result = service.update(d);

		// verify result
		assertNotNull(result);
		assertEquals(d.getId(), result.getId());
		assertEquals(d.getBrand(), result.getBrand());
		assertEquals(d.getChip(), result.getChip());
		assertEquals(d.getDeposit(), result.getDeposit());
		assertEquals(d.getDescription(), result.getDescription());
		assertEquals(d.getName(), result.getName());
		assertEquals(d.getPriceExtra(), result.getPriceExtra());
		assertEquals(d.getPricePonctual(), result.getPricePonctual());
		assertEquals(d.getPriceSub(), result.getPriceSub());
		assertEquals(d.getRentalDurationInHours(), result.getRentalDurationInHours());
		assertEquals(d.getRules(), result.getRules());
		assertEquals(d.getState(), result.getState());
		assertEquals(d.getStateComment(), result.getStateComment());
		assertTrue(result.isDeleted() == false);

	}

	@Test
	public void updateDeviceIfNotExist_shouldReturnNull() {

		// prepare inputs
		Device d = new Device();
		d.setId(11112L);
		d.setBrand("samsung");
		d.setChip(222222L);
		d.setDeposit(300D);
		d.setDescription("Ceci est un aspirateur");
		d.setName("Objet qui aspire");
		d.setPriceExtra(10D);
		d.setPricePonctual(10D);
		d.setPriceSub(10D);
		d.setRentalDurationInHours(2D);
		d.setRules("No rules");
		d.setState("Cassé");
		d.setStateComment("Je viens de casser ce bel aspirateur");
		d.setDeleted(false);
		d.setUrlImage("url");

		// invoke method
		Device result = service.update(d);

		// verify result
		assertNull(result);

	}

	@Test
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (11113, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readByIdIfExist_shouldReturnDevice() {

		// invoke method
		Device result = service.readById(11113L);

		// verify result
		assertNotNull(result);
		assertEquals(11113L, result.getId());
		assertEquals("dyson", result.getBrand());
		assertEquals(111111L, result.getChip());
		assertEquals(150D, result.getDeposit());
		assertEquals("aspirateur trop stylé", result.getDescription());
		assertEquals("aspirateur", result.getName());
		assertEquals(15D, result.getPriceExtra());
		assertEquals(15D, result.getPricePonctual());
		assertEquals(15D, result.getPriceSub());
		assertEquals(1L, result.getRentalDurationInHours());
		assertEquals("Pas de règles", result.getRules());
		assertEquals("Bon état", result.getState());
		assertEquals("Rien à signaler", result.getStateComment());
		assertTrue(result.isDeleted() == false);

	}

	@Test
	public void readByIdIfNotExist_shouldReturnNull() {

		// invoke method
		Device result = service.readById(11113L);

		// verify result
		assertNull(result);
	}

	@Test
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (11114, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (11115, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (11116, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllIfExist_shouldReturnDeviceList() {

		// invoke method
		List<Device> result = service.readAll();

		// verify result
		assertNotNull(result);
		assertTrue(result.size() == 3);

		assertEquals(11114L, result.get(0).getId());
		assertEquals("dyson", result.get(0).getBrand());
		assertEquals(111111L, result.get(0).getChip());
		assertEquals(150D, result.get(0).getDeposit());
		assertEquals("aspirateur trop stylé", result.get(0).getDescription());
		assertEquals("aspirateur", result.get(0).getName());
		assertEquals(15D, result.get(0).getPriceExtra());
		assertEquals(15D, result.get(0).getPricePonctual());
		assertEquals(15D, result.get(0).getPriceSub());
		assertEquals(1L, result.get(0).getRentalDurationInHours());
		assertEquals("Pas de règles", result.get(0).getRules());
		assertEquals("Bon état", result.get(0).getState());
		assertEquals("Rien à signaler", result.get(0).getStateComment());
		assertTrue(result.get(0).isDeleted() == false);

		assertEquals(11115L, result.get(1).getId());
		assertEquals("dyson", result.get(1).getBrand());
		assertEquals(111111L, result.get(1).getChip());
		assertEquals(150D, result.get(1).getDeposit());
		assertEquals("aspirateur trop stylé", result.get(1).getDescription());
		assertEquals("aspirateur", result.get(1).getName());
		assertEquals(15D, result.get(1).getPriceExtra());
		assertEquals(15D, result.get(1).getPricePonctual());
		assertEquals(15D, result.get(1).getPriceSub());
		assertEquals(1L, result.get(1).getRentalDurationInHours());
		assertEquals("Pas de règles", result.get(1).getRules());
		assertEquals("Bon état", result.get(1).getState());
		assertEquals("Rien à signaler", result.get(1).getStateComment());
		assertTrue(result.get(1).isDeleted() == false);

		assertEquals(11116L, result.get(2).getId());
		assertEquals("dyson", result.get(2).getBrand());
		assertEquals(111111L, result.get(2).getChip());
		assertEquals(150D, result.get(2).getDeposit());
		assertEquals("aspirateur trop stylé", result.get(2).getDescription());
		assertEquals("aspirateur", result.get(2).getName());
		assertEquals(15D, result.get(2).getPriceExtra());
		assertEquals(15D, result.get(2).getPricePonctual());
		assertEquals(15D, result.get(2).getPriceSub());
		assertEquals(1L, result.get(2).getRentalDurationInHours());
		assertEquals("Pas de règles", result.get(2).getRules());
		assertEquals("Bon état", result.get(2).getState());
		assertEquals("Rien à signaler", result.get(2).getStateComment());
		assertTrue(result.get(1).isDeleted() == false);

	}

	@Test
	public void readAllIfNotExist_shouldReturnEmptyList() {

		// invoke method
		List<Device> result = service.readAll();

		// verify result
		assertEquals(new ArrayList<Device>(), result);

	}

	@Test
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (11117, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (11118, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', true)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (11119, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', true)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readDeleteIfExist_shouldReturnDeletedDeviceList() {

		// invoke method
		List<Device> result = service.readDelete();

		// verify method
		assertTrue(result.size() == 2);

		assertEquals(11118L, result.get(0).getId());
		assertEquals("dyson", result.get(0).getBrand());
		assertEquals(111111L, result.get(0).getChip());
		assertEquals(150D, result.get(0).getDeposit());
		assertEquals("aspirateur trop stylé", result.get(0).getDescription());
		assertEquals("aspirateur", result.get(0).getName());
		assertEquals(15D, result.get(0).getPriceExtra());
		assertEquals(15D, result.get(0).getPricePonctual());
		assertEquals(15D, result.get(0).getPriceSub());
		assertEquals(1L, result.get(0).getRentalDurationInHours());
		assertEquals("Pas de règles", result.get(0).getRules());
		assertEquals("Bon état", result.get(0).getState());
		assertEquals("Rien à signaler", result.get(0).getStateComment());
		assertTrue(result.get(0).isDeleted() == true);

		assertEquals(11119L, result.get(1).getId());
		assertEquals("dyson", result.get(1).getBrand());
		assertEquals(111111L, result.get(1).getChip());
		assertEquals(150D, result.get(1).getDeposit());
		assertEquals("aspirateur trop stylé", result.get(1).getDescription());
		assertEquals("aspirateur", result.get(1).getName());
		assertEquals(15D, result.get(1).getPriceExtra());
		assertEquals(15D, result.get(1).getPricePonctual());
		assertEquals(15D, result.get(1).getPriceSub());
		assertEquals(1L, result.get(1).getRentalDurationInHours());
		assertEquals("Pas de règles", result.get(1).getRules());
		assertEquals("Bon état", result.get(1).getState());
		assertEquals("Rien à signaler", result.get(1).getStateComment());
		assertTrue(result.get(1).isDeleted() == true);

	}

	@Test
	public void readDeleteIfNotExist_shouldReturnEmptyList() {

		// invoke method
		List<Device> result = service.readDelete();

		// verify method
		assertEquals(new ArrayList<Device>(), result);

	}

	@Test
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (11120, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (11121, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (11122, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', true)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllRealIfExist_shouldReturnDeviceList() {

		// invoke method
		List<Device> result = service.readAllReal();

		// verify method
		assertTrue(result.size() == 3);

		assertEquals(11120L, result.get(0).getId());
		assertEquals("dyson", result.get(0).getBrand());
		assertEquals(111111L, result.get(0).getChip());
		assertEquals(150D, result.get(0).getDeposit());
		assertEquals("aspirateur trop stylé", result.get(0).getDescription());
		assertEquals("aspirateur", result.get(0).getName());
		assertEquals(15D, result.get(0).getPriceExtra());
		assertEquals(15D, result.get(0).getPricePonctual());
		assertEquals(15D, result.get(0).getPriceSub());
		assertEquals(1L, result.get(0).getRentalDurationInHours());
		assertEquals("Pas de règles", result.get(0).getRules());
		assertEquals("Bon état", result.get(0).getState());
		assertEquals("Rien à signaler", result.get(0).getStateComment());
		assertTrue(result.get(0).isDeleted() == false);

		assertEquals(11121L, result.get(1).getId());
		assertEquals("dyson", result.get(1).getBrand());
		assertEquals(111111L, result.get(1).getChip());
		assertEquals(150D, result.get(1).getDeposit());
		assertEquals("aspirateur trop stylé", result.get(1).getDescription());
		assertEquals("aspirateur", result.get(1).getName());
		assertEquals(15D, result.get(1).getPriceExtra());
		assertEquals(15D, result.get(1).getPricePonctual());
		assertEquals(15D, result.get(1).getPriceSub());
		assertEquals(1L, result.get(1).getRentalDurationInHours());
		assertEquals("Pas de règles", result.get(1).getRules());
		assertEquals("Bon état", result.get(1).getState());
		assertEquals("Rien à signaler", result.get(1).getStateComment());
		assertTrue(result.get(1).isDeleted() == false);

	}

	@Test
	public void readAllRealIfNotExist_shouldReturnEmptyList() {

		// invoke method
		List<Device> result = service.readAllReal();

		// verify method
		assertEquals(new ArrayList<Device>(), result);

	}

	
	
	@Test
	public void readAllCatalogIfNotExist_shouldReturnEmptyList() {
		List<Device> result = service.readAllCatalog();
		
		assertEquals(new ArrayList<Device>(), result);
	}
	
	@Test
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (11120, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (11121, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (11122, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllCatalogIfExist_shouldReturnDeviceList() {
		
		// invoke method
		List<Device> result = service.readAllCatalog();
				
		// verify method
		assertTrue(result.size() == 3);
		
		assertEquals(11120L, result.get(0).getId());
		assertEquals("dyson", result.get(0).getBrand());
		assertEquals(111111L, result.get(0).getChip());
		assertEquals(150D, result.get(0).getDeposit());
		assertEquals("aspirateur trop stylé", result.get(0).getDescription());
		assertEquals("aspirateur", result.get(0).getName());
		assertEquals(15D, result.get(0).getPriceExtra());
		assertEquals(15D, result.get(0).getPricePonctual());
		assertEquals(15D, result.get(0).getPriceSub());
		assertEquals(1L, result.get(0).getRentalDurationInHours());
		assertEquals("Pas de règles", result.get(0).getRules());
		assertEquals("Bon état", result.get(0).getState());
		assertEquals("Rien à signaler", result.get(0).getStateComment());
		assertTrue(result.get(0).isDeleted() == false);


		assertEquals(11121L, result.get(1).getId());
		assertEquals("dyson", result.get(1).getBrand());
		assertEquals(111111L, result.get(1).getChip());
		assertEquals(150D, result.get(1).getDeposit());
		assertEquals("aspirateur trop stylé", result.get(1).getDescription());
		assertEquals("aspirateur", result.get(1).getName());
		assertEquals(15D, result.get(1).getPriceExtra());
		assertEquals(15D, result.get(1).getPricePonctual());
		assertEquals(15D, result.get(1).getPriceSub());
		assertEquals(1L, result.get(1).getRentalDurationInHours());
		assertEquals("Pas de règles", result.get(1).getRules());
		assertEquals("Bon état", result.get(1).getState());
		assertEquals("Rien à signaler", result.get(1).getStateComment());
		assertTrue(result.get(1).isDeleted() == false);
		
	}
	
	
	
	@Test
	@Sql(statements = { "delete from device", "delete from slot " }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO slot (id, dimensions, number,deleted) values (1,'10x10x10',45676,false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD )
	@Sql(statements = "INSERT INTO slot (id, dimensions, number,deleted) values (2,'10x10x10',45676,false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD )
	@Sql(statements = "INSERT INTO slot (id, dimensions, number,deleted) values (3,'10x10x10',45676,false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD )
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted, id_slot) "
			+ "VALUES (11120, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false, 1)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted, id_slot) "
			+ "VALUES (11121, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false, 2)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted, id_slot) "
			+ "VALUES (11122, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false, 3)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "delete from device", "delete from slot " }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllCatalogIfExistSlot_shouldReturnDeviceList() {
		
		// invoke method
		List<Device> result = service.readAllCatalog();
				
		// verify method
		assertTrue(result.size() == 0);
		
	}
	
	
	
	// Tester les méthodes addSlot et deleteSlot
	
	@Test
	@Sql(statements = "Insert into Slot (id, dimensions, number,deleted) values (11111,'10x10x10',45676,false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (11123, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = {"DELETE from Device", "DELETE from Slot"} , executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addAssociationWithSlotThatExist_shouldReturnTrue() {
		
		assertTrue(service.addAssociationWithSlot(11123L, 11111L));
		
	}
	
	@Test
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (11199, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE from Device" , executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addAssociationWithSlotThatDoesNotExist_shouldReturnFalse() {
		
		assertFalse(service.addAssociationWithSlot(11199L, 66666L));
		
	}
	
	
	@Test
	@Sql(statements = "Insert into Slot (id, dimensions, number,deleted) values (11113,'10x10x10',45676,false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, id_slot, deleted) "
			+ "VALUES (11125, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', 11113, false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = {"DELETE from Device", "DELETE from Slot"} , executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteAssociationWithSlotThatExist_shouldReturnTrue() {
		
		assertTrue(service.deleteAssociationWithSlot(11125L));
			
	}
	
	@Test
	public void deleteAssociationWithSlotThatDoesNotExist_shouldReturnFalse() {
		
		assertFalse(service.deleteAssociationWithSlot(11126L));
		
	}
	
	

	@Test
	@Sql(statements = "INSERT INTO locker(id,adress,zip_code,city,slot,name,deleted)"
			+ "VALUES(1,'adress1',123456789,'lyon',123,'pierre',false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO locker(id,adress,zip_code,city,slot,name,deleted)"
			+ "VALUES(2,'adress1',123456789,'lyon',123,'jacques',false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO slot (id,deleted,number,id_locker) "
			+ "VALUES (1,false,123,1);", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO slot (id,deleted,number,id_locker) "
			+ "VALUES (2,false,223,1);", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO slot (id,deleted,number,id_locker) "
			+ "VALUES (3,false,323,2);", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = {
			"INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted,id_slot) "
					+ "VALUES (21120, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false,1)",
			"UPDATE Slot SET id_device=21120 where id=1" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = {
			"INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted,id_slot) "
					+ "VALUES (21121, 'rowenta', 111111, 150, 'aspirateur moins stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false,2)",
			"UPDATE Slot SET id_device=21121 where id=2 " }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = {
			"INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted,id_slot) "
					+ "VALUES (21122, 'miele', 111111, 150, 'aspirateur pas du tout stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false,3)",
			"UPDATE Slot SET id_device=21122 where id=3" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = {"UPDATE slot Set id_locker=null","UPDATE device set id_slot=null","DELETE FROM slot","DELETE FROM device","DELETE FROM locker"}, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllDevicesAvailableWithValidLockerId_shouldReturnListOfDevices() {

		List<Device> listDevices = service.readAvailableDevicesInLockerById(1L);

		assertEquals(listDevices.size(), 2);
		assertThat(listDevices).hasOnlyElementsOfType(Device.class);

		assertEquals("dyson", listDevices.get(0).getBrand() );
		assertEquals("aspirateur trop stylé", listDevices.get(0).getDescription() );

		assertEquals("rowenta", listDevices.get(1).getBrand() );
		assertEquals("aspirateur moins stylé", listDevices.get(1).getDescription() );


		assertEquals("dyson",listDevices.get(0).getBrand());
		assertEquals("aspirateur trop stylé",listDevices.get(0).getDescription());

		assertEquals("rowenta",listDevices.get(1).getBrand());
		assertEquals("aspirateur moins stylé",listDevices.get(1).getDescription());

	}
	
	@Test
	public void readAllDevicesAvailableWithValidNotLockerId_shouldReturnEmptyList() {

		List<Device> listDevices = service.readAvailableDevicesInLockerById(1L);

		assertEquals(0,listDevices.size());
		assertThat(listDevices).hasOnlyElementsOfType(Device.class);
	}
	
	@Test
	public void readAllDevicesAvailableWithNullId_shouldReturnEmptyList() {
		List<Device> listDevices = service.readAvailableDevicesInLockerById(null);
		assertEquals(0,listDevices.size());
		assertThat(listDevices).hasOnlyElementsOfType(Device.class);
	}
}
