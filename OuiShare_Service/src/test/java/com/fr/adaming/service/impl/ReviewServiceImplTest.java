package com.fr.adaming.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.adaming.OuiShareServiceApplication;
import com.fr.adaming.entity.Review;
import com.fr.adaming.service.IReviewService;




/**
 * @author Dylan
 *
 */
@SpringBootTest(classes = OuiShareServiceApplication.class)
public class ReviewServiceImplTest {

	@Autowired
	private IReviewService service;
	
	
	
	
	
	
	@Test
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveValidReview_shouldReturnEntityWithNotNullId() {
		//prepare inputs
		Review r = new Review();
		r.setRating(2);
		r.setComment("fxgfggh");
		r.setDeleted(false);
		
		//invoke method
		Review result = service.create(r);
		
		//verify result
		assertNotNull(result);
		assertNotNull(result.getId());
		assertEquals(2, result.getRating());
		assertEquals("fxgfggh", result.getComment());
		assertEquals(false, result.isDeleted());
		
	}
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (2,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveExistingReview_shouldReturnEntityWithNotNullId() {
		//prepare inputs
		Review r = new Review();
		r.setId(2L);
		r.setRating(2);
		r.setComment("dfgf");
		r.setDeleted(false);
		
		
		//invoke method
		
		
		Review result = service.create(r);
		
		assertNull(result);
		
		
	}
	
	
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (4,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateExistingReview_shouldReturnEntityWithNotNullId() {
		
		Review r = new Review();
		r.setId(4L);
		r.setRating(5);
		r.setComment("aaaa");
		r.setDeleted(true);
		
		Review result = service.update(r);
				
		assertNotNull(result);
		assertNotNull(result.getId());
		assertEquals(5, result.getRating());
		assertEquals("aaaa", result.getComment());
		
	}
	
	
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (3,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateDifferentIdReview_shouldNotReturnNUll() {
		
		Review r = new Review();
		r.setId(4L);
		r.setRating(5);
		r.setComment("bbbb");
		r.setDeleted(true);
		
		Review result = service.update(r);
		
		assertNull(result);
		
		
	
		
	}
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (3,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateReviewWithNullId_shouldNotReturnNUll() {
		
		Review r = new Review();
		r.setId(null);
		r.setRating(5);
		r.setComment("bbbb");
		r.setDeleted(true);
		
		Review result = service.update(r);
		
		assertNull(result);
		
		
	
		
	}
	
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (3,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readByIdAnExistingReview_shouldReturnReviewWithIdNotNull() {
		
		
		
		Review result = service.readById(3L);
		
		assertNotNull(result);
		assertNotNull(result.getId());
		assertEquals(2, result.getRating());
		assertEquals("dfgf", result.getComment());
		
		
	
		
	}
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (3,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readByIdWithIdReviewNotExisting_shouldReturnNull() {
		
		
		
		Review result = service.readById(200L);
		
		assertNull(result);
		
		
		
	
		
	}
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (3,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readByIdWithNullIdReview_shouldReturnNull() {
		
		
		
		assertThrows(InvalidDataAccessApiUsageException.class, () -> {
			service.readById(null);
		});
		
		
		
	
		
	}
	
	
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (4,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review ",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
		public void deleteValidReview_shouldReturnTrue() {
		
		Review r = new Review();
		r.setId(4L);
		r.setRating(5);
		r.setComment("cccc");
		r.setDeleted(true);
		
		
		assertTrue(service.deleteById(r.getId()));
	}
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (5,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
		public void deleteNotExistingReview_shouldReturnFalse() {
		
		Review r = new Review();
		r.setId(6L);
		r.setRating(5);
		r.setComment("dddd");
		r.setDeleted(true);
		assertFalse(service.deleteById(r.getId()));
	}
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (5,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
		public void deleteReviewWithIdNull_shouldReturnFalse() {
		
		
		assertFalse(service.deleteById(null));
	}
	
	
	
	@Test
	@Sql(statements = "delete from review", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void EmptyListReview_shouldReturnEmptyList() {
	
	List<Review> list = service.readAll();
	assertTrue(list.isEmpty());

}
	
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (5,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
		public void ListValidReview_shouldNotReturnEmptyList() {
		
		List<Review> list = service.readAllReal();
		assertFalse(list.isEmpty());
	
}
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (6,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
		public void setDeletedExistingReview_shouldReturnTrue() {
		
		Review r = new Review();
		r.setId(6L);
		r.setRating(5);
		r.setComment("cccc");
		
		
		
		assertTrue(service.setDeletedTrue(r.getId()));
		
	}
	
	@Test
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
		public void setDeletedNotExistingReview_shouldReturnFalse() {
		
		Review r = new Review();
		r.setId(100L);
		r.setRating(5);
		r.setComment("cccc");
		r.setDeleted(true);
		
		
		assertFalse(service.setDeletedTrue(r.getId()));
	}
	
	@Test
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
		public void setDeletedReviewWithIdNull_shouldReturnFalse() {
		
		
		
		
		assertFalse(service.setDeletedTrue(null));
	}
	
	
	
	@Test
	@Sql(statements = {"delete from review","insert into review (id,rating,comment,deleted) values (6,2,'dfgf',false)","insert into review (id,rating,comment,deleted) values (7,2,'dfgf',true)","insert into review (id,rating,comment,deleted) values (8,2,'dfgf',true)"},executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void List2SetDeletedTrueAnd1SetDeletedFalse_shouldReturnAListWith2Elements() {
		
		List<Review> list  = service.readDelete();
		assertTrue(list.size()==2);
	}
	
	@Test
	@Sql(statements = {"delete from review","insert into review (id,rating,comment,deleted) values (6,2,'dfgf',false)","insert into review (id,rating,comment,deleted) values (7,2,'dfgf',false)","insert into review (id,rating,comment,deleted) values (8,2,'dfgf',false)"},executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void List0SetDeletedTrueAnd3SetDeletedFalse_shouldReturnEmptyList() {
		
		List<Review> list  = service.readDelete();
		assertTrue(list.isEmpty());
	}
	
	
	@Test
	@Sql(statements = {"delete from review","insert into review (id,rating,comment,deleted) values (6,2,'dfgf',false)","insert into review (id,rating,comment,deleted) values (7,2,'dfgf',true)","insert into review (id,rating,comment,deleted) values (8,2,'dfgf',true)"},executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void List2SetDeletedTrueAnd1SetDeletedFalse_shouldReturnAListWith1Element() {
		
		List<Review> list  = service.readAll();
		assertTrue(list.size()==1);
	}
	
	@Test
	@Sql(statements = {"delete from review","insert into review (id,rating,comment,deleted) values (6,2,'dfgf',true)","insert into review (id,rating,comment,deleted) values (7,2,'dfgf',true)","insert into review (id,rating,comment,deleted) values (8,2,'dfgf',true)"},executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void List3SetDeletedTrueAnd0SetDeletedFalse_shouldReturnEmptyList() {
		
		List<Review> list  = service.readAll();
		assertTrue(list.isEmpty());
	}
	
}
