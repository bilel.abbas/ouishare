package com.fr.adaming.service;

import java.util.List;

import com.fr.adaming.entity.Device;
import com.fr.adaming.entity.Rental;

/**
 * @author Nicolas RUFFIER
 *
 */
public interface IRentalService extends IAbstractService<Rental, Long>{

	/**
	 * @param Device - The given entity
	 * @return List<Rental> where end date is after the datetime of the request
	 */
	public List<Rental> availability(Device device);
	
	/**
	 * @param idRental - Id of target Rental
	 * @param idReview - Id of target Review
	 * @return True if ok - False if target Rental and/or Review doesn't exist By Id
	 */
	public boolean addReview (Long idRental, Long idReview);
	
	
	/**
	 * @param rental - The given Rental entity
	 * @return True if Rental not null contain Customer not null with an Id and Device not null with id - False if not
	 */
	public boolean validateCreate(Rental rental);

	/**
	 * @param id - The id of the Rental to update
	 * @return 0D if Rental exist with endDateReal null and After StartDate - null if not - Other positive Double for the difference between EndDateReal and EndDateTheoritical
	 */
	public Long addEndDateReal(Long id);

	boolean isAvailableNow(Long idDevice);
}
