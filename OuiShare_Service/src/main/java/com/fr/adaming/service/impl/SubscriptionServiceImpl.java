package com.fr.adaming.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.fr.adaming.entity.Customer;
import com.fr.adaming.entity.Subscription;
import com.fr.adaming.repository.ISubscriptionRepository;
import com.fr.adaming.service.ISubscriptionService;

/**
 * @author Thibaud, Guillaume Vittoz
 *
 */
@Service
public class SubscriptionServiceImpl implements ISubscriptionService {

	@Autowired
	ISubscriptionRepository repo;

	/**
	 * Create the subscription in the database
	 * 
	 * @param subscription
	 * @return The subscription if OK, else null
	 */
	@Override
	public Subscription create(Subscription subscription) {
		if (!repo.exists(Example.of(subscription))) {
			if (subscription.getStartDate()!= null) {
				return repo.save(subscription);
			} else {
				return null;
			}
		} else
			return null;

	}

	/**
	 * Update an existing subscription in the database
	 * 
	 * @param subscription with existing id
	 * @return The updated subscription if OK, else null
	 */
	@Override
	public Subscription update(Subscription subscription) {
		if (subscription != null && subscription.getId() != null) {
			if (repo.existsById(subscription.getId())) {
				if (subscription.getStartDate() != null) {
					return repo.save(subscription);
				} else
					return null;
			} else
				return null;
		} else {
			return null;
		}
	}

	/**
	 * Read a subscription by its id
	 * 
	 * @param Long id
	 * @return This subscription if id is OK, else null
	 */
	@Override
	public Subscription readById(Long id) {
		if (id != null) {
			if (repo.existsById(id))
				return repo.findById(id).get();
			else
				return null;
		} else {
			return null;
		}
	}

	/**
	 * Read all subscriptions which are not deleted in the database
	 * 
	 * @return List of subscriptions
	 */
	@Override
	public List<Subscription> readAll() {
		return repo.findByDeletedFalse();
	}

	/**
	 * Delete definitely a subscription in the database
	 * 
	 * @param Long id
	 * @return True if success, else false
	 */
	@Override
	public boolean deleteById(Long id) {
		if (id != null) {
			Subscription subscription = readById(id);
			if (subscription != null) {
				repo.deleteById(id);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Find a subscription by its customer
	 * 
	 * @param A customer
	 * @return The subscription if customer is not null, else list null
	 */
	@Override
	public List<Subscription> findByCustomer(Customer customer) {
		if (customer != null)
			return repo.findByCustomer(customer);
		else
			return null;
	}

	/**
	 * Set deleted a subscription
	 * 
	 * @param Long id
	 * @return True if id is not null and known, else false
	 */
	@Override
	public boolean setDeletedTrue(Long id) {
		if (id != null) {
			if (repo.fakeDelete(id) == 1) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Read all subscriptions which are deleted in the database
	 *
	 * @return List of subscriptions
	 */
	@Override
	public List<Subscription> readDelete() {
		return repo.findByDeletedTrue();

	}

	/**
	 * Read all subscriptions in the database
	 *
	 * @return List of subscriptions
	 */
	@Override
	public List<Subscription> readAllReal() {
		return repo.findAll();

	}

	/**
	 * check every active subscription to check if they are to renew or not and
	 * renew the ones who need it
	 */
	@Override
	public void renewing() {
		List<Subscription> subscriptions = readAll();
		LocalDate today = LocalDate.now();
		for (Subscription s : subscriptions) {
			if (s.isRenew() == true && s.getEndDate().isBefore(today)) {
				LocalDate newStart = s.getEndDate();
				s.setEndDate(newStart.plusMonths(1));
				update(s);
			}

		}
	}

	/**
	 * reactive the renewing for the given subscription and set the endDate a month
	 * after the day of reactivation
	 * 
	 * @param id
	 * @return true if reactivation is possible, false if not
	 */
	@Override
	public boolean reactiveSubscription(Long id) {
		if (id != null) {
			Subscription subscription = readById(id);
			if (subscription != null && !subscription.isDeleted()) {
				if (!subscription.isRenew()) {
					subscription.setRenew(true);
					subscription.setEndDate(LocalDate.now().plusMonths(1));
					update(subscription);
					return true;
				} else
					return false;
			} else
				return false;
		}
		return false;
	}

	/**
	 * Deactivate the renewing for the given subscription, the subscription runs
	 * until one month after the last renewing it went through
	 * 
	 * @param id
	 * @return true if desactivation is possible, false if not
	 */
	@Override
	public boolean desactiveSubscription(Long id) {
		if (id != null) {
			Subscription subscription = readById(id);
			if (subscription != null && !subscription.isDeleted()) {
				if (subscription.isRenew()) {
					subscription.setRenew(false);
					update(subscription);
					return true;
				} else
					return false;
			} else
				return false;
		}
		return false;
	}
}
