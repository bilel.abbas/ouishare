package com.fr.adaming.service.impl;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fr.adaming.entity.Device;
import com.fr.adaming.entity.Rental;
import com.fr.adaming.repository.IDeviceRepository;
import com.fr.adaming.repository.IRentalRepository;
import com.fr.adaming.repository.IReviewRepository;
import com.fr.adaming.service.IRentalService;

/**
 * @author Nicolas RUFFIER
 *
 */
@Service
public class RentalServiceImpl implements IRentalService {

	@Autowired
	private IRentalRepository repo;

	@Autowired
	private IReviewRepository repoReview;
	
	@Autowired
	private IDeviceRepository repoDevice;

	@Override
	public Rental create(Rental rental) {
		if (validateCreate(rental)) {
			List<Rental> list = repo.available(rental.getDevice().getId(), rental.getStartDate(),
					rental.getEndDateTheoritical());
			if (list.isEmpty()) {
				return repo.save(rental);
			}
		}
		return null;

	}

	@Override
	public Rental update(Rental rental) {
		if (validateCreate(rental) && rental.getId() != null && repo.existsById(rental.getId())) {
			return repo.save(rental);
		} else {
			return null;
		}
	}

	@Override
	public Rental readById(Long id) {
		return repo.findById(id).orElse(null);
	}

	@Override
	public List<Rental> readAll() {
		return repo.findByDeletedFalse();
	}

	@Override
	public List<Rental> readDelete() {
		return repo.findByDeletedTrue();
	}

	@Override
	public List<Rental> readAllReal() {
		return repo.findAll();
	}

	@Override
	public boolean deleteById(Long id) {
		if (id != null && repo.existsById(id)) {
			repo.deleteById(id);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean setDeletedTrue(Long id) {

		return (id != null && repo.fakeDelete(id) != 0);

	}

	@Override
	public List<Rental> availability(Device device) {
		return repo.findByDeviceAndEndDateTheoriticalAfter(device, LocalDateTime.now());
	}

	@Override
	public boolean addReview(Long idRental, Long idReview) {
		Rental rental = readById(idRental);
		if (rental != null && rental.getReview() == null && repoReview.existsById(idReview)) {
			repo.addReview(idRental, idReview);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Long addEndDateReal(Long id) {
		Rental rental = readById(id);
		if (rental == null || rental.getEndDateReal() != null || LocalDateTime.now().isBefore(rental.getStartDate())) {
			return null;
		} else if (LocalDateTime.now().isAfter(rental.getEndDateTheoritical())) {
			repo.addEndDateReal(id, LocalDateTime.now());
			return rental.getEndDateTheoritical().until(LocalDateTime.now(), ChronoUnit.MINUTES);
		} else {
			repo.addEndDateReal(id, LocalDateTime.now());
			return 0L;
		}

	}

	@Override
	public boolean validateCreate(Rental rental) {
		return (rental != null && rental.getDevice() != null && rental.getCustomer() != null
				&& rental.getCustomer().getId() != null && rental.getDevice().getId() != null
				&& rental.getStartDate().isBefore(rental.getEndDateTheoritical()));
	}
	
	@Override
	public boolean isAvailableNow(Long idDevice) {
		
		Device device=repoDevice.findById(idDevice).get();
		long hours=(long)Math.rint(device.getRentalDurationInHours());
		
		if(repo.available(idDevice,LocalDateTime.now(),LocalDateTime.now().plusHours(hours)).isEmpty()) {
			return true;
		}
		else
			return false;	
	}
	

}
