package com.fr.adaming.service;

import java.util.List;

import com.fr.adaming.entity.Customer;
import com.fr.adaming.entity.Subscription;

/**
 * @author Thibaud, Guillaume Vittoz
 *
 */
public interface ISubscriptionService extends IAbstractService<Subscription, Long>{

	List<Subscription> findByCustomer(Customer customer);
	
	void renewing();
	
	boolean reactiveSubscription(Long id);
	
	boolean desactiveSubscription(Long id);
	
}
