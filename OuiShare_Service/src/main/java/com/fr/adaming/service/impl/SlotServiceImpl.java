package com.fr.adaming.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.fr.adaming.entity.Slot;
import com.fr.adaming.repository.ISlotRepository;
import com.fr.adaming.service.ISlotService;

/**
 * @author Aurélien
 *
 */
@Service
public class SlotServiceImpl implements ISlotService {

	@Autowired
	private ISlotRepository repo;

	/**
	 * Saves a non existing slot in the database
	 * 
	 * @param slot to be saved
	 * @return returns the slot with the generated id, or null in case of error
	 * @author Aurélien
	 */
	@Override
	public Slot create(Slot slot) {

		if (slot != null) {
			if (!repo.exists(Example.of(slot)))
				return repo.save(slot);
			else
				return null;

		} else
			return null;
	}

	/**
	 * Updates an existing slot in the database
	 * 
	 * @param slot to be updated
	 * @return returns the updated slot, or null in case of error
	 * @author Aurélien
	 */
	@Override
	public Slot update(Slot slot) {
		if (slot != null) {
			if (slot.getId() != null) {
				if (repo.existsById(slot.getId()))
					return repo.save(slot);
				else
					return null;
			} else
				return null;
		} else
			return null;
	}

	/**
	 * Returns a slot with a matching id
	 * 
	 * @param id of the slot to get
	 * @return returns the corresponding slot, or null in case of error
	 * @author Aurélien
	 */
	@Override
	public Slot readById(Long id) {
		return repo.findById(id).orElse(null);
	}

	/**
	 * Returns all slots in the database
	 * 
	 * @return returns the slots
	 * @author Aurélien
	 */
	@Override
	public List<Slot> readAll() {

		return repo.findAll();
	}

	/**
	 * Deletes a slot with a matching id from the database
	 * 
	 * @param id of the slot to delete
	 * @return returns true if deletion is OK, false otherwise (non-matching id)
	 * @author Aurélien
	 */
	@Override
	public boolean deleteById(Long id) {
		if (repo.existsById(id)) {
			repo.deleteById(id);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Changes the deleted attribute to false for a slot with a matching id from the
	 * database
	 * 
	 * @param id of the slot to delete
	 * @return returns true if deletion is OK, false otherwise (non-matching id)
	 * @author Aurélien
	 */
	@Override
	public boolean setDeletedTrue(Long id) {
		if (repo.existsById(id)) {
			repo.fakeDelete(id);
			return true;
		} else
			return false;
	}

	/**
	 * Get all slots having the deleted attribute set to true
	 * 
	 * @return returns a list of the corresponding slots
	 * @author Aurélien
	 */
	@Override
	public List<Slot> readDelete() {
		return repo.findByDeletedTrue();
	}

	/**
	 * Get all slots having the deleted attribute set to false
	 * 
	 * @return returns a list of the corresponding slots
	 * @author Aurélien
	 */
	@Override
	public List<Slot> readAllReal() {
		return repo.findByDeletedFalse();
	}
}
