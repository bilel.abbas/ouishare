package com.fr.adaming.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.fr.adaming.entity.Device;
import com.fr.adaming.repository.IDeviceRepository;
import com.fr.adaming.repository.ISlotRepository;
import com.fr.adaming.service.IDeviceService;

/**
 * @author Julie Noterman and Aurélien Journet
 *
 */

@Service
public class DeviceServiceImpl implements IDeviceService {

	@Autowired
	private IDeviceRepository repo;
	
	@Autowired
	private ISlotRepository repoSlot;

	/**
	 * Saves a given device in the database
	 * 
	 * @param device - the given entity
	 * @return device if the given device didn't exist in the database - else,
	 *         returns null
	 * @author Julie Noterman
	 */
	@Override
	public Device create(Device entity) {
		if (entity != null) {
			if (repo.exists(Example.of(entity))) {
				return null;
			} else {
				return repo.save(entity);
			}
		} else {
			return null;
		}

	}

	/**
	 * Updates the values of a given device
	 * 
	 * @param device - the given entity
	 * @return the device if the given device has been modified - else, returns null
	 * @author Julie Noterman
	 */
	@Override
	public Device update(Device entity) {
		if (repo.existsById(entity.getId())) {
			repo.save(entity);
			return entity;
		} else {
			return null;
		}
	}

	/**
	 * Find the device in database that has the same id as the id given
	 * 
	 * @return device - in case no client has the same id as the one given, it
	 *         returns null
	 * @author Julie Noterman
	 */

	@Override
	public Device readById(Long id) {
		return repo.findById(id).orElse(null);
		
	}

	/**
	 * Finds all devices in database where deleted = false
	 * 
	 * @return List of device - in case no device has been inserted, it returns an
	 *         empty list
	 * @author Julie Noterman
	 */
	@Override
	public List<Device> readAll() {
		return repo.findByDeletedFalse();
	}

	/**
	 * Finds all devices in database where deleted = true
	 * 
	 * @return List of device - in case no device has been inserted, it returns an
	 *         empty list
	 * @author Julie Noterman
	 */
	@Override
	public List<Device> readDelete() {
		return repo.findByDeletedTrue();
	}

	/**
	 * Finds all devices in database
	 * 
	 * @return List of device - in case no device has been inserted, it returns an
	 *         empty list
	 * @author Julie Noterman
	 */
	@Override
	public List<Device> readAllReal() {
		return repo.findAll();
	}
	
	
	/** Finds all devices in database that has no association with Slot (= id_slot = null)
	 * @return List of device - in case no device has been inserted, it returns an empty list
	 * @author Julie Noterman
	 */
	public List<Device> readAllCatalog(){
		return repo.readAllCatalog();
	}

	/**
	 * Delete a client of the database
	 * 
	 * @param the id of a device
	 * @return true if the device has been deleted - false if the device hasn't been
	 *         deleted
	 * @author Julie Noterman
	 */
	@Override
	public boolean deleteById(Long id) {
		if (id != null) {
			if (repo.fakeDelete(id) == 1) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	
	/**
	 * Set the attribute deleted of the entity to true
	 * 
	 * @param the id of the entity
	 * @return true if the attribute has been changed to true - false if the
	 *         attribute steel equals to false
	 * @author Julie Noterman
	 */
	@Override
	public boolean setDeletedTrue(Long id) {
		Device d = repo.findById(id).orElse(null);
		if (d != null) {
			d.setDeleted(true);
			repo.save(d);
			return true;
		} else {
			return false;
		}

	}
	
	
	
	/**
	 * Adds a Slot to the given Device
	 * 
	 * @param the id of the entity
	 * @return boolean - true if the given device was found, else false 
	 * 
	 * @author Jean-Baptiste Jamet
	 */
	@Override
	public boolean addAssociationWithSlot(Long idDevice, Long idSlot) {
		
		Device device = readById(idDevice);
		if (device != null && device.getSlot() == null && repoSlot.existsById(idSlot)) {
			repo.addSlot(idDevice, idSlot);
			return true;
		} else {
			return false;
		}
		
		
	}
	
	
	/**
	 * Updates the given device with a null Slot
	 * 
	 * @param the id of the entity
	 * @return boolean - true if the given device was found, else false
	 * 
	 * @author Jean-Baptiste Jamet
	 */
	@Override
	public boolean deleteAssociationWithSlot(Long id) {
		
		Device d = repo.findById(id).orElse(null);
		
		if(d != null) {
			
			d.setSlot(null);
			
			repo.save(d);
			
			return true;
		
		} else {
			
			return false;
			
		}
		

	}
				
	
		
			
	
	/**Finds all devices belonging to a given locker with its id
	 *@param the id of the locker
	 *@return Returns the list of the corresponding devices
	 *@author Aurélien Journet
	 */
	@Override
	public List<Device> readAvailableDevicesInLockerById(Long id) {
		return repo.getDevicesInLockerById(id);
	}

	/**Finds all devices belonging to a given locker with customer id
	 *@param the id of the customer
	 *@return Returns the list of the corresponding devices
	 *@author Aurélien Journet
	 */
	@Override
	public List<Device> readAvailableDevicesInCustomerById(Long id) {
		// TODO Auto-generated method stub
		return repo.getDevicesInCustomerById(id);
	}

	/**Finds all devices which are subscribed by a given customer
	 *@param the id of the customer
	 *@return Returns the list of the corresponding devices
	 *@author Aurélien Journet
	 */
	@Override
	public List<Device> readDevicesInSubscriptionsByCustomerId(Long id) {
		return repo.getDevicesInSubscriptionsByCustomerId(id);
	}


	public List<Device> readDevicesInLockerNotInSubscriptionsByCustomerId(Long id) {
		
		List<Device> list=new ArrayList<>();
		
		list=repo.getDevicesInCustomerById(id);
		list.removeAll(repo.getDevicesInSubscriptionsByCustomerId(id));
		
		return list;
	}
	
}
