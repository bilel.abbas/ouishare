package com.fr.adaming.service;

import java.util.List;

import com.fr.adaming.entity.Customer;

/**
 * @author CORNELOUP Theo
 *
 */
public interface ICustomerService extends IAbstractService<Customer, Long>{

	public List<Customer> findByAdress(String adress);
	public List<Customer> findByLastName(String lastName);
	public List<Customer> findByZipCode(Integer zipCode);
	public List<Customer> findByCity(String city);
	Customer login(String email, String pwd);
	public Customer findByEmail(String email);
	
}
