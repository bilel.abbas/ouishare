package com.fr.adaming.service.impl.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fr.adaming.service.ISubscriptionService;

@Component
public class Scheduler {

	@Autowired
	private ISubscriptionService service;
	
	@Scheduled(cron = "0 0 4 * * *")
	public void scheduler() {
		service.renewing();
	}

}
