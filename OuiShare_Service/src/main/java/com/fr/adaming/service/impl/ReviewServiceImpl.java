package com.fr.adaming.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.fr.adaming.entity.Review;
import com.fr.adaming.repository.IReviewRepository;
import com.fr.adaming.service.IReviewService;

@Service
public class ReviewServiceImpl implements IReviewService {

	@Autowired
	private IReviewRepository dao; 
	
	
	/**@author Dylan
	 * Save a given review in the database
	 * 
	 * @param review - the given entity
	 * @return updated review if the given entity is not already in the database
	 */
	public Review create(Review entity) {
		
		
		if (entity != null && dao.exists(Example.of(entity))) {
			return null;
		} else {
			return dao.save(entity);
		}
		
	}

	
	/**@author Dylan
	 * Update a given review in the database
	 * 
	 * @param review - the given entity
	 * @return updated review if the given entity is already in the database
	 */
	public Review update(Review entity) {
		
		if (entity.getId()!= null && dao.existsById(entity.getId())) {
			dao.save(entity);
			return entity;
		} else {
			return null;
		}

	}

	/**@author Dylan
	 * Read a given review with an specific id in the database
	 * 
	 * @param id - the given entity
	 * @return updated review if the given entity is already in the database
	 */
	public Review readById(Long id) {
		
			return dao.findById(id).orElse(null);
		
		
	}

	/**@author Dylan
	 * List
	 * 
	 * @return List<Review> where deleted=false
	 */
	public List<Review> readAll() {
		
		 return dao.findByDeletedFalse();
	}
	
	/**@author Dylan
	 * List
	 * 
	 * @return List<Review> where deleted=true
	 */
	public List<Review> readDelete() {
		
		return dao.findByDeletedTrue(); 
	}

	/**@author Dylan
	 * List
	 * 
	 * @return List<Review>
	 */
	public List<Review> readAllReal() {
		return dao.findAll();
	}

	/**@author Dylan
	 * Delete the given entity if the id (from Review) exists in the database
	 * 
	 * @param id 
	 * @return a boolean : true if the entity exists else return false
	 */
	public boolean deleteById(Long id) {
		
		if (id != null && dao.existsById(id)) {
			dao.deleteById(id);
			return true;
		} else {
			return false;
		}
	}
	
	/**@author Dylan
	 * Change the "deleted" boolean to true  if the id (from Review) exists in the database
	 * 
	 * @param id 
	 * @return a boolean : true if the entity exists else return false
	 */
	public boolean setDeletedTrue(Long id) {
		
		if (id !=null && dao.existsById(id)) {
			dao.fakeDelete(id);
			return true;
		} else {
			return false;
		}
	}


	
	
	
	

	

}
