package com.fr.adaming.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fr.adaming.entity.Locker;
import com.fr.adaming.repository.ILockerRepository;
import com.fr.adaming.service.ILockerService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Ambroise RENE
 *
 */
@Service
public class LockerServiceImpl implements ILockerService {

	@Autowired
	private ILockerRepository repository;

	@Override
	public Locker create(Locker locker) {
		if (locker != null) {
			if (locker.getName() != null && !locker.getName().isEmpty()
					&& repository.existsByName(locker.getName()) != null || locker.getName() == null) {
				return null;
			} else {
				return repository.save(locker);
			}
		} else {
			return null;
		}
	}

	@Override
	public Locker update(Locker locker) {
		if (locker != null && locker.getId() != null && repository.findById(locker.getId()).orElse(null) != null) {
			return repository.save(locker);
		} else {
			return null;
		}
	}

	@Override
	public Locker readById(Long id) {
		return repository.findById(id).orElse(null);

	}

	@Override
	public List<Locker> readAll() {
		return repository.findAll();
	}

	@Override
	public boolean deleteById(Long id) {
		if (readById(id) != null) {
			repository.deleteById(id);
			return true;
		} else {
			return false;
		}
	}

	public boolean setDeletedTrue(Long id) {
		if (readById(id) != null) {
			repository.fakeDelete(id);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<Locker> readDelete() {
		return repository.findByDeletedTrue();
	}

	@Override
	public List<Locker> readAllReal() {
		return repository.findByDeletedFalse();
	}

	@Override
	public List<String> allType(Long idLocker) {
		return repository.typeList(idLocker);
	}

}
