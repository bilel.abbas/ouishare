package com.fr.adaming.service;

import com.fr.adaming.entity.Admin;


/**
 * @author bilel
 *
 */
public interface IAdminService extends IAbstractService<Admin, Long> {
	
	public Admin login(String email, String pwd);
	


}
