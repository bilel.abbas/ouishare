package com.fr.adaming.service;

import com.fr.adaming.entity.Type;

/**
 * @author Aurélien
 *
 */
public interface ITypeService extends IAbstractService<Type, Long>{

}
