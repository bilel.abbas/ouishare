package com.fr.adaming.service.impl;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.fr.adaming.entity.Admin;
import com.fr.adaming.repository.IAdminRepository;
import com.fr.adaming.service.IAdminService;

/**
 * @author bilel
 *
 */
@Service
public class AdminServiceImpl implements IAdminService {

	@Autowired
	private IAdminRepository repo;

	/** Methode Create
	 * Elle permet de créer un admin 
	 *@param
	 *@returns La méthode retourne admin si l'admin n'existe pas sinon elle retourne null
	 */
	@Override
	public Admin create(Admin admin) {
		if (repo.exists(Example.of(admin))) {
			return null;
		} else {
			return repo.save(admin);
		}
	}

	/**La methode Update
	 * Elle permet de modifier les paramètres d'un admin
	 *@param
	 *@returns Si l'admin existe, la méthode retourne l'admin sinon elle retourne null
	 */
	@Override
	public Admin update(Admin admin) {
		if (repo.existsById(admin.getId())) {
			return repo.save(admin);
		} else {
			return null;
		}
	}

	/** La méthode readById
	 * Elle permet d'afficher un admin selon son id
	 *@param
	 *@return La méthode retourne l'admin si il existe sinon il retourne null
	 */
	@Override
	public Admin readById(Long id) {
		try {
			return repo.findById(id).get();
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	/** La méthode readAll
	 * La méthode permet d'afficher une liste d'admin dont l'attribut delete est false
	 * @param
	 * @returns La méthode renvoie une liste d'admins dont le delete est false. Si il n'y a pas d'admins avec un delete false, la liste sera vide.
	 *
	 */
	@Override
	public List<Admin> readAll() {
	return repo.findByDeletedFalse();
	}

	/**La méthode deleteById
	 * Elle permet de supprimer définitivement un admin de la base de données selon son id
	 *@param
	 *@returns La méthode renvoie true si l'admin a été supprimée sinon elle affiche false
	 *
	 */
	@Override
	public boolean deleteById(Long id) {

		if (repo.existsById(id)) {
			repo.deleteById(id);
			return true;
		} else {
			return false;
		}
	}

//	@Override
//	public boolean setDeletedTrue(Long id) {
//		try {
//		Admin admin = repo.findById(id).get();
//			admin.setDeleted(true);
//			repo.save(admin);
//			return true;
//		}
//		catch (NoSuchElementException e) {
//			return false;
//		}
//	}
	


	/** La méthode login
	 * Elle permet d'afficher un admin selon son email et son mot de passe (pwd)
	 *@param
	 *@returns Si l'email et le mot de passe correspondent à un admin, la méthode renvoie l'admin sinon elle renvoie null.
	 *
	 */
	@Override
	public Admin login(String email, String pwd) {
		return repo.readByEmailAndPwd(email, pwd);

	}

	/** La méthode setDeletedTrue
	 * Elle permet d'ajouter false à l'attribut delete d'un admin existant.
	 *@param
	 *@returns Si l'admin existe, la méthode retourne true sinon elle retourne false
	 */
	@Override
	public boolean setDeletedTrue(Long id) {
		if(repo.existsById(id)) {
		if(repo.fakeDelete(id)==1) {
		return true;
		}
		else {
		return false;	
		}
		}
		else {
		return false;
			}
		}
	


	/** La méthode readDelete
	 * Elle permet d'afficher la liste des admins dont l'attribut delete est true
	 *@param
	 *@return Elle retourne une liste d'admins si il y a des admins dont l'attribut delete est true sinon elle affiche une liste vide
	 */
	@Override
	public List<Admin> readDelete() {
		return repo.findByDeletedTrue();
	}

	/** La méthode readAllReal
	 * Elle permet d'afficher la liste des admins quelque soit la valeur de leur attributs delete
	 *@param
	 *@returns Elle renvoie la liste des admins sans distinction de leurs attribut delete. Si il n'y a pas d'admins, la liste sera vide.
	 *
	 */
	@Override
	public List<Admin> readAllReal() {
		return repo.findAll();
	}



	
}
