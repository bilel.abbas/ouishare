package com.fr.adaming.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fr.adaming.entity.Customer;
import com.fr.adaming.repository.ICustomerRepository;
import com.fr.adaming.service.ICustomerService;

/**
 * @author CORNELOUP Theo
 *
 */
@Service
public class CustomerServiceImpl implements ICustomerService {

	@Autowired
	private ICustomerRepository repo;

	
	/**
	 * Save a given costumer in the database
	 * 
	 * @param customer - the given entity
	 * @return null if the given entity is already in the database, else create a new customer
	 */
	public Customer create(Customer customer) {
		if (repo.existsByEmail(customer.getEmail())) {
			return null;
		} else {
			return repo.save(customer);
		}
	}

	/**
	 * Update a given customer in the database
	 * 
	 * @param customer - the given entity
	 * @return updated customer if the given entity already in the database, else return null
	 */
	public Customer update(Customer customer) {
		if (customer.getId() != null && readById(customer.getId()) != null) {
			repo.save(customer);
			return customer;
		} else {
			return null;
		}

	}

	/**
	 * Read a customer's id in the database
	 * 
	 *  @param customer - the given entity
	 *  @return find a customer by his id in the database if the id exist, if exception return null. 
	 */
	public Customer readById(Long id) {
		if (id != null) {
			return repo.findById(id).orElse(null);
		} else {
			return null;
		}

	}
	
	/**
	 *Get a list of customers
	 *
	 * @return list<Customer> where deleted = false
	 */
	public List<Customer> readAll() {
		return repo.findByDeletedFalse();
	}

	/**
	 * Delete the given entity if the id (from Customer) exists in the database
	 * 
	 * @param id 
	 * @return a boolean : true if the entity exists else return false
	 */
	public boolean deleteById(Long id) {
		if (id != null && readById(id) != null){
			repo.deleteById(id);
			return true;
		} else {
			return false;
		}

	}
	
	/**
	 * Read a customer's email in the database
	 * 
	 *  @param customer - the given entity
	 *  @return find a customer by his email in the database if the email exist, if exception return null. 
	 */
	public Customer findByEmail(String email) {
		if (email != null && repo.existsByEmail(email)) {
			return repo.findByEmail(email);
		} else {
			return null;
		}
	}

	/**
	 *Get a list of customers
	 *
	 * @return list<Customer> with the same adress.
	 */
	public List<Customer> findByAdress(String adress) {
		return repo.findByAdress(adress);
	}

	/**
	 *Get a list of customers
	 *
	 * @return list<Customer> with the same lastname
	 */
	public List<Customer> findByLastName(String lastName) {
		return repo.findByLastName(lastName);
	}

	/**
	 *Get a list of customers
	 *
	 * @return list<Customer> with the same zipcode
	 */
	public List<Customer> findByZipCode(Integer zipCode) {
		return repo.findByZipCode(zipCode);
	}

	/**
	 *Get a list of customers
	 *
	 * @return list<Customer> with the same city
	 */
	public List<Customer> findByCity(String city) {
		return repo.findByCity(city);
	}

	/**
	 * Change the "deleted" boolean to true  if the id (from Customer) exists in the database
	 * 
	 * @param id 
	 * @return a boolean : true if the entity exists else return false
	 */
	public boolean setDeletedTrue(Long id) {
		if (id != null && readById(id) != null){
			repo.fakeDelete(id);
			return true;
		} else {
			return false;
		}
	}

	/**
	 *Get a list of customers
	 *
	 * @return list<Customer> with the same lastname
	 */
	public Customer login(String email, String pwd) {
		
		return repo.findByEmailAndPwd(email, pwd);
	}

	/**
	 *Get a list of customers
	 *
	 * @return list<Customer> where deleted = false
	 */
	public List<Customer> readDelete() {
		return repo.findByDeletedTrue();
	}

	/**
	 * List
	 * 
	 * @return List<Customer>
	 */
	public List<Customer> readAllReal() {
		return repo.findAll();
	}

}
