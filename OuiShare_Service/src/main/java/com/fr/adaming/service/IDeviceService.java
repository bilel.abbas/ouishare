package com.fr.adaming.service;

import java.util.List;

import com.fr.adaming.entity.Device;
import com.fr.adaming.entity.Slot;



/**
 * @author Julie Noterman
 *
 */

public interface IDeviceService extends IAbstractService<Device, Long>{
	
	
	/**
	 * @return the list of all the objects available in the catalog
	 */
	public List<Device> readAllCatalog();

	// Dessassociate a Device from a Slot
	public boolean addAssociationWithSlot(Long idDevice, Long idSlot);
	public boolean deleteAssociationWithSlot(Long id);
	
	
	public List<Device> readAvailableDevicesInLockerById(Long id);
	
	public List<Device> readAvailableDevicesInCustomerById(Long id);
	
	public List<Device> readDevicesInSubscriptionsByCustomerId(Long id);
	
	public List<Device> readDevicesInLockerNotInSubscriptionsByCustomerId(Long id);

}
