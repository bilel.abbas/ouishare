package com.fr.adaming.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.fr.adaming.entity.Type;
import com.fr.adaming.repository.ITypeRepository;
import com.fr.adaming.service.ITypeService;

/**
 * @author Aurélien
 *
 */
@Service
public class TypeServiceImpl implements ITypeService {

	@Autowired
	private ITypeRepository repo;

	/**
	 * Saves a non existing type in the database
	 * 
	 * @param type to be saved
	 * @return returns the type with the generated id, or null in case of error
	 * @author Aurélien
	 */
	@Override
	public Type create(Type type) {

		if (type != null) {
			if (!repo.exists(Example.of(type))) {
				return repo.save(type);
			} else
				return null;

		} else
			return null;
	}

	/**
	 * Updates an existing type in the database
	 * 
	 * @param type to be updated
	 * @return returns the updated type, or null in case of error
	 * @author Aurélien
	 */
	@Override
	public Type update(Type type) {
		if (type != null) {
			if (type.getId() != null) {
				if (repo.existsById(type.getId()))
					return repo.save(type);
				else
					return null;
			} else
				return null;
		} else
			return null;
	}

	/**
	 * Returns a type with a matching id
	 * 
	 * @param id of the type to get
	 * @return returns the corresponding type, or null in case of error
	 * @author Aurélien
	 */
	@Override
	public Type readById(Long id) {
		return repo.findById(id).orElse(null);
	}

	/**
	 * Returns all types in the database
	 * 
	 * @return returns the types
	 * @author Aurélien
	 */
	@Override
	public List<Type> readAll() {
		return repo.findAll();
	}

	/**
	 * Deletes a type with a matching id from the database
	 * 
	 * @param id of the type to delete
	 * @return returns true if deletion is OK, false otherwise (non-matching id)
	 * @author Aurélien
	 */
	@Override
	public boolean deleteById(Long id) {
		if (repo.existsById(id)) {
			repo.deleteById(id);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Changes the deleted attribute to false for a type with a matching id from the
	 * database
	 * 
	 * @param id of the type to delete
	 * @return returns true if deletion is OK, false otherwise (non-matching id)
	 * @author Aurélien
	 */
	@Override
	public boolean setDeletedTrue(Long id) {
		if (readById(id) != null) {
			repo.fakeDelete(id);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Get all types having the deleted attribute set to true
	 * 
	 * @return returns a list of the corresponding types
	 * @author Aurélien
	 */
	@Override
	public List<Type> readDelete() {
		return repo.findByDeletedTrue();
	}

	/**
	 * Get all types having the deleted attribute set to false
	 * 
	 * @return returns a list of the corresponding types
	 * @author Aurélien
	 */
	@Override
	public List<Type> readAllReal() {
		return repo.findByDeletedFalse();
	}
}