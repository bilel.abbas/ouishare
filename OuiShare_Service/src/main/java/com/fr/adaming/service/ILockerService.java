package com.fr.adaming.service;

import java.util.List;

import com.fr.adaming.entity.Locker;

/**
 * @author Ambroise RENE
 *
 */

public interface ILockerService extends IAbstractService<Locker, Long>{

	public List<String> allType(Long idLocker);
}
