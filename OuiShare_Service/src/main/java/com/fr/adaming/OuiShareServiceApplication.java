package com.fr.adaming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class OuiShareServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OuiShareServiceApplication.class, args);
	}
}
