package com.fr.adaming.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fr.adaming.constants.AbstractConstants;
import com.fr.adaming.dto.CustomerUpdateDto;
import com.fr.adaming.dto.SubscriptionCreateDto;
import com.fr.adaming.dto.SubscriptionUpdateDto;

/**
 * @author Guillaume V., Thibaud
 *
 */
@RequestMapping
@CrossOrigin
public interface ISubscriptionController {

	@PostMapping(path = AbstractConstants.Subscription.CREATE)
	public SubscriptionCreateDto create(@Valid @RequestBody SubscriptionCreateDto dto);

	@PutMapping(path = AbstractConstants.Subscription.UPDATE)
	public SubscriptionUpdateDto update(@Valid @RequestBody SubscriptionUpdateDto dto);

	@GetMapping(path = AbstractConstants.Subscription.READ_ID)
	public SubscriptionUpdateDto readById(@PathVariable Long id);

	@GetMapping(path = AbstractConstants.Subscription.READ_ALL_REAL)
	public List<SubscriptionUpdateDto> readAllReal();

	@GetMapping(path = AbstractConstants.Subscription.READ_DELETE)
	public List<SubscriptionUpdateDto> readDeleted();

	@GetMapping(path = AbstractConstants.Subscription.READ_ALL)
	public List<SubscriptionUpdateDto> readAll();

	@DeleteMapping(path = AbstractConstants.Subscription.DELETE_ID)
	public boolean deleteById(@PathVariable Long id);

	@PutMapping(path = AbstractConstants.Subscription.SET_DELETED_TRUE)
	public boolean setDeletedTrue(@PathVariable Long id);

	@PutMapping(path = AbstractConstants.Subscription.REACTIVE)
	public boolean reactive(@PathVariable Long id);

	@PutMapping(path = AbstractConstants.Subscription.DESACTIVE)
	public boolean desactive(@PathVariable Long id);
	
	@PostMapping(path = AbstractConstants.Subscription.FIND_CUSTOMER)
	public List<SubscriptionUpdateDto> getByCustomer(@Valid @RequestBody CustomerUpdateDto dto);
}
