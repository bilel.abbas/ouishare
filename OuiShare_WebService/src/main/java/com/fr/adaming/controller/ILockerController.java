package com.fr.adaming.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fr.adaming.dto.LockerDto;
import com.fr.adaming.dto.LockerDtoUpdate;

/**
 * @author Ambroise RENE
 *
 */
@RequestMapping(path = "api/locker/")
@CrossOrigin
public interface ILockerController {


	@PostMapping
	public LockerDto create(@Valid @RequestBody LockerDto dto);

	@PutMapping
	public LockerDtoUpdate update(@Valid @RequestBody LockerDtoUpdate dto);
	
	@GetMapping(path = "{id}")
	public LockerDtoUpdate readById(@PathVariable Long id);
	
	@GetMapping
	public List<LockerDto> readAll();
	
	@GetMapping(path = "/deleted")
	public List<LockerDto> readDeleted();
	
	@GetMapping(path = "/real")
	public List<LockerDto> readAllReal();
	
	@DeleteMapping(path = "{id}")
	public boolean deleteById(@PathVariable Long id);
	
	@DeleteMapping(path = "{id}/fake")
	public boolean fakeDelete(@PathVariable Long id);
	
	@GetMapping(path = "{id}/alltype")
	public List<String> allType(@PathVariable Long idLocker);
}

