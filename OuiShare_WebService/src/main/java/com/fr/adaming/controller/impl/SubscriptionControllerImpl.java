package com.fr.adaming.controller.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.controller.ISubscriptionController;
import com.fr.adaming.converter.CustomerUpdateDtoConverter;
import com.fr.adaming.converter.SubscriptionCreateDtoConverter;
import com.fr.adaming.converter.SubscriptionUpdateDtoConverter;
import com.fr.adaming.dto.CustomerUpdateDto;
import com.fr.adaming.dto.SubscriptionCreateDto;
import com.fr.adaming.dto.SubscriptionUpdateDto;
import com.fr.adaming.entity.Subscription;
import com.fr.adaming.service.ISubscriptionService;

/**
 * @author Thibaud, Guillaume V.
 *
 */
@RestController
public class SubscriptionControllerImpl implements ISubscriptionController {

	@Autowired
	@Qualifier("subscriptionServiceImpl")
	private ISubscriptionService service;

	@Override
	public SubscriptionCreateDto create(@Valid SubscriptionCreateDto dto) {
		return SubscriptionCreateDtoConverter.MAPPER
				.convert(service.create(SubscriptionCreateDtoConverter.MAPPER.convert(dto)));

	}

	@Override
	public SubscriptionUpdateDto update(@Valid SubscriptionUpdateDto dto) {
		return SubscriptionUpdateDtoConverter.MAPPER
				.convert(service.update(SubscriptionUpdateDtoConverter.MAPPER.convert(dto)));
	}

	@Override
	public SubscriptionUpdateDto readById(Long id) {
		return SubscriptionUpdateDtoConverter.MAPPER.convert(service.readById(id));
	}

	@Override
	public List<SubscriptionUpdateDto> readAll() {
		return SubscriptionUpdateDtoConverter.MAPPER.convert(service.readAll());
	}

	@Override
	public boolean deleteById(Long id) {
		return service.deleteById(id);
	}

	@Override
	public boolean setDeletedTrue(Long id) {
		return service.setDeletedTrue(id);
	}

	@Override
	public List<SubscriptionUpdateDto> readAllReal() {
		return SubscriptionUpdateDtoConverter.MAPPER.convert(service.readAllReal());
	}

	@Override
	public List<SubscriptionUpdateDto> readDeleted() {
		return SubscriptionUpdateDtoConverter.MAPPER.convert(service.readDelete());
	}

	@Override
	public boolean reactive(Long id) {
		return service.reactiveSubscription(id);
	}

	@Override
	public boolean desactive(Long id) {
		return service.desactiveSubscription(id);
	}

	@Override
	public List<SubscriptionUpdateDto> getByCustomer(@Valid CustomerUpdateDto dto) {
		return SubscriptionUpdateDtoConverter.MAPPER.convert(service.findByCustomer(CustomerUpdateDtoConverter.MAPPER.convert(dto)));
	}

}
