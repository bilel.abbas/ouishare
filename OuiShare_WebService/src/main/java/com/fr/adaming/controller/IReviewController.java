package com.fr.adaming.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fr.adaming.constants.AbstractConstants;
import com.fr.adaming.dto.ReviewCreateDto;
import com.fr.adaming.dto.ReviewUpdateDto;




/**
 * @author Dylan
 *
 */
@RequestMapping
@CrossOrigin
public interface IReviewController {

	
	@PostMapping(path = AbstractConstants.Review.CREATE)
	public ReviewCreateDto create(@Valid @RequestBody ReviewCreateDto dto);

	@PutMapping(path = AbstractConstants.Review.UPDATE)
	public ReviewUpdateDto update(@Valid @RequestBody ReviewUpdateDto dto);
	
	@GetMapping(path = AbstractConstants.Review.READ_ID)
	public ReviewCreateDto readById(@PathVariable Long id);
	
	@GetMapping(path = AbstractConstants.Review.READ_ALL)
	public List<ReviewCreateDto> readAll();
	
	@GetMapping(path = AbstractConstants.Review.READ_DELETE)
	public List<ReviewCreateDto> readDelete();
	
	@GetMapping(path = AbstractConstants.Review.READ_ALL_REAL)
	public List<ReviewCreateDto> readAllReal();
	
	@DeleteMapping(path = AbstractConstants.Review.DELETE_ID)
	public boolean deleteById(@PathVariable Long id);
	
	@PutMapping(path = AbstractConstants.Review.SET_DELETED_TRUE)
	public boolean setDeletedTrue(@PathVariable Long id);
}
