package com.fr.adaming.controller.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.controller.ISlotController;
import com.fr.adaming.converter.SlotDtoConverter;

import com.fr.adaming.dto.SlotDto;
import com.fr.adaming.dto.SlotDtoWithoutId;
import com.fr.adaming.service.ISlotService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Aurélien
 *
 */
@RestController
@Slf4j
public class SlotControllerImpl implements ISlotController {

	@Autowired
	@Qualifier("slotServiceImpl")
	private ISlotService service;

	public SlotDto create(@Valid SlotDtoWithoutId dto) {
		log.info("Creating slot number : "+dto.getNumber());
		return SlotDtoConverter.MAPPER.convert(service.create(SlotDtoConverter.MAPPER.convert(dto)));
	}

	public SlotDto update(@Valid SlotDto dto) {
		log.info("Updating slot of id "+dto.getId());
		return SlotDtoConverter.MAPPER.convert(service.update(SlotDtoConverter.MAPPER.convert(dto)));
	}

	public SlotDto readById(Long id) {
		log.info("Getting slot of id "+id);
		return SlotDtoConverter.MAPPER.convert(service.readById(id));
	}

	public List<SlotDto> readAll() {
		log.info("Reading all slots");
		return SlotDtoConverter.MAPPER.convertt(service.readAll());
	}

	public boolean deleteById(Long id) {
		log.info("Deleting slot of id "+id);
		return service.deleteById(id);
	}

	public boolean fakeDeleteById(Long id) {
		log.info("Fake deleting slot of id "+id);
		return service.setDeletedTrue(id);
	}
}
