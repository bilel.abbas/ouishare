package com.fr.adaming.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fr.adaming.constants.AbstractConstants;
import com.fr.adaming.dto.TypeDtoCreate;
import com.fr.adaming.dto.TypeDtoUpdate;

/**
 * @author Aurélien et Bilel
 *
 */
@RequestMapping
@CrossOrigin
public interface ITypeController {

	@PostMapping(path = AbstractConstants.Type.CREATE)
	public TypeDtoCreate create(@Valid @RequestBody TypeDtoCreate dto);

	@PutMapping(path = AbstractConstants.Type.UPDATE)
	public TypeDtoUpdate update(@Valid @RequestBody TypeDtoUpdate dto);
	
	@GetMapping(path = AbstractConstants.Type.READ_ID)
	public TypeDtoUpdate readById(@PathVariable Long id);
	
	@GetMapping(path = AbstractConstants.Type.READ_ALL_REAL)
	public List<TypeDtoUpdate> readAllReal();
	
	@DeleteMapping(path = AbstractConstants.Type.DELETE_ID)
	public boolean deleteById(@PathVariable Long id);
	
	@GetMapping(path = AbstractConstants.Type.READ_ALL)
	public List<TypeDtoUpdate> findByDeletedFalse();
	
	@GetMapping(path = AbstractConstants.Type.READ_DELETE)
	public List<TypeDtoUpdate> findByDeletedTrue();

	@PutMapping(path = AbstractConstants.Type.SET_DELETED_TRUE)
	public boolean setDeletedTrue(@PathVariable(name = "id") Long id);
}
