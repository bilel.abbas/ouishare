package com.fr.adaming.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fr.adaming.constants.AbstractConstants;
import com.fr.adaming.dto.AdminDtoCreate;
import com.fr.adaming.dto.AdminDtoUpdate;

/**
 * @author bilel
 *
 */
@RequestMapping
@CrossOrigin
public interface IAdminController {

	@PostMapping(path = AbstractConstants.Admin.CREATE)
	public AdminDtoCreate create(@Valid @RequestBody AdminDtoCreate dto);

	@PutMapping(path = AbstractConstants.Admin.UPDATE)
	public AdminDtoUpdate update(@Valid @RequestBody AdminDtoUpdate dto);

	@GetMapping(path = AbstractConstants.Admin.READ_ID)
	public AdminDtoUpdate readById(@PathVariable(name = "id") Long id);

	@GetMapping(path = AbstractConstants.Admin.READ_ALL_REAL)
	public List<AdminDtoUpdate> readAllReal();

	@DeleteMapping(path = AbstractConstants.Admin.DELETE_ID)
	public boolean deleteById(@PathVariable(name = "id") Long id);
	
	@GetMapping(path = AbstractConstants.Admin.READ_ALL)
	public List<AdminDtoUpdate> findByDeletedFalse();
	
	@GetMapping(path = AbstractConstants.Admin.READ_DELETE)
	public List<AdminDtoUpdate> findByDeletedTrue();
	
	@PutMapping(path = AbstractConstants.Admin.SET_DELETED_TRUE)
	public boolean setDeletedTrue(@PathVariable(name = "id") Long id);

}
