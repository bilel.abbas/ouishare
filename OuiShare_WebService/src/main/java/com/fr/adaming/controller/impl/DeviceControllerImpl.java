package com.fr.adaming.controller.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.controller.IDeviceController;
import com.fr.adaming.converter.DeviceDtoConverter;
import com.fr.adaming.converter.DeviceDtoWithIdConverter;
import com.fr.adaming.dto.DeviceDto;
import com.fr.adaming.dto.DeviceDtoWithId;
import com.fr.adaming.service.IDeviceService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Julie Noterman and Aurélien Journet
 *
 */
@RestController
@Slf4j
public class DeviceControllerImpl implements IDeviceController {

	@Autowired
	@Qualifier("deviceServiceImpl")
	private IDeviceService service;
	
	@Override
	public DeviceDto create(@Valid DeviceDto dto) {
		log.info("Create Device : "+dto);
		return DeviceDtoConverter.MAPPER.convert(service.create(DeviceDtoConverter.MAPPER.convert(dto)));
	}

	@Override
	public DeviceDtoWithId update(@Valid DeviceDtoWithId dto) {
		log.info("Update Device : "+dto);
		return DeviceDtoWithIdConverter.MAPPER.convert(service.update(DeviceDtoWithIdConverter.MAPPER.convert(dto)));
	}

	@Override
	public DeviceDtoWithId readById(Long id) {
		log.info("Read by id : "+id);
		return DeviceDtoWithIdConverter.MAPPER.convert(service.readById(id));
	}

	@Override
	public List<DeviceDto> readAll() {
		log.info("Read all");
		return DeviceDtoConverter.MAPPER.convert(service.readAll());
	}
	
	@Override
	public List<DeviceDto> readDelete() {
		log.info("Read delete");
		return DeviceDtoConverter.MAPPER.convert(service.readDelete());
	}

	@Override
	public List<DeviceDto> readAllReal() {
		log.debug("Read all real");
		return DeviceDtoConverter.MAPPER.convert(service.readAllReal());
	}

	@Override
	public boolean deleteById(Long id) {
		log.debug("Delete by id : "+id);
		return service.deleteById(id);
	}


	@Override
	public boolean setDeletedTrue(Long id) {
		log.debug("Set Deleted True :"+id);
		return service.setDeletedTrue(id);
	}
	
//	@Override
//	public boolean addAssociationWithSlot(Long idDevice, Long idSlot) {
//		log.debug("Add association with Slot - idDevice :"+idDevice+" and "+idSlot);
//		return service.addAssociationWithSlot(idDevice, idSlot);	
//	}
	
	@Override
	public boolean deleteAssociationWithSlot(Long idDevice) {
		log.debug("Delete association with Slot - idDevice :"+idDevice);
		return service.deleteAssociationWithSlot(idDevice);
	}
	
	@Override
	public List<DeviceDtoWithId> readAllCatalog() {
		log.debug("Read all catalogue");
		return DeviceDtoConverter.MAPPER.convertt(service.readAllCatalog());
	}

	public List<DeviceDto> readAllFromLockerId(Long id) {
		log.debug("Read all from locker id :"+id);
		return DeviceDtoConverter.MAPPER.convert(service.readAvailableDevicesInLockerById(id));
	}
	
	public List<DeviceDtoWithId> readAllFromCustomerNotInSubsId(Long id) {
		log.debug("Read all from customer id :"+id);
		return DeviceDtoConverter.MAPPER.convertt(service.readDevicesInLockerNotInSubscriptionsByCustomerId(id));
	}

	@Override
	public List<DeviceDtoWithId> readAllSubscriptionsFromCustomerId(Long id) {
		log.debug("Read all from subs for customer id :"+id);
		return DeviceDtoConverter.MAPPER.convertt(service.readDevicesInSubscriptionsByCustomerId(id));
	}

}
