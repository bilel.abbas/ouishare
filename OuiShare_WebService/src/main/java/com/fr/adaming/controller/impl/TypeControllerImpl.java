package com.fr.adaming.controller.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.controller.ITypeController;
import com.fr.adaming.converter.TypeDtoConverter;
import com.fr.adaming.dto.TypeDtoCreate;
import com.fr.adaming.dto.TypeDtoUpdate;
import com.fr.adaming.service.ITypeService;

import lombok.extern.slf4j.Slf4j;



/**
 * @author Aurélien et Bilel
 *
 */

@RestController
@Slf4j
public class TypeControllerImpl implements ITypeController{

	@Autowired
	@Qualifier("typeServiceImpl")
	private ITypeService service;
	
	@Override
	public TypeDtoCreate create(@Valid TypeDtoCreate dto) {
		log.info("add type "+dto);
		return TypeDtoConverter.MAPPER.convertCreate(service.create(TypeDtoConverter.MAPPER.convertCreate(dto)));
	}

	@Override
	public TypeDtoUpdate update(@Valid TypeDtoUpdate dto) {
		log.info("update type "+dto);
		return TypeDtoConverter.MAPPER.convertUpdate(service.update(TypeDtoConverter.MAPPER.convertUpdate(dto)));
	}

	@Override
	public TypeDtoUpdate readById(Long id) {
		log.info("show type with id="+id);
		return TypeDtoConverter.MAPPER.convertUpdate((service.readById(id)));
	}


	@Override
	public List<TypeDtoUpdate> readAllReal() {
		log.info("show all types");
		return TypeDtoConverter.MAPPER.convert(service.readAllReal());
	}

	@Override
	public boolean deleteById(Long id) {
		log.info("delete type with id="+id);
		return service.deleteById(id);
	}

	@Override
	public List<TypeDtoUpdate> findByDeletedFalse() {
		log.info("show all types with deleted=false");
		return TypeDtoConverter.MAPPER.convert(service.readAll());
	}

	@Override
	public List<TypeDtoUpdate> findByDeletedTrue() {
		log.info("show all types with deleted=true");
		return TypeDtoConverter.MAPPER.convert(service.readDelete());
	}

	@Override
	public boolean setDeletedTrue(Long id) {
		log.info("add deleted=true for type with id="+id);
		return service.setDeletedTrue(id);
	}

}