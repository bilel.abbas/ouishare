package com.fr.adaming.controller.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.controller.IReviewController;

import com.fr.adaming.converter.ReviewCreateDtoConverter;
import com.fr.adaming.converter.ReviewUpdateDtoConverter;

import com.fr.adaming.dto.ReviewCreateDto;
import com.fr.adaming.dto.ReviewUpdateDto;

import com.fr.adaming.service.IReviewService;

import lombok.extern.slf4j.Slf4j;



/**
 * @author Dylan
 *
 */
@RestController
@Slf4j
public class ReviewControllerImpl implements IReviewController {

	@Autowired
	@Qualifier("reviewServiceImpl")
	private IReviewService service;

	
	public ReviewCreateDto create(@Valid ReviewCreateDto dto) {
		log.info("create a new review : "+dto);
		return ReviewCreateDtoConverter.MAPPER.convert(service.create(ReviewCreateDtoConverter.MAPPER.convert(dto)));
	}

	
	public ReviewUpdateDto update(@Valid ReviewUpdateDto dto) {
		log.info("update an existing review : "+dto);
		return ReviewUpdateDtoConverter.MAPPER.convert(service.update(ReviewUpdateDtoConverter.MAPPER.convert(dto)));
	}

	
	public ReviewCreateDto readById(Long id) {
		log.info("read the review where id is :"+id);
		return ReviewCreateDtoConverter.MAPPER.convert(service.readById(id));
	}

	
	public List<ReviewCreateDto> readAll() {
		log.info("a list of review where deleted = false");
		return ReviewCreateDtoConverter.MAPPER.convert(service.readAll());
	}

	
	public List<ReviewCreateDto> readDelete() {
		log.info("a list of review where deleted = true");
		return ReviewCreateDtoConverter.MAPPER.convert(service.readDelete());
	}

	
	public List<ReviewCreateDto> readAllReal() {
		log.info("a list of review where deleted = true and deleted=false");
		return ReviewCreateDtoConverter.MAPPER.convert(service.readAllReal());
	}

	
	public boolean deleteById(Long id) {
		log.info("delete a review where id is : "+id);
		return service.deleteById(id);
	}

	
	public boolean setDeletedTrue(Long id) {
		log.info("change the deleted value of the review to true where id review is : " +id);
		return service.setDeletedTrue(id);
	}
	
	
	
}
