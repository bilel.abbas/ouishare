package com.fr.adaming.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fr.adaming.constants.AbstractConstants;
import com.fr.adaming.dto.RentalDto;
import com.fr.adaming.dto.RentalDtoAvail;
import com.fr.adaming.dto.RentalDtoCreate;
import com.fr.adaming.dto.RentalDtoUpdate;

@RequestMapping
@CrossOrigin
public interface IRentalController {

	@PostMapping(path = AbstractConstants.Rental.CREATE)
	public ResponseEntity<RentalDtoUpdate> create(@RequestBody RentalDtoCreate rentalDto);
	@PutMapping(path = AbstractConstants.Rental.UPDATE)
	public ResponseEntity<RentalDtoUpdate> update(@RequestBody RentalDtoUpdate rentalDto);
	@GetMapping(path = AbstractConstants.Rental.READ_ID)
	public ResponseEntity<RentalDto> readById(@PathVariable(name = "id") Long id);
	@GetMapping(path = AbstractConstants.Rental.READ_ALL)
	public ResponseEntity<List<RentalDto>> readAll();
	@GetMapping(path = AbstractConstants.Rental.READ_DELETE)
	public ResponseEntity<List<RentalDto>> readDelete();
	@GetMapping(path = AbstractConstants.Rental.READ_ALL_REAL)
	public ResponseEntity<List<RentalDto>> readAllReal();
	@DeleteMapping(path = AbstractConstants.Rental.DELETE_ID)
	public ResponseEntity<String> deleteById(@PathVariable(name = "id") Long id);
	@PutMapping(path = AbstractConstants.Rental.SET_DELETED_TRUE)
	public ResponseEntity<String> setDeletedTrue(@PathVariable(name = "id") Long id);
	@GetMapping(path = AbstractConstants.Rental.AVAILABILITY)
	public ResponseEntity<List<RentalDtoAvail>> availability(@PathVariable(name = "id") Long idDevice);
	@PutMapping(path = AbstractConstants.Rental.ADD_REVIEW)
	public ResponseEntity<String> addReview(@PathVariable(name = "idRental") Long idRental, @PathVariable(name = "idReview") Long idReview);
	@PutMapping(path = AbstractConstants.Rental.END)
	public ResponseEntity<Long> addEndDateReal(@PathVariable(name = "id") Long id);
	
	public boolean checkDate(LocalDateTime startDate, LocalDateTime endDateTheoritical);
	@GetMapping(path = AbstractConstants.Rental.AVAILABILITY_NOW)
	public boolean checkAvailabilityNow(@PathVariable(name = "id") Long id);
}
