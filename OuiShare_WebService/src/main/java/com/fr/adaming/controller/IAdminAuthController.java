package com.fr.adaming.controller;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fr.adaming.constants.AbstractConstants;
import com.fr.adaming.dto.AdminDtoLogin;

/**
 * @author bilel
 *
 */
@RequestMapping(path = AbstractConstants.Auth.LOGIN_ADMIN)
@CrossOrigin
public interface IAdminAuthController {

	@PostMapping
	public AdminDtoLogin login(@Valid @RequestBody AdminDtoLogin dto);

}
