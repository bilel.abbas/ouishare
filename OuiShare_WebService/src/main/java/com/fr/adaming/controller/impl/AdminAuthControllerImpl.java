package com.fr.adaming.controller.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.controller.IAdminAuthController;
import com.fr.adaming.converter.AdminDtoConverter;
import com.fr.adaming.dto.AdminDtoLogin;
import com.fr.adaming.service.IAdminService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author bilel
 *
 */
@RestController
@Slf4j
public class AdminAuthControllerImpl implements IAdminAuthController {
	
	@Autowired
	@Qualifier("adminServiceImpl")
	private IAdminService service;
	

	@Override
	public AdminDtoLogin login(@Valid AdminDtoLogin dto) {
		log.info("connexion admin "+dto);
		return AdminDtoConverter.MAPPER.convertLogin(service.login(((AdminDtoConverter.MAPPER.convertLogin(dto)).getEmail()),((AdminDtoConverter.MAPPER.convertLogin(dto)).getPwd())));
	}

}
