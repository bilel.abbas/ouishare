package com.fr.adaming.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fr.adaming.constants.AbstractConstants;
import com.fr.adaming.dto.DeviceDto;
import com.fr.adaming.dto.DeviceDtoWithId;


/**
 * @author Julie Noterman and Aurélien Journet
 *
 */
@RequestMapping
@CrossOrigin
public interface IDeviceController {
	
	@PostMapping(path = AbstractConstants.Device.CREATE)
	public DeviceDto create(@Valid @RequestBody DeviceDto dto);

	@PutMapping(path = AbstractConstants.Device.UPDATE)
	public DeviceDtoWithId update(@Valid @RequestBody DeviceDtoWithId dto);
	
	@GetMapping(path = AbstractConstants.Device.READ_ID)
	public DeviceDtoWithId readById(@PathVariable Long id);
	
	@GetMapping (path = AbstractConstants.Device.READ_ALL)
	public List<DeviceDto> readAll();
	
	@GetMapping(path = AbstractConstants.Device.READ_DELETE)
	public List<DeviceDto> readDelete();
	
	@GetMapping(path = AbstractConstants.Device.READ_ALL_REAL)
	public List<DeviceDto> readAllReal();
	
	@GetMapping (path = AbstractConstants.Device.READ_ALL_CATALOG)
	public List<DeviceDtoWithId> readAllCatalog();
	
	@DeleteMapping(path = AbstractConstants.Device.DELETE_ID)
	public boolean deleteById(@PathVariable Long id);
	
	@PutMapping(path = AbstractConstants.Device.SET_DELETED_TRUE)
	public boolean setDeletedTrue(@PathVariable Long id);
	
//	@GetMapping(path = AbstractConstants.Device.ADD_ASSOCIATION_WITH_SLOT)
//	public boolean addAssociationWithSlot(@PathVariable Long idDevice, @PathVariable Long idSlot);

	@GetMapping (path=AbstractConstants.Device.READ_ALL_FROM_LOCKER_ID)
	public List<DeviceDto> readAllFromLockerId(@PathVariable Long id);
	
	@GetMapping (path=AbstractConstants.Device.READ_ALL_FROM_CUSTOMER_ID)
	public List<DeviceDtoWithId> readAllFromCustomerNotInSubsId(@PathVariable Long id);
	
	@GetMapping (path=AbstractConstants.Device.READ_ALL_SUBS_FROM_CUSTOMER_ID)
	public List<DeviceDtoWithId> readAllSubscriptionsFromCustomerId(@PathVariable Long id);
	
	@GetMapping(path = "{idDevice}/deleteAssociationWithSlot")
	boolean deleteAssociationWithSlot(@PathVariable Long idDevice);

}
