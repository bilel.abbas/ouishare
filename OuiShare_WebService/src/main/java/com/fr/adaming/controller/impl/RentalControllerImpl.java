package com.fr.adaming.controller.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.controller.IRentalController;
import com.fr.adaming.converter.RentalDtoAvailConverter;
import com.fr.adaming.converter.RentalDtoConverter;
import com.fr.adaming.converter.RentalDtoCreateConverter;
import com.fr.adaming.converter.RentalDtoUpdateConverter;
import com.fr.adaming.dto.RentalDto;
import com.fr.adaming.dto.RentalDtoAvail;
import com.fr.adaming.dto.RentalDtoCreate;
import com.fr.adaming.dto.RentalDtoUpdate;
import com.fr.adaming.entity.Rental;
import com.fr.adaming.service.impl.DeviceServiceImpl;
import com.fr.adaming.service.impl.RentalServiceImpl;

/**
 * @author Nicolas RUFFIER
 *
 */

@RestController
public class RentalControllerImpl implements IRentalController {

	@Autowired
	private RentalServiceImpl service;

	@Autowired
	private DeviceServiceImpl serviceD;

	private ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	private Validator validator = factory.getValidator();

	@Override 
	public ResponseEntity<RentalDtoUpdate> create(RentalDtoCreate rentalDto) {
		Set<ConstraintViolation<RentalDtoCreate>> violations = validator.validate(rentalDto);

		if (violations.isEmpty() && checkDate(rentalDto.getStartDate(), rentalDto.getEndDateTheoritical())) {
			Rental rental = service.create(RentalDtoCreateConverter.MAPPER.convert(rentalDto));
			if (rental != null) {
				return new ResponseEntity<>(RentalDtoUpdateConverter.MAPPER.convert(rental),
						HttpStatus.OK);
			}

		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

	@Override
	public ResponseEntity<RentalDtoUpdate> update(RentalDtoUpdate rentalDto) {
		Set<ConstraintViolation<RentalDtoUpdate>> violations = validator.validate(rentalDto);
		if (violations.isEmpty() && checkDate(rentalDto.getStartDate(), rentalDto.getEndDateTheoritical())) {
			Rental rental = service.create(RentalDtoUpdateConverter.MAPPER.convert(rentalDto));
			if (rental != null) {
				return new ResponseEntity<>(RentalDtoUpdateConverter.MAPPER.convert(rental),
						HttpStatus.OK);
			}

		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

	@Override
	public ResponseEntity<RentalDto> readById(Long id) {
		Rental rental = service.readById(id);
		if (rental != null) {
			return new ResponseEntity<>(RentalDtoConverter.MAPPER.convert(rental), HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

	@Override
	public ResponseEntity<List<RentalDto>> readAll() {
		return new ResponseEntity<>(RentalDtoConverter.MAPPER.convert2(service.readAll()),
				HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<RentalDto>> readDelete() {
		return new ResponseEntity<>(RentalDtoConverter.MAPPER.convert2(service.readAll()),
				HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<RentalDto>> readAllReal() {
		return new ResponseEntity<>(RentalDtoConverter.MAPPER.convert2(service.readAll()),
				HttpStatus.OK);
	}

	@Override
	public ResponseEntity<String> deleteById(Long id) {
		if (service.deleteById(id)) {
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	public ResponseEntity<String> setDeletedTrue(Long id) {
		if (service.setDeletedTrue(id)) {
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	public ResponseEntity<List<RentalDtoAvail>> availability(Long idDevice) {
		return new ResponseEntity<>(
				RentalDtoAvailConverter.MAPPER.convert2(service.availability(serviceD.readById(idDevice))),
				HttpStatus.OK);
	}

	@Override
	public ResponseEntity<String> addReview(@NotNull Long idRental,@NotNull Long idReview) {
		if (service.addReview(idRental, idReview)) {
			return new ResponseEntity<>("SUCCES", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("FAIL", HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	public ResponseEntity<Long> addEndDateReal(Long id) {
		Long resp = service.addEndDateReal(id);
		if (resp == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else if (resp == 0L) {
			return new ResponseEntity<>(0L, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(resp, HttpStatus.OK);
		}
	}

	@Override
	public boolean checkDate(LocalDateTime startDate, LocalDateTime endDateTheoritical) {
			return startDate.isBefore(endDateTheoritical);
	}

	@Override
	public boolean checkAvailabilityNow(Long id) {
		return service.isAvailableNow(id);
	}

}
