package com.fr.adaming.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fr.adaming.constants.AbstractConstants;
import com.fr.adaming.dto.CustomerCreateDto;
import com.fr.adaming.dto.CustomerLoginDto;
import com.fr.adaming.dto.CustomerUpdateDto;

/**
 * @author CORNELOUP Theo
 *
 */
@RequestMapping
@CrossOrigin
public interface ICustomerController {

	@PostMapping(path = AbstractConstants.Customer.CREATE)
	public CustomerCreateDto create(@Valid @RequestBody CustomerCreateDto dto);

	@PutMapping(path = AbstractConstants.Customer.UPDATE)
	public CustomerUpdateDto update(@Valid @RequestBody CustomerUpdateDto dto);
	
	@GetMapping(path = AbstractConstants.Customer.READ_ID)
	public CustomerCreateDto readById(@PathVariable Long id);
	
	@GetMapping(path = AbstractConstants.Customer.READ_EMAIL)
	public CustomerUpdateDto findByEmail(@PathVariable String email);
	
	@GetMapping(path = AbstractConstants.Customer.GET_ADRESS)
	public List<CustomerCreateDto> findByAdress(@PathVariable String adress);
	
	@GetMapping(path = AbstractConstants.Customer.GET_LASTNAME)
	public List<CustomerCreateDto> findByLastName(@PathVariable String lastName);
	
	@GetMapping(path = AbstractConstants.Customer.GET_ZIPCODE)
	public List<CustomerCreateDto> findByZipCode(@PathVariable Integer zipCode);
	
	@GetMapping(path = AbstractConstants.Customer.GET_CITY)
	public List<CustomerCreateDto> findByCity(@PathVariable String city);
	
	@GetMapping(path = AbstractConstants.Customer.READ_ALL)
	public List<CustomerCreateDto> readAll();
	
	@GetMapping(path = AbstractConstants.Customer.READ_DELETE)
	public List<CustomerCreateDto> readDelete();
	
	@GetMapping(path = AbstractConstants.Customer.READ_ALL_REAL)
	public List<CustomerCreateDto> readAllReal();
	
	@DeleteMapping(path = AbstractConstants.Customer.DELETE_ID)
	public boolean deleteById(@PathVariable Long id);
	
	@PutMapping(path = AbstractConstants.Customer.SET_DELETED_TRUE)
	public boolean setDeletedTrue(@PathVariable Long id);
	
	@PostMapping(path = AbstractConstants.Customer.LOGIN)
	public CustomerLoginDto login(@Valid @RequestBody CustomerLoginDto loginDto);
	
}
