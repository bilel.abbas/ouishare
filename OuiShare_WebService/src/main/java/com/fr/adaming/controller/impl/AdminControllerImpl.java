package com.fr.adaming.controller.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.controller.IAdminController;
import com.fr.adaming.converter.AdminDtoConverter;
import com.fr.adaming.dto.AdminDtoCreate;
import com.fr.adaming.dto.AdminDtoUpdate;
import com.fr.adaming.service.IAdminService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author bilel
 *
 */
@RestController
@Slf4j
public class AdminControllerImpl implements IAdminController {
	
	@Autowired
	@Qualifier("adminServiceImpl")
	private IAdminService service;
	

	@Override
	
	public AdminDtoCreate create(@Valid AdminDtoCreate dto) {
		log.info("add admin "+dto);
		return AdminDtoConverter.MAPPER.convertCreate(service.create(AdminDtoConverter.MAPPER.convertCreate(dto)));
	}

	@Override
	public AdminDtoUpdate update(@Valid AdminDtoUpdate dto) {
		log.info("update admin "+dto);
		return AdminDtoConverter.MAPPER.convertUpdate(service.update(AdminDtoConverter.MAPPER.convertUpdate(dto)));

	}

	@Override
	public AdminDtoUpdate readById(Long id) {
		log.info("show admin with id="+id);
		return AdminDtoConverter.MAPPER.convertUpdate(service.readById(id));

	}

	@Override
	public List<AdminDtoUpdate> readAllReal() {
		log.info("show all admins");
		return AdminDtoConverter.MAPPER.convert(service.readAllReal());

	}

	@Override
	public boolean deleteById(Long id) {
		log.info("delete admin with id="+id);
		return service.deleteById(id);
	}

	@Override
	public List<AdminDtoUpdate> findByDeletedFalse() {
		log.info("show all admins with deleted=false");
		return AdminDtoConverter.MAPPER.convert(service.readAll());
	}

	@Override
	public List<AdminDtoUpdate> findByDeletedTrue() {
		log.info("show all admins with deleted=true");
		return AdminDtoConverter.MAPPER.convert(service.readDelete());
	}

	@Override
	public boolean setDeletedTrue(Long id) {
		log.info("add deleted=true for admin with id="+id);
		return service.setDeletedTrue(id);
	}










}
