package com.fr.adaming.controller.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.controller.ICustomerController;
import com.fr.adaming.converter.CustomerCreateDtoConverter;
import com.fr.adaming.converter.CustomerUpdateDtoConverter;
import com.fr.adaming.dto.CustomerCreateDto;
import com.fr.adaming.dto.CustomerLoginDto;
import com.fr.adaming.dto.CustomerUpdateDto;
import com.fr.adaming.service.ICustomerService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author CORNELOUP Theo
 *
 */
@RestController
@Slf4j
public class CustomerControllerImpl implements ICustomerController {

	@Autowired
	@Qualifier("customerServiceImpl")
	private ICustomerService service;
	
	
	public CustomerCreateDto create(@Valid CustomerCreateDto dto) {
		log.info("Create a new customer" +dto);
		return CustomerCreateDtoConverter.MAPPER.convert(service.create(CustomerCreateDtoConverter.MAPPER.convert(dto)));
	}

	
	public CustomerUpdateDto update(@Valid CustomerUpdateDto dto) {
		log.info("Update a customer" +dto);
		return CustomerUpdateDtoConverter.MAPPER.convert(service.update(CustomerUpdateDtoConverter.MAPPER.convert(dto)));
	}

	
	public CustomerCreateDto readById(Long id) {
		log.info("Read information for a customer where id is :" +id);
		return CustomerCreateDtoConverter.MAPPER.convert(service.readById(id));
	}
	
	public CustomerUpdateDto findByEmail(String email) {
		log.info("Read information for a customer where email is :" +email);
		return CustomerUpdateDtoConverter.MAPPER.convert(service.findByEmail(email));
	}
	
	public List<CustomerCreateDto> findByAdress(String adress) {
		log.info("Find a customer by his adress :" +adress);
		return CustomerCreateDtoConverter.MAPPER.convert(service.findByAdress(adress));
	}

	
	public List<CustomerCreateDto> findByLastName(String lastName) {
		log.info("Find a customer by his lastname :" +lastName);
		return CustomerCreateDtoConverter.MAPPER.convert(service.findByLastName(lastName));
	}

	
	public List<CustomerCreateDto> findByZipCode(Integer zipCode) {
		log.info("Find a customer by his zipcode :" +zipCode);
		return CustomerCreateDtoConverter.MAPPER.convert(service.findByZipCode(zipCode));
	}

	
	public List<CustomerCreateDto> findByCity(String city) {
		log.info("Find a customer by his city :" +city);
		return CustomerCreateDtoConverter.MAPPER.convert(service.findByCity(city));
	}

	
	public List<CustomerCreateDto> readAll() {
		log.info("Read all customer where deleted = false");
		return CustomerCreateDtoConverter.MAPPER.convert(service.readAll());
	}

	
	public List<CustomerCreateDto> readDelete() {
		log.info("Read all customer where deleted = true");
		return CustomerCreateDtoConverter.MAPPER.convert(service.readDelete());
	}

	
	public List<CustomerCreateDto> readAllReal() {
		log.info("Read all customer");
		return CustomerCreateDtoConverter.MAPPER.convert(service.readAllReal());
	}

	
	public boolean deleteById(Long id) {
		log.info("Delete a customer where id is :" +id);
		return service.deleteById(id);
	}

	
	public boolean setDeletedTrue(Long id) {
		log.info("SetDeleteTrue a customer where id is :" +id);
		return service.setDeletedTrue(id);
	}


	public CustomerLoginDto login(CustomerLoginDto loginDto) {
		log.info("Login customer where login is" +loginDto);
		if (service.login(loginDto.getEmail(), loginDto.getPwd()) != null) {
			return loginDto;
		} else {
			return null;
		}
	}

}
