package com.fr.adaming.controller.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RestController;

import com.fr.adaming.controller.ILockerController;
import com.fr.adaming.converter.LockerDtoConverter;
import com.fr.adaming.converter.LockerDtoConverterUpdate;
import com.fr.adaming.dto.LockerDto;
import com.fr.adaming.dto.LockerDtoUpdate;
import com.fr.adaming.service.ILockerService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Ambroise RENE
 *
 */
@RestController
@Slf4j
public class LockerControllerImpl implements ILockerController {

	@Autowired
	@Qualifier("lockerServiceImpl")
	private ILockerService service;
	
	@Override
	public LockerDto create(@Valid LockerDto dto) {
		log.info("add locker"+dto);
		return LockerDtoConverter.MAPPER.convert(service.create(LockerDtoConverter.MAPPER.convert(dto)));
	}

	@Override
	public LockerDtoUpdate update(@Valid LockerDtoUpdate dto) {
		return LockerDtoConverterUpdate.MAPPER.convert(service.update(LockerDtoConverterUpdate.MAPPER.convert(dto)));
	}

	@Override
	public LockerDtoUpdate readById(Long id) {
		return LockerDtoConverterUpdate.MAPPER.convert(service.readById(id));
	}

	@Override
	public List<LockerDto> readAll() {
		return LockerDtoConverter.MAPPER.convert(service.readAll());
	}

	@Override
	public boolean deleteById(Long id) {
		return service.deleteById(id);
	}

	@Override
	public boolean fakeDelete(Long id) {
		return service.setDeletedTrue(id);
	}

	@Override
	public List<LockerDto> readDeleted() {
		return LockerDtoConverter.MAPPER.convert(service.readDelete());
	}

	@Override
	public List<LockerDto> readAllReal() {
		return LockerDtoConverter.MAPPER.convert(service.readAllReal());
	}

	public List<String> allType(Long idLocker) {
		return service.allType(idLocker);

	}

}
