package com.fr.adaming.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fr.adaming.constants.AbstractConstants;
import com.fr.adaming.dto.SlotDto;
import com.fr.adaming.dto.SlotDtoWithoutId;

/**
 * @author Aurélien
 *
 */
@RequestMapping(path = AbstractConstants.Slot.BASE_PATH)
@CrossOrigin
public interface ISlotController {

	@PostMapping
	public SlotDto create(@Valid @RequestBody SlotDtoWithoutId dto);

	@PutMapping
	public SlotDto update(@Valid @RequestBody SlotDto dto);
	
	@GetMapping(path = "{id}")
	public SlotDto readById(@PathVariable Long id);
	
	@GetMapping
	public List<SlotDto> readAll();
	
	@DeleteMapping(path = "{id}")
	public boolean deleteById(@PathVariable Long id);
	
	@DeleteMapping(path="fake/{id}")
	public boolean fakeDeleteById(@PathVariable Long id);
}