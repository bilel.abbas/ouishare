package com.fr.adaming.constants;

/**
 * @author bilel
 *
 */
public class AbstractConstants {

	/**
	 * Remplacer vos URL par les constantes ci-dessous. Choisissez votre classe et
	 * prenez les constantes correspondant à votre URL. Composez vos URL avec ces
	 * constantes Exemple : URL (/api/admin/) Avec les constantes ça donne :
	 * (AbstractConstants.Admin.BASE_PATH)
	 * 
	 */
	public static final String GLOBAL_PATH = "/api/";
	private static final String CREATE_PATH = "";
	private static final String UPDATE_PATH = "";
	private static final String READ_ID_PATH = "{id}/";
	private static final String READ_ALL_PATH = "read-all/";
	private static final String READ_DELETE_PATH = "read-delete/";
	private static final String READ_ALL_REAL_PATH = "read-all-real/";
	private static final String DELETE_ID_PATH = "{id}/";
	private static final String SET_DELETED_TRUE_PATH = "{id}/";

	public static class UserExample {
		public static final String BASE_PATH = GLOBAL_PATH + "example-user/";
	}

	public static class Admin {
		public static final String BASE_PATH = GLOBAL_PATH + "admin/";
		public static final String CREATE = BASE_PATH + CREATE_PATH;
		public static final String UPDATE = BASE_PATH + UPDATE_PATH;
		public static final String READ_ID = BASE_PATH + READ_ID_PATH;
		public static final String READ_ALL = BASE_PATH + READ_ALL_PATH;
		public static final String READ_DELETE = BASE_PATH + READ_DELETE_PATH;
		public static final String READ_ALL_REAL = BASE_PATH + READ_ALL_REAL_PATH;
		public static final String DELETE_ID = BASE_PATH + DELETE_ID_PATH;
		public static final String SET_DELETED_TRUE = BASE_PATH + SET_DELETED_TRUE_PATH;
	}

	public static class Type {
		public static final String BASE_PATH = GLOBAL_PATH + "type/";
		public static final String CREATE = BASE_PATH + CREATE_PATH;
		public static final String UPDATE = BASE_PATH + UPDATE_PATH;
		public static final String READ_ID = BASE_PATH + READ_ID_PATH;
		public static final String READ_ALL = BASE_PATH + READ_ALL_PATH;
		public static final String READ_DELETE = BASE_PATH + READ_DELETE_PATH;
		public static final String READ_ALL_REAL = BASE_PATH + READ_ALL_REAL_PATH;
		public static final String DELETE_ID = BASE_PATH + DELETE_ID_PATH;
		public static final String SET_DELETED_TRUE = BASE_PATH + SET_DELETED_TRUE_PATH;
	}

	public static class Auth {
		public static final String LOGIN = GLOBAL_PATH + "auth/login";
		public static final String LOGIN_ADMIN = GLOBAL_PATH + "admin/login";
		public static final String LOGIN_CUSTOMER = GLOBAL_PATH + "customer/login";
		public static final String REGISTER = GLOBAL_PATH + "auth/register";
	}
	


	public static class Customer {
		public static final String BASE_PATH = GLOBAL_PATH + "customer/";
		public static final String CREATE = BASE_PATH + CREATE_PATH;
		public static final String UPDATE = BASE_PATH + UPDATE_PATH;
		public static final String READ_ID = BASE_PATH + READ_ID_PATH;
		public static final String READ_ALL = BASE_PATH + READ_ALL_PATH;
		public static final String READ_DELETE = BASE_PATH + READ_DELETE_PATH;
		public static final String READ_ALL_REAL = BASE_PATH + READ_ALL_REAL_PATH;
		public static final String DELETE_ID = BASE_PATH + DELETE_ID_PATH;
		public static final String SET_DELETED_TRUE = BASE_PATH + SET_DELETED_TRUE_PATH;
		public static final String READ_EMAIL = BASE_PATH + "get-email/{email}/";
		public static final String GET_ADRESS = BASE_PATH + "get-adress/{adress}/";
		public static final String GET_LASTNAME = BASE_PATH + "get-lastname/{lastName}/";
		public static final String GET_ZIPCODE = BASE_PATH +"get-zipcode/{zipCode}/";
		public static final String GET_CITY = BASE_PATH + "get-city/{city}/";
		public static final String LOGIN = BASE_PATH + "login/";

	}

	public static class Device {
		public static final String BASE_PATH = GLOBAL_PATH + "device/";
		public static final String CREATE = BASE_PATH + CREATE_PATH;
		public static final String UPDATE = BASE_PATH + UPDATE_PATH;
		public static final String READ_ID = BASE_PATH + READ_ID_PATH;
		public static final String READ_ALL = BASE_PATH + READ_ALL_PATH;
		public static final String READ_DELETE = BASE_PATH + READ_DELETE_PATH;
		public static final String READ_ALL_REAL = BASE_PATH + READ_ALL_REAL_PATH;
		public static final String DELETE_ID = BASE_PATH + DELETE_ID_PATH;
		public static final String SET_DELETED_TRUE = BASE_PATH + SET_DELETED_TRUE_PATH;
		public static final String READ_ALL_CATALOG = BASE_PATH + "read-all-catalog/";
		//public static final String ADD_ASSOCIATION_WITH_SLOT = BASE_PATH + ADD_ASSOCIATION_WITH_SLOT_PATH;
		public static final String READ_ALL_FROM_LOCKER_ID = BASE_PATH + "read_all_from_locker/{id}/";
		public static final String READ_ALL_FROM_CUSTOMER_ID = BASE_PATH + "read_all_from_customer/{id}/";
		public static final String READ_ALL_SUBS_FROM_CUSTOMER_ID = BASE_PATH + "read_all_subs_from_customer/{id}/";
	}

	public static class Locker {
		public static final String BASE_PATH = GLOBAL_PATH + "locker/";
		public static final String CREATE = BASE_PATH + CREATE_PATH;
		public static final String UPDATE = BASE_PATH + UPDATE_PATH;
		public static final String READ_ID = BASE_PATH + READ_ID_PATH;
		public static final String READ_ALL = BASE_PATH + READ_ALL_PATH;
		public static final String READ_DELETE = BASE_PATH + READ_DELETE_PATH;
		public static final String READ_ALL_REAL = BASE_PATH + READ_ALL_REAL_PATH;
		public static final String DELETE_ID = BASE_PATH + DELETE_ID_PATH;
		public static final String SET_DELETED_TRUE = BASE_PATH + SET_DELETED_TRUE_PATH;
	}

	public static class Rental {
		public static final String BASE_PATH = GLOBAL_PATH + "rental/";
		public static final String CREATE = BASE_PATH + CREATE_PATH;
		public static final String UPDATE = BASE_PATH + UPDATE_PATH;
		public static final String READ_ID = BASE_PATH + READ_ID_PATH;
		public static final String READ_ALL = BASE_PATH + READ_ALL_PATH;
		public static final String READ_DELETE = BASE_PATH + READ_DELETE_PATH;
		public static final String READ_ALL_REAL = BASE_PATH + READ_ALL_REAL_PATH;
		public static final String DELETE_ID = BASE_PATH + DELETE_ID_PATH;
		public static final String SET_DELETED_TRUE = BASE_PATH + SET_DELETED_TRUE_PATH;
		public static final String AVAILABILITY = BASE_PATH + "/{id}/availability";
		public static final String AVAILABILITY_NOW = BASE_PATH + "/{id}/availability-now/";
		public static final String ADD_REVIEW = BASE_PATH + "/{idRental}/add-review/{idReview}";
		public static final String END = BASE_PATH + "/{id}/end";
	}

	public static class Review {
		public static final String BASE_PATH = GLOBAL_PATH + "review/";
		public static final String CREATE = BASE_PATH + CREATE_PATH;
		public static final String UPDATE = BASE_PATH + UPDATE_PATH;
		public static final String READ_ID = BASE_PATH + READ_ID_PATH;
		public static final String READ_ALL = BASE_PATH + READ_ALL_PATH;
		public static final String READ_DELETE = BASE_PATH + READ_DELETE_PATH;
		public static final String READ_ALL_REAL = BASE_PATH + READ_ALL_REAL_PATH;
		public static final String DELETE_ID = BASE_PATH + DELETE_ID_PATH;
		public static final String SET_DELETED_TRUE = BASE_PATH + SET_DELETED_TRUE_PATH;
	}

	public static class Slot {
		public static final String BASE_PATH = GLOBAL_PATH + "slot/";
		public static final String CREATE = BASE_PATH + CREATE_PATH;
		public static final String UPDATE = BASE_PATH + UPDATE_PATH;
		public static final String READ_ID = BASE_PATH + READ_ID_PATH;
		public static final String READ_ALL = BASE_PATH + READ_ALL_PATH;
		public static final String READ_DELETE = BASE_PATH + READ_DELETE_PATH;
		public static final String READ_ALL_REAL = BASE_PATH + READ_ALL_REAL_PATH;
		public static final String DELETE_ID = BASE_PATH + DELETE_ID_PATH;
		public static final String SET_DELETED_TRUE = BASE_PATH + SET_DELETED_TRUE_PATH;
	}

	public static class Subscription {
		public static final String BASE_PATH = GLOBAL_PATH + "subscription/";
		public static final String CREATE = BASE_PATH + CREATE_PATH;
		public static final String UPDATE = BASE_PATH + UPDATE_PATH;
		public static final String READ_ID = BASE_PATH + READ_ID_PATH;
		public static final String READ_ALL = BASE_PATH + READ_ALL_PATH;
		public static final String READ_DELETE = BASE_PATH + READ_DELETE_PATH;
		public static final String READ_ALL_REAL = BASE_PATH + READ_ALL_REAL_PATH;
		public static final String DELETE_ID = BASE_PATH + DELETE_ID_PATH;
		public static final String SET_DELETED_TRUE = BASE_PATH + SET_DELETED_TRUE_PATH;
		public static final String REACTIVE = BASE_PATH + "{id}/reactive";
		public static final String DESACTIVE = BASE_PATH + "{id}/desactive";
		public static final String FIND_CUSTOMER = BASE_PATH + "getByCustomer";
	}

}
