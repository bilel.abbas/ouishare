package com.fr.adaming.jackson;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * Deserialization of java.time.LocalDate "dd/MM/yyyy"
 *
 * @author mbensalha, Brias Guillaume
 *
 */
public class LocalDateDeserializer extends JsonDeserializer<LocalDate> {
	 @Override
	    public LocalDate deserialize(JsonParser parser, DeserializationContext context) throws IOException {

	    	return LocalDate.parse(parser.getText(), DateTimeFormatter.ISO_LOCAL_DATE);
	    }
}
