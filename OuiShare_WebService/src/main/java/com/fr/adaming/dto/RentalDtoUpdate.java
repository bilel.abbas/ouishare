package com.fr.adaming.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Nicolas RUFFIER
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RentalDtoUpdate extends RentalDtoCreate {

	@NotNull
	private Long id;

	@Override
	public String toString() {
		return "RentalDtoUpdate [id=" + id + ", isSubscription()=" + isSubscription() + ", getIdDevice()="
				+ getIdDevice() + ", getIdCustomer()=" + getIdCustomer() + ", getStartDate()=" + getStartDate()
				+ ", getEndDateTheoritical()=" + getEndDateTheoritical() + "]";
	}

}
