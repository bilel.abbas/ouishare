package com.fr.adaming.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fr.adaming.jackson.LocalDateDeserializer;
import com.fr.adaming.jackson.LocalDateSerializer;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Guillaume V., Thibaud
 *
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class SubscriptionCreateDto {

	@NotNull
	@PastOrPresent
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@ApiModelProperty(required = true, example = "2010-06-18")
	private LocalDate startDate;
	

	@ApiModelProperty(required = true)
	private CustomerUpdateDto customer;
	@ApiModelProperty(required = true)
	private DeviceDtoWithId device;
}
