package com.fr.adaming.dto;

import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author bilel
 *
 */
@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class AdminDtoCreate {

    @NotNull
    @ApiModelProperty(required = true, example = "bilel-abbas")
	private String userName;
	
    @Email
    @NotNull
    @ApiModelProperty(required = true, example = "bilel.abbas@gmail.com")
	private String email;
	
    @NotNull
    @Size(min = 4,max = 8)
    @ApiModelProperty(required = true, example = "azerty")
	private String pwd;	
	
   
	
}
