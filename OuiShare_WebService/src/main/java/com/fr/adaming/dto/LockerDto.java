package com.fr.adaming.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Ambroise RENE
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class LockerDto {
	
	@NotBlank
	@ApiModelProperty(required=true,example="12 rue des chocapics")
	private String adress;
	
	@NotNull
	@ApiModelProperty(required=true,example="01234")
	private Long zipCode;
	
	@NotBlank
	@ApiModelProperty(required=true,example="Chaille-les-oies")
	private String city;
	
	@Positive
	@NotNull
	@ApiModelProperty(required=true,example="24")
	private Long slot;
	
	@NotBlank
	@ApiModelProperty(required=true,example="Jean-Jacques le casier")
	private String name;
	
	@ApiModelProperty(required=true,example="false")
	private boolean deleted;
	
}
