package com.fr.adaming.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


/**
 * @author Dylan
 *
 */
@Getter @Setter @ToString @NoArgsConstructor
public class ReviewCreateDto {

	@Min(value = 0, message = "The value must be between 0 and 5")
	@Max(value = 5,message = "The value must be between 0 and 5")
	@ApiModelProperty(required = false, example = "2",  notes = "A value between 0 and 5")
	private int rating;
	@ApiModelProperty(required = false, example = "This is an example of comment" )
	private String comment;
	@ApiModelProperty(required = false, example = "True" )
	private boolean deleted;
	
	
	
	

	
	public ReviewCreateDto(int rating, String comment, boolean deleted) {
		super();
		this.rating = rating;
		this.comment = comment;
		this.deleted = deleted;
	}
}
