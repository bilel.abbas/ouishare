package com.fr.adaming.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



/**
 * @author Julie Noterman
 *
 */
@Getter @Setter @NoArgsConstructor
public class DeviceDto {
	
	@NotBlank
	@ApiModelProperty(example = "Aspirateur")
	protected String name;
	
	@ApiModelProperty(example = "Dyson")
	protected String brand;
	
	@NotBlank
	@ApiModelProperty(example = "Ceci est un aspirateur")
	protected String description;
	
	@NotNull
	@ApiModelProperty(example = "5")
	protected Double priceSub;
	
	@NotNull
	@ApiModelProperty(example = "10")
	protected Double pricePonctual;
	
	@NotNull
	@ApiModelProperty(example = "400")
	protected Double deposit;
	
	@NotNull
	@ApiModelProperty(example = "12")
	protected Double priceExtra;
	
	@ApiModelProperty(example = "bon état")
	protected String state;
	
	@ApiModelProperty(example = "Commentaire sur l'état")
	protected String stateComment;
	
	@NotNull
	@ApiModelProperty(example = "100")
	protected Long chip;
	
	@NotNull
	@ApiModelProperty(example = "1")
	protected Double rentalDurationInHours;
	
	@ApiModelProperty(example = "les regles d'utilisations sont les suivantes")
	protected String rules;
	
	@ApiModelProperty(example = "nomImage.jpg")
	protected String urlImage;

}
