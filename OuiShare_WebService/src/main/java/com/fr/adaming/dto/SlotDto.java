package com.fr.adaming.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Aurélien
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SlotDto {
	
	@ApiModelProperty(required = true,example = "123")
	private Long id;
	
	@NotNull
	@ApiModelProperty(required = true,example = "20005043")
	private Long number;

	@Pattern(regexp = "([0-9]+x[0-9]+x[0-9]+)")
	@ApiModelProperty(required = false,example = "10x20x30")
	private String dimensions;
	
	@NotNull
	@ApiModelProperty(required = false,example = "false")
	private boolean deleted;
}
