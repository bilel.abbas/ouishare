package com.fr.adaming.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author bilel
 *
 */
@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class TypeDtoUpdate {

	@NotNull
    @ApiModelProperty(required = true, example = "1")
	private Long id;

	@NotNull
    @ApiModelProperty(required = true, example = "Aspirateur")
	private String name;

	@NotNull
	@PositiveOrZero
    @ApiModelProperty(required = true, example = "45")
	private int stock;
	
    @ApiModelProperty(required = true, example = "true")
	private boolean deleted;

}
