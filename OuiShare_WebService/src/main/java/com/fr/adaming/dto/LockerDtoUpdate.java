package com.fr.adaming.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class LockerDtoUpdate {

	@NotNull
	@ApiModelProperty(required=true,example="13")
	private Long id;
	
	@NotBlank
	@ApiModelProperty(required=true,example="12 rue des chocapics")
	private String adress;

	@NotNull
	@ApiModelProperty(required=true,example="01234")
	private Long zipCode;

	@NotBlank
	@ApiModelProperty(required=true,example="Chaille-les-oies")
	private String city;

	@Positive
	@ApiModelProperty(required=true,example="24")
	private Long slot;

	@NotBlank
	@ApiModelProperty(required=true,example="Jean-Jacques le casier")
	private String name;
	
	@ApiModelProperty(required=true,example="false")
	private boolean deleted;

	public LockerDtoUpdate( Long id,  String adress,  Long zipCode,  String city, Long slot,  String name, boolean deleted) {
		super();
		this.id = id;
		this.adress = adress;
		this.zipCode = zipCode;
		this.city = city;
		this.slot = slot;
		this.name = name;
		this.deleted = deleted;
	}

	
}
