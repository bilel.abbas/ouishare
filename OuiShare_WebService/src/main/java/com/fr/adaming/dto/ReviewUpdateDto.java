package com.fr.adaming.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;



/**
 * @author Dylan
 *
 */
@Getter @Setter @ToString @NoArgsConstructor
public class ReviewUpdateDto {

	@ApiModelProperty(required = true, example = "2" )
	private Long id;
	@Min(value = 0, message = "The value must be between 0 and 5")
	@Max(value = 5,message = "The value must be between 0 and 5")
	@ApiModelProperty(required = false, example = "2",  notes = "A value between 0 and 5")
	private int rating;
	@ApiModelProperty(required = false, example = "This is an example of comment" )
	private String comment;
	@ApiModelProperty(required = false, example = "True" )
	private boolean deleted;
	
	
	
	

	
	public ReviewUpdateDto(int rating, String comment, boolean deleted) {
		super();
		this.rating = rating;
		this.comment = comment;
		this.deleted = deleted;
	}






	public ReviewUpdateDto(Long id, int rating, String comment, boolean deleted) {
		super();
		this.id = id;
		this.rating = rating;
		this.comment = comment;
		this.deleted = deleted;
	}
	
	
	
}
