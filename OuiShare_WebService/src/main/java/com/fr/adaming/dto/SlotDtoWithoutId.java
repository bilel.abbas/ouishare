package com.fr.adaming.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Aurélien et Jean-Baptiste
 *
 */
@Getter
@Setter
public class SlotDtoWithoutId {

	
	@NotNull
	@ApiModelProperty(required = true,example = "20005043")
	private Long number;

	@Pattern(regexp = "([0-9]+x[0-9]+x[0-9]+)")
	@ApiModelProperty(required = false,example = "10x20x30")
	private String dimensions;
	
	@NotNull
	@ApiModelProperty(required = false,example = "false")
	private boolean deleted;
}