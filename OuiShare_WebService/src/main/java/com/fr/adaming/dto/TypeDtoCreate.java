package com.fr.adaming.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author bilel
 *
 */
@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class TypeDtoCreate {

	@NotNull
    @ApiModelProperty(required = true, example = "Aspirateur")
	private String name;

	@NotNull
    @ApiModelProperty(required = true, example = "45")
	@PositiveOrZero
	private int stock;

}
