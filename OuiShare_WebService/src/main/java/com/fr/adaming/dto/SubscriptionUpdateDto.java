package com.fr.adaming.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fr.adaming.jackson.LocalDateDeserializer;
import com.fr.adaming.jackson.LocalDateSerializer;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Guillaume V. et Thibaud
 *
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class SubscriptionUpdateDto {

	@NotNull
	@ApiModelProperty(required = true, example = "115")
	private Long id;

	@NotNull
	@PastOrPresent
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@ApiModelProperty(required = true, example = "2012-09-12")
	private LocalDate startDate;
	
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@ApiModelProperty(required = true, example = "2012-10-12")
	private LocalDate endDate;
	

	@ApiModelProperty(required = true, example = "false")
	private boolean deleted;

	@ApiModelProperty(required = true, example = "true")
	private boolean renew;

	@ApiModelProperty(required = true)
	private CustomerUpdateDto customer;
	@ApiModelProperty(required = true)
	private DeviceDtoWithId device;

}
