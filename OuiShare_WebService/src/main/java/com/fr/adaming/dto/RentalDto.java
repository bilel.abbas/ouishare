package com.fr.adaming.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fr.adaming.jackson.LocalDateTimeDeserializer;
import com.fr.adaming.jackson.LocalDateTimeSerializer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Nicolas RUFFIER
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RentalDto {

	@NotNull
	private Long id;

	@NotNull
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime startDate;

	@NotNull
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime endDateTheoritical;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime endDateReal;

	private boolean subscription;

	private boolean deleted;

	@NotNull
	private DeviceDtoWithId device;

	@NotNull
	private CustomerUpdateDto customer;

	private ReviewUpdateDto review;

	@Override
	public String toString() {
		return "RentalDto [id=" + id + ", startDate=" + startDate + ", endDateTheoritical=" + endDateTheoritical
				+ ", endDateReal=" + endDateReal + ", subscription=" + subscription + ", deleted=" + deleted
				+ ", Device=" + device + ", Customer=" + customer + ", Review=" + review + "]";
	}

}
