package com.fr.adaming.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Nicolas RUFFIER
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RentalDtoCreate extends RentalDtoAvail{

	private boolean subscription;

	@NotNull
	private Long idDevice;

	@NotNull
	private Long idCustomer;

	@Override
	public String toString() {
		return "RentalDtoCreate [subscription=" + subscription + ", idDevice=" + idDevice + ", idCustomer=" + idCustomer
				+ ", getStartDate()=" + getStartDate() + ", getEndDateTheoritical()=" + getEndDateTheoritical() + "]";
	}
	
}
