package com.fr.adaming.converter;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.fr.adaming.dto.ReviewUpdateDto;

import com.fr.adaming.entity.Review;


/**
 * @author Dylan
 *
 */
@Mapper
public interface ReviewUpdateDtoConverter {

	
	ReviewUpdateDtoConverter MAPPER = Mappers.getMapper(ReviewUpdateDtoConverter.class);

	Review convert(ReviewUpdateDto dto);
	
	List<Review> convert(ArrayList<ReviewUpdateDto> dtos);

	ReviewUpdateDto convert(Review entity);
	
	List<ReviewUpdateDto> convert(List<Review> entities);
}
