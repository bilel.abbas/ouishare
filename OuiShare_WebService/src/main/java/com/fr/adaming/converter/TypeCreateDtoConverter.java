package com.fr.adaming.converter;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.factory.Mappers;

import com.fr.adaming.dto.TypeDtoCreate;
import com.fr.adaming.entity.Type;

/**
 * @author Guillaume V.
 *
 */
public interface TypeCreateDtoConverter {

	TypeCreateDtoConverter MAPPER = Mappers.getMapper(TypeCreateDtoConverter.class);

	Type convert(TypeDtoCreate dto);

	List<Type> convert(ArrayList<TypeDtoCreate> dtos);

	TypeDtoCreate convert(Type type);

	List<TypeDtoCreate> convert(List<Type> type);
}
