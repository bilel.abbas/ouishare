package com.fr.adaming.converter;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.fr.adaming.dto.CustomerLoginDto;
import com.fr.adaming.entity.Customer;

@Mapper
public interface CustomerLoginDtoConverter {

	CustomerLoginDtoConverter MAPPER = Mappers.getMapper(CustomerLoginDtoConverter.class);

	List<Customer> convert(ArrayList<CustomerLoginDto> dtos);
	
	Customer convert(CustomerLoginDto dto);
	
	CustomerLoginDto convert(Customer entity);
	
	List<CustomerLoginDto> convert(List<Customer> entities);
}
