package com.fr.adaming.converter;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.fr.adaming.dto.CustomerUpdateDto;
import com.fr.adaming.entity.Customer;

/**
 * @author CORNELOUP Theo
 *
 */
@Mapper
public interface CustomerUpdateDtoConverter {

	CustomerUpdateDtoConverter MAPPER = Mappers.getMapper(CustomerUpdateDtoConverter.class);

	Customer convert(CustomerUpdateDto dto);
	
	List<Customer> convert(ArrayList<CustomerUpdateDto> dtos);

	CustomerUpdateDto convert(Customer entity);
	
	List<CustomerUpdateDto> convert(List<Customer> entities);
	
}
