package com.fr.adaming.converter;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.fr.adaming.dto.DeviceDto;
import com.fr.adaming.dto.DeviceDtoWithId;
import com.fr.adaming.entity.Device;


/**
 * @author Julie Noterman
 *
 */
@Mapper
public interface DeviceDtoConverter {
	
	DeviceDtoConverter MAPPER = Mappers.getMapper(DeviceDtoConverter.class);

	Device convert(DeviceDto dto);
	
	List<Device> convert(ArrayList<DeviceDto> dtos);

	DeviceDto convert(Device entity);
	
	List<DeviceDto> convert(List<Device> entities);
	
	List<DeviceDtoWithId> convertt(List<Device> entities);

}
