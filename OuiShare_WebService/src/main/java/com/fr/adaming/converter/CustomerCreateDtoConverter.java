package com.fr.adaming.converter;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.fr.adaming.dto.CustomerCreateDto;
import com.fr.adaming.entity.Customer;

/**
 * @author CORNELOUP Theo
 *
 */
@Mapper
public interface CustomerCreateDtoConverter {

	CustomerCreateDtoConverter MAPPER = Mappers.getMapper(CustomerCreateDtoConverter.class);

	Customer convert(CustomerCreateDto dto);
	
	List<Customer> convert(ArrayList<CustomerCreateDto> dtos);

	CustomerCreateDto convert(Customer entity);
	
	List<CustomerCreateDto> convert(List<Customer> entities);
	
}
