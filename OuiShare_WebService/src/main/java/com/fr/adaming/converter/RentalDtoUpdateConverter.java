package com.fr.adaming.converter;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.fr.adaming.dto.RentalDtoUpdate;
import com.fr.adaming.entity.Rental;

/**
 * @author Nicolas RUFFIER
 *
 */
@Mapper
public interface RentalDtoUpdateConverter {

	RentalDtoUpdateConverter MAPPER = Mappers.getMapper(RentalDtoUpdateConverter.class);

	@Mapping(target = "device.id", source = "dto.idDevice")
	@Mapping(target = "customer.id", source = "dto.idCustomer")
	Rental convert(RentalDtoUpdate dto);

	List<Rental> convert1(List<RentalDtoUpdate> dtos);

	@Mapping(target = "idDevice", source = "rental.device.id")
	@Mapping(target = "idCustomer", source = "rental.customer.id")
	RentalDtoUpdate convert(Rental rental);

	List<RentalDtoUpdate> convert2(List<Rental> rentals);

}
