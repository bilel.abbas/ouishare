package com.fr.adaming.converter;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.fr.adaming.dto.LockerDtoUpdate;
import com.fr.adaming.entity.Locker;

@Mapper
public interface LockerDtoConverterUpdate {

	LockerDtoConverterUpdate MAPPER = Mappers.getMapper(LockerDtoConverterUpdate.class);

	Locker convert(LockerDtoUpdate dto);
	
	List<Locker> convert(ArrayList<LockerDtoUpdate> dtos);

	LockerDtoUpdate convert(Locker locker);
	
	List<LockerDtoUpdate> convert(List<Locker> lockers);
}
