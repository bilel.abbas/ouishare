package com.fr.adaming.converter;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.fr.adaming.dto.AdminDtoCreate;
import com.fr.adaming.dto.AdminDtoLogin;
import com.fr.adaming.dto.AdminDtoUpdate;
import com.fr.adaming.entity.Admin;


/**
 * @author bilel
 *
 */
@Mapper
public interface AdminDtoConverter {

	AdminDtoConverter MAPPER = Mappers.getMapper(AdminDtoConverter.class);

	Admin convertCreate(AdminDtoCreate dto);
	Admin convertUpdate(AdminDtoUpdate dto);
	Admin convertLogin(AdminDtoLogin dto);
	
	List<Admin> convert(ArrayList<AdminDtoUpdate> dtos);

	AdminDtoCreate convertCreate(Admin entity);
	AdminDtoUpdate convertUpdate(Admin entity);
	AdminDtoLogin convertLogin(Admin entity);
	
	List<AdminDtoUpdate> convert(List<Admin> entities);	
	
	
	
}
