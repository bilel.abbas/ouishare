package com.fr.adaming.converter;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.fr.adaming.dto.DeviceDtoWithId;
import com.fr.adaming.entity.Device;


/**
 * @author Julie Noterman
 *
 */
@Mapper
public interface DeviceDtoWithIdConverter {
	
	DeviceDtoWithIdConverter MAPPER = Mappers.getMapper(DeviceDtoWithIdConverter.class);

	Device convert(DeviceDtoWithId dto);
	
	List<Device> convert(ArrayList<DeviceDtoWithId> dtos);

	DeviceDtoWithId convert(Device entity);
	
	List<DeviceDtoWithId> convert(List<Device> entities);

}
