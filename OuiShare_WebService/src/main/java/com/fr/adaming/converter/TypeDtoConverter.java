package com.fr.adaming.converter;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.fr.adaming.dto.TypeDtoCreate;
import com.fr.adaming.dto.TypeDtoUpdate;
import com.fr.adaming.entity.Type;

/**
 * @author bilel
 *
 */
@Mapper
public interface TypeDtoConverter {

	TypeDtoConverter MAPPER = Mappers.getMapper(TypeDtoConverter.class);

	Type convertCreate(TypeDtoCreate dto);
	Type convertUpdate(TypeDtoUpdate dto);

	
	List<Type> convert(ArrayList<TypeDtoUpdate> dtos);

	TypeDtoCreate convertCreate(Type entity);
	TypeDtoUpdate convertUpdate(Type entity);

	
	List<TypeDtoUpdate> convert(List<Type> entities);	
		
	
}
