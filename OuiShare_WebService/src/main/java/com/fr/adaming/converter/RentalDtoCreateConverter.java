package com.fr.adaming.converter;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.fr.adaming.dto.RentalDtoCreate;
import com.fr.adaming.entity.Rental;

/**
 * @author Nicolas RUFFIER
 *
 */
@Mapper
public interface RentalDtoCreateConverter {

	RentalDtoCreateConverter MAPPER = Mappers.getMapper(RentalDtoCreateConverter.class);

	@Mapping(target = "device.id", source = "dto.idDevice")
	@Mapping(target = "customer.id", source = "dto.idCustomer")
	Rental convert(RentalDtoCreate dto);

	List<Rental> convert1(List<RentalDtoCreate> dtos);

	@Mapping(target = "idDevice", source = "rental.device.id")
	@Mapping(target = "idCustomer", source = "rental.customer.id")
	RentalDtoCreate convert(Rental rental);

	List<RentalDtoCreate> convert2(List<Rental> rentals);

}
