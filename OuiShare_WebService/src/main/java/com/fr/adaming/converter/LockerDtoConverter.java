package com.fr.adaming.converter;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.fr.adaming.dto.LockerDto;
import com.fr.adaming.entity.Locker;

@Mapper
public interface LockerDtoConverter {

	LockerDtoConverter MAPPER = Mappers.getMapper(LockerDtoConverter.class);

	Locker convert(LockerDto dto);
	
	List<Locker> convert(ArrayList<LockerDto> dtos);

	LockerDto convert(Locker locker);
	
	List<LockerDto> convert(List<Locker> lockers);
}
