package com.fr.adaming.converter;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.fr.adaming.dto.RentalDto;
import com.fr.adaming.entity.Rental;

/**
 * @author Nicolas RUFFIER
 *
 */
@Mapper
public interface RentalDtoConverter {

	RentalDtoConverter MAPPER = Mappers.getMapper(RentalDtoConverter.class);

	@Mapping(target = "device", source = "dto.device")
	@Mapping(target = "customer", source = "dto.customer")
	@Mapping(target = "review", source = "dto.review")
	Rental convert(RentalDto dto);

	List<Rental> convert1(List<RentalDto> dtos);

	@Mapping(target = "device", source = "rental.device")
	@Mapping(target = "customer", source = "rental.customer")
	@Mapping(target = "review", source = "rental.review")
	RentalDto convert(Rental rental);

	List<RentalDto> convert2(List<Rental> rental);

}
