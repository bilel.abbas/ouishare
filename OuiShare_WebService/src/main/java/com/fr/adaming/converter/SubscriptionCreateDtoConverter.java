package com.fr.adaming.converter;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.fr.adaming.dto.SubscriptionCreateDto;
import com.fr.adaming.entity.Subscription;

/**
 * @author Thibaud
 *
 */
@Mapper(uses = {DeviceDtoWithIdConverter.class, CustomerUpdateDtoConverter.class})
public interface SubscriptionCreateDtoConverter {

	SubscriptionCreateDtoConverter MAPPER = Mappers.getMapper(SubscriptionCreateDtoConverter.class);

	Subscription convert(SubscriptionCreateDto dto);

	List<Subscription> convert(ArrayList<SubscriptionCreateDto> dtos);

	SubscriptionCreateDto convert(Subscription subscription);

	List<SubscriptionCreateDto> convert(List<Subscription> subscription);
}
