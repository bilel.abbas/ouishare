package com.fr.adaming.converter;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.fr.adaming.dto.TypeDtoUpdate;
import com.fr.adaming.entity.Type;

/**
 * @author Guillaume V.
 *
 */
@Mapper
public interface TypeUpdateDtoConverter {

	TypeUpdateDtoConverter MAPPER = Mappers.getMapper(TypeUpdateDtoConverter.class);

	Type convert(TypeDtoUpdate dto);

	List<Type> convert(ArrayList<TypeDtoUpdate> dtos);

	TypeDtoUpdate convert(Type type);

	List<TypeDtoUpdate> convert(List<Type> type);
}
