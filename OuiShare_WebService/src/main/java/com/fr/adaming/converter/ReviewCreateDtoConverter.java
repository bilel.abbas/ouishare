package com.fr.adaming.converter;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


import com.fr.adaming.dto.ReviewCreateDto;
import com.fr.adaming.entity.Review;


/**
 * @author Dylan
 *
 */
@Mapper
public interface ReviewCreateDtoConverter {

	ReviewCreateDtoConverter MAPPER = Mappers.getMapper(ReviewCreateDtoConverter.class);

	List<Review> convert(ArrayList<ReviewCreateDto> dtos);
	
	Review convert(ReviewCreateDto dto);
	
	

	ReviewCreateDto convert(Review entity);
	
	List<ReviewCreateDto> convert(List<Review> entities);
}
