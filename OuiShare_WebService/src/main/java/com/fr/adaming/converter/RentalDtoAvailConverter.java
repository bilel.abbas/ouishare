package com.fr.adaming.converter;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.fr.adaming.dto.RentalDtoAvail;
import com.fr.adaming.entity.Rental;

/**
 * @author Nicolas RUFFIER
 *
 */
@Mapper
public interface RentalDtoAvailConverter {

	RentalDtoAvailConverter MAPPER = Mappers.getMapper(RentalDtoAvailConverter.class);
	
	RentalDtoAvail convert(Rental rental);
	
	List<RentalDtoAvail> convert2(List<Rental> rentals);
}
