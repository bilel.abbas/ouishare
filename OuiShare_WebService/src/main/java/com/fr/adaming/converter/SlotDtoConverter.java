package com.fr.adaming.converter;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.fr.adaming.dto.SlotDto;
import com.fr.adaming.dto.SlotDtoWithoutId;
import com.fr.adaming.entity.Slot;

/**
 * @author Aurélien
 *
 */
@Mapper
public interface SlotDtoConverter {
	
	SlotDtoConverter MAPPER = Mappers.getMapper(SlotDtoConverter.class);

	Slot convert(SlotDto dto);
	
	Slot convert(SlotDtoWithoutId dto);
	
	List<Slot> convert(ArrayList<SlotDto> dtos);
	
	List<Slot> convertt(ArrayList<SlotDtoWithoutId> dtos);

	SlotDto convert(Slot entity);
	
	SlotDtoWithoutId convertt(Slot entity);
	
	List<SlotDtoWithoutId> convert(List<Slot> entities);
	List<SlotDto> convertt(List<Slot> entities);
}
