package com.fr.adaming.converter;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.fr.adaming.dto.SubscriptionUpdateDto;
import com.fr.adaming.entity.Subscription;

/**
 * @author Guillaume V.
 *
 */
@Mapper
public interface SubscriptionUpdateDtoConverter {

	SubscriptionUpdateDtoConverter MAPPER = Mappers.getMapper(SubscriptionUpdateDtoConverter.class);

	Subscription convert(SubscriptionUpdateDto dto);
	
	List<Subscription> convert(ArrayList<SubscriptionUpdateDto> dtos);

	SubscriptionUpdateDto convert(Subscription subscription);
	
	List<SubscriptionUpdateDto> convert(List<Subscription> subscription);

}
