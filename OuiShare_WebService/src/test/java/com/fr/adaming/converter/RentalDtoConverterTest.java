package com.fr.adaming.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.fr.adaming.OuiShareWebServiceApplication;
import com.fr.adaming.dto.CustomerUpdateDto;
import com.fr.adaming.dto.DeviceDtoWithId;
import com.fr.adaming.dto.RentalDto;
import com.fr.adaming.dto.ReviewUpdateDto;
import com.fr.adaming.entity.Customer;
import com.fr.adaming.entity.Device;
import com.fr.adaming.entity.Rental;
import com.fr.adaming.entity.Review;

/**
 * @author Nicolas RUFFIER
 *
 */
@SpringBootTest(classes = OuiShareWebServiceApplication.class)
public class RentalDtoConverterTest {

	@Test
	public void convertDtoToEntity_shouldSatisfyAssertion() {
		RentalDto dto = new RentalDto();

		DeviceDtoWithId device = new DeviceDtoWithId();
		device.setId(1L);
		CustomerUpdateDto customer = new CustomerUpdateDto();
		customer.setId(1L);
		ReviewUpdateDto review = new ReviewUpdateDto();
		review.setId(1L);

		dto.setId(1L);
		dto.setStartDate(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		dto.setEndDateTheoritical(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		dto.setEndDateReal(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		dto.setSubscription(false);
		dto.setDeleted(false);
		dto.setDevice(device);
		dto.setCustomer(customer);
		dto.setReview(review);

		Rental rental = RentalDtoConverter.MAPPER.convert(dto);

		assertNotNull(rental);
		assertThat(rental).hasFieldOrPropertyWithValue("id", 1L);
		assertThat(rental).hasFieldOrPropertyWithValue("startDate", LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertThat(rental).hasFieldOrPropertyWithValue("endDateTheoritical",
				LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertThat(rental).hasFieldOrPropertyWithValue("endDateReal", LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertEquals(1L, rental.getDevice().getId());
		assertEquals(1L, rental.getCustomer().getId());
		assertEquals(1L, rental.getReview().getId());
		assertFalse(rental.isDeleted());
		assertFalse(rental.isSubscription());
	}

	@Test
	public void convertDtosToEntities_shouldSatisfyAssertion() {
		DeviceDtoWithId device = new DeviceDtoWithId();
		device.setId(1L);
		CustomerUpdateDto customer = new CustomerUpdateDto();
		customer.setId(1L);
		ReviewUpdateDto review = new ReviewUpdateDto();
		review.setId(1L);

		RentalDto dto = new RentalDto();
		dto.setId(1L);
		dto.setStartDate(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		dto.setEndDateTheoritical(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		dto.setEndDateReal(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		dto.setSubscription(false);
		dto.setDeleted(false);
		dto.setDevice(device);
		dto.setCustomer(customer);
		dto.setReview(review);

		DeviceDtoWithId device2 = new DeviceDtoWithId();
		device2.setId(2L);
		CustomerUpdateDto customer2 = new CustomerUpdateDto();
		customer2.setId(2L);
		ReviewUpdateDto review2 = new ReviewUpdateDto();
		review2.setId(2L);

		RentalDto dto2 = new RentalDto();

		dto2.setId(2L);
		dto2.setStartDate(LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		dto2.setEndDateTheoritical(LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		dto2.setEndDateReal(LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		dto2.setSubscription(false);
		dto2.setDeleted(false);
		dto2.setDevice(device2);
		dto2.setCustomer(customer2);
		dto2.setReview(review2);

		List<RentalDto> dtos = new ArrayList<>();
		dtos.add(dto);
		dtos.add(dto2);

		List<Rental> rentals = RentalDtoConverter.MAPPER.convert1(dtos);

		assertThat(rentals).isNotNull().hasSize(2);
		assertThat(rentals.get(0)).isNotNull().hasFieldOrPropertyWithValue("id", 1L);
		assertThat(rentals.get(0)).isNotNull().hasFieldOrPropertyWithValue("startDate",
				LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertThat(rentals.get(0)).isNotNull().hasFieldOrPropertyWithValue("endDateTheoritical",
				LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertThat(rentals.get(0)).isNotNull().hasFieldOrPropertyWithValue("endDateReal",
				LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertEquals(1L, rentals.get(0).getDevice().getId());
		assertEquals(1L, rentals.get(0).getCustomer().getId());
		assertEquals(1L, rentals.get(0).getReview().getId());
		assertFalse(rentals.get(0).isDeleted());
		assertFalse(rentals.get(0).isSubscription());

		assertThat(rentals.get(1)).isNotNull().hasFieldOrPropertyWithValue("id", 2L);
		assertThat(rentals.get(1)).isNotNull().hasFieldOrPropertyWithValue("startDate",
				LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		assertThat(rentals.get(1)).isNotNull().hasFieldOrPropertyWithValue("endDateTheoritical",
				LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		assertThat(rentals.get(1)).isNotNull().hasFieldOrPropertyWithValue("endDateReal",
				LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		assertEquals(2L, rentals.get(1).getDevice().getId());
		assertEquals(2L, rentals.get(1).getCustomer().getId());
		assertEquals(2L, rentals.get(1).getReview().getId());
		assertFalse(rentals.get(1).isDeleted());
		assertFalse(rentals.get(1).isSubscription());
	}

	@Test
	public void convertEntityToDto_shouldSatisfyAssertion() {
		Device device = new Device();
		device.setId(1L);
		Customer customer = new Customer();
		customer.setId(1L);
		Review review = new Review();
		review.setId(1L);

		Rental rental = new Rental();
		rental.setId(1L);
		rental.setStartDate(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setEndDateTheoritical(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setEndDateReal(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setSubscription(false);
		rental.setDeleted(false);
		rental.setDevice(device);
		rental.setCustomer(customer);
		rental.setReview(review);

		RentalDto dto = RentalDtoConverter.MAPPER.convert(rental);

		assertNotNull(dto);
		assertThat(dto).hasFieldOrPropertyWithValue("id", 1L);
		assertThat(dto).hasFieldOrPropertyWithValue("startDate", LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertThat(dto).hasFieldOrPropertyWithValue("endDateTheoritical", LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertThat(dto).hasFieldOrPropertyWithValue("endDateReal", LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertEquals(1L, dto.getDevice().getId());
		assertEquals(1L, dto.getCustomer().getId());
		assertEquals(1L, dto.getReview().getId());
		assertFalse(dto.isDeleted());
		assertFalse(dto.isSubscription());
	}

	@Test
	public void convertEntitiesToDtos_shouldSatisfyAssertion() {
		Device device = new Device();
		device.setId(1L);
		Customer customer = new Customer();
		customer.setId(1L);
		Review review = new Review();
		review.setId(1L);

		Rental rental = new Rental();
		rental.setId(1L);
		rental.setStartDate(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setEndDateTheoritical(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setEndDateReal(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setSubscription(false);
		rental.setDeleted(false);
		rental.setDevice(device);
		rental.setCustomer(customer);
		rental.setReview(review);
		
		Device device2 = new Device();
		device2.setId(2L);
		Customer customer2 = new Customer();
		customer2.setId(2L);
		Review review2 = new Review();
		review2.setId(2L);

		Rental rental2 = new Rental();
		rental2.setId(2L);
		rental2.setStartDate(LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		rental2.setEndDateTheoritical(LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		rental2.setEndDateReal(LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		rental2.setSubscription(false);
		rental2.setDeleted(false);
		rental2.setDevice(device2);
		rental2.setCustomer(customer2);
		rental2.setReview(review2);

		List<Rental> rentals = new ArrayList<>();
		rentals.add(rental);
		rentals.add(rental2);

		List<RentalDto> dtos = RentalDtoConverter.MAPPER.convert2(rentals);

		assertThat(dtos).isNotNull().hasSize(2);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("id", 1L);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("startDate",
				LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("endDateTheoritical",
				LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("endDateReal",
				LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertEquals(1L, dtos.get(0).getDevice().getId());
		assertEquals(1L, dtos.get(0).getCustomer().getId());
		assertEquals(1L, dtos.get(0).getReview().getId());
		assertFalse(dtos.get(0).isDeleted());
		assertFalse(dtos.get(0).isSubscription());

		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("id", 2L);
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("startDate",
				LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("endDateTheoritical",
				LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("endDateReal",
				LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		assertEquals(2L, dtos.get(1).getDevice().getId());
		assertEquals(2L, dtos.get(1).getCustomer().getId());
		assertEquals(2L, dtos.get(1).getReview().getId());
		assertFalse(dtos.get(1).isDeleted());
		assertFalse(dtos.get(1).isSubscription());
	}
}
