package com.fr.adaming.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.fr.adaming.OuiShareWebServiceApplication;
import com.fr.adaming.dto.DeviceDtoWithId;
import com.fr.adaming.entity.Device;


/**
 * @author Julie Noterman
 *
 */

@SpringBootTest(classes = OuiShareWebServiceApplication.class)
public class DeviceDtoWithIdConverterTest {

	@Test
	public void convertDtoToEntity_shouldSatisfyAssertion() {
		DeviceDtoWithId dto = new DeviceDtoWithId();
		dto.setId(213L);
		dto.setBrand("dyson");
		dto.setChip(500L);
		dto.setDeposit(10D);
		dto.setName("aspi");
		dto.setPriceExtra(15D);
		dto.setPricePonctual(10D);
		dto.setPriceSub(5D);
		dto.setRentalDurationInHours(1D);
		dto.setRules("regles");
		dto.setState("super");
		dto.setStateComment("ok");
		

		Device entity = DeviceDtoWithIdConverter.MAPPER.convert(dto);

		assertNotNull(entity);
		assertThat(entity).hasFieldOrPropertyWithValue("brand", "dyson");
		assertThat(entity).hasFieldOrPropertyWithValue("chip", 500L);
		assertThat(entity).hasFieldOrPropertyWithValue("deposit", 10D);
		assertThat(entity).hasFieldOrPropertyWithValue("name", "aspi");
		assertThat(entity).hasFieldOrPropertyWithValue("priceExtra", 15D);
		assertThat(entity).hasFieldOrPropertyWithValue("pricePonctual", 10D);
		assertThat(entity).hasFieldOrPropertyWithValue("priceSub", 5D);
		assertThat(entity).hasFieldOrPropertyWithValue("rentalDurationInHours", 1D);
		assertThat(entity).hasFieldOrPropertyWithValue("rules", "regles");
		assertThat(entity).hasFieldOrPropertyWithValue("state", "super");
		assertThat(entity).hasFieldOrPropertyWithValue("stateComment", "ok");
	}

	@Test
	public void convertDtosToEntities_shouldSatisfyAssertion() {
		DeviceDtoWithId dto = new DeviceDtoWithId();
		dto.setId(213L);
		dto.setBrand("dyson");
		dto.setChip(500L);
		dto.setDeposit(10D);
		dto.setName("aspi");
		dto.setPriceExtra(15D);
		dto.setPricePonctual(10D);
		dto.setPriceSub(5D);
		dto.setRentalDurationInHours(1D);
		dto.setRules("regles");
		dto.setState("super");
		dto.setStateComment("ok");

		DeviceDtoWithId dto2 = new DeviceDtoWithId();
		dto2.setId(223L);
		dto2.setBrand("bosch");
		dto2.setChip(500L);
		dto2.setDeposit(10D);
		dto2.setName("perceuse");
		dto2.setPriceExtra(15D);
		dto2.setPricePonctual(10D);
		dto2.setPriceSub(5D);
		dto2.setRentalDurationInHours(1D);
		dto2.setRules("regles");
		dto2.setState("super");
		dto2.setStateComment("ok");

		ArrayList<DeviceDtoWithId> dtos = new ArrayList<>();
		dtos.add(dto);
		dtos.add(dto2);

		List<Device> entities = DeviceDtoWithIdConverter.MAPPER.convert(dtos);

		assertThat(entities).isNotNull().hasSize(2);
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("brand", "dyson");
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("chip", 500L);
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("deposit", 10D);
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("name", "aspi");
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("priceExtra", 15D);
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("pricePonctual", 10D);
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("priceSub", 5D);
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("rentalDurationInHours", 1D);
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("rules", "regles");
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("state", "super");
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("stateComment", "ok");

		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("brand", "bosch");
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("chip", 500L);
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("deposit", 10D);
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("name", "perceuse");
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("priceExtra", 15D);
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("pricePonctual", 10D);
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("priceSub", 5D);
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("rentalDurationInHours", 1D);
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("rules", "regles");
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("state", "super");
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("stateComment", "ok");

	}

	@Test
	public void convertEntityToDto_shouldSatisfyAssertion() {
		Device entity = new Device();
		entity.setId(425L);
		entity.setBrand("dyson");
		entity.setChip(500L);
		entity.setDeposit(10D);
		entity.setName("aspi");
		entity.setPriceExtra(15D);
		entity.setPricePonctual(10D);
		entity.setPriceSub(5D);
		entity.setRentalDurationInHours(1D);
		entity.setRules("regles");
		entity.setState("super");
		entity.setStateComment("ok");

		DeviceDtoWithId dto = DeviceDtoWithIdConverter.MAPPER.convert(entity);

		assertNotNull(dto);
		assertThat(dto).hasFieldOrPropertyWithValue("brand", "dyson");
		assertThat(dto).hasFieldOrPropertyWithValue("chip", 500L);
		assertThat(dto).hasFieldOrPropertyWithValue("deposit", 10D);
		assertThat(dto).hasFieldOrPropertyWithValue("name", "aspi");
		assertThat(dto).hasFieldOrPropertyWithValue("priceExtra", 15D);
		assertThat(dto).hasFieldOrPropertyWithValue("pricePonctual", 10D);
		assertThat(dto).hasFieldOrPropertyWithValue("priceSub", 5D);
		assertThat(dto).hasFieldOrPropertyWithValue("rentalDurationInHours", 1D);
		assertThat(dto).hasFieldOrPropertyWithValue("rules", "regles");
		assertThat(dto).hasFieldOrPropertyWithValue("state", "super");
		assertThat(dto).hasFieldOrPropertyWithValue("stateComment", "ok");
	}

	@Test
	public void convertEntitiesToDtos_shouldSatisfyAssertion() {
		Device entity1 = new Device();

		entity1.setId(475L);
		entity1.setBrand("dyson");
		entity1.setChip(500L);
		entity1.setDeposit(10D);
		entity1.setName("aspi");
		entity1.setPriceExtra(15D);
		entity1.setPricePonctual(10D);
		entity1.setPriceSub(5D);
		entity1.setRentalDurationInHours(1D);
		entity1.setRules("regles");
		entity1.setState("super");
		entity1.setStateComment("ok");

		Device entity2 = new Device();

		entity2.setId(425L);
		entity2.setBrand("bosch");
		entity2.setChip(500L);
		entity2.setDeposit(10D);
		entity2.setName("perceuse");
		entity2.setPriceExtra(15D);
		entity2.setPricePonctual(10D);
		entity2.setPriceSub(5D);
		entity2.setRentalDurationInHours(1D);
		entity2.setRules("regles");
		entity2.setState("super");
		entity2.setStateComment("ok");

		List<Device> entities = new ArrayList<>();
		entities.add(entity1);
		entities.add(entity2);

		List<DeviceDtoWithId> dtos = DeviceDtoWithIdConverter.MAPPER.convert(entities);

		assertThat(dtos).isNotNull().hasSize(2);
		
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("brand", "dyson");
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("chip", 500L);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("deposit", 10D);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("name", "aspi");
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("priceExtra", 15D);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("pricePonctual", 10D);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("priceSub", 5D);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("rentalDurationInHours", 1D);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("rules", "regles");
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("state", "super");
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("stateComment", "ok");
		
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("brand", "bosch");
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("chip", 500L);
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("deposit", 10D);
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("name", "perceuse");
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("priceExtra", 15D);
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("pricePonctual", 10D);
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("priceSub", 5D);
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("rentalDurationInHours", 1D);
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("rules", "regles");
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("state", "super");
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("stateComment", "ok");
		
	}
	
	
}
