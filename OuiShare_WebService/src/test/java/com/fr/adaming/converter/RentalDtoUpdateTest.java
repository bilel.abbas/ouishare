package com.fr.adaming.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.fr.adaming.dto.RentalDtoUpdate;
import com.fr.adaming.entity.Customer;
import com.fr.adaming.entity.Device;
import com.fr.adaming.entity.Rental;
import com.fr.adaming.entity.Review;

/**
 * @author Nicolas RUFFIER
 *
 */
public class RentalDtoUpdateTest {
	@Test
	public void convertDtoToEntity_shouldSatisfyAssertion() {
		RentalDtoUpdate dto = new RentalDtoUpdate();
		dto.setId(1L);
		dto.setStartDate(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		dto.setEndDateTheoritical(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		dto.setSubscription(false);
		dto.setIdDevice(1L);
		dto.setIdCustomer(1L);

		Rental rental = RentalDtoUpdateConverter.MAPPER.convert(dto);

		assertNotNull(rental);
		assertThat(rental).hasFieldOrPropertyWithValue("id", 1L);
		assertThat(rental).hasFieldOrPropertyWithValue("startDate", LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertThat(rental).hasFieldOrPropertyWithValue("endDateTheoritical",
				LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertEquals(1L, rental.getDevice().getId());
		assertEquals(1L, rental.getCustomer().getId());
		assertFalse(rental.isDeleted());
		assertFalse(rental.isSubscription());
	}

	@Test
	public void convertDtosToEntities_shouldSatisfyAssertion() {
		RentalDtoUpdate dto = new RentalDtoUpdate();
		dto.setId(1L);
		dto.setStartDate(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		dto.setEndDateTheoritical(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		dto.setSubscription(false);
		dto.setIdDevice(1L);
		dto.setIdCustomer(1L);

		RentalDtoUpdate dto2 = new RentalDtoUpdate();
		dto2.setId(2L);
		dto2.setStartDate(LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		dto2.setEndDateTheoritical(LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		dto2.setSubscription(false);
		dto2.setIdDevice(2L);
		dto2.setIdCustomer(2L);

		List<RentalDtoUpdate> dtos = new ArrayList<>();
		dtos.add(dto);
		dtos.add(dto2);

		List<Rental> rentals = RentalDtoUpdateConverter.MAPPER.convert1(dtos);

		assertThat(rentals).isNotNull().hasSize(2);
		assertThat(rentals.get(0)).isNotNull().hasFieldOrPropertyWithValue("id", 1L);
		assertThat(rentals.get(0)).isNotNull().hasFieldOrPropertyWithValue("startDate",
				LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertThat(rentals.get(0)).isNotNull().hasFieldOrPropertyWithValue("endDateTheoritical",
				LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertEquals(1L, rentals.get(0).getDevice().getId());
		assertEquals(1L, rentals.get(0).getCustomer().getId());
		assertFalse(rentals.get(0).isDeleted());
		assertFalse(rentals.get(0).isSubscription());

		assertThat(rentals.get(1)).isNotNull().hasFieldOrPropertyWithValue("id", 2L);
		assertThat(rentals.get(1)).isNotNull().hasFieldOrPropertyWithValue("startDate",
				LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		assertThat(rentals.get(1)).isNotNull().hasFieldOrPropertyWithValue("endDateTheoritical",
				LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		assertEquals(2L, rentals.get(1).getDevice().getId());
		assertEquals(2L, rentals.get(1).getCustomer().getId());
		assertFalse(rentals.get(1).isDeleted());
		assertFalse(rentals.get(1).isSubscription());
	}

	@Test
	public void convertEntityToDto_shouldSatisfyAssertion() {
		Device device = new Device();
		device.setId(1L);
		Customer customer = new Customer();
		customer.setId(1L);
		Review review = new Review();
		review.setId(1L);

		Rental rental = new Rental();
		rental.setId(1L);
		rental.setStartDate(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setEndDateTheoritical(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setEndDateReal(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setSubscription(false);
		rental.setDeleted(false);
		rental.setDevice(device);
		rental.setCustomer(customer);
		rental.setReview(review);

		RentalDtoUpdate dto = RentalDtoUpdateConverter.MAPPER.convert(rental);

		assertNotNull(dto);
		assertThat(dto).hasFieldOrPropertyWithValue("id", 1L);
		assertThat(dto).hasFieldOrPropertyWithValue("startDate", LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertThat(dto).hasFieldOrPropertyWithValue("endDateTheoritical", LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertEquals(1L, dto.getIdDevice());
		assertEquals(1L, dto.getIdCustomer());
		assertFalse(dto.isSubscription());
	}

	@Test
	public void convertEntitiesToDtos_shouldSatisfyAssertion() {
		Device device = new Device();
		device.setId(1L);
		Customer customer = new Customer();
		customer.setId(1L);
		Review review = new Review();
		review.setId(1L);

		Rental rental = new Rental();
		rental.setId(1L);
		rental.setStartDate(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setEndDateTheoritical(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setEndDateReal(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setSubscription(false);
		rental.setDeleted(false);
		rental.setDevice(device);
		rental.setCustomer(customer);
		rental.setReview(review);

		Device device2 = new Device();
		device2.setId(2L);
		Customer customer2 = new Customer();
		customer2.setId(2L);
		Review review2 = new Review();
		review2.setId(2L);

		Rental rental2 = new Rental();
		rental2.setId(2L);
		rental2.setStartDate(LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		rental2.setEndDateTheoritical(LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		rental2.setEndDateReal(LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		rental2.setSubscription(false);
		rental2.setDeleted(false);
		rental2.setDevice(device2);
		rental2.setCustomer(customer2);
		rental2.setReview(review2);

		List<Rental> rentals = new ArrayList<>();
		rentals.add(rental);
		rentals.add(rental2);

		List<RentalDtoUpdate> dtos = RentalDtoUpdateConverter.MAPPER.convert2(rentals);

		assertThat(dtos).isNotNull().hasSize(2);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("id", 1L);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("startDate",
				LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("endDateTheoritical",
				LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertEquals(1L, dtos.get(0).getIdDevice());
		assertEquals(1L, dtos.get(0).getIdCustomer());
		assertFalse(dtos.get(0).isSubscription());

		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("id", 2L);
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("startDate",
				LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("endDateTheoritical",
				LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		assertEquals(2L, dtos.get(1).getIdDevice());
		assertEquals(2L, dtos.get(1).getIdCustomer());
		assertFalse(dtos.get(1).isSubscription());
	}
}
