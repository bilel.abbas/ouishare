package com.fr.adaming.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.fr.adaming.OuiShareWebServiceApplication;
import com.fr.adaming.dto.TypeDtoCreate;
import com.fr.adaming.dto.TypeDtoUpdate;
import com.fr.adaming.entity.Type;

/**
 * @author bilel
 *
 */
@SpringBootTest(classes = OuiShareWebServiceApplication.class)
public class TypeDtoConverterTest {

	@Test
	public void convertTypeDtoCreateToType_shouldSatisfyAssertion() {

		TypeDtoCreate dto = new TypeDtoCreate();

		dto.setName("Aspirateur");
		dto.setStock(45);

		Type type = TypeDtoConverter.MAPPER.convertCreate(dto);

		assertNotNull(type);
		assertEquals("Aspirateur", type.getName());
		assertEquals(45, type.getStock());
	}

	@Test
	public void convertTypeToTypeDtoCreate_shouldSatisfyAssertion() {

		Type type = new Type();

		type.setName("Aspirateur");
		type.setStock(45);


		TypeDtoCreate dto = TypeDtoConverter.MAPPER.convertCreate(type);

		assertNotNull(dto);
		assertEquals("Aspirateur", type.getName());
		assertEquals(45, type.getStock());
	}

	@Test
	public void convertTypeDtoUpdateToType_shouldSatisfyAssertion() {

		TypeDtoUpdate dto = new TypeDtoUpdate();

		dto.setId(1L);
		dto.setName("Aspirateur");
		dto.setStock(45);
		dto.setDeleted(true);

		Type type = TypeDtoConverter.MAPPER.convertUpdate(dto);

		assertNotNull(type);
		assertEquals(1, type.getId().intValue());
		assertEquals("Aspirateur", type.getName());
		assertEquals(45, type.getStock());
		assertTrue(type.isDeleted());
	}

	@Test
	public void convertTypeToTypeDtoUpdate_shouldSatisfyAssertion() {

		Type type = new Type();

		type.setId(1L);
		type.setName("Aspirateur");
		type.setStock(45);
		type.setDeleted(true);

		TypeDtoUpdate dto = TypeDtoConverter.MAPPER.convertUpdate(type);

		assertNotNull(dto);
		assertEquals(1, dto.getId().intValue());
		assertEquals("Aspirateur", dto.getName());
		assertEquals(45, dto.getStock());
		assertTrue(dto.isDeleted());
	}


	@Test
	public void convertListTypeDtoUpdateToListType_shouldSatisfyAssertion() {

		TypeDtoUpdate dto = new TypeDtoUpdate();

		dto.setId(1L);
		dto.setName("Aspirateur");
		dto.setStock(45);
		dto.setDeleted(true);


		TypeDtoUpdate dto2 = new TypeDtoUpdate();

		dto2.setId(2L);
		dto2.setName("Trotinette");
		dto2.setStock(18);
		dto2.setDeleted(true);


		ArrayList<TypeDtoUpdate> dtos = new ArrayList<>();
		dtos.add(dto);
		dtos.add(dto2);

		List<Type> types = TypeDtoConverter.MAPPER.convert(dtos);

		assertNotNull(types);
		assertEquals(1, types.get(0).getId().intValue());
		assertEquals("Aspirateur", types.get(0).getName());
		assertEquals(45, types.get(0).getStock());
		assertTrue(types.get(0).isDeleted());

		assertEquals(2, types.get(1).getId().intValue());
		assertEquals("Trotinette", types.get(1).getName());
		assertEquals(18, types.get(1).getStock());
		assertTrue(types.get(1).isDeleted());;

	}

	@Test
	public void convertListTypeToListTypeDtoUpdate_shouldSatisfyAssertion() {

		Type type = new Type();

		type.setId(1L);
		type.setName("Aspirateur");
		type.setStock(45);
		type.setDeleted(true);

		Type type2 = new Type();

		type2.setId(2L);
		type2.setName("Trotinette");
		type2.setStock(18);
		type2.setDeleted(false);

		ArrayList<Type> types = new ArrayList<>();
		types.add(type);
		types.add(type2);

		List<TypeDtoUpdate> dtos = TypeDtoConverter.MAPPER.convert(types);

		assertNotNull(dtos);
		assertEquals(1, dtos.get(0).getId().intValue());
		assertEquals("Aspirateur", dtos.get(0).getName());
		assertEquals(45, dtos.get(0).getStock());
		assertTrue(dtos.get(0).isDeleted());

		assertEquals(2, dtos.get(1).getId().intValue());
		assertEquals("Trotinette", dtos.get(1).getName());
		assertEquals(18, dtos.get(1).getStock());
		assertFalse(dtos.get(1).isDeleted());

	}

}

