package com.fr.adaming.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.fr.adaming.OuiShareWebServiceApplication;
import com.fr.adaming.dto.ReviewUpdateDto;
import com.fr.adaming.entity.Review;

@SpringBootTest(classes = OuiShareWebServiceApplication.class)
public class ReviewUpdateDtoconverterTest {

	
	@Test
	public void convertDtoToEntity_shouldSatisfyAssertion() {
		ReviewUpdateDto dto = new ReviewUpdateDto();
		dto.setId(1L);
		dto.setRating(2);
		dto.setComment("aaaaa");
		dto.setDeleted(true);
		
		Review entity = ReviewUpdateDtoConverter.MAPPER.convert(dto);
		
		assertNotNull(entity);
		assertThat(entity).hasFieldOrPropertyWithValue("id", 1L);
		assertThat(entity).hasFieldOrPropertyWithValue("rating", 2);
		assertThat(entity).hasFieldOrPropertyWithValue("comment", "aaaaa");
		assertThat(entity).hasFieldOrPropertyWithValue("deleted", true);
		
		
	}
	
	
	@Test
	public void convertEntityToDto_shouldSatisfyAssertion() {
	
		Review entity = new Review();
		entity.setId(2L);
		entity.setRating(2);
		entity.setComment("bbbbb");
		entity.setDeleted(false);
		
		ReviewUpdateDto dto = ReviewUpdateDtoConverter.MAPPER.convert(entity);
		
		assertNotNull(dto);
		assertThat(dto).hasFieldOrPropertyWithValue("id", 2L);
		assertThat(dto).hasFieldOrPropertyWithValue("rating", 2);
		assertThat(dto).hasFieldOrPropertyWithValue("comment", "bbbbb");
		assertThat(dto).hasFieldOrPropertyWithValue("deleted", false);
		
		
	}
	
	@Test
	public void convertDtosToEntities_shouldSatisfyAssertion() {
		
		ReviewUpdateDto dto = new ReviewUpdateDto();
		dto.setId(3L);
		dto.setRating(2);
		dto.setComment("aaaaa");
		dto.setDeleted(true);
		
		
		ReviewUpdateDto dto2 = new ReviewUpdateDto();
		dto2.setId(4L);
		dto2.setRating(5);
		dto2.setComment("ccccc");
		dto2.setDeleted(false);
		
		
		ArrayList<ReviewUpdateDto> dtos = new ArrayList<>();
		dtos.add(dto);
		dtos.add(dto2);
		
		
		
		List<Review> entities = ReviewUpdateDtoConverter.MAPPER.convert(dtos);
		
		assertThat(entities).isNotNull().hasSize(2);
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("id", 3L);
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("rating", 2);
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("comment", "aaaaa");
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("deleted", true);
		
		
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("id", 4L);
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("rating", 5);
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("comment", "ccccc");
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("deleted", false);

		
	}
	
	@Test
	public void convertEntitiesToDtos_shouldSatisfyAssertion(){
		
		Review entity = new Review();
		entity.setId(5L);
		entity.setRating(2);
		entity.setComment("bbbbb");
		entity.setDeleted(false);
		
		Review entity2 = new Review();
		entity2.setId(6L);
		entity2.setRating(4);
		entity2.setComment("ddddd");
		entity2.setDeleted(true);
		
		List <Review> entities = new ArrayList<>();
		entities.add(entity);
		entities.add(entity2);
		
		List<ReviewUpdateDto> dtos = ReviewUpdateDtoConverter.MAPPER.convert(entities);
		
		assertThat(dtos).isNotNull().hasSize(2);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("id", 5L);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("rating", 2);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("comment", "bbbbb");
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("deleted", false);
		
		
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("id", 6L);
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("rating", 4);
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("comment", "ddddd");
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("deleted", true);
		
		
		
	}
}
