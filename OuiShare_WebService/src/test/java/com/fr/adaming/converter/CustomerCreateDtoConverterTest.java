package com.fr.adaming.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.fr.adaming.OuiShareWebServiceApplication;

import com.fr.adaming.dto.CustomerCreateDto;



import com.fr.adaming.entity.Customer;

@SpringBootTest(classes = OuiShareWebServiceApplication.class)
public class CustomerCreateDtoConverterTest {

	
	@Test
	public void convertDtoToEntity_shouldSatisfyAssertion() {
		CustomerCreateDto dto = new CustomerCreateDto();
		
		dto.setEmail("a@a.fr");
		dto.setLastName("Murray");
		dto.setFirstName("billy");
		dto.setAdress("adresse");
		dto.setZipCode(1234);
		dto.setCity("city");
		dto.setPwd("azerty");
		dto.setPhone("012345678");
		
		Customer entity = CustomerCreateDtoConverter.MAPPER.convert(dto);
		
		assertNotNull(entity);
		assertThat(entity).hasFieldOrPropertyWithValue("email", "a@a.fr");
		assertThat(entity).hasFieldOrPropertyWithValue("lastName", "Murray");
		assertThat(entity).hasFieldOrPropertyWithValue("firstName", "billy");
		assertThat(entity).hasFieldOrPropertyWithValue("adress", "adresse");
		assertThat(entity).hasFieldOrPropertyWithValue("zipCode", 1234);
		assertThat(entity).hasFieldOrPropertyWithValue("city", "city");
		assertThat(entity).hasFieldOrPropertyWithValue("pwd", "azerty");
		assertThat(entity).hasFieldOrPropertyWithValue("phone", "012345678");
		
		
	}
	
	
	@Test
	public void convertEntityToDto_shouldSatisfyAssertion() {
	
		Customer entity = new Customer();
		
		entity.setEmail("a@a.fr");
		entity.setLastName("Murray");
		entity.setFirstName("billy");
		entity.setAdress("adresse");
		entity.setZipCode(1234);
		entity.setCity("city");
		entity.setPwd("azerty");
		entity.setPhone("012345678");
		
		CustomerCreateDto dto = CustomerCreateDtoConverter.MAPPER.convert(entity);
		
		assertNotNull(dto);
		assertThat(dto).hasFieldOrPropertyWithValue("email", "a@a.fr");
		assertThat(dto).hasFieldOrPropertyWithValue("lastName", "Murray");
		assertThat(dto).hasFieldOrPropertyWithValue("firstName", "billy");
		assertThat(dto).hasFieldOrPropertyWithValue("adress", "adresse");
		assertThat(dto).hasFieldOrPropertyWithValue("zipCode", 1234);
		assertThat(dto).hasFieldOrPropertyWithValue("city", "city");
		assertThat(dto).hasFieldOrPropertyWithValue("pwd", "azerty");
		assertThat(dto).hasFieldOrPropertyWithValue("phone", "012345678");
		
		
	}
	
	@Test
	public void convertDtosToEntities_shouldSatisfyAssertion() {
		
		CustomerCreateDto dto = new CustomerCreateDto();
		
		dto.setEmail("a@a.fr");
		dto.setLastName("Murray");
		dto.setFirstName("billy");
		dto.setAdress("adresse");
		dto.setZipCode(1234);
		dto.setCity("city");
		dto.setPwd("azerty");
		dto.setPhone("012345678");
		
		
		CustomerCreateDto dto2 = new CustomerCreateDto();
		
		dto2.setEmail("b@a.fr");
		dto2.setLastName("Burray");
		dto2.setFirstName("Milly");
		dto2.setAdress("adresse2");
		dto2.setZipCode(5678);
		dto2.setCity("city2");
		dto2.setPwd("azerty2");
		dto2.setPhone("876543210");
		
		
		ArrayList<CustomerCreateDto> dtos = new ArrayList<>();
		dtos.add(dto);
		dtos.add(dto2);
		
		
		
		List<Customer> entities = CustomerCreateDtoConverter.MAPPER.convert(dtos);
		
		assertThat(entities).isNotNull().hasSize(2);
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("email", "a@a.fr");
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("lastName", "Murray");
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("firstName", "billy");
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("adress", "adresse");
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("zipCode", 1234);
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("city", "city");
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("pwd", "azerty");
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("phone", "012345678");
		
		
		
		
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("email", "b@a.fr");
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("lastName", "Burray");
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("firstName", "Milly");
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("adress", "adresse2");
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("zipCode", 5678);
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("city", "city2");
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("pwd", "azerty2");
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("phone", "876543210");

	
	}
	
	@Test
	public void convertEntitiesToDtos_shouldSatisfyAssertion(){
		
		Customer entity = new Customer();
		
		entity.setEmail("a@a.fr");
		entity.setLastName("Murray");
		entity.setFirstName("billy");
		entity.setAdress("adresse");
		entity.setZipCode(1234);
		entity.setCity("city");
		entity.setPwd("azerty");
		entity.setPhone("012345678");
		
		Customer entity2 = new Customer();
		
		entity2.setEmail("b@a.fr");
		entity2.setLastName("Burray");
		entity2.setFirstName("Milly");
		entity2.setAdress("adresse2");
		entity2.setZipCode(5678);
		entity2.setCity("city2");
		entity2.setPwd("azerty2");
		entity2.setPhone("876543210");
		
		List <Customer> entities = new ArrayList<>();
		entities.add(entity);
		entities.add(entity2);
		
		List<CustomerCreateDto> dtos = CustomerCreateDtoConverter.MAPPER.convert(entities);
		
		assertThat(dtos).isNotNull().hasSize(2);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("email", "a@a.fr");
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("lastName", "Murray");
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("firstName", "billy");
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("adress", "adresse");
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("zipCode", 1234);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("city", "city");
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("pwd", "azerty");
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("phone", "012345678");
		
		
		
		
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("email", "b@a.fr");
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("lastName", "Burray");
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("firstName", "Milly");
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("adress", "adresse2");
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("zipCode", 5678);
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("city", "city2");
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("pwd", "azerty2");
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("phone", "876543210");
		
		
		
	}
}
