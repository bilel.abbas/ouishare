package com.fr.adaming.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.fr.adaming.OuiShareWebServiceApplication;
import com.fr.adaming.dto.RentalDtoAvail;
import com.fr.adaming.entity.Customer;
import com.fr.adaming.entity.Device;
import com.fr.adaming.entity.Rental;
import com.fr.adaming.entity.Review;

/**
 * @author Nicolas RUFFIER
 *
 */
@SpringBootTest(classes = OuiShareWebServiceApplication.class)
public class RentalDtoAvailConverterTest {

	@Test
	public void convertEntityToDto_shouldSatisfyAssertion() {
		Device device = new Device();
		device.setId(1L);
		Customer customer = new Customer();
		customer.setId(1L);
		Review review = new Review();
		review.setId(1L);

		Rental rental = new Rental();
		rental.setId(1L);
		rental.setStartDate(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setEndDateTheoritical(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setEndDateReal(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setSubscription(false);
		rental.setDeleted(false);
		rental.setDevice(device);
		rental.setCustomer(customer);
		rental.setReview(review);

		RentalDtoAvail dto = RentalDtoAvailConverter.MAPPER.convert(rental);

		assertNotNull(dto);
		assertThat(dto).hasFieldOrPropertyWithValue("startDate", LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertThat(dto).hasFieldOrPropertyWithValue("endDateTheoritical", LocalDateTime.of(1991, 01, 22, 18, 55, 30));
	}

	@Test
	public void convertEntitiesToDtos_shouldSatisfyAssertion() {
		Device device = new Device();
		device.setId(1L);
		Customer customer = new Customer();
		customer.setId(1L);
		Review review = new Review();
		review.setId(1L);

		Rental rental = new Rental();
		rental.setId(1L);
		rental.setStartDate(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setEndDateTheoritical(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setEndDateReal(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setSubscription(false);
		rental.setDeleted(false);
		rental.setDevice(device);
		rental.setCustomer(customer);
		rental.setReview(review);
		
		Device device2 = new Device();
		device2.setId(2L);
		Customer customer2 = new Customer();
		customer2.setId(2L);
		Review review2 = new Review();
		review2.setId(2L);

		Rental rental2 = new Rental();
		rental2.setId(2L);
		rental2.setStartDate(LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		rental2.setEndDateTheoritical(LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		rental2.setEndDateReal(LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		rental2.setSubscription(false);
		rental2.setDeleted(false);
		rental2.setDevice(device2);
		rental2.setCustomer(customer2);
		rental2.setReview(review2);

		List<Rental> rentals = new ArrayList<>();
		rentals.add(rental);
		rentals.add(rental2);

		List<RentalDtoAvail> dtos = RentalDtoAvailConverter.MAPPER.convert2(rentals);

		assertThat(dtos).isNotNull().hasSize(2);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("startDate",
				LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("endDateTheoritical",
				LocalDateTime.of(1991, 01, 22, 18, 55, 30));

		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("startDate",
				LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("endDateTheoritical",
				LocalDateTime.of(1992, 01, 22, 18, 55, 30));
	}
}
