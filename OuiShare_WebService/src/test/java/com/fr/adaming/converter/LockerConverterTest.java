package com.fr.adaming.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.fr.adaming.OuiShareWebServiceApplication;
import com.fr.adaming.dto.LockerDto;
import com.fr.adaming.entity.Locker;

@SpringBootTest(classes = OuiShareWebServiceApplication.class)
public class LockerConverterTest {

	@Test
	public void convertDtoToEntity_shouldSatisfyAssertion() {
		LockerDto dto = new LockerDto();

		dto.setName("name4Test");
		dto.setAdress("address");
		dto.setCity("paris");
		dto.setSlot(500L);
		dto.setZipCode(987654321L);
		dto.setDeleted(false);
		
		Locker entity = LockerDtoConverter.MAPPER.convert(dto);

		assertNotNull(entity);
		assertThat(entity).hasFieldOrPropertyWithValue("name", "name4Test");
		assertThat(entity).hasFieldOrPropertyWithValue("adress", "address");
		assertThat(entity).hasFieldOrPropertyWithValue("city", "paris");
	}

	@Test
	public void convertDtosToEntities_shouldSatisfyAssertion() {
		LockerDto dto = new LockerDto();

		dto.setName("name4Test");
		dto.setAdress("address");
		dto.setCity("paris");
		dto.setSlot(500L);
		dto.setZipCode(987654321L);
		dto.setDeleted(false);
		
		LockerDto dto2 = new LockerDto();

		dto2.setName("name4Test2");
		dto2.setAdress("address2");
		dto2.setCity("paris");
		dto2.setSlot(500L);
		dto2.setZipCode(123456789L);
		dto2.setDeleted(false);
		
		ArrayList<LockerDto> dtos = new ArrayList<>();
		dtos.add(dto);
		dtos.add(dto2);

		List<Locker> entities = LockerDtoConverter.MAPPER.convert(dtos);

		assertThat(entities).isNotNull().hasSize(2);
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("name", "name4Test");
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("adress", "address");

		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("name", "name4Test2");
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("adress", "address2");
	}

	@Test
	public void convertEntityToDto_shouldSatisfyAssertion() {
		Locker entity = new Locker();

		entity.setName("name4Test2");
		entity.setAdress("address2");
		entity.setCity("paris");
		entity.setSlot(500L);
		entity.setZipCode(123456789L);
		entity.setDeleted(false);

		LockerDto dto = LockerDtoConverter.MAPPER.convert(entity);

		assertNotNull(dto);
		assertThat(dto).hasFieldOrPropertyWithValue("name", "name4Test2");
		assertThat(dto).hasFieldOrPropertyWithValue("adress", "address2");
	}

	@Test
	public void convertEntitiesToDtos_shouldSatisfyAssertion() {
		Locker entity1 = new Locker();

		entity1.setName("name4Test2");
		entity1.setAdress("address3");
		entity1.setCity("paris");
		entity1.setSlot(500L);
		entity1.setZipCode(123456789L);
		entity1.setDeleted(false);

		Locker entity2 = new Locker();

		entity2.setName("name4Test2");
		entity2.setAdress("address4");
		entity2.setCity("paris");
		entity2.setSlot(500L);
		entity2.setZipCode(123456789L);
		entity2.setDeleted(false);

		List<Locker> entities = new ArrayList<>();
		entities.add(entity1);
		entities.add(entity2);

		List<LockerDto> dtos = LockerDtoConverter.MAPPER.convert(entities);

		assertThat(dtos).isNotNull().hasSize(2);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("name", "name4Test2");
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("adress", "address3");

		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("name", "name4Test2");
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("adress", "address4");
	}
}
