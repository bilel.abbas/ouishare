package com.fr.adaming.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.fr.adaming.dto.CustomerUpdateDto;
import com.fr.adaming.dto.DeviceDtoWithId;
import com.fr.adaming.dto.SubscriptionUpdateDto;
import com.fr.adaming.entity.Customer;
import com.fr.adaming.entity.Device;
import com.fr.adaming.entity.Subscription;

/**
 * @author Thibaud
 *
 */
public class SubscriptionUpdateDtoConverterTest {
	
	@Test
	public void convertDtoToEntity_shouldSatisfyAssertion() {
		SubscriptionUpdateDto dto = new SubscriptionUpdateDto();
		CustomerUpdateDto customer = new CustomerUpdateDto();
		customer.setCity("Tours");
		customer.setEmail("truc@machin.bidule");
		DeviceDtoWithId device = new DeviceDtoWithId();
		device.setName("schmilblick");
		
		dto.setCustomer(customer);
		dto.setDeleted(false);
		dto.setDevice(device);
		dto.setEndDate(LocalDate.of(2020, 10, 10));
		dto.setStartDate(LocalDate.of(2001, 10, 10));
		dto.setId(98L);
		dto.setRenew(true);
		
		Subscription entity = SubscriptionUpdateDtoConverter.MAPPER.convert(dto);
		
		assertNotNull(entity);
		assertEquals(dto.getCustomer().getCity(), entity.getCustomer().getCity());
		assertEquals(dto.getCustomer().getEmail(), entity.getCustomer().getEmail());
		assertThat(entity).hasFieldOrPropertyWithValue("deleted", false);
		assertEquals(dto.getDevice().getName(), entity.getDevice().getName());
		assertThat(entity).hasFieldOrPropertyWithValue("endDate", LocalDate.of(2020, 10, 10));
		assertThat(entity).hasFieldOrPropertyWithValue("startDate", LocalDate.of(2001, 10, 10));
		assertThat(entity).hasFieldOrPropertyWithValue("id", 98L);
		assertThat(entity).hasFieldOrPropertyWithValue("renew", true);
	}

	@Test
	public void convertDtosToEntities_shouldSatisfyAssertion() {
		SubscriptionUpdateDto dto = new SubscriptionUpdateDto();
		CustomerUpdateDto customer = new CustomerUpdateDto();
		customer.setCity("Tours");
		customer.setEmail("truc@machin.bidule");
		DeviceDtoWithId device = new DeviceDtoWithId();
		device.setName("schmilblick");
		
		dto.setCustomer(customer);
		dto.setDeleted(false);
		dto.setDevice(device);
		dto.setEndDate(LocalDate.of(2020, 10, 10));
		dto.setStartDate(LocalDate.of(2001, 10, 10));
		dto.setId(98L);
		dto.setRenew(true);
		
		SubscriptionUpdateDto dto2 = new SubscriptionUpdateDto();
		CustomerUpdateDto customer2 = new CustomerUpdateDto();
		customer2.setCity("Tours");
		customer2.setEmail("truc@machin2.bidule");
		DeviceDtoWithId device2 = new DeviceDtoWithId();
		device2.setName("schmilblick");
		
		dto2.setCustomer(customer2);
		dto2.setDeleted(false);
		dto2.setDevice(device2);
		dto2.setEndDate(LocalDate.of(2020, 10, 10));
		dto2.setStartDate(LocalDate.of(2001, 10, 10));
		dto2.setId(99L);
		dto2.setRenew(true);
		
		ArrayList<SubscriptionUpdateDto> dtos = new ArrayList<>();
		dtos.add(dto);
		dtos.add(dto2);
		
		List<Subscription> entities = SubscriptionUpdateDtoConverter.MAPPER.convert(dtos);
		
		assertThat(entities).isNotNull().hasSize(2);
		assertNotNull(entities.get(0));
		assertEquals(dto.getCustomer().getCity(), entities.get(0).getCustomer().getCity());
		assertEquals(dto.getCustomer().getEmail(), entities.get(0).getCustomer().getEmail());
		assertThat(entities.get(0)).hasFieldOrPropertyWithValue("deleted", false);
		assertEquals(dto.getDevice().getName(), entities.get(0).getDevice().getName());
		assertThat(entities.get(0)).hasFieldOrPropertyWithValue("endDate", LocalDate.of(2020, 10, 10));
		assertThat(entities.get(0)).hasFieldOrPropertyWithValue("startDate", LocalDate.of(2001, 10, 10));
		assertThat(entities.get(0)).hasFieldOrPropertyWithValue("id", 98L);
		assertThat(entities.get(0)).hasFieldOrPropertyWithValue("renew", true);
		
		assertNotNull(entities.get(1));
		assertEquals(dto2.getCustomer().getCity(), entities.get(1).getCustomer().getCity());
		assertEquals(dto2.getCustomer().getEmail(), entities.get(1).getCustomer().getEmail());
		assertThat(entities.get(1)).hasFieldOrPropertyWithValue("deleted", false);
		assertEquals(dto2.getDevice().getName(), entities.get(1).getDevice().getName());
		assertThat(entities.get(1)).hasFieldOrPropertyWithValue("endDate", LocalDate.of(2020, 10, 10));
		assertThat(entities.get(1)).hasFieldOrPropertyWithValue("startDate", LocalDate.of(2001, 10, 10));
		assertThat(entities.get(1)).hasFieldOrPropertyWithValue("id", 99L);
		assertThat(entities.get(1)).hasFieldOrPropertyWithValue("renew", true);
	}
	
	@Test
	public void convertEntityToDto_shouldSatisfyAssertion() {
		Subscription entity = new Subscription();
		Customer customer = new Customer();
		customer.setCity("Tours");
		customer.setEmail("truc@machin2.bidule");
		Device device = new Device();
		device.setName("schmilblick");
		
		entity.setCustomer(customer);
		entity.setDeleted(false);
		entity.setDevice(device);
		entity.setEndDate(LocalDate.of(2020, 10, 10));
		entity.setStartDate(LocalDate.of(2001, 10, 10));
		entity.setId(98L);
		entity.setRenew(true);
		
		SubscriptionUpdateDto dto = SubscriptionUpdateDtoConverter.MAPPER.convert(entity);
		
		assertNotNull(dto);
		assertEquals(dto.getCustomer().getCity(), entity.getCustomer().getCity());
		assertEquals(dto.getCustomer().getEmail(), entity.getCustomer().getEmail());
		assertThat(dto).hasFieldOrPropertyWithValue("deleted", false);
		assertEquals(dto.getDevice().getName(), entity.getDevice().getName());
		assertThat(dto).hasFieldOrPropertyWithValue("endDate", LocalDate.of(2020, 10, 10));
		assertThat(dto).hasFieldOrPropertyWithValue("startDate", LocalDate.of(2001, 10, 10));
		assertThat(dto).hasFieldOrPropertyWithValue("id", 98L);
		assertThat(dto).hasFieldOrPropertyWithValue("renew", true);
	}

	@Test
	public void convertEntitiesToDtos_shouldSatisfyAssertion() {
		Subscription entity1 = new Subscription();
		Customer customer = new Customer();
		customer.setCity("Tours");
		customer.setEmail("truc@machin2.bidule");
		Device device = new Device();
		device.setName("schmilblick");
		
		entity1.setCustomer(customer);
		entity1.setDeleted(false);
		entity1.setDevice(device);
		entity1.setEndDate(LocalDate.of(2020, 10, 10));
		entity1.setStartDate(LocalDate.of(2001, 10, 10));
		entity1.setId(98L);
		entity1.setRenew(true);
		
		Subscription entity2 = new Subscription();
		
		entity2.setCustomer(customer);
		entity2.setDeleted(false);
		entity2.setDevice(device);
		entity2.setEndDate(LocalDate.of(2020, 10, 10));
		entity2.setStartDate(LocalDate.of(2001, 10, 10));
		entity2.setId(99L);
		entity2.setRenew(true);
		
		List<Subscription> entities = new ArrayList<>();
		entities.add(entity1);
		entities.add(entity2);
		
		List<SubscriptionUpdateDto> dtos = SubscriptionUpdateDtoConverter.MAPPER.convert(entities);
		
		assertThat(dtos).isNotNull().hasSize(2);
		assertNotNull(dtos.get(0));
		assertEquals(dtos.get(0).getCustomer().getCity(), entity1.getCustomer().getCity());
		assertEquals(dtos.get(0).getCustomer().getEmail(), entity1.getCustomer().getEmail());
		assertThat(dtos.get(0)).hasFieldOrPropertyWithValue("deleted", false);
		assertEquals(dtos.get(0).getDevice().getName(), entity1.getDevice().getName());
		assertThat(dtos.get(0)).hasFieldOrPropertyWithValue("endDate", LocalDate.of(2020, 10, 10));
		assertThat(dtos.get(0)).hasFieldOrPropertyWithValue("startDate", LocalDate.of(2001, 10, 10));
		assertThat(dtos.get(0)).hasFieldOrPropertyWithValue("id", 98L);
		assertThat(dtos.get(0)).hasFieldOrPropertyWithValue("renew", true);
		
		assertNotNull(dtos.get(1));
		assertEquals(dtos.get(1).getCustomer().getCity(), entity2.getCustomer().getCity());
		assertEquals(dtos.get(1).getCustomer().getEmail(), entity2.getCustomer().getEmail());
		assertThat(dtos.get(1)).hasFieldOrPropertyWithValue("deleted", false);
		assertEquals(dtos.get(1).getDevice().getName(), entity2.getDevice().getName());
		assertThat(dtos.get(1)).hasFieldOrPropertyWithValue("endDate", LocalDate.of(2020, 10, 10));
		assertThat(dtos.get(1)).hasFieldOrPropertyWithValue("startDate", LocalDate.of(2001, 10, 10));
		assertThat(dtos.get(1)).hasFieldOrPropertyWithValue("id", 99L);
		assertThat(dtos.get(1)).hasFieldOrPropertyWithValue("renew", true);
	}

}
