package com.fr.adaming.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.fr.adaming.OuiShareWebServiceApplication;

import com.fr.adaming.dto.CustomerLoginDto;

import com.fr.adaming.entity.Customer;

@SpringBootTest(classes = OuiShareWebServiceApplication.class)
public class CustomerLoginDtoConverterTest {

	@Test
	public void convertDtoToEntity_shouldSatisfyAssertion() {
		
		CustomerLoginDto dto = new CustomerLoginDto();
		
		dto.setEmail("a@a.fr");
		
		dto.setPwd("azerty");
		
		Customer entity = CustomerLoginDtoConverter.MAPPER.convert(dto);
		
		assertNotNull(entity);
		assertThat(entity).hasFieldOrPropertyWithValue("email", "a@a.fr");	
		assertThat(entity).hasFieldOrPropertyWithValue("pwd", "azerty");
		
		
	
	}
	
	@Test
	public void convertEntityToDto_shouldSatisfyAssertion() {
	
		Customer entity = new Customer();
		
		entity.setEmail("a@a.fr");
		
		entity.setPwd("azerty");
		
		
		CustomerLoginDto dto = CustomerLoginDtoConverter.MAPPER.convert(entity);
		
		assertNotNull(dto);
		assertThat(dto).hasFieldOrPropertyWithValue("email", "a@a.fr");
		
		assertThat(dto).hasFieldOrPropertyWithValue("pwd", "azerty");
		
		
		
	}
	
	@Test
	public void convertDtosToEntities_shouldSatisfyAssertion() {
		
		CustomerLoginDto dto = new CustomerLoginDto();
		
		dto.setEmail("a@a.fr");
		
		dto.setPwd("azerty");
		
		
		
		CustomerLoginDto dto2 = new CustomerLoginDto();
		
		dto2.setEmail("b@a.fr");
		
		dto2.setPwd("azerty2");
		
		
		
		ArrayList<CustomerLoginDto> dtos = new ArrayList<>();
		dtos.add(dto);
		dtos.add(dto2);
		
		
		
		List<Customer> entities = CustomerLoginDtoConverter.MAPPER.convert(dtos);
		
		assertThat(entities).isNotNull().hasSize(2);
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("email", "a@a.fr");
		
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("pwd", "azerty");

		
		
		
		
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("email", "b@a.fr");

		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("pwd", "azerty2");


		
	}
	
	@Test
	public void convertEntitiesToDtos_shouldSatisfyAssertion(){
		
		Customer entity = new Customer();
		
		entity.setEmail("a@a.fr");
		
		entity.setPwd("azerty");
		
		
		Customer entity2 = new Customer();
		
		entity2.setEmail("b@a.fr");
		
		entity2.setPwd("azerty2");
	
		List <Customer> entities = new ArrayList<>();
		entities.add(entity);
		entities.add(entity2);
		
		List<CustomerLoginDto> dtos = CustomerLoginDtoConverter.MAPPER.convert(entities);
		
		assertThat(dtos).isNotNull().hasSize(2);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("email", "a@a.fr");

		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("pwd", "azerty");
	
		
		
		
		
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("email", "b@a.fr");
	
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("pwd", "azerty2");
		
		
		
		
	}
	
}
