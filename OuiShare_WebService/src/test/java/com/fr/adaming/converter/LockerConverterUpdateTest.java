package com.fr.adaming.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.fr.adaming.OuiShareWebServiceApplication;
import com.fr.adaming.dto.LockerDtoUpdate;
import com.fr.adaming.entity.Locker;

@SpringBootTest(classes = OuiShareWebServiceApplication.class)
public class LockerConverterUpdateTest {

	@Test
	public void convertDtoToEntity_shouldSatisfyAssertion() {
		LockerDtoUpdate dto = new LockerDtoUpdate();

		dto.setId(123L);
		dto.setName("name4Test");
		dto.setAdress("address");
		dto.setCity("paris");
		dto.setSlot(500L);
		dto.setZipCode(987654321L);
		dto.setDeleted(false);
		
		Locker entity = LockerDtoConverterUpdate.MAPPER.convert(dto);

		assertNotNull(entity);
		assertThat(entity).hasFieldOrPropertyWithValue("name", "name4Test");
		assertThat(entity).hasFieldOrPropertyWithValue("adress", "address");
		assertThat(entity).hasFieldOrPropertyWithValue("city", "paris");
	}

	@Test
	public void convertDtosToEntities_shouldSatisfyAssertion() {
		LockerDtoUpdate dto = new LockerDtoUpdate();

		dto.setId(123L);
		dto.setName("name4Test");
		dto.setAdress("address");
		dto.setCity("paris");
		dto.setSlot(500L);
		dto.setZipCode(987654321L);
		dto.setDeleted(false);
		
		LockerDtoUpdate dto2 = new LockerDtoUpdate();

		dto2.setId(123L);
		dto2.setName("name4Test2");
		dto2.setAdress("address2");
		dto2.setCity("paris");
		dto2.setSlot(500L);
		dto2.setZipCode(123456789L);
		dto2.setDeleted(false);
		
		ArrayList<LockerDtoUpdate> dtos = new ArrayList<>();
		dtos.add(dto);
		dtos.add(dto2);

		List<Locker> entities = LockerDtoConverterUpdate.MAPPER.convert(dtos);

		assertThat(entities).isNotNull().hasSize(2);
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("name", "name4Test");
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("adress", "address");

		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("name", "name4Test2");
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("adress", "address2");
	}

	@Test
	public void convertEntityToDto_shouldSatisfyAssertion() {
		Locker entity = new Locker();

		entity.setId(123L);
		entity.setName("name4Test2");
		entity.setAdress("address2");
		entity.setCity("paris");
		entity.setSlot(500L);
		entity.setZipCode(123456789L);
		entity.setDeleted(false);

		LockerDtoUpdate dto = LockerDtoConverterUpdate.MAPPER.convert(entity);

		assertNotNull(dto);
		assertThat(dto).hasFieldOrPropertyWithValue("name", "name4Test2");
		assertThat(dto).hasFieldOrPropertyWithValue("adress", "address2");
	}

	@Test
	public void convertEntitiesToDtos_shouldSatisfyAssertion() {
		Locker entity1 = new Locker();

		entity1.setId(123L);
		entity1.setName("name4Test2");
		entity1.setAdress("address3");
		entity1.setCity("paris");
		entity1.setSlot(500L);
		entity1.setZipCode(123456789L);
		entity1.setDeleted(false);

		Locker entity2 = new Locker();

		entity2.setId(123L);
		entity2.setName("name4Test2");
		entity2.setAdress("address4");
		entity2.setCity("paris");
		entity2.setSlot(500L);
		entity2.setZipCode(123456789L);
		entity2.setDeleted(false);

		List<Locker> entities = new ArrayList<>();
		entities.add(entity1);
		entities.add(entity2);

		List<LockerDtoUpdate> dtos = LockerDtoConverterUpdate.MAPPER.convert(entities);

		assertThat(dtos).isNotNull().hasSize(2);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("name", "name4Test2");
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("adress", "address3");

		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("name", "name4Test2");
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("adress", "address4");
	}
}
