package com.fr.adaming.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.fr.adaming.OuiShareWebServiceApplication;
import com.fr.adaming.dto.CustomerUpdateDto;
import com.fr.adaming.dto.DeviceDtoWithId;
import com.fr.adaming.dto.SubscriptionCreateDto;
import com.fr.adaming.entity.Customer;
import com.fr.adaming.entity.Device;
import com.fr.adaming.entity.Subscription;

@SpringBootTest(classes = OuiShareWebServiceApplication.class)
public class SubscriptionCreateDtoConverterTest {

	@Test
	public void convertDtoToEntity_shouldSatisfyAssertion() {
		SubscriptionCreateDto dto = new SubscriptionCreateDto();
		CustomerUpdateDto dtoCustomer = new CustomerUpdateDto();
		dtoCustomer.setCity("LYON");
		dtoCustomer.setEmail("aa@aa.fr");
		DeviceDtoWithId dtoDevice = new DeviceDtoWithId();
		dtoDevice.setName("Aspirateur");
		
		dto.setStartDate(LocalDate.of(1991, 01, 22));
		dto.setCustomer(dtoCustomer);
		dto.setDevice(dtoDevice);

		
		Subscription entity = SubscriptionCreateDtoConverter.MAPPER.convert(dto);
		
		assertNotNull(entity);
		assertThat(entity).hasFieldOrPropertyWithValue("startDate", LocalDate.of(1991, 01, 22));
		assertEquals(dto.getCustomer().getEmail(), entity.getCustomer().getEmail());
		assertEquals(dto.getCustomer().getCity(), entity.getCustomer().getCity());
		assertEquals(dto.getDevice().getName(), entity.getDevice().getName());
	}
	
	@Test
	public void convertDtosToEntities_ShouldSatisfyAssertion() {
		SubscriptionCreateDto dto = new SubscriptionCreateDto();
		CustomerUpdateDto dtoCustomer = new CustomerUpdateDto();
		dtoCustomer.setCity("LYON");
		dtoCustomer.setEmail("aa@aa.fr");
		DeviceDtoWithId dtoDevice = new DeviceDtoWithId();
		dtoDevice.setName("Aspirateur");
		
		dto.setStartDate(LocalDate.of(1991, 01, 22));
		dto.setCustomer(dtoCustomer);
		dto.setDevice(dtoDevice);
		
		
		SubscriptionCreateDto dto2 = new SubscriptionCreateDto();
		CustomerUpdateDto dtoCustomer2 = new CustomerUpdateDto();
		dtoCustomer.setCity("LYON2");
		dtoCustomer.setEmail("aa2@aa.fr");
		DeviceDtoWithId dtoDevice2 = new DeviceDtoWithId();
		dtoDevice.setName("Mixer");
		
		dto2.setStartDate(LocalDate.of(1994, 12, 27));
		dto2.setCustomer(dtoCustomer2);
		dto2.setDevice(dtoDevice2);
		
		ArrayList<SubscriptionCreateDto> dtos = new ArrayList<SubscriptionCreateDto>();
		dtos.add(dto);
		dtos.add(dto2);
		
		List<Subscription> entities = SubscriptionCreateDtoConverter.MAPPER.convert(dtos);
		
		assertThat(entities).isNotNull().hasSize(2);
		assertThat(entities.get(0)).hasFieldOrPropertyWithValue("startDate", LocalDate.of(1991, 01, 22));
		assertThat(entities.get(1)).hasFieldOrPropertyWithValue("startDate", LocalDate.of(1994, 12, 27));
		assertEquals(dto.getCustomer().getEmail(), entities.get(0).getCustomer().getEmail());
		assertEquals(dto.getCustomer().getCity(), entities.get(0).getCustomer().getCity());
		assertEquals(dto.getDevice().getName(), entities.get(0).getDevice().getName());
		
		assertEquals(dto2.getCustomer().getEmail(), entities.get(1).getCustomer().getEmail());
		assertEquals(dto2.getCustomer().getCity(), entities.get(1).getCustomer().getCity());
		assertEquals(dto2.getDevice().getName(), entities.get(1).getDevice().getName());
		
	}
	
	@Test
	public void convertEntityToDto_ShouldSatisfyAssertion() {
		Subscription entity = new Subscription();
		Customer entityCustomer = new Customer();
		Device entityDevice = new Device();
		
		entity.setStartDate(LocalDate.of(2017, 05, 24));
		entity.setDeleted(false);
		entity.setCustomer(entityCustomer);
		entity.setDevice(entityDevice);
		
		entityCustomer.setEmail("aa@aa.fr");
		entityDevice.setName("Aspirateur");
		
		SubscriptionCreateDto dto = SubscriptionCreateDtoConverter.MAPPER.convert(entity);
		
		assertNotNull(dto);
		assertEquals(entity.getStartDate(), dto.getStartDate());
		assertEquals(entity.getCustomer().getEmail(), dto.getCustomer().getEmail());
		assertEquals(entity.getDevice().getName(), dto.getDevice().getName());
	}
	
	@Test
	public void convertEntitiesToDtos_ShouldSatisfyAssertion() {
		Subscription entity = new Subscription();
		Customer entityCustomer = new Customer();
		Device entityDevice = new Device();
		
		entity.setStartDate(LocalDate.of(2017, 05, 24));
		entity.setDeleted(false);
		entity.setCustomer(entityCustomer);
		entity.setDevice(entityDevice);
		
		entityCustomer.setEmail("aa@aa.fr");
		entityDevice.setName("Aspirateur");
		
		Subscription entity2 = new Subscription();
		Customer entityCustomer2 = new Customer();
		Device entityDevice2 = new Device();
		
		entity2.setStartDate(LocalDate.of(2015, 06, 12));
		entity2.setDeleted(true);
		entity2.setCustomer(entityCustomer2);
		entity2.setDevice(entityDevice2);
		
		entityCustomer2.setEmail("bb@aa.fr");
		entityDevice2.setName("Raclette");
		
		ArrayList<Subscription> entities = new ArrayList<Subscription>();
		entities.add(entity);
		entities.add(entity2);
		
		List<SubscriptionCreateDto> dtos = SubscriptionCreateDtoConverter.MAPPER.convert(entities);
		
		assertThat(dtos).isNotNull().hasSize(2);
		assertThat(dtos.get(0)).hasFieldOrPropertyWithValue("startDate", LocalDate.of(2017, 05, 24));
		assertThat(dtos.get(1)).hasFieldOrPropertyWithValue("startDate", LocalDate.of(2015, 06, 12));
		assertEquals(entity.getCustomer().getEmail(), dtos.get(0).getCustomer().getEmail());
		assertEquals(entity.getDevice().getName(), dtos.get(0).getDevice().getName());
		
		assertEquals(entity2.getCustomer().getEmail(), dtos.get(1).getCustomer().getEmail());
		assertEquals(entity2.getCustomer().getCity(), dtos.get(1).getCustomer().getCity());
		assertEquals(entity2.getDevice().getName(), dtos.get(1).getDevice().getName());
	}
}
