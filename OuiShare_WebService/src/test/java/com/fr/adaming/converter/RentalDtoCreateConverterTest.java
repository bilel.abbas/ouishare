package com.fr.adaming.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.adaming.OuiShareWebServiceApplication;
import com.fr.adaming.dto.RentalDtoCreate;
import com.fr.adaming.entity.Customer;
import com.fr.adaming.entity.Device;
import com.fr.adaming.entity.Rental;
import com.fr.adaming.entity.Review;

/**
 * @author Nicolas RUFFIER
 *
 */
@SpringBootTest(classes = OuiShareWebServiceApplication.class)
public class RentalDtoCreateConverterTest {
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER", "DELETE FROM REVIEW",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into review (id, deleted, rating) "
					+ "values(1, 0, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER", "DELETE FROM REVIEW" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void convertDtoToEntity_shouldSatisfyAssertion() {
		RentalDtoCreate dto = new RentalDtoCreate();

		dto.setStartDate(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		dto.setEndDateTheoritical(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		dto.setSubscription(false);
		dto.setIdDevice(1L);
		dto.setIdCustomer(1L);

		Rental rental = RentalDtoCreateConverter.MAPPER.convert(dto);

		assertNotNull(rental);
		assertThat(rental).hasFieldOrPropertyWithValue("startDate", LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertThat(rental).hasFieldOrPropertyWithValue("endDateTheoritical",
				LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertEquals(1L, rental.getDevice().getId());
		assertEquals(1L, rental.getCustomer().getId());
		assertFalse(rental.isDeleted());
		assertFalse(rental.isSubscription());
	}

	@Test
	public void convertDtosToEntities_shouldSatisfyAssertion() {
		RentalDtoCreate dto = new RentalDtoCreate();
		dto.setStartDate(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		dto.setEndDateTheoritical(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		dto.setSubscription(false);
		dto.setIdDevice(1L);
		dto.setIdCustomer(1L);

		RentalDtoCreate dto2 = new RentalDtoCreate();
		dto2.setStartDate(LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		dto2.setEndDateTheoritical(LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		dto2.setSubscription(false);
		dto2.setIdDevice(2L);
		dto2.setIdCustomer(2L);

		List<RentalDtoCreate> dtos = new ArrayList<>();
		dtos.add(dto);
		dtos.add(dto2);

		List<Rental> rentals = RentalDtoCreateConverter.MAPPER.convert1(dtos);

		assertThat(rentals).isNotNull().hasSize(2);
		assertThat(rentals.get(0)).isNotNull().hasFieldOrPropertyWithValue("startDate",
				LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertThat(rentals.get(0)).isNotNull().hasFieldOrPropertyWithValue("endDateTheoritical",
				LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertEquals(1L, rentals.get(0).getDevice().getId());
		assertEquals(1L, rentals.get(0).getCustomer().getId());
		assertFalse(rentals.get(0).isDeleted());
		assertFalse(rentals.get(0).isSubscription());

		assertThat(rentals.get(1)).isNotNull().hasFieldOrPropertyWithValue("startDate",
				LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		assertThat(rentals.get(1)).isNotNull().hasFieldOrPropertyWithValue("endDateTheoritical",
				LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		assertEquals(2L, rentals.get(1).getDevice().getId());
		assertEquals(2L, rentals.get(1).getCustomer().getId());
		assertFalse(rentals.get(0).isDeleted());
		assertFalse(rentals.get(0).isSubscription());
	}

	@Test
	public void convertEntityToDto_shouldSatisfyAssertion() {
		Device device = new Device();
		device.setId(1L);
		Customer customer = new Customer();
		customer.setId(1L);
		Review review = new Review();
		review.setId(1L);

		Rental rental = new Rental();
		rental.setId(1L);
		rental.setStartDate(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setEndDateTheoritical(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setEndDateReal(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setSubscription(false);
		rental.setDeleted(false);
		rental.setDevice(device);
		rental.setCustomer(customer);
		rental.setReview(review);

		RentalDtoCreate dto = RentalDtoCreateConverter.MAPPER.convert(rental);

		assertNotNull(dto);
		assertThat(dto).hasFieldOrPropertyWithValue("startDate", LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertThat(dto).hasFieldOrPropertyWithValue("endDateTheoritical", LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertEquals(1L, dto.getIdDevice());
		assertEquals(1L, dto.getIdCustomer());
		assertFalse(dto.isSubscription());
	}

	@Test
	public void convertEntitiesToDtos_shouldSatisfyAssertion() {
		Device device = new Device();
		device.setId(1L);
		Customer customer = new Customer();
		customer.setId(1L);
		Review review = new Review();
		review.setId(1L);

		Rental rental = new Rental();
		rental.setId(1L);
		rental.setStartDate(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setEndDateTheoritical(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setEndDateReal(LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		rental.setSubscription(false);
		rental.setDeleted(false);
		rental.setDevice(device);
		rental.setCustomer(customer);
		rental.setReview(review);
		
		Device device2 = new Device();
		device2.setId(2L);
		Customer customer2 = new Customer();
		customer2.setId(2L);
		Review review2 = new Review();
		review2.setId(2L);

		Rental rental2 = new Rental();
		rental2.setId(2L);
		rental2.setStartDate(LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		rental2.setEndDateTheoritical(LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		rental2.setEndDateReal(LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		rental2.setSubscription(false);
		rental2.setDeleted(false);
		rental2.setDevice(device2);
		rental2.setCustomer(customer2);
		rental2.setReview(review2);

		List<Rental> rentals = new ArrayList<>();
		rentals.add(rental);
		rentals.add(rental2);

		List<RentalDtoCreate> dtos = RentalDtoCreateConverter.MAPPER.convert2(rentals);

		assertThat(dtos).isNotNull().hasSize(2);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("startDate",
				LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("endDateTheoritical",
				LocalDateTime.of(1991, 01, 22, 18, 55, 30));
		assertEquals(1L, dtos.get(0).getIdDevice());
		assertEquals(1L, dtos.get(0).getIdCustomer());
		assertFalse(dtos.get(0).isSubscription());
		

		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("startDate",
				LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("endDateTheoritical",
				LocalDateTime.of(1992, 01, 22, 18, 55, 30));
		assertEquals(2L, dtos.get(1).getIdDevice());
		assertEquals(2L, dtos.get(1).getIdCustomer());
		assertFalse(dtos.get(1).isSubscription());
	}
}
