package com.fr.adaming.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.fr.adaming.OuiShareWebServiceApplication;
import com.fr.adaming.dto.AdminDtoCreate;
import com.fr.adaming.dto.AdminDtoLogin;
import com.fr.adaming.dto.AdminDtoUpdate;
import com.fr.adaming.entity.Admin;

/**
 * @author bilel
 *
 */
@SpringBootTest(classes = OuiShareWebServiceApplication.class)
public class AdminDtoConverterTest {

	@Test
	public void convertAdminDtoCreateToAdmin_shouldSatisfyAssertion() {

		AdminDtoCreate dto = new AdminDtoCreate();

		dto.setEmail("bilel.abbas@gmail.com");
		dto.setPwd("azerty");
		dto.setUserName("bilel-abbas");

		Admin admin = AdminDtoConverter.MAPPER.convertCreate(dto);

		assertNotNull(admin);
		assertEquals("bilel.abbas@gmail.com", admin.getEmail());
		assertEquals("azerty", admin.getPwd());
		assertEquals("bilel-abbas", admin.getUserName());
	}

	@Test
	public void convertAdminToAdminDtoCreate_shouldSatisfyAssertion() {

		Admin admin = new Admin();

		admin.setEmail("bilel.abbas@gmail.com");
		admin.setPwd("azerty");
		admin.setUserName("bilel-abbas");

		AdminDtoCreate dto = AdminDtoConverter.MAPPER.convertCreate(admin);

		assertNotNull(dto);
		assertEquals("bilel.abbas@gmail.com", dto.getEmail());
		assertEquals("azerty", dto.getPwd());
		assertEquals("bilel-abbas", dto.getUserName());
	}

	@Test
	public void convertAdminDtoUpdateToAdmin_shouldSatisfyAssertion() {

		AdminDtoUpdate dto = new AdminDtoUpdate();

		dto.setId(1L);
		dto.setEmail("bilel.abbas@gmail.com");
		dto.setPwd("azerty");
		dto.setUserName("bilel-abbas");
		dto.setDeleted(true);

		Admin admin = AdminDtoConverter.MAPPER.convertUpdate(dto);

		assertNotNull(admin);
		assertEquals(1, admin.getId().intValue());
		assertEquals("bilel.abbas@gmail.com", admin.getEmail());
		assertEquals("azerty", admin.getPwd());
		assertEquals("bilel-abbas", admin.getUserName());
		assertTrue(admin.isDeleted());
	}

	@Test
	public void convertAdminToAdminDtoUpdate_shouldSatisfyAssertion() {

		Admin admin = new Admin();

		admin.setId(1L);
		admin.setEmail("bilel.abbas@gmail.com");
		admin.setPwd("azerty");
		admin.setUserName("bilel-abbas");
		admin.setDeleted(true);

		AdminDtoUpdate dto = AdminDtoConverter.MAPPER.convertUpdate(admin);

		assertNotNull(dto);
		assertEquals(1, dto.getId().intValue());
		assertEquals("bilel.abbas@gmail.com", dto.getEmail());
		assertEquals("azerty", dto.getPwd());
		assertEquals("bilel-abbas", dto.getUserName());
		assertTrue(dto.isDeleted());
	}

	@Test
	public void convertAdminDtoLoginToAdmin_shouldSatisfyAssertion() {

		AdminDtoLogin dto = new AdminDtoLogin();

		dto.setEmail("bilel.abbas@gmail.com");
		dto.setPwd("azerty");

		Admin admin = AdminDtoConverter.MAPPER.convertLogin(dto);

		assertNotNull(admin);
		assertEquals("bilel.abbas@gmail.com", admin.getEmail());
		assertEquals("azerty", admin.getPwd());
	}

	@Test
	public void convertAdminToAdminDtoLogin_shouldSatisfyAssertion() {

		Admin admin = new Admin();

		admin.setEmail("bilel.abbas@gmail.com");
		admin.setPwd("azerty");

		AdminDtoLogin dto = AdminDtoConverter.MAPPER.convertLogin(admin);

		assertNotNull(dto);
		assertEquals("bilel.abbas@gmail.com", dto.getEmail());
		assertEquals("azerty", dto.getPwd());
	}

	@Test
	public void convertListAdminDtoUpdateToListAdmin_shouldSatisfyAssertion() {

		AdminDtoUpdate dto = new AdminDtoUpdate();

		dto.setId(1L);
		dto.setEmail("bilel.abbas@gmail.com");
		dto.setPwd("azerty");
		dto.setUserName("bilel-abbas");
		dto.setDeleted(true);

		AdminDtoUpdate dto2 = new AdminDtoUpdate();

		dto2.setId(2L);
		dto2.setEmail("admin@adaming.fr");
		dto2.setPwd("inti");
		dto2.setUserName("lyon-plage");
		dto2.setDeleted(false);

		ArrayList<AdminDtoUpdate> dtos = new ArrayList<>();
		dtos.add(dto);
		dtos.add(dto2);

		List<Admin> admins = AdminDtoConverter.MAPPER.convert(dtos);

		assertNotNull(admins);
		assertEquals(1, admins.get(0).getId().intValue());
		assertEquals("bilel.abbas@gmail.com", admins.get(0).getEmail());
		assertEquals("azerty", admins.get(0).getPwd());
		assertEquals("bilel-abbas", admins.get(0).getUserName());
		assertTrue(admins.get(0).isDeleted());

		assertEquals(2, admins.get(1).getId().intValue());
		assertEquals("admin@adaming.fr", admins.get(1).getEmail());
		assertEquals("inti", admins.get(1).getPwd());
		assertEquals("lyon-plage", admins.get(1).getUserName());
		assertFalse(admins.get(1).isDeleted());

	}

	@Test
	public void convertListAdminToListAdminDtoUpdate_shouldSatisfyAssertion() {

		Admin admin = new Admin();

		admin.setId(1L);
		admin.setEmail("bilel.abbas@gmail.com");
		admin.setPwd("azerty");
		admin.setUserName("bilel-abbas");
		admin.setDeleted(true);

		Admin admin2 = new Admin();

		admin2.setId(2L);
		admin2.setEmail("admin@adaming.fr");
		admin2.setPwd("inti");
		admin2.setUserName("lyon-plage");
		admin2.setDeleted(false);

		ArrayList<Admin> admins = new ArrayList<>();
		admins.add(admin);
		admins.add(admin2);

		List<AdminDtoUpdate> dtos = AdminDtoConverter.MAPPER.convert(admins);

		assertNotNull(dtos);
		assertEquals(1, dtos.get(0).getId().intValue());
		assertEquals("bilel.abbas@gmail.com", dtos.get(0).getEmail());
		assertEquals("azerty", dtos.get(0).getPwd());
		assertEquals("bilel-abbas", dtos.get(0).getUserName());
		assertTrue(dtos.get(0).isDeleted());

		assertEquals(2, dtos.get(1).getId().intValue());
		assertEquals("admin@adaming.fr", dtos.get(1).getEmail());
		assertEquals("inti", dtos.get(1).getPwd());
		assertEquals("lyon-plage", dtos.get(1).getUserName());
		assertFalse(dtos.get(1).isDeleted());

	}

}
