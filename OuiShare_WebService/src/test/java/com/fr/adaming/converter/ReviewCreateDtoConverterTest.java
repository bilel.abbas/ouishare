package com.fr.adaming.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.fr.adaming.OuiShareWebServiceApplication;
import com.fr.adaming.dto.ReviewCreateDto;
import com.fr.adaming.entity.Review;

@SpringBootTest(classes = OuiShareWebServiceApplication.class)
public class ReviewCreateDtoConverterTest {

	
	@Test
	public void convertDtoToEntity_shouldSatisfyAssertion() {
		ReviewCreateDto dto = new ReviewCreateDto();
		
		dto.setRating(2);
		dto.setComment("aaaaa");
		dto.setDeleted(true);
		
		Review entity = ReviewCreateDtoConverter.MAPPER.convert(dto);
		
		assertNotNull(entity);
		assertThat(entity).hasFieldOrPropertyWithValue("rating", 2);
		assertThat(entity).hasFieldOrPropertyWithValue("comment", "aaaaa");
		assertThat(entity).hasFieldOrPropertyWithValue("deleted", true);
		
		
	}
	
	
	@Test
	public void convertEntityToDto_shouldSatisfyAssertion() {
	
		Review entity = new Review();
		
		entity.setRating(2);
		entity.setComment("bbbbb");
		entity.setDeleted(false);
		
		ReviewCreateDto dto = ReviewCreateDtoConverter.MAPPER.convert(entity);
		
		assertNotNull(dto);
		assertThat(dto).hasFieldOrPropertyWithValue("rating", 2);
		assertThat(dto).hasFieldOrPropertyWithValue("comment", "bbbbb");
		assertThat(dto).hasFieldOrPropertyWithValue("deleted", false);
		
		
	}
	
	@Test
	public void convertDtosToEntities_shouldSatisfyAssertion() {
		
		ReviewCreateDto dto = new ReviewCreateDto();
		
		dto.setRating(2);
		dto.setComment("aaaaa");
		dto.setDeleted(true);
		
		
		ReviewCreateDto dto2 = new ReviewCreateDto();
		
		dto2.setRating(5);
		dto2.setComment("ccccc");
		dto2.setDeleted(false);
		
		
		ArrayList<ReviewCreateDto> dtos = new ArrayList<>();
		dtos.add(dto);
		dtos.add(dto2);
		
		
		
		List<Review> entities = ReviewCreateDtoConverter.MAPPER.convert(dtos);
		
		assertThat(entities).isNotNull().hasSize(2);
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("rating", 2);
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("comment", "aaaaa");
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("deleted", true);
		
		
		
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("rating", 5);
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("comment", "ccccc");
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("deleted", false);

		
	}
	
	@Test
	public void convertEntitiesToDtos_shouldSatisfyAssertion(){
		
		Review entity = new Review();
		
		entity.setRating(2);
		entity.setComment("bbbbb");
		entity.setDeleted(false);
		
		Review entity2 = new Review();
		
		entity2.setRating(4);
		entity2.setComment("ddddd");
		entity2.setDeleted(true);
		
		List <Review> entities = new ArrayList<>();
		entities.add(entity);
		entities.add(entity2);
		
		List<ReviewCreateDto> dtos = ReviewCreateDtoConverter.MAPPER.convert(entities);
		
		assertThat(dtos).isNotNull().hasSize(2);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("rating", 2);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("comment", "bbbbb");
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("deleted", false);
		
		
		
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("rating", 4);
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("comment", "ddddd");
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("deleted", true);
		
		
		
	}
	
}
