package com.fr.adaming.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.fr.adaming.converter.SlotDtoConverter;
import com.fr.adaming.OuiShareWebServiceApplication;
import com.fr.adaming.dto.SlotDto;
import com.fr.adaming.dto.SlotDtoWithoutId;
import com.fr.adaming.entity.Slot;


/**
 * @author Aurélien
 *
 */
@SpringBootTest(classes = OuiShareWebServiceApplication.class)
public class SlotDtoConverterTest {

	@Test
	public void convertDtoToEntity_shouldSatisfyAssertion() {
		SlotDto dto = new SlotDto();
		
		dto.setDimensions("10x10x10");
		dto.setId(13L);
		dto.setNumber(123L);
		
		Slot entity = SlotDtoConverter.MAPPER.convert(dto);
		
		assertNotNull(entity);
		assertThat(entity).hasFieldOrPropertyWithValue("dimensions", "10x10x10");
		assertThat(entity).hasFieldOrPropertyWithValue("number", 123L);
		assertThat(entity).hasFieldOrPropertyWithValue("id", 13L);
	}
	
	@Test
	public void convertDtoWithoutIdToEntity_shouldSatisfyAssertion() {
		SlotDtoWithoutId dto = new SlotDtoWithoutId();
		
		dto.setDimensions("10x10x10");
		dto.setNumber(123L);
		
		Slot entity = SlotDtoConverter.MAPPER.convert(dto);
		
		assertNotNull(entity);
		assertThat(entity).hasFieldOrPropertyWithValue("dimensions", "10x10x10");
		assertThat(entity).hasFieldOrPropertyWithValue("number", 123L);
	}

	@Test
	public void convertDtosToEntities_shouldSatisfyAssertion() {
		SlotDto dto = new SlotDto();
		
		dto.setDimensions("10x10x10");

		dto.setNumber(123L);
		dto.setId(12L);
		
		SlotDto dto2 = new SlotDto();
		
		dto2.setDimensions("12x10x10");
		dto2.setDeleted(true);
		dto2.setNumber(124L);
		dto2.setId(13L);
		
		ArrayList<SlotDto> dtos = new ArrayList<>();
		dtos.add(dto);
		dtos.add(dto2);
		
		List<Slot> entities =SlotDtoConverter.MAPPER.convert(dtos);
		
		assertThat(entities).isNotNull().hasSize(2);
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("dimensions", "10x10x10");
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("number", 123L);
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("id", 12L);
		
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("dimensions","12x10x10");
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("number", 124L);
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("id", 13L);
	}
	
	@Test
	public void convertDtosWithoutIdToEntities_shouldSatisfyAssertion() {
		SlotDtoWithoutId dto = new SlotDtoWithoutId();
		
		dto.setDimensions("10x10x10");

		dto.setNumber(123L);
		
		SlotDtoWithoutId dto2 = new SlotDtoWithoutId();
		
		dto2.setDimensions("12x10x10");
		dto2.setDeleted(true);
		dto2.setNumber(124L);
		
		ArrayList<SlotDtoWithoutId> dtos = new ArrayList<>();
		dtos.add(dto);
		dtos.add(dto2);
		
		List<Slot> entities =SlotDtoConverter.MAPPER.convertt(dtos);
		
		assertThat(entities).isNotNull().hasSize(2);
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("dimensions", "10x10x10");
		assertThat(entities.get(0)).isNotNull().hasFieldOrPropertyWithValue("number", 123L);
		
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("dimensions","12x10x10");
		assertThat(entities.get(1)).isNotNull().hasFieldOrPropertyWithValue("number", 124L);
	}
	
	@Test
	public void convertEntityToDto_shouldSatisfyAssertion() {
		Slot entity = new Slot();
		
		entity.setDimensions("10x10x10");
		entity.setNumber(123L);
		entity.setId(13L);
		
		SlotDto dto = SlotDtoConverter.MAPPER.convert(entity);
		
		assertNotNull(dto);
		assertThat(dto).hasFieldOrPropertyWithValue("dimensions", "10x10x10");
		assertThat(dto).hasFieldOrPropertyWithValue("number", 123L);
		assertThat(dto).hasFieldOrPropertyWithValue("id", 13L);
	}
	
	@Test
	public void convertEntityToDtoWithoutId_shouldSatisfyAssertion() {
		Slot entity = new Slot();
		
		entity.setDimensions("10x10x10");
		entity.setNumber(123L);
		
		SlotDtoWithoutId dto = SlotDtoConverter.MAPPER.convertt(entity);
		
		assertNotNull(dto);
		assertThat(dto).hasFieldOrPropertyWithValue("dimensions", "10x10x10");
		assertThat(dto).hasFieldOrPropertyWithValue("number", 123L);
	}

	@Test
	public void convertEntitiesToDtos_shouldSatisfyAssertion() {
		Slot entity1 = new Slot();
		
		entity1.setDimensions("10x10x10");
		entity1.setNumber(123L);
		entity1.setId(12L);
		
		Slot entity2 = new Slot();
		
		entity2.setDimensions("12x10x10");
		entity2.setNumber(124L);
		entity2.setId(13L);
		
		List<Slot> entities = new ArrayList<>();
		entities.add(entity1);
		entities.add(entity2);
		
		
		List<SlotDto> dtos = SlotDtoConverter.MAPPER.convertt(entities);
		
		assertThat(dtos).isNotNull().hasSize(2);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("dimensions", "10x10x10");
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("number", 123L);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("id", 12L);
		
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("dimensions", "12x10x10");
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("number", 124L);
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("id", 13L);
	}
	
	@Test
	public void convertEntitiesToDtosWithoutId_shouldSatisfyAssertion() {
		Slot entity1 = new Slot();
		
		entity1.setDimensions("10x10x10");
		entity1.setNumber(123L);
		
		Slot entity2 = new Slot();
		
		entity2.setDimensions("12x10x10");
		entity2.setNumber(124L);
		
		List<Slot> entities = new ArrayList<>();
		entities.add(entity1);
		entities.add(entity2);
		
		
		List<SlotDtoWithoutId> dtos = SlotDtoConverter.MAPPER.convert(entities);
		
		assertThat(dtos).isNotNull().hasSize(2);
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("dimensions", "10x10x10");
		assertThat(dtos.get(0)).isNotNull().hasFieldOrPropertyWithValue("number", 123L);
		
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("dimensions", "12x10x10");
		assertThat(dtos.get(1)).isNotNull().hasFieldOrPropertyWithValue("number", 124L);
	}
}

