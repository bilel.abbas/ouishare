package com.fr.adaming;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest(classes = OuiShareWebServiceApplication.class)
@AutoConfigureMockMvc
public class OuiShareWebServiceApplicationTest {

	@Autowired
	protected MockMvc mvc;
	
	protected ObjectMapper mapper = new ObjectMapper();
	
	@Test
	public void exampleOfTestCase() {
		assertTrue(true);
	}
}
