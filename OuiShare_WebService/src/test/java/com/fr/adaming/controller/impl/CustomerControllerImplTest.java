package com.fr.adaming.controller.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fr.adaming.OuiShareWebServiceApplicationTest;
import com.fr.adaming.constants.AbstractConstants;
import com.fr.adaming.dto.CustomerCreateDto;
import com.fr.adaming.dto.CustomerLoginDto;
import com.fr.adaming.dto.CustomerUpdateDto;

/**
 * @author CORNELOUP Theo
 *
 */
public class CustomerControllerImplTest extends OuiShareWebServiceApplicationTest {

	@Test
	@Sql(statements = "delete from customer where id=1", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveValideCustomerExample_shouldReturnStatus200WithContentNotNull()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		try {
			CustomerCreateDto dto = new CustomerCreateDto();

			// prepare inputs
			dto.setEmail("a@a.fr");
			dto.setLastName("Murray");
			dto.setFirstName("billy");
			dto.setAdress("adresse");
			dto.setZipCode(123);
			dto.setCity("city");
			dto.setPwd("azerty");
			dto.setPhone("012345678");

			String json = mvc
					.perform(post(AbstractConstants.Customer.CREATE).contentType(MediaType.APPLICATION_JSON)
							.content(mapper.writeValueAsString(dto)))
					.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

			CustomerCreateDto response = mapper.readValue(json, CustomerCreateDto.class);

			// verify result
			assertNotNull(response);
			assertThat(response).hasFieldOrPropertyWithValue("email", "a@a.fr");
			assertThat(response).hasFieldOrPropertyWithValue("lastName", "Murray");
			assertThat(response).hasFieldOrPropertyWithValue("firstName", "billy");
			assertThat(response).hasFieldOrPropertyWithValue("adress", "adresse");
			assertThat(response).hasFieldOrPropertyWithValue("zipCode", 123);
			assertThat(response).hasFieldOrPropertyWithValue("city", "city");
			assertThat(response).hasFieldOrPropertyWithValue("pwd", "azerty");
			assertThat(response).hasFieldOrPropertyWithValue("phone", "012345678");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'b@b.fr','Murray', 'adresse', 123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveExistingCustomer_shouldReturnStatus200WithNoNewCustomer()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		// prepare inputs
		CustomerCreateDto dto = new CustomerCreateDto();

		// prepare inputs
		dto.setEmail("b@b.fr");
		dto.setLastName("Murray");
		dto.setFirstName("billy");
		dto.setAdress("adresse");
		dto.setZipCode(123);
		dto.setCity("city");
		dto.setPwd("azerty");
		dto.setPhone("012345678");

		String json = mvc
				.perform(post(AbstractConstants.Customer.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("", json);

	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 0123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateValidCustomer_shouldReturnCustomerWithModification()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		// prepare inputs
		CustomerUpdateDto dto = new CustomerUpdateDto();

		// prepare inputs
		dto.setId(1L);
		dto.setEmail("b@b.fr");
		dto.setLastName("Jean");
		dto.setFirstName("marc");
		dto.setAdress("adr");
		dto.setZipCode(145);
		dto.setCity("lyon");
		dto.setPwd("pmloik");
		dto.setPhone("987654321");

		String json = mvc
				.perform(put(AbstractConstants.Customer.UPDATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		CustomerUpdateDto response = mapper.readValue(json, CustomerUpdateDto.class);

		// verify result
		assertNotNull(response);

	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 0123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateDifferentIdCustomer_shouldNotReturnEntityWithIdNull()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		CustomerUpdateDto dto = new CustomerUpdateDto();

		// prepare inputs
		dto.setId(50L);
		dto.setEmail("b@b.fr");
		dto.setLastName("Jean");
		dto.setFirstName("marc");
		dto.setAdress("adr");
		dto.setZipCode(145);
		dto.setCity("lyon");
		dto.setPwd("pmloik");
		dto.setPhone("987654321");

		String json = mvc
				.perform(put(AbstractConstants.Customer.UPDATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		// verify result
		assertEquals("", json);

	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 0123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteValidCustomer_shouldReturnTrue()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		CustomerUpdateDto dto = new CustomerUpdateDto();

		dto.setId(1L);

		String json = mvc
				.perform(delete(AbstractConstants.Customer.DELETE_ID, 1L).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("true", json);

	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'v@v.fr','Murray', 'adresse', 0123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteNotExistingCustomer_shouldReturnTrue()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		CustomerUpdateDto dto = new CustomerUpdateDto();

		dto.setId(145L);

		String json = mvc
				.perform(delete(AbstractConstants.Customer.DELETE_ID, 145L).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("false", json);

	}

	@Test
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void EmptyListCustomer_shouldReturnEmptyList()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		String json = mvc.perform(get(AbstractConstants.Customer.READ_ALL_REAL).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("[]", json);
	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 0123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void ListValidCustomer_shouldNotReturnEmptyList() throws UnsupportedEncodingException, Exception {

		String json = mvc.perform(get(AbstractConstants.Customer.READ_ALL_REAL).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].email", is("a@a.fr"))).andReturn().getResponse().getContentAsString();

		List<CustomerUpdateDto> response = mapper.readValue(json, new TypeReference<List<CustomerUpdateDto>>() {
		});

		assertEquals(1, response.size());

	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 0123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void setDeletedTrueExistingCustomer_shouldReturnTrue()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		CustomerUpdateDto dto = new CustomerUpdateDto();

		String json = mvc
				.perform(put(AbstractConstants.Customer.SET_DELETED_TRUE, 1L).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("true", json);

	}

	@Test
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void setDeletedTrueNotExistingCustomer_shouldReturnFalse()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		CustomerUpdateDto dto = new CustomerUpdateDto();

		String json = mvc
				.perform(put(AbstractConstants.Customer.SET_DELETED_TRUE, 15L).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("false", json);

	}

	@Test
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void setDeletedNotExistingCustomer_shouldReturnFalse()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		CustomerUpdateDto dto = new CustomerUpdateDto();

		String json = mvc
				.perform(put(AbstractConstants.Customer.SET_DELETED_TRUE, 15L).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("false", json);

	}

	@Test
	@Sql(statements = { "delete from customer",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 0123, 'city', 'azerty', false)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (2, 'b@b.fr','Murray', 'adresse', 0123, 'lyon', 'azerty', true)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (3, 'c@c.fr','Murray', 'adresse', 0123, 'roanne', 'azerty', true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void List2SetDeletedTrueAnd1SetDeletedFalse_shouldReturnAListWith2Elements()
			throws UnsupportedEncodingException, Exception {

		String json = mvc.perform(get(AbstractConstants.Customer.READ_DELETE).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].city", is("lyon"))).andExpect(jsonPath("$[1].city", is("roanne"))).andReturn()
				.getResponse().getContentAsString();

		List<CustomerUpdateDto> response = mapper.readValue(json, new TypeReference<List<CustomerUpdateDto>>() {
		});

		assertEquals(2, response.size());

	}

	@Test
	@Sql(statements = { "delete from customer",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 0123, 'city', 'azerty', false)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (2, 'b@b.fr','Murray', 'adresse', 0123, 'city', 'azerty', true)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (3, 'c@c.fr','Murray', 'adresse', 0123, 'city', 'azerty', true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void List2SetDeletedTrueAnd1SetDeletedFalse_shouldReturnAListWith1Element()
			throws UnsupportedEncodingException, Exception {

		String json = mvc.perform(get(AbstractConstants.Customer.READ_ALL).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].city", is("city"))).andReturn().getResponse().getContentAsString();

		List<CustomerUpdateDto> response = mapper.readValue(json, new TypeReference<List<CustomerUpdateDto>>() {
		});

		assertEquals(1, response.size());
	}

	@Test
	@Sql(statements = { "delete from customer",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'rue', 0123, 'city', 'azerty', false)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (2, 'b@b.fr','Murray', 'rue', 0123, 'city', 'azerty', true)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (3, 'c@c.fr','Jean', 'adresse', 0123, 'city', 'azerty', true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void ListOfCustomer_shouldReturnListOfCustomerByTheirLastName()
			throws UnsupportedEncodingException, Exception {

		String json = mvc
				.perform(get(AbstractConstants.Customer.GET_LASTNAME, "Jean").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].email", is("c@c.fr"))).andReturn().getResponse().getContentAsString();

		List<CustomerUpdateDto> response = mapper.readValue(json, new TypeReference<List<CustomerUpdateDto>>() {
		});

		assertEquals(1, response.size());
	}

	@Test
	@Sql(statements = { "delete from customer",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'rue', 456, 'city', 'azerty', false)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (2, 'b@b.fr','Murray', 'rue', 456, 'city', 'azerty', true)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (3, 'c@c.fr','Murray', 'adresse', 145, 'city', 'azerty', true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void ListOfCustomer_shouldReturnListOfCustomerByTheirZipCode()
			throws UnsupportedEncodingException, Exception {

		String json = mvc.perform(get(AbstractConstants.Customer.GET_ZIPCODE, 456L).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].email", is("a@a.fr"))).andExpect(jsonPath("$[1].email", is("b@b.fr")))
				.andReturn().getResponse().getContentAsString();

		List<CustomerUpdateDto> response = mapper.readValue(json, new TypeReference<List<CustomerUpdateDto>>() {
		});

		assertEquals(2, response.size());

	}

	@Test
	@Sql(statements = { "delete from customer",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'rue', 0123, 'city', 'azerty', false)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (2, 'b@b.fr','Murray', 'rue', 0123, 'city', 'azerty', true)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (3, 'c@c.fr','Murray', 'adresse', 0123, 'lyon', 'azerty', true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void ListOfCustomer_shouldReturnListOfCustomerByTheirCity() throws UnsupportedEncodingException, Exception {

		String json = mvc.perform(get(AbstractConstants.Customer.GET_CITY, "lyon").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].email", is("c@c.fr"))).andReturn().getResponse().getContentAsString();

		List<CustomerUpdateDto> response = mapper.readValue(json, new TypeReference<List<CustomerUpdateDto>>() {
		});

		assertEquals(1, response.size());
	}

	@Test
	@Sql(statements = { "delete from customer",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'rue', 123, 'city', 'azerty', false)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (2, 'b@b.fr','Murray', 'rue', 123, 'city', 'azerty', true)",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (3, 'c@c.fr','Murray', 'adresse', 123, 'city', 'azerty', true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void ListOfCustomer_shouldReturnListOfCustomerByTheirAdress()
			throws UnsupportedEncodingException, Exception {

		String json = mvc.perform(get(AbstractConstants.Customer.GET_ADRESS, "rue").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].email", is("a@a.fr"))).andExpect(jsonPath("$[1].email", is("b@b.fr")))
				.andReturn().getResponse().getContentAsString();

		List<CustomerUpdateDto> response = mapper.readValue(json, new TypeReference<List<CustomerUpdateDto>>() {
		});

		assertEquals(2, response.size());

	}

	@Test
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testFalsePatternEmail_shouldReturnNullAndNoNewCustomer()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		// prepare inputs
		CustomerCreateDto dto = new CustomerCreateDto();

		// prepare inputs
		dto.setEmail("lapin");
		dto.setLastName("Murray");
		dto.setFirstName("billy");
		dto.setAdress("adresse");
		dto.setZipCode(123);
		dto.setCity("city");
		dto.setPwd("azerty");
		dto.setPhone("012345678");

		String json = mvc
				.perform(post(AbstractConstants.Customer.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", json);

	}

	@Test
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testNullEmail_shouldReturnNullAndNoNewCustomer()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		// prepare inputs
		CustomerCreateDto dto = new CustomerCreateDto();

		// prepare inputs
		dto.setEmail(null);
		dto.setLastName("Murray");
		dto.setFirstName("billy");
		dto.setAdress("adresse");
		dto.setZipCode(123);
		dto.setCity("city");
		dto.setPwd("azerty");
		dto.setPhone("012345678");

		String json = mvc
				.perform(post(AbstractConstants.Customer.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", json);

	}

	@Test
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testBlankEmail_shouldReturnNullAndNoNewCustomer()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		// prepare inputs
		CustomerCreateDto dto = new CustomerCreateDto();

		// prepare inputs
		dto.setEmail("    ");
		dto.setLastName("Murray");
		dto.setFirstName("billy");
		dto.setAdress("adresse");
		dto.setZipCode(123);
		dto.setCity("city");
		dto.setPwd("azerty");
		dto.setPhone("012345678");

		String json = mvc
				.perform(post(AbstractConstants.Customer.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", json);

	}

	@Test
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testNullLastName_shouldReturnNullAndNoNewCustomer()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		// prepare inputs
		CustomerCreateDto dto = new CustomerCreateDto();

		// prepare inputs
		dto.setEmail("a@a.fr");
		dto.setLastName(null);
		dto.setFirstName("billy");
		dto.setAdress("adresse");
		dto.setZipCode(123);
		dto.setCity("city");
		dto.setPwd("azerty");
		dto.setPhone("012345678");

		String json = mvc
				.perform(post(AbstractConstants.Customer.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", json);
	}

	@Test
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testNullAdress_shouldReturnNullAndNoNewCustomer()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		// prepare inputs
		CustomerCreateDto dto = new CustomerCreateDto();

		// prepare inputs
		dto.setEmail("a@a.fr");
		dto.setLastName("Murray");
		dto.setFirstName("billy");
		dto.setAdress(null);
		dto.setZipCode(123);
		dto.setCity("city");
		dto.setPwd("azerty");
		dto.setPhone("012345678");

		String json = mvc
				.perform(post(AbstractConstants.Customer.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", json);
	}

	@Test
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testNullZipCode_shouldReturnNullAndNoNewCustomer()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		// prepare inputs
		CustomerCreateDto dto = new CustomerCreateDto();

		// prepare inputs
		dto.setEmail("a@a.fr");
		dto.setLastName("Murray");
		dto.setFirstName("billy");
		dto.setAdress("adresse");
		dto.setZipCode(null);
		dto.setCity("city");
		dto.setPwd("azerty");
		dto.setPhone("012345678");

		String json = mvc
				.perform(post(AbstractConstants.Customer.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", json);
	}

	@Test
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testNullCity_shouldReturnNullAndNoNewCustomer()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		// prepare inputs
		CustomerCreateDto dto = new CustomerCreateDto();

		// prepare inputs
		dto.setEmail("a@a.fr");
		dto.setLastName("Murray");
		dto.setFirstName("billy");
		dto.setAdress("adresse");
		dto.setZipCode(123);
		dto.setCity(null);
		dto.setPwd("azerty");
		dto.setPhone("012345678");

		String json = mvc
				.perform(post(AbstractConstants.Customer.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", json);
	}

	@Test
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testNullPwd_shouldReturnNullAndNoNewCustomer()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		// prepare inputs
		CustomerCreateDto dto = new CustomerCreateDto();

		// prepare inputs
		dto.setEmail("a@a.fr");
		dto.setLastName("Murray");
		dto.setFirstName("billy");
		dto.setAdress("adresse");
		dto.setZipCode(123);
		dto.setCity("city");
		dto.setPwd(null);
		dto.setPhone("012345678");

		String json = mvc
				.perform(post(AbstractConstants.Customer.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", json);
	}

	@Test
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testEmptyPwd_shouldReturnNullAndNoNewCustomer()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		// prepare inputs
		CustomerCreateDto dto = new CustomerCreateDto();

		// prepare inputs
		dto.setEmail("a@a.fr");
		dto.setLastName("Murray");
		dto.setFirstName("billy");
		dto.setAdress("adresse");
		dto.setZipCode(123);
		dto.setCity("city");
		dto.setPwd("");
		dto.setPhone("012345678");

		String json = mvc
				.perform(post(AbstractConstants.Customer.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", json);
	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void loginCustomer_shouldReturnSuccesForLogin()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		// prepare inputs
		CustomerLoginDto dto = new CustomerLoginDto();

		// prepare inputs
		dto.setEmail("a@a.fr");
		dto.setPwd("azerty");

		String json = mvc
				.perform(post(AbstractConstants.Customer.LOGIN).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		CustomerCreateDto response = mapper.readValue(json, CustomerCreateDto.class);

		assertNotNull(response);
		assertThat(response).hasFieldOrPropertyWithValue("email", "a@a.fr");
		assertThat(response).hasFieldOrPropertyWithValue("pwd", "azerty");

	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void loginCustomerWithFalsePatternEmail_shouldReturnFailForLogin()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		// prepare inputs
		CustomerLoginDto dto = new CustomerLoginDto();

		// prepare inputs
		dto.setEmail("test");
		dto.setPwd("azerty");

		String json = mvc
				.perform(post(AbstractConstants.Customer.LOGIN).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", json);

	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void loginCustomerWithNullEmail_shouldReturnFailForLogin()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		// prepare inputs
		CustomerLoginDto dto = new CustomerLoginDto();

		// prepare inputs
		dto.setEmail(null);
		dto.setPwd("azerty");

		String json = mvc
				.perform(post(AbstractConstants.Customer.LOGIN).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", json);

	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void loginCustomerWithBlankEmail_shouldReturnFailForLogin()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		// prepare inputs
		CustomerLoginDto dto = new CustomerLoginDto();

		// prepare inputs
		dto.setEmail("   ");
		dto.setPwd("azerty");

		String json = mvc
				.perform(post(AbstractConstants.Customer.LOGIN).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", json);

	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void loginCustomerWithNullPwd_shouldReturnFailForLogin()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		// prepare inputs
		CustomerLoginDto dto = new CustomerLoginDto();

		// prepare inputs
		dto.setEmail("a@a.fr");
		dto.setPwd(null);

		String json = mvc
				.perform(post(AbstractConstants.Customer.LOGIN).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", json);

	}

	@Test
	@Sql(statements = "insert into customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 123, 'city', 'azerty', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void loginCustomerWithEmptyPwd_shouldReturnFailForLogin()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		// prepare inputs
		CustomerLoginDto dto = new CustomerLoginDto();

		// prepare inputs
		dto.setEmail("a@a.fr");
		dto.setPwd("");

		String json = mvc
				.perform(post(AbstractConstants.Customer.LOGIN).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", json);

	}
}
