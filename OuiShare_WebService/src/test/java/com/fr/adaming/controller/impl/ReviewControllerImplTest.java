package com.fr.adaming.controller.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fr.adaming.OuiShareWebServiceApplicationTest;
import com.fr.adaming.constants.AbstractConstants;
import com.fr.adaming.dto.ReviewCreateDto;
import com.fr.adaming.dto.ReviewUpdateDto;


/**
 * @author Dylan
 *
 */
public class ReviewControllerImplTest extends OuiShareWebServiceApplicationTest {

	
//	private static final String BASE_PATH = "/api/review/";
	

	@Test
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveValidReviewDto_shouldReturnReviewDto() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		//prepare inputs
		ReviewCreateDto r = new ReviewCreateDto();
		r.setRating(2);
		r.setComment("fxgfggh");
		r.setDeleted(false);
		
		String bodyAsJson = mvc.perform(
				post(AbstractConstants.Review.CREATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(r))
				)
				.andExpect(status().isOk())
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		ReviewCreateDto response = mapper.readValue(bodyAsJson, ReviewCreateDto.class);
				
				assertNotNull(response);
				assertThat(response).hasFieldOrPropertyWithValue("rating", 2);
				assertThat(response).hasFieldOrPropertyWithValue("comment", "fxgfggh");
				assertThat(response).hasFieldOrPropertyWithValue("deleted", false);
			
				
		
	}
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (2,2,'dfgf',false)")
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveExistingReviewDto_shouldReturnEmpty() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		//prepare inputs
		ReviewCreateDto r = new ReviewCreateDto();
		
		r.setRating(2);
		r.setComment("dfgf");
		r.setDeleted(false);
		
		String bodyAsJson = mvc.perform(
				post(AbstractConstants.Review.CREATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(r))
				)
				.andExpect(status().isOk())
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		
				
				assertEquals("", bodyAsJson);
				
			
				
		
	}
	
	@Test
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveValidReviewDtoWithAMinRating_shouldReturnReviewDto() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		//prepare inputs
		ReviewCreateDto r = new ReviewCreateDto();
		r.setRating(0);
		r.setComment("fxgfggh");
		r.setDeleted(false);
		
		String bodyAsJson = mvc.perform(
				post(AbstractConstants.Review.CREATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(r))
				)
				.andExpect(status().isOk())
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		ReviewCreateDto response = mapper.readValue(bodyAsJson, ReviewCreateDto.class);
				
				assertNotNull(response);
				assertThat(response).hasFieldOrPropertyWithValue("rating", 0);
				assertThat(response).hasFieldOrPropertyWithValue("comment", "fxgfggh");
				assertThat(response).hasFieldOrPropertyWithValue("deleted", false);
			
				
		
	}
	
	@Test
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveValidReviewDtoWithAMaxRating_shouldReturnReviewDto() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		//prepare inputs
		ReviewCreateDto r = new ReviewCreateDto();
		r.setRating(5);
		r.setComment("fxgfggh");
		r.setDeleted(false);
		
		String bodyAsJson = mvc.perform(
				post(AbstractConstants.Review.CREATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(r))
				)
				.andExpect(status().isOk())
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		ReviewCreateDto response = mapper.readValue(bodyAsJson, ReviewCreateDto.class);
				
				assertNotNull(response);
				assertThat(response).hasFieldOrPropertyWithValue("rating", 5);
				assertThat(response).hasFieldOrPropertyWithValue("comment", "fxgfggh");
				assertThat(response).hasFieldOrPropertyWithValue("deleted", false);
			
				
		
	}
	
	@Test	
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveReviewDtoWithANegativeRating_shouldReturnError400() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		//prepare inputs
		ReviewCreateDto r = new ReviewCreateDto();
		
		r.setRating(-20);
		r.setComment("dfgf");
		r.setDeleted(false);
		
		String bodyAsJson = mvc.perform(
				post(AbstractConstants.Review.CREATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(r))
				)
				.andExpect(status().is(400))
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		
				
				assertEquals("", bodyAsJson);
				
			
				
		
	}
	
	@Test	
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void saveReviewDtoWithAWrongRating_shouldReturnError400() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		//prepare inputs
		ReviewCreateDto r = new ReviewCreateDto();
		
		r.setRating(10);
		r.setComment("dfgf");
		r.setDeleted(false);
		
		String bodyAsJson = mvc.perform(
				post(AbstractConstants.Review.CREATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(r))
				)
				.andExpect(status().is(400))
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		
				
				assertEquals("", bodyAsJson);
				
			
				
		
	}
	
	
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (3,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateExistingReviewDto_shouldReturnReviewDtoWithNotNullId() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		ReviewUpdateDto r = new ReviewUpdateDto();
		r.setId(3L);
		r.setRating(4);
		r.setComment("aaaa");
		r.setDeleted(true);
		
		
				
		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Review.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(r))
				)
				.andExpect(status().isOk())
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		ReviewUpdateDto response = mapper.readValue(bodyAsJson, ReviewUpdateDto.class);
				
				assertNotNull(response);
				assertNotNull(response.getId());
				assertThat(response).hasFieldOrPropertyWithValue("rating", 4);
				assertThat(response).hasFieldOrPropertyWithValue("comment", "aaaa");
				assertThat(response).hasFieldOrPropertyWithValue("deleted", true);
		
	}
	
	
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (10,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateDifferentIdReviewDto_shouldReturnEmpty() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		ReviewUpdateDto r = new ReviewUpdateDto();
		r.setId(100L);
		r.setRating(4);
		r.setComment("bbbb");
		r.setDeleted(true);
		
		
		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Review.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(r))
				)
				.andExpect(status().isOk())
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		
				
				assertEquals("", bodyAsJson);
		
		
		
	
		
	}
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (3,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateExistingReviewDtoWithAMinRating_shouldReturnReviewDtoWithNotNullId() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		ReviewUpdateDto r = new ReviewUpdateDto();
		r.setId(3L);
		r.setRating(0);
		r.setComment("aaaa");
		r.setDeleted(true);
		
		
				
		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Review.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(r))
				)
				.andExpect(status().isOk())
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		ReviewUpdateDto response = mapper.readValue(bodyAsJson, ReviewUpdateDto.class);
				
				assertNotNull(response);
				assertNotNull(response.getId());
				assertThat(response).hasFieldOrPropertyWithValue("rating", 0);
				assertThat(response).hasFieldOrPropertyWithValue("comment", "aaaa");
				assertThat(response).hasFieldOrPropertyWithValue("deleted", true);
		
	}
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (3,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateExistingReviewDtoWithAMaxRating_shouldReturnReviewDtoWithNotNullId() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		ReviewUpdateDto r = new ReviewUpdateDto();
		r.setId(3L);
		r.setRating(5);
		r.setComment("aaaa");
		r.setDeleted(true);
		
		
				
		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Review.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(r))
				)
				.andExpect(status().isOk())
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		ReviewUpdateDto response = mapper.readValue(bodyAsJson, ReviewUpdateDto.class);
				
				assertNotNull(response);
				assertNotNull(response.getId());
				assertThat(response).hasFieldOrPropertyWithValue("rating", 5);
				assertThat(response).hasFieldOrPropertyWithValue("comment", "aaaa");
				assertThat(response).hasFieldOrPropertyWithValue("deleted", true);
		
	}
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (4,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review where id=3",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateReviewDtoWithNegativeRating_shouldReturnError400() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		ReviewUpdateDto r = new ReviewUpdateDto();
		r.setId(4L);
		r.setRating(-100);
		r.setComment("bbbb");
		r.setDeleted(true);
		
		
		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Review.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(r))
				)
				.andExpect(status().is(400))
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		
				
				assertEquals("", bodyAsJson);
		
		
		
	
		
	}
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (4,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review where id=3",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateReviewDtoWithWrongRating_shouldReturnError400() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		ReviewUpdateDto r = new ReviewUpdateDto();
		r.setId(4L);
		r.setRating(10);
		r.setComment("bbbb");
		r.setDeleted(true);
		
		
		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Review.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(r))
				)
				.andExpect(status().is(400))
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		
				
				assertEquals("", bodyAsJson);
		
		
		
	
		
	}
	
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (4,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review where id=3",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateReviewDtoWithNullId_shouldNotReturnNUll() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		ReviewUpdateDto r = new ReviewUpdateDto();
		r.setId(null);
		r.setRating(5);
		r.setComment("bbbb");
		r.setDeleted(true);
		
		
		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Review.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(r))
				)
				.andExpect(status().isOk())
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		
				
				assertEquals("", bodyAsJson);
		
		
		
	
		
	}
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (5,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review ",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
		public void deleteValidReviewDto_shouldReturnTrue() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		

		
		
		String bodyAsJson = mvc.perform(
				delete(AbstractConstants.Review.DELETE_ID, 5L)
				.contentType(MediaType.APPLICATION_JSON)
				)
				
				.andExpect(status().isOk())
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		
				
				assertEquals("true", bodyAsJson);
		
	}
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (6,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
		public void deleteNotExistingReviewDto_shouldReturnFalse() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		

		
		
		
		String bodyAsJson = mvc.perform(
				delete(AbstractConstants.Review.DELETE_ID, 100L)
				.contentType(MediaType.APPLICATION_JSON)
				
				)
				.andExpect(status().isOk())
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		
				
				assertEquals("false", bodyAsJson);
		
	}
	

	
	
	
	@Test
	@Sql(statements = "delete from review", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void EmptyListReview_shouldReturnEmptyList() throws UnsupportedEncodingException, Exception {
	
		String bodyAsJson = mvc.perform(
				get(AbstractConstants.Review.READ_ALL_REAL, 100L)
				.contentType(MediaType.APPLICATION_JSON)
				
				)
				.andExpect(status().isOk())
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		
				
				assertEquals("[]", bodyAsJson);

}
	
	
	
	@Test
	@Sql(statements = "insert into review (id,rating,comment,deleted) values (5,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
		public void ListValidReview_shouldNotReturnEmptyList() throws Exception {
		
		
		String bodyAsJson = mvc.perform(get(AbstractConstants.Review.READ_ALL_REAL).contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$[0].rating", is(2)))
        .andExpect(jsonPath("$[0].comment", is("dfgf")))
        .andExpect(jsonPath("$[0].deleted", is(false)))
        .andExpect(status().isOk())
		.andReturn()
		.getResponse()
		.getContentAsString();
		
		
		List<ReviewUpdateDto> response = mapper.readValue(bodyAsJson, new TypeReference <List<ReviewUpdateDto>>() {});
	
		assertEquals(1, response.size());
	
}
	
	
		
		@Test
		@Sql(statements = "insert into review (id,rating,comment,deleted) values (5,2,'dfgf',false)",executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
		@Sql(statements = "delete from review ",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
			public void setDeletedExistingReviewDto_shouldReturnTrue() throws UnsupportedEncodingException, JsonProcessingException, Exception {
			
			

			
			
			
			
			String bodyAsJson = mvc.perform(
					put(AbstractConstants.Review.SET_DELETED_TRUE, 5L)
					.contentType(MediaType.APPLICATION_JSON)
					
					)
					
					.andExpect(status().isOk())
					.andReturn()
					.getResponse()
					.getContentAsString();
					
			
					
					assertEquals("true", bodyAsJson);
		
	}
	
	@Test
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
		public void setDeletedNotExistingReviewDto_shouldReturnFalse() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		

		
		
		
		
		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Review.SET_DELETED_TRUE, 5L)
				.contentType(MediaType.APPLICATION_JSON)
				
				)
				
				.andExpect(status().isOk())
				.andReturn()
				.getResponse()
				.getContentAsString();
				

		
				
				assertEquals("false", bodyAsJson);
	}
	
	

	

	
	@Test
	@Sql(statements = {"delete from review","insert into review (id,rating,comment,deleted) values (6,2,'dfgf',false)","insert into review (id,rating,comment,deleted) values (7,3,'aaaa',true)","insert into review (id,rating,comment,deleted) values (8,5,'dfgf',true)"},executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void List2SetDeletedTrueAnd1SetDeletedFalse_shouldReturnAListWith2Elements() throws Exception {
		
	
		
		
		String bodyAsJson = mvc.perform(get(AbstractConstants.Review.READ_DELETE).contentType(MediaType.APPLICATION_JSON)
				
			
				)
		.andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$[0].rating", is(3)))
        .andExpect(jsonPath("$[0].comment", is("aaaa")))
        .andExpect(jsonPath("$[0].deleted", is(true)))
        .andExpect(jsonPath("$[1].rating", is(5)))
        .andExpect(jsonPath("$[1].comment", is("dfgf")))
        .andExpect(jsonPath("$[1].deleted", is(true)))
        
		.andReturn()
		.getResponse()
		.getContentAsString();
		
		
		List<ReviewUpdateDto> response = mapper.readValue(bodyAsJson, new TypeReference <List<ReviewUpdateDto>>() {});
		
		assertEquals(2, response.size());
		
		
	}
	
	
	@Test
	@Sql(statements = {"delete from review","insert into review (id,rating,comment,deleted) values (6,2,'dfgf',false)","insert into review (id,rating,comment,deleted) values (7,3,'aaaa',false)","insert into review (id,rating,comment,deleted) values (8,5,'dfgf',false)"},executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void List0SetDeletedTrueAnd3SetDeletedFalse_shouldReturnAListWith2Elements() throws Exception {
		
	
		
		
		String bodyAsJson = mvc.perform(get(AbstractConstants.Review.READ_DELETE).contentType(MediaType.APPLICATION_JSON)
				
			
				)
		.andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))      
  		.andReturn()
		.getResponse()
		.getContentAsString();
		
		
		List<ReviewUpdateDto> response = mapper.readValue(bodyAsJson, new TypeReference <List<ReviewUpdateDto>>() {});
		
		assertTrue(response.isEmpty());
		
		
	}
	
	@Test
	@Sql(statements = {"delete from review","insert into review (id,rating,comment,deleted) values (6,2,'dfgf',false)","insert into review (id,rating,comment,deleted) values (7,2,'dfgf',true)","insert into review (id,rating,comment,deleted) values (8,2,'dfgf',true)"},executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void List2SetDeletedTrueAnd1SetDeletedFalse_shouldReturnAListWith1Element() throws UnsupportedEncodingException, Exception {
		
		String bodyAsJson = mvc.perform(get(AbstractConstants.Review.READ_ALL).contentType(MediaType.APPLICATION_JSON)
				
				
				)
		.andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$[0].rating", is(2)))
        
        
		.andReturn()
		.getResponse()
		.getContentAsString();
		
		
		List<ReviewUpdateDto> response = mapper.readValue(bodyAsJson, new TypeReference <List<ReviewUpdateDto>>() {});
		
		assertEquals(1, response.size());
	}
	
	
	@Test
	@Sql(statements = {"delete from review","insert into review (id,rating,comment,deleted) values (6,2,'dfgf',true)","insert into review (id,rating,comment,deleted) values (7,2,'dfgf',true)","insert into review (id,rating,comment,deleted) values (8,2,'dfgf',true)"},executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from review",executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void List3SetDeletedTrueAnd0SetDeletedFalse_shouldReturnAListWith1Element() throws UnsupportedEncodingException, Exception {
		
		String bodyAsJson = mvc.perform(get(AbstractConstants.Review.READ_ALL).contentType(MediaType.APPLICATION_JSON)
				
				
				)
		.andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))        
		.andReturn()
		.getResponse()
		.getContentAsString();
		
		
		List<ReviewUpdateDto> response = mapper.readValue(bodyAsJson, new TypeReference <List<ReviewUpdateDto>>() {});
		
		assertTrue(response.isEmpty());
	}
}
