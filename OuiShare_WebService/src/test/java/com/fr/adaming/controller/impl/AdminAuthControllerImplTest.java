package com.fr.adaming.controller.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fr.adaming.OuiShareWebServiceApplicationTest;
import com.fr.adaming.constants.AbstractConstants;
import com.fr.adaming.dto.AdminDtoLogin;

/**
 * @author bilel
 *
 */
public class AdminAuthControllerImplTest extends OuiShareWebServiceApplicationTest {

//	METHODE Login

	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel.abbas@gmail.com','azerty',0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testLoginAdminExisting_shouldReturnStatus200ContentNotNull() throws JsonProcessingException, Exception {

		AdminDtoLogin dto = new AdminDtoLogin();
		dto.setEmail("bilel.abbas@gmail.com");
		dto.setPwd("azerty");

		String bodyAsJson = mvc
				.perform(post(AbstractConstants.Auth.LOGIN_ADMIN).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(200)).andReturn().getResponse().getContentAsString();

		System.out.println("Le résultat est " + bodyAsJson);

		AdminDtoLogin response = mapper.readValue(bodyAsJson, AdminDtoLogin.class);

		assertNotNull(response);
		assertEquals("bilel.abbas@gmail.com", response.getEmail());
		assertEquals("azerty", response.getPwd());

	}

	@Test
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testLoginAdminInExisting_shouldReturnStatus200ContentNull() throws JsonProcessingException, Exception {

		AdminDtoLogin dto = new AdminDtoLogin();
		dto.setEmail("bilel.abbas@gmail.com");
		dto.setPwd("azerty");

		String bodyAsJson = mvc.perform(post(AbstractConstants.Auth.LOGIN_ADMIN).contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto))).andExpect(status().is(200)).andReturn().getResponse()
				.getContentAsString();
		
		assertEquals("",bodyAsJson);


	}

}
