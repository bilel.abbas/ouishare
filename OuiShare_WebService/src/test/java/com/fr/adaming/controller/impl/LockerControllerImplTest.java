package com.fr.adaming.controller.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.UnsupportedEncodingException;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fr.adaming.OuiShareWebServiceApplicationTest;
import com.fr.adaming.dto.LockerDto;
import com.fr.adaming.dto.LockerDtoUpdate;

/**
 * @author Ambroise RENE
 *
 */
public class LockerControllerImplTest extends OuiShareWebServiceApplicationTest {

	@Rule
	public ExpectedException exception = ExpectedException.none();

	private static final String BASE_PATH = "/api/locker/";

	@Test
	@Sql(statements = "delete from locker where city like'lyon'", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreatingValidLocker_shouldReturnStatus200WithContentNotNull()
			throws JsonProcessingException, Exception {
		LockerDto dto = new LockerDto();
		dto.setCity("lyon");
		dto.setName("userNameForTest");
		dto.setAdress("asdred");
		dto.setSlot(123L);
		dto.setZipCode(123456789L);

		String Json = mvc
				.perform(
						post(BASE_PATH).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		LockerDto response = mapper.readValue(Json, LockerDto.class);

		assertNotNull(response);
		assertThat(response).hasFieldOrPropertyWithValue("city", "lyon");
		assertThat(response).hasFieldOrPropertyWithValue("name", "userNameForTest");
		assertEquals(123, response.getSlot());
	}

	@Test
	@Sql(statements = "delete from locker where slot = 124", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreateNonValidLockerWithoutCity_shouldReturnStatus400() throws JsonProcessingException, Exception {
		LockerDto dto = new LockerDto();

		dto.setName("userNameForTest");
		dto.setAdress("asdress");
		dto.setSlot(124L);
		dto.setZipCode(123456789L);

		String result = mvc
				.perform(
						post(BASE_PATH).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", result);

	}

	@Test
	@Sql(statements = "delete from locker where city like 'lyon'", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreateNonValidLockerWithoutName_shouldReturnStatus400() throws JsonProcessingException, Exception {
		LockerDto dto = new LockerDto();

		dto.setCity("lyon");
		dto.setAdress("asdress");
		dto.setSlot(123L);
		dto.setZipCode(123456789L);

		String result = mvc
				.perform(
						post(BASE_PATH).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", result);

	}

	@Test
	@Sql(statements = "delete from locker where name like 'little'", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreateNonValidLockerWithoutAdress_shouldReturnStatus400()
			throws JsonProcessingException, Exception {
		LockerDto dto = new LockerDto();

		dto.setName("little");
		dto.setCity("lyon");
		dto.setSlot(123L);
		dto.setZipCode(123456789L);

		String result = mvc
				.perform(
						post(BASE_PATH).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", result);

	}

	@Test
	@Sql(statements = "delete from locker where name like 'little-name'", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreateNonValidLockerWithoutZipCode_shouldReturnStatus400()
			throws JsonProcessingException, Exception {
		LockerDto dto = new LockerDto();

		dto.setName("little-name");
		dto.setAdress("adress");
		dto.setCity("lyon");
		dto.setSlot(123L);

		String result = mvc
				.perform(
						post(BASE_PATH).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", result);

	}

	@Test
	@Sql(statements = "delete from locker where city like 'lyon'", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreateNonValidLockerWithoutSlot_shouldReturnStatus400() throws JsonProcessingException, Exception {
		LockerDto dto = new LockerDto();

		dto.setName("little name");
		dto.setCity("lyon");
		dto.setAdress("address");
		dto.setZipCode(123456789L);

		String result = mvc
				.perform(
						post(BASE_PATH).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", result);

	}

	@Test
	@Sql(statements = "delete from locker where city like 'lyon'", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreateNonValidLockerWithNegativeSlot_shouldReturnStatus400()
			throws JsonProcessingException, Exception {
		LockerDto dto = new LockerDto();

		dto.setName("little name");
		dto.setCity("lyon");
		dto.setAdress("address");
		dto.setSlot(-123L);
		dto.setZipCode(123456789L);

		String result = mvc
				.perform(
						post(BASE_PATH).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", result);

	}

	@Test
	@Sql(statements = "insert into locker(id,deleted,adress,city,name,slot,zip_code) values (111,false,'11 rue des oliviers','lyon','toto',123,123456789)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id = 111", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testUpdateGoodLocker_shouldReturnStatus200() throws UnsupportedEncodingException, Exception {

		LockerDtoUpdate dto = new LockerDtoUpdate();
		dto.setId(111L);
		dto.setName("name");
		dto.setCity("paris");
		dto.setAdress("address");
		dto.setSlot(500L);
		dto.setZipCode(987654321L);
		dto.setDeleted(false);

		String result = mvc
				.perform(
						post(BASE_PATH).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		LockerDtoUpdate dtoResult = mapper.readValue(result, LockerDtoUpdate.class);

		assertNotNull(dtoResult);
		assertEquals(500L, dtoResult.getSlot(), 0.001);
		assertEquals(987654321L, dtoResult.getZipCode(), 0.001);
		assertEquals("name", dtoResult.getName());
		assertEquals("paris", dtoResult.getCity());
		assertEquals("address", dtoResult.getAdress());
	}

	@Test
	@Sql(statements = "insert into locker(id,deleted,adress,city,name,slot,zip_code) values (112,false,'11 rue des oliviers','lyon','tdcd',123,123456789)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id = 112", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testUpdateLockerwithoutName_shouldReturnStatus400() throws UnsupportedEncodingException, Exception {

		LockerDtoUpdate dto = new LockerDtoUpdate();

		dto.setId(112L);
		dto.setCity("paris");
		dto.setAdress("address");
		dto.setSlot(500L);
		dto.setZipCode(987654321L);

		String result = mvc
				.perform(
						post(BASE_PATH).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", result);
	}

	@Test
	@Sql(statements = "insert into locker(id,deleted,adress,city,name,slot,zip_code) values (112,false,'11 rue des oliviers','tamtam','toto',123,123456789)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id = 112", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testUpdateLockerwithoutAdress_shouldReturnStatus400() throws UnsupportedEncodingException, Exception {

		LockerDtoUpdate dto = new LockerDtoUpdate();

		dto.setId(112L);
		dto.setName("name");
		dto.setCity("paris");
		dto.setSlot(500L);
		dto.setZipCode(987654321L);

		String result = mvc
				.perform(
						post(BASE_PATH).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", result);
	}

	@Test
	@Sql(statements = "insert into locker(id,deleted,adress,city,name,slot,zip_code) values (112,false,'11 rue des oliviers','lyon','timtom',123,123456789)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id = 112", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testUpdateLockerwithoutSlot_shouldReturnStatus400() throws UnsupportedEncodingException, Exception {

		LockerDtoUpdate dto = new LockerDtoUpdate();

		dto.setId(112L);
		dto.setName("name");
		dto.setAdress("address");
		dto.setCity("paris");
		dto.setZipCode(987654321L);

		String result = mvc
				.perform(
						post(BASE_PATH).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", result);
	}

	@Test
	@Sql(statements = "insert into locker(id,deleted,adress,city,name,slot,zip_code) values (112,false,'11 rue des oliviers','lyon','tutute',123,123456789)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id = 112", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testUpdateLockerwithNegativeSlot_shouldReturnStatus400()
			throws UnsupportedEncodingException, Exception {

		LockerDtoUpdate dto = new LockerDtoUpdate();

		dto.setId(112L);
		dto.setName("name");
		dto.setAdress("address");
		dto.setCity("paris");
		dto.setSlot(-500L);
		dto.setZipCode(987654321L);

		String result = mvc
				.perform(
						post(BASE_PATH).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", result);
	}

	@Test
	@Sql(statements = "insert into locker(id,deleted,adress,city,name,slot,zip_code) values (112,false,'11 rue des oliviers','lyon','tete',123,123456789)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id = 112", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testUpdateLockerwithoutCity_shouldReturnStatus400() throws UnsupportedEncodingException, Exception {

		LockerDtoUpdate dto = new LockerDtoUpdate();

		dto.setId(112L);
		dto.setName("name");
		dto.setAdress("address");
		dto.setSlot(500L);
		dto.setZipCode(987654321L);

		String result = mvc
				.perform(
						post(BASE_PATH).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", result);
	}

	@Test
	@Sql(statements = "insert into locker(id,deleted,adress,city,name,slot,zip_code) values (112,false,'11 rue des oliviers','lyon','tutut',123,123456789)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id = 112", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testUpdateLockerwithoutZipCode_shouldReturnStatus400() throws UnsupportedEncodingException, Exception {

		LockerDtoUpdate dto = new LockerDtoUpdate();

		dto.setId(112L);
		dto.setName("name");
		dto.setAdress("address");
		dto.setSlot(500L);
		dto.setCity("paris");

		String result = mvc
				.perform(
						post(BASE_PATH).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", result);
	}

	@Test
	@Sql(statements = "insert into locker(id,deleted,adress,city,name,slot,zip_code) values (222,false,'11 rue des oliviers','lyon','tonton',123,123456789)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id = 222", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readByIdWithGoodId_shouldReturnStatus200() throws UnsupportedEncodingException, Exception {

		String result = mvc.perform(get(BASE_PATH + "222").contentType(MediaType.APPLICATION_JSON)).andReturn()
				.getResponse().getContentAsString();

		LockerDtoUpdate response = mapper.readValue(result, LockerDtoUpdate.class);

		assertNotNull(response);
//		assertEquals("(id=222, adress=11 rue des oliviers, zipCode=123456789, city=lyon, slot=123, name=toto, deleted=false)",response);
		assertEquals("lyon", response.getCity());
		assertEquals("tonton", response.getName());
		assertEquals(123456789, response.getZipCode());
	}

	@Test
	@Sql(statements = "insert into locker(id,deleted,adress,city,name,slot,zip_code) values (223,false,'11 rue des oliviers','lyon','tnt',123,123456789)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id = 223", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readByIdWithWrongId_shouldReturnSError() throws UnsupportedEncodingException, Exception {

		String result = mvc.perform(get(BASE_PATH + "224").contentType(MediaType.APPLICATION_JSON)).andReturn()
				.getResponse().getContentAsString();

		assertEquals("", result);
	}

	@Test
	@Sql(statements = "insert into locker(id,deleted,adress,city,name,slot,zip_code) values (333,false,'11 rue des oliviers','lyon','toto',123,123456789)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id = 333", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "insert into locker(id,deleted,adress,city,name,slot,zip_code) values (334,false,'11 rue des oliviers','lyon','tata',123,123456789)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id = 334", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllLocker_shouldreturnList() throws UnsupportedEncodingException, Exception {
		String result = mvc.perform(get(BASE_PATH).contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse()
				.getContentAsString();

		assertNotNull(result);
		assertEquals(
				"[{\"adress\":\"11 rue des oliviers\",\"zipCode\":123456789,\"city\":\"lyon\",\"slot\":123,\"name\":\"toto\",\"deleted\":false},{\"adress\":\"11 rue des oliviers\",\"zipCode\":123456789,\"city\":\"lyon\",\"slot\":123,\"name\":\"tata\",\"deleted\":false}]",
				result);
		
	}

	@Test
	@Sql(statements = "insert into locker(id,deleted,adress,city,name,slot,zip_code) values (444,false,'11 rue des oliviers','lyon','tutu',123,123456789)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id = 444", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void fakeDeleteValidLocker_shouldReturnTrue() throws UnsupportedEncodingException, Exception {

		// invoquer la methode
		String result = mvc.perform(delete(BASE_PATH + 444 + "/fake").contentType(MediaType.APPLICATION_JSON))
				.andReturn().getResponse().getContentAsString();

		assertEquals("true", result);
	}

	@Test
	public void fakeDeleteLockerWithWrongId_shouldReturnEmpty() throws UnsupportedEncodingException, Exception {

		// invoquer la methode
		String result = mvc.perform(post(BASE_PATH + 444 + "/fake").contentType(MediaType.APPLICATION_JSON)).andReturn()
				.getResponse().getContentAsString();

		assertTrue(result.isEmpty());
	}

	@Test
	@Sql(statements = "insert into locker(id,deleted,adress,city,name,slot,zip_code) values (555,false,'11 rue des oliviers','lyon','titi',123,123456789)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from locker where id = 555", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteValidLocker_shouldReturntrue() throws UnsupportedEncodingException, Exception {

		// invoquer la methode
		String result = mvc.perform(delete(BASE_PATH + 555).contentType(MediaType.APPLICATION_JSON)).andReturn()
				.getResponse().getContentAsString();

		assertEquals("true", result);
	}

	@Test
	public void deleteLockerWithWrongId_shouldReturnNothing() throws UnsupportedEncodingException, Exception {

		// invoquer la methode
		String result = mvc.perform(post(BASE_PATH + 556).contentType(MediaType.APPLICATION_JSON)).andReturn()
				.getResponse().getContentAsString();

		assertTrue(result.isEmpty());
	}
}
