package com.fr.adaming.controller.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.UnsupportedEncodingException;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.adaming.OuiShareWebServiceApplicationTest;
import com.fr.adaming.constants.AbstractConstants;

/**
 * @author Nicolas RUFFIER
 *
 */
public class RentalControllerImplTest2 extends OuiShareWebServiceApplicationTest{

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 30000101, 30000131, 0, 1, 1)",
					"insert into review (id, deleted, rating) values(1, 0, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addReviewValid_shouldReturnStatus200() throws Exception {
		String result = mvc.perform(
				put(AbstractConstants.Rental.ADD_REVIEW, 1L, 1L)
				).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
		
		assertEquals("SUCCES", result);
	}
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 30000101, 30000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addReviewInvalid_shouldReturnStatus400() throws Exception {
		String result = mvc.perform(
				put(AbstractConstants.Rental.ADD_REVIEW, 1L, 1L)
				).andExpect(status().is(400)).andReturn().getResponse().getContentAsString();
		
		assertEquals("FAIL", result);
	}
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, NOW() - INTERVAL 1 HOUR, NOW() + INTERVAL 1 HOUR, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addEndDateRealValid_shouldreturnStatus200andLong0() throws UnsupportedEncodingException, Exception {
		String result = mvc.perform(
				put(AbstractConstants.Rental.END, 1L)
				).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
		
		assertEquals("0", result);
	}
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, NOW() - INTERVAL 1 HOUR, NOW() - INTERVAL 5 MINUTE, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addEndDateRealLate_shouldreturnStatus200andLong5() throws UnsupportedEncodingException, Exception {
		String result = mvc.perform(
				put(AbstractConstants.Rental.END, 1L)
				).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
		
		assertEquals("5", result);
	}
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, NOW() + INTERVAL 1 HOUR, NOW() + INTERVAL 2 HOUR, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addEndDateRealBeforeStartDate_shouldreturnStatus400andNull() throws UnsupportedEncodingException, Exception {
		String result = mvc.perform(
				put(AbstractConstants.Rental.END, 1L, 1L)
				).andExpect(status().is(400)).andReturn().getResponse().getContentAsString();
		
		assertEquals("", result);
	}
	
	
	
	
}
