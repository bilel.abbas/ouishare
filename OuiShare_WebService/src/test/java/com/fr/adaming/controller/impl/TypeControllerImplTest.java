package com.fr.adaming.controller.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.web.util.NestedServletException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fr.adaming.OuiShareWebServiceApplicationTest;
import com.fr.adaming.constants.AbstractConstants;
import com.fr.adaming.dto.TypeDtoCreate;
import com.fr.adaming.dto.TypeDtoUpdate;

/**
 * @author Bilel, Ambroise and Aurélien
 *
 */
public class TypeControllerImplTest extends OuiShareWebServiceApplicationTest {


//	private static final String BASE_PATH = "/api/type/";
	
	@Rule
	public ExpectedException exceptionRule = ExpectedException.none();

	@Test
	@Sql(statements = "delete from type", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void saveValidType_shouldReturnStatus200AndEntityWithNotNullId()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		TypeDtoCreate type = new TypeDtoCreate();
		type.setName("aspirateur");
		type.setStock(30);

		String bodyAsJson = mvc
				.perform(post(AbstractConstants.Type.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(type)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		TypeDtoUpdate response = mapper.readValue(bodyAsJson, TypeDtoUpdate.class);

		assertNotNull(response);
		assertThat(response).hasFieldOrPropertyWithValue("name", "aspirateur");
		assertThat(response).hasFieldOrPropertyWithValue("stock", 30);
	}

	@Test
	@Sql(statements = "Delete from type", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void saveTypeWithInvalidNumber_shouldReturnStatus400AndNull()
			throws UnsupportedEncodingException, Exception {

		TypeDtoCreate dto = new TypeDtoCreate();
		dto.setName("chocapic");
		dto.setStock(-123);

		String result = mvc
				.perform(post(AbstractConstants.Type.CREATE).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", result);
	}


	@Test
	@Sql(statements = { "delete from type",
	"Insert into type (id,name,stock,deleted) values (13,'Aspirateur',30,false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void saveTypeWithAlreadyExistingName_shouldThrowException()
			throws UnsupportedEncodingException, Exception {
		
		TypeDtoCreate dto = new TypeDtoCreate();
		dto.setName("Aspirateur");
		dto.setStock(123);
		
		assertThrows(NestedServletException.class,()->{mvc.perform(post(AbstractConstants.Type.CREATE).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)));});

	}

	@Test
	@Sql(statements = { "delete from type",
			"Insert into type (id,name,stock,deleted) values (13,'Aspirateur',30,false),(14,'Perceuse',40,false),(15,'tondeuse',15,true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void readAllTypes_shouldReturnStatus200AndEntities()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		String bodyAsJson = mvc.perform(get(AbstractConstants.Type.READ_ALL_REAL)).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();

		List<TypeDtoUpdate> response = mapper.readValue(bodyAsJson, new TypeReference<List<TypeDtoUpdate>>() {
		});

		// Only not deleted types are returned
		assertEquals(2,response.size());

		assertEquals("Aspirateur",response.get(0).getName());
		assertEquals(30,response.get(0).getStock());
		assertEquals(false,response.get(0).isDeleted());

		assertEquals("Perceuse",response.get(1).getName());
		assertEquals(40,response.get(1).getStock());
		assertEquals(false,response.get(1).isDeleted());

	}

	@Test
	@Sql(statements = { "delete from type",
			"Insert into type (id,name,stock,deleted) values (13,'Aspirateur',30,false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void readValidType_shouldReturnStatus200AndEntity()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		String bodyAsJson = mvc.perform(get(AbstractConstants.Type.READ_ID,13L)).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();

		TypeDtoUpdate response = mapper.readValue(bodyAsJson, TypeDtoUpdate.class);

		assertEquals("Aspirateur",response.getName());
		assertEquals(30,response.getStock());
		assertEquals(false,response.isDeleted());

	}

	@Test
	@Sql(statements = { "delete from type" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void readNotValidIdType_shouldReturnStatus200AndNull()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		String bodyAsJson = mvc.perform(get(AbstractConstants.Type.READ_ID,13L)).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();

		assertEquals("",bodyAsJson);
	}

	@Sql(statements = "insert into type (id,deleted, name, stock) values (111,false,'polo',2)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from type where id = 111", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testUpdateGoodType_shouldReturnStatus200() throws UnsupportedEncodingException, Exception {

		TypeDtoUpdate dto = new TypeDtoUpdate();
		dto.setId(111L);
		dto.setName("chocapic");
		dto.setStock(123);
		dto.setDeleted(false);

		String bodyAsJson = mvc
				.perform(put(AbstractConstants.Type.UPDATE).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))

				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("false", bodyAsJson);

	}

	@Test
	@Sql(statements = "insert into type (id,deleted, name, stock) values (111,false,'polo',2)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from type where id = 111", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testUpdateTypeWithoutId_shouldReturnError() throws UnsupportedEncodingException, Exception {

		TypeDtoUpdate dto = new TypeDtoUpdate();
		dto.setName("chocapic");
		dto.setStock(123);
		dto.setDeleted(false);

		String result = mvc
				.perform(put(AbstractConstants.Type.UPDATE).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", result);
	}

	@Test
	@Sql(statements = "insert into type (id,deleted, name, stock) values (111,false,'polo',2)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from type where id = 111", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testUpdateTypeWithoutName_souldReturnError() throws UnsupportedEncodingException, Exception {

		TypeDtoUpdate dto = new TypeDtoUpdate();
		dto.setId(111L);
		dto.setStock(123);
		dto.setDeleted(false);

		String result = mvc
				.perform(put(AbstractConstants.Type.UPDATE).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", result);
	}

	@Test
	@Sql(statements = "insert into type (id,deleted, name, stock) values (111,false,'polo',2)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from type where id = 111", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testUpdateTypeWithNegativeStock_souldReturnError() throws UnsupportedEncodingException, Exception {

		TypeDtoUpdate dto = new TypeDtoUpdate();
		dto.setId(111L);
		dto.setStock(-123);
		dto.setName("chocapic");
		dto.setDeleted(false);

		String result = mvc
				.perform(put(AbstractConstants.Type.UPDATE).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assertEquals("", result);
	}


//	Methode Delete

	@Test
	@Sql(statements = { "delete from type",
			"insert into type (id, name, stock, deleted) values(1, 'aspirateur', 45, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from type", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testdeleteByIdType_shouldReturnStatus200andValueTrue() throws JsonProcessingException, Exception {

//		typeDtoUpdate dto = new typeDtoUpdate();


		String bodyAsJson = mvc.perform(delete(AbstractConstants.Type.DELETE_ID,1L).contentType(MediaType.APPLICATION_JSON))
//				.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("true", bodyAsJson);
	}

	@Test
	@Sql(statements = "delete from type", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from type", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testdeleteByIdInexistType_shouldReturn200andValueFalse() throws JsonProcessingException, Exception {

//		typeDtoUpdate dto = new typeDtoUpdate();


		String bodyAsJson = mvc.perform(delete(AbstractConstants.Type.DELETE_ID,1L).contentType(MediaType.APPLICATION_JSON))
//				.content(mapper.writeValueAsString(dto)))
		.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

         assertEquals("false", bodyAsJson);
	
}
	
//	FindDeleteFalse

	@Test
	@Sql(statements = { "delete from type",
			"insert into type (id, name, stock, deleted) values(1, 'aspirateur', '45', 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from type", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testFindDeleteFalse_shouldReturnStatus200WithContentNotNull()
			throws JsonProcessingException, Exception {

		String bodyAsJson = mvc.perform(get(AbstractConstants.Type.READ_ALL).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
		System.out.println("Le message est " + bodyAsJson);
		
		List<TypeDtoUpdate> response = mapper.readValue(bodyAsJson, new TypeReference <List<TypeDtoUpdate>>() {});
		
		assertNotNull(response);
		assertEquals(1,response.get(0).getId().intValue());
		assertEquals("aspirateur",response.get(0).getName());
		assertEquals(45,response.get(0).getStock());
	    assertFalse(response.get(0).isDeleted());

	}
	
	
//	FindDeleteTrue

	@Test
	@Sql(statements = { "delete from type",
			"insert into type (id, name, stock, deleted) values(1, 'aspirateur', 45, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from type", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testFindDeleteTrue_shouldReturnStatus200WithContentNotNull() throws JsonProcessingException, Exception {

		String bodyAsJson = mvc.perform(get(AbstractConstants.Type.READ_DELETE).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
		System.out.println("Le message est " + bodyAsJson);

	

	List<TypeDtoUpdate> response = mapper.readValue(bodyAsJson, new TypeReference <List<TypeDtoUpdate>>() {});
	
	assertNotNull(response);
	assertEquals(1,response.get(0).getId().intValue());
	assertEquals("aspirateur",response.get(0).getName());
	assertEquals(45,response.get(0).getStock());
    assertTrue(response.get(0).isDeleted());

	}
	
	
//	setDeleteTrue

	@Test
	@Sql(statements = { "delete from type",
			"insert into type (id, name, stock, deleted) values(1, 'aspirateur', 45, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from type", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testSetDeleteTrue_shouldReturnStatus200WithContentTrue() throws JsonProcessingException, Exception {

	

		String bodyAsJson = mvc
				.perform(put(AbstractConstants.Type.SET_DELETED_TRUE,1L).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("true", bodyAsJson);

	}

	
	
	
}

