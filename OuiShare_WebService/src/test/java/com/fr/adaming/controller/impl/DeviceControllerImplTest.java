package com.fr.adaming.controller.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fr.adaming.OuiShareWebServiceApplicationTest;
import com.fr.adaming.constants.AbstractConstants;
import com.fr.adaming.dto.DeviceDto;
import com.fr.adaming.dto.DeviceDtoWithId;


/**
 * @author Julie Noterman
 *
 */
public class DeviceControllerImplTest extends OuiShareWebServiceApplicationTest {


	@Test
	@Sql(statements = "delete from device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreatingValidDevice_shouldReturnStatus200WithContentNotNull()
			throws JsonProcessingException, Exception {
		DeviceDto dto = new DeviceDto();
		dto.setBrand("dyson");
		dto.setChip(150L);
		dto.setDeposit(400D);
		dto.setDescription("ceci est une description");
		dto.setName("aspirateur");
		dto.setPriceExtra(10D);
		dto.setPricePonctual(10D);
		dto.setPriceSub(5D);
		dto.setRentalDurationInHours(1D);
		dto.setRules("les regles");
		dto.setUrlImage("url");

		String bodyAsJson = mvc
				.perform(post(AbstractConstants.Device.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		DeviceDto response = mapper.readValue(bodyAsJson, DeviceDto.class);

		assertNotNull(response);
		assertThat(response).hasFieldOrPropertyWithValue("brand", "dyson");
		assertThat(response).hasFieldOrPropertyWithValue("chip", 150L);

	}
	
	@Test
	@Sql(statements = "delete from device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreatingDeviceWithBlankName_shouldReturnStatus400()
			throws JsonProcessingException, Exception {
		DeviceDto dto = new DeviceDto();
		dto.setBrand("dyson");
		dto.setChip(150L);
		dto.setDeposit(400D);
		dto.setDescription("ceci est une description");
		dto.setName(" ");
		dto.setPriceExtra(10D);
		dto.setPricePonctual(10D);
		dto.setPriceSub(5D);
		dto.setRentalDurationInHours(1D);
		dto.setRules("les regles");
		dto.setUrlImage("url");

		String bodyAsJson = mvc
				.perform(post(AbstractConstants.Device.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();


		assertEquals("",bodyAsJson);
		
	}
	
	@Test
	@Sql(statements = "delete from device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreatingDeviceWithDescriptionBlank_shouldReturnStatus400()
			throws JsonProcessingException, Exception {
		DeviceDto dto = new DeviceDto();
		dto.setBrand("dyson");
		dto.setChip(150L);
		dto.setDeposit(400D);
		dto.setDescription(" ");
		dto.setName("aspirateur");
		dto.setPriceExtra(10D);
		dto.setPricePonctual(10D);
		dto.setPriceSub(5D);
		dto.setRentalDurationInHours(1D);
		dto.setRules("les regles");
		dto.setUrlImage("url");

		String bodyAsJson = mvc
				.perform(post(AbstractConstants.Device.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();


		assertEquals("",bodyAsJson);

	}
	
	
	@Test
	@Sql(statements = "delete from device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreatingDeviceWithChipNull_shouldReturnStatus400()
			throws JsonProcessingException, Exception {
		DeviceDto dto = new DeviceDto();
		dto.setBrand("dyson");
		dto.setChip(null);
		dto.setDeposit(400D);
		dto.setDescription("description");
		dto.setName("aspirateur");
		dto.setPriceExtra(10D);
		dto.setPricePonctual(10D);
		dto.setPriceSub(5D);
		dto.setRentalDurationInHours(1D);
		dto.setRules("les regles");
		dto.setUrlImage("url");

		String bodyAsJson = mvc
				.perform(post(AbstractConstants.Device.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();


		assertEquals("",bodyAsJson);

	}
	
	@Test
	@Sql(statements = "delete from device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreatingDeviceWithDepositNull_shouldReturnStatus400()
			throws JsonProcessingException, Exception {
		DeviceDto dto = new DeviceDto();
		dto.setBrand("dyson");
		dto.setChip(500L);
		dto.setDeposit(null);
		dto.setDescription("description");
		dto.setName("aspirateur");
		dto.setPriceExtra(10D);
		dto.setPricePonctual(10D);
		dto.setPriceSub(5D);
		dto.setRentalDurationInHours(1D);
		dto.setRules("les regles");
		dto.setUrlImage("url");

		String bodyAsJson = mvc
				.perform(post(AbstractConstants.Device.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();


		assertEquals("",bodyAsJson);

	}
	
	@Test
	@Sql(statements = "delete from device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreatingDeviceWithPriceExtraNull_shouldReturnStatus400()
			throws JsonProcessingException, Exception {
		DeviceDto dto = new DeviceDto();
		dto.setBrand("dyson");
		dto.setChip(500L);
		dto.setDeposit(400D);
		dto.setDescription("description");
		dto.setName("aspirateur");
		dto.setPriceExtra(null);
		dto.setPricePonctual(10D);
		dto.setPriceSub(5D);
		dto.setRentalDurationInHours(1D);
		dto.setRules("les regles");
		dto.setUrlImage("url");

		String bodyAsJson = mvc
				.perform(post(AbstractConstants.Device.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();


		assertEquals("",bodyAsJson);

	}
	
	@Test
	@Sql(statements = "delete from device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreatingDeviceWithPricePonctualNull_shouldReturnStatus400()
			throws JsonProcessingException, Exception {
		DeviceDto dto = new DeviceDto();
		dto.setBrand("dyson");
		dto.setChip(500L);
		dto.setDeposit(400D);
		dto.setDescription("description");
		dto.setName("aspirateur");
		dto.setPriceExtra(10D);
		dto.setPricePonctual(null);
		dto.setPriceSub(5D);
		dto.setRentalDurationInHours(1D);
		dto.setRules("les regles");
		dto.setUrlImage("url");

		String bodyAsJson = mvc
				.perform(post(AbstractConstants.Device.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();


		assertEquals("",bodyAsJson);

	}
	
	@Test
	@Sql(statements = "delete from device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreatingDeviceWithPriceSubNull_shouldReturnStatus400()
			throws JsonProcessingException, Exception {
		DeviceDto dto = new DeviceDto();
		dto.setBrand("dyson");
		dto.setChip(500L);
		dto.setDeposit(400D);
		dto.setDescription("description");
		dto.setName("aspirateur");
		dto.setPriceExtra(10D);
		dto.setPricePonctual(10D);
		dto.setPriceSub(null);
		dto.setRentalDurationInHours(1D);
		dto.setRules("les regles");
		dto.setUrlImage("url");

		String bodyAsJson = mvc
				.perform(post(AbstractConstants.Device.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();


		assertEquals("",bodyAsJson);

	}
	
	@Test
	@Sql(statements = "delete from device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreatingDeviceWithRentalDurationNull_shouldReturnStatus400()
			throws JsonProcessingException, Exception {
		DeviceDto dto = new DeviceDto();
		dto.setBrand("dyson");
		dto.setChip(500L);
		dto.setDeposit(400D);
		dto.setDescription("description");
		dto.setName("aspirateur");
		dto.setPriceExtra(10D);
		dto.setPricePonctual(10D);
		dto.setPriceSub(5D);
		dto.setRentalDurationInHours(null);
		dto.setRules("les regles");
		dto.setUrlImage("url");

		String bodyAsJson = mvc
				.perform(post(AbstractConstants.Device.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();


		assertEquals("",bodyAsJson);

	}
	
	@Test
	@Sql(statements = "delete from device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreatingNullDevice_shouldReturnStatus400WithContentNotNull()
			throws JsonProcessingException, Exception {
		DeviceDto dto = null;
		

		String bodyAsJson = mvc
				.perform(post(AbstractConstants.Device.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		

		assertEquals("",bodyAsJson);

	}

	@Test
	@Sql(statements = "INSERT INTO device(id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, deleted) VALUES"
			+ "(123, 'dyson', 132456, 350, 'ceci est un aspirateur', 'aspirateur', 10, 5, 10, 1, 'regle', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteExistingDeviceById_shouldReturnStatus200AndTrue()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		DeviceDto dto = new DeviceDto();

		String bodyAsJson = mvc
				.perform(delete(AbstractConstants.Device.DELETE_ID, 123L).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("true", bodyAsJson);

	}

	@Test
	@Sql(statements = "INSERT INTO device(id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, deleted) VALUES"
			+ "(123, 'dyson', 132456, 350, 'ceci est un aspirateur', 'aspirateur', 10, 5, 10, 1, 'regle', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteNotExistingDeviceById_shouldReturnStatus200AndFalse()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		DeviceDto dto = new DeviceDto();

		String bodyAsJson = mvc
				.perform(delete(AbstractConstants.Device.DELETE_ID, 23L).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("false", bodyAsJson);

	}

	@Test
	@Sql(statements = "INSERT INTO device(id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, deleted) VALUES"
			+ "(123, 'dyson', 132456, 350, 'ceci est un aspirateur', 'aspirateur', 10, 5, 10, 1, 'regle', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void setdeleteTrueExistingDevice_shouldReturnStatus200AndTrue()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		DeviceDto dto = new DeviceDto();

		String bodyAsJson = mvc
				.perform(put(AbstractConstants.Device.SET_DELETED_TRUE, 123L).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("true", bodyAsJson);

	}
	
	
	@Test
	@Sql(statements = "INSERT INTO device(id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, deleted) VALUES"
			+ "(123, 'dyson', 132456, 350, 'ceci est un aspirateur', 'aspirateur', 10, 5, 10, 1, 'regle', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void setdeleteTrueNotExistingDevice_shouldReturnStatus200AndFalse()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		DeviceDto dto = new DeviceDto();

		String bodyAsJson = mvc
				.perform(put(AbstractConstants.Device.SET_DELETED_TRUE, 23L).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		
		assertEquals("false", bodyAsJson);
		
		

	}

	
	
	@Test
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (33333, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateExistingDevice_shouldReturnStatus200AndUpdatedDevice() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		DeviceDtoWithId dto = new DeviceDtoWithId();
		dto.setId(33333L);
		dto.setBrand("samsung");
		dto.setChip(222222L);
		dto.setDeposit(300D);
		dto.setDescription("Ceci est un aspirateur");
		dto.setName("Objet qui aspire");
		dto.setPriceExtra(10D);
		dto.setPricePonctual(10D);
		dto.setPriceSub(10D);
		dto.setRentalDurationInHours(2D);
		dto.setRules("No rules");
		dto.setState("Casse");
		dto.setStateComment("Je viens de casser ce bel aspirateur");
		dto.setUrlImage("url");
		
		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Device.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto))
			)
			.andExpect(status().isOk())
			.andReturn()
			.getResponse()
			.getContentAsString();
		
		DeviceDtoWithId response = mapper.readValue(bodyAsJson, DeviceDtoWithId.class);
		
		assertNotNull(response);
		assertThat(response).hasFieldOrPropertyWithValue("brand", "samsung");
		assertThat(response).hasFieldOrPropertyWithValue("chip", 222222L);
		assertThat(response).hasFieldOrPropertyWithValue("deposit", 300D);
		assertThat(response).hasFieldOrPropertyWithValue("description", "Ceci est un aspirateur");
		assertThat(response).hasFieldOrPropertyWithValue("name", "Objet qui aspire");
		assertThat(response).hasFieldOrPropertyWithValue("priceExtra", 10D);
		assertThat(response).hasFieldOrPropertyWithValue("pricePonctual", 10D);
		assertThat(response).hasFieldOrPropertyWithValue("priceSub", 10D);
		assertThat(response).hasFieldOrPropertyWithValue("rentalDurationInHours", 2D);
		assertThat(response).hasFieldOrPropertyWithValue("rules", "No rules");
		assertThat(response).hasFieldOrPropertyWithValue("state", "Casse");
		assertThat(response).hasFieldOrPropertyWithValue("stateComment", "Je viens de casser ce bel aspirateur");
		
	}
	
	
	@Test
	public void updateNonExistingDevice_shouldReturnStatus200AndEmptyResponse() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		DeviceDtoWithId dto = new DeviceDtoWithId();
		dto.setId(33334L);
		dto.setBrand("samsung");
		dto.setChip(222222L);
		dto.setDeposit(300D);
		dto.setDescription("Ceci est un aspirateur");
		dto.setName("Objet qui aspire");
		dto.setPriceExtra(10D);
		dto.setPricePonctual(10D);
		dto.setPriceSub(10D);
		dto.setRentalDurationInHours(2D);
		dto.setRules("No rules");
		dto.setState("Cassé");
		dto.setStateComment("Je viens de casser ce bel aspirateur");
		dto.setUrlImage("url");
		
		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Device.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto))
			)
			.andExpect(status().isOk())
			.andReturn()
			.getResponse()
			.getContentAsString();
		
		assertThat(bodyAsJson).isEmpty(); 
		
	}
	
	@Test
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (33335, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateDeviceWithIdNull_shouldReturnStatus400AndEmptyJson() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		DeviceDtoWithId dto = new DeviceDtoWithId();
		dto.setId(null);
		dto.setBrand("samsung");
		dto.setChip(222222L);
		dto.setDeposit(300D);
		dto.setDescription("Ceci est un aspirateur");
		dto.setName("bla");
		dto.setPriceExtra(10D);
		dto.setPricePonctual(10D);
		dto.setPriceSub(10D);
		dto.setRentalDurationInHours(2D);
		dto.setRules("No rules");
		dto.setState("Cassé");
		dto.setStateComment("Je viens de casser ce bel aspirateur");
		dto.setUrlImage("url");
		
		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Device.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto))
			)
			.andExpect(status().is(400))
			.andReturn()
			.getResponse()
			.getContentAsString();
		
		assertThat(bodyAsJson).isEmpty(); 
		
	}
	
	
	@Test
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (33336, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateDeviceWithBlankName_shouldReturnStatus400AndEmptyJson() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		DeviceDtoWithId dto = new DeviceDtoWithId();
		dto.setId(33336L);
		dto.setBrand("samsung");
		dto.setChip(222222L);
		dto.setDeposit(300D);
		dto.setDescription("Ceci est un aspirateur");
		dto.setName("");
		dto.setPriceExtra(10D);
		dto.setPricePonctual(10D);
		dto.setPriceSub(10D);
		dto.setRentalDurationInHours(2D);
		dto.setRules("No rules");
		dto.setState("Cassé");
		dto.setStateComment("Je viens de casser ce bel aspirateur");
		dto.setUrlImage("url");
		
		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Device.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto))
			)
			.andExpect(status().is(400))
			.andReturn()
			.getResponse()
			.getContentAsString();
		
		assertThat(bodyAsJson).isEmpty(); 
		
	}
	
	@Test
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (33336, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateDeviceWithBlankDescription_shouldReturnStatus400AndEmptyJson() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		DeviceDtoWithId dto = new DeviceDtoWithId();
		dto.setId(33336L);
		dto.setBrand("samsung");
		dto.setChip(222222L);
		dto.setDeposit(300D);
		dto.setDescription("");
		dto.setName("bla");
		dto.setPriceExtra(10D);
		dto.setPricePonctual(10D);
		dto.setPriceSub(10D);
		dto.setRentalDurationInHours(2D);
		dto.setRules("No rules");
		dto.setState("Cassé");
		dto.setStateComment("Je viens de casser ce bel aspirateur");
		dto.setUrlImage("url");
		
		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Device.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto))
			)
			.andExpect(status().is(400))
			.andReturn()
			.getResponse()
			.getContentAsString();
		
		assertThat(bodyAsJson).isEmpty(); 
		
	}
	
	@Test
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (33336, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateDeviceWithPriceSubNull_shouldReturnStatus400AndEmptyJson() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		DeviceDtoWithId dto = new DeviceDtoWithId();
		dto.setId(33336L);
		dto.setBrand("samsung");
		dto.setChip(222222L);
		dto.setDeposit(300D);
		dto.setDescription("Ceci est un aspirateur");
		dto.setName("bla");
		dto.setPriceExtra(10D);
		dto.setPricePonctual(10D);
		dto.setPriceSub(null);
		dto.setRentalDurationInHours(2D);
		dto.setRules("No rules");
		dto.setState("Cassé");
		dto.setStateComment("Je viens de casser ce bel aspirateur");
		dto.setUrlImage("url");
		
		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Device.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto))
			)
			.andExpect(status().is(400))
			.andReturn()
			.getResponse()
			.getContentAsString();
		
		assertThat(bodyAsJson).isEmpty(); 
		
	}
	
	@Test
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (33336, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateDeviceWithPricePonctualNull_shouldReturnStatus400AndEmptyJson() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		DeviceDtoWithId dto = new DeviceDtoWithId();
		dto.setId(33336L);
		dto.setBrand("samsung");
		dto.setChip(222222L);
		dto.setDeposit(300D);
		dto.setDescription("Ceci est un aspirateur");
		dto.setName("bla");
		dto.setPriceExtra(10D);
		dto.setPricePonctual(null);
		dto.setPriceSub(10D);
		dto.setRentalDurationInHours(2D);
		dto.setRules("No rules");
		dto.setState("Cassé");
		dto.setStateComment("Je viens de casser ce bel aspirateur");
		dto.setUrlImage("url");
		
		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Device.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto))
			)
			.andExpect(status().is(400))
			.andReturn()
			.getResponse()
			.getContentAsString();
		
		assertThat(bodyAsJson).isEmpty(); 
		
	}
	
	@Test
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (33336, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateDeviceWithDepositNull_shouldReturnStatus400AndEmptyJson() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		DeviceDtoWithId dto = new DeviceDtoWithId();
		dto.setId(33336L);
		dto.setBrand("samsung");
		dto.setChip(222222L);
		dto.setDeposit(null);
		dto.setDescription("Ceci est un aspirateur");
		dto.setName("bla");
		dto.setPriceExtra(10D);
		dto.setPricePonctual(10D);
		dto.setPriceSub(10D);
		dto.setRentalDurationInHours(2D);
		dto.setRules("No rules");
		dto.setState("Cassé");
		dto.setStateComment("Je viens de casser ce bel aspirateur");
		dto.setUrlImage("url");
		
		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Device.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto))
			)
			.andExpect(status().is(400))
			.andReturn()
			.getResponse()
			.getContentAsString();
		
		assertThat(bodyAsJson).isEmpty(); 
		
	}
	
	@Test
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (33336, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateDeviceWithNullPriceExtra_shouldReturnStatus400AndEmptyJson() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		DeviceDtoWithId dto = new DeviceDtoWithId();
		dto.setId(33336L);
		dto.setBrand("samsung");
		dto.setChip(222222L);
		dto.setDeposit(300D);
		dto.setDescription("Ceci est un aspirateur");
		dto.setName("bla");
		dto.setPriceExtra(null);
		dto.setPricePonctual(10D);
		dto.setPriceSub(10D);
		dto.setRentalDurationInHours(2D);
		dto.setRules("No rules");
		dto.setState("Cassé");
		dto.setStateComment("Je viens de casser ce bel aspirateur");
		dto.setUrlImage("url");
		
		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Device.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto))
			)
			.andExpect(status().is(400))
			.andReturn()
			.getResponse()
			.getContentAsString();
		
		assertThat(bodyAsJson).isEmpty(); 
		
	}
	
	@Test
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (33336, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateDeviceWithNullChip_shouldReturnStatus400AndEmptyJson() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		DeviceDtoWithId dto = new DeviceDtoWithId();
		dto.setId(33336L);
		dto.setBrand("samsung");
		dto.setChip(null);
		dto.setDeposit(300D);
		dto.setDescription("Ceci est un aspirateur");
		dto.setName("bla");
		dto.setPriceExtra(10D);
		dto.setPricePonctual(10D);
		dto.setPriceSub(10D);
		dto.setRentalDurationInHours(2D);
		dto.setRules("No rules");
		dto.setState("Cassé");
		dto.setStateComment("Je viens de casser ce bel aspirateur");
		dto.setUrlImage("url");
		
		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Device.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto))
			)
			.andExpect(status().is(400))
			.andReturn()
			.getResponse()
			.getContentAsString();
		
		assertThat(bodyAsJson).isEmpty(); 
		
	}
	
	@Test
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (33336, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateDeviceWithNullRentalDurationHours_shouldReturnStatus400AndEmptyJson() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		DeviceDtoWithId dto = new DeviceDtoWithId();
		dto.setId(33336L);
		dto.setBrand("samsung");
		dto.setChip(222222L);
		dto.setDeposit(300D);
		dto.setDescription("Ceci est un aspirateur");
		dto.setName("bla");
		dto.setPriceExtra(10D);
		dto.setPricePonctual(10D);
		dto.setPriceSub(10D);
		dto.setRentalDurationInHours(null);
		dto.setRules("No rules");
		dto.setState("Cassé");
		dto.setStateComment("Je viens de casser ce bel aspirateur");
		dto.setUrlImage("url");
		
		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Device.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto))
			)
			.andExpect(status().is(400))
			.andReturn()
			.getResponse()
			.getContentAsString();
		
		assertThat(bodyAsJson).isEmpty(); 
		
	}
	
	@Test
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (33337, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readByIdWithExistingDevice_shouldReturnStatus200AndNotEmptyJson() throws UnsupportedEncodingException, Exception {
		
		String bodyAsJson = mvc.perform(
				get(AbstractConstants.Device.READ_ID, 33337L)
				.contentType(MediaType.APPLICATION_JSON)
			)
			.andExpect(status().isOk())
			.andReturn()
			.getResponse()
			.getContentAsString();	
		
		
		assertFalse(bodyAsJson.isEmpty());
		
		
	}
	
	@Test
	public void readByIdWithNonExistingDevice_shouldReturnStatus200AndEmptyJson() throws UnsupportedEncodingException, Exception {
		
		String bodyAsJson = mvc.perform(
				get(AbstractConstants.Device.READ_ID, 33338L)
				.contentType(MediaType.APPLICATION_JSON)
			)
			.andExpect(status().isOk())
			.andReturn()
			.getResponse()
			.getContentAsString();	
		
		
		assertTrue(bodyAsJson.isEmpty());
		
	}
	
	
	
	@Test
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (33338, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (33339, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (33340, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllWithExistingDevices_shouldReturnStatus200AndNoneEmptyJson() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		String bodyAsJson = mvc.perform(
				get(AbstractConstants.Device.READ_ALL)
				.contentType(MediaType.APPLICATION_JSON)
			)
			.andExpect(status().isOk())
			.andReturn()
			.getResponse()
			.getContentAsString();	
		
		assertFalse(bodyAsJson.isEmpty());
		
		
	}
	
	
	@Test
	public void readAllWithNoneExistingDevices_shouldReturnStatus200AndJsonEqualsEmptyList() throws UnsupportedEncodingException, Exception {
		
		String bodyAsJson = mvc.perform(
				get(AbstractConstants.Device.READ_ALL)
				.contentType(MediaType.APPLICATION_JSON)
			)
			.andExpect(status().isOk())
			.andReturn()
			.getResponse()
			.getContentAsString();	
		
		assertTrue(bodyAsJson.equals("[]"));
		
		
	}
	
	
	@Test
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (33341, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', true)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (33342, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', true)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (33343, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', true)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readDeleteWithExistingDevices_shouldReturnStatus200AndNotEmptyJson() throws UnsupportedEncodingException, Exception {
		
		String bodyAsJson = mvc.perform(
				get(AbstractConstants.Device.READ_DELETE)
				.contentType(MediaType.APPLICATION_JSON)
			)
			.andExpect(status().isOk())
			.andReturn()
			.getResponse()
			.getContentAsString();	
		
		
		assertFalse(bodyAsJson.isEmpty());
		
		
	}
	
	@Test
	public void readDeleteWithNoneExistingDevices_shouldReturn200AndEmptyJson() throws UnsupportedEncodingException, Exception {
		
		String bodyAsJson = mvc.perform(
				get(AbstractConstants.Device.READ_DELETE)
				.contentType(MediaType.APPLICATION_JSON)
			)
			.andExpect(status().isOk())
			.andReturn()
			.getResponse()
			.getContentAsString();	
		
		assertFalse(bodyAsJson.isEmpty());
		
	}
	
	
	@Test
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (33344, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', true)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (33345, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', true)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (33346, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', true)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllRealWithExistingDevices_shouldReturnStatus200AndNoneEmptyJson() throws UnsupportedEncodingException, Exception {
		
		String bodyAsJson = mvc.perform(
				get(AbstractConstants.Device.READ_ALL_REAL)
				.contentType(MediaType.APPLICATION_JSON)
			)
			.andExpect(status().isOk())
			.andReturn()
			.getResponse()
			.getContentAsString();	
		
		assertFalse(bodyAsJson.isEmpty());
		
	}
	
	@Test
	public void readAllRealWithNoneExistingDevices_shouldReturnStatus200AndEmptyJson() throws UnsupportedEncodingException, Exception {
		
		String bodyAsJson = mvc.perform(
				get(AbstractConstants.Device.READ_ALL_REAL)
				.contentType(MediaType.APPLICATION_JSON)
			)
			.andExpect(status().isOk())
			.andReturn()
			.getResponse()
			.getContentAsString();	
		
		assertTrue(bodyAsJson.equals("[]"));
	}

	@Test
	@Sql(statements = "INSERT INTO locker(id,adress,zip_code,city,slot,name,deleted)"
			+ "VALUES(1,'adress1',123456789,'lyon',123,'pierre',false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO locker(id,adress,zip_code,city,slot,name,deleted)"
			+ "VALUES(2,'adress1',123456789,'lyon',123,'jacques',false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO slot (id,deleted,number,id_locker) "
			+ "VALUES (1,false,123,1);", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO slot (id,deleted,number,id_locker) "
			+ "VALUES (2,false,223,1);", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO slot (id,deleted,number,id_locker) "
			+ "VALUES (3,false,323,2);", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = {
			"INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted,id_slot) "
					+ "VALUES (21120, 'dyson', 111111, 150, 'aspirateur trop style', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false,1)",
			"UPDATE Slot SET id_device=21120 where id=1" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = {
			"INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted,id_slot) "
					+ "VALUES (21121, 'rowenta', 111111, 150, 'aspirateur moins style', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false,2)",
			"UPDATE Slot SET id_device=21121 where id=2 " }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = {
			"INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted,id_slot) "
					+ "VALUES (21122, 'miele', 111111, 150, 'aspirateur pas du tout style', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false,3)",
			"UPDATE Slot SET id_device=21122 where id=3" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = {"UPDATE slot Set id_locker=null","UPDATE device set id_slot=null","DELETE FROM slot","DELETE FROM device","DELETE FROM locker"}, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllDevicesFromLockerId_shouldReturnStatus200AndDevices() throws UnsupportedEncodingException, Exception {
		
		String bodyAsJson = mvc.perform(get(AbstractConstants.Device.READ_ALL_FROM_LOCKER_ID, 1L)).andExpect(status().is(200)).andReturn().getResponse().getContentAsString();	
							
				List<DeviceDto> response = mapper.readValue(bodyAsJson,  new TypeReference<List<DeviceDto>>() {});
				
				assertEquals(response.size(),2);
				
				assertEquals(response.get(0).getBrand(), "dyson");
				assertEquals(response.get(0).getDescription(), "aspirateur trop style");

				assertEquals(response.get(1).getBrand(), "rowenta");
				assertEquals(response.get(1).getDescription(), "aspirateur moins style");
				
			}

	//@Disabled	//TODO should fix this TestCase
	@Test
	public void readAllCatalogWithNoneExistingDevices_shouldReturnStatus200AndEmptyJson() throws UnsupportedEncodingException, Exception {
		
		String bodyAsJson = mvc.perform(
				get(AbstractConstants.Device.READ_ALL_CATALOG)

				.contentType(MediaType.APPLICATION_JSON)
			)
			.andExpect(status().isOk())
			.andReturn()
			.getResponse()
			.getContentAsString();	
		
		assertTrue(bodyAsJson.equals("[]"));
	}

	//@Disabled	//TODO should fix this TestCase
	@Test
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (11120, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (11121, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
			+ "VALUES (11122, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM device", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void readAllCatalogWithExistingDevices_shouldReturnStatus200AndNoneEmptyJson() throws UnsupportedEncodingException, Exception {
		
		String bodyAsJson = mvc.perform(
				get(AbstractConstants.Device.READ_ALL_CATALOG)
				.contentType(MediaType.APPLICATION_JSON)
			)
			.andExpect(status().isOk())
			.andReturn()
			.getResponse()
			.getContentAsString();	
		
		assertFalse(bodyAsJson.isEmpty());
		
	}
	
	
// IMPOSSIBLE DE LANCER CES TESTS POUR L INSTANT MAIS LA COUCHE SERVICE MARCHE DONC CA DEVRAIT MARCHER
//	@Test
//	@Sql(statements = "INSERT INTO slot (id,deleted,number) VALUES (11111, false, 11111)", executionPhase = ExecutionPhase.AFTER_TEST_METHOD )
//	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
//			+ "VALUES (11120, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
//	@Sql(statements = {"DELETE FROM device", "DELETE FROM slot"}, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
//	public void addAssociationWithSlot_shouldReturnStatus200AndTrue() throws UnsupportedEncodingException, Exception {
//		
//		String bodyAsJson = mvc.perform(
//				get("/api/device/11120/11111/addAssociationWithSlot")
//				.contentType(MediaType.APPLICATION_JSON)
//			)
//			.andExpect(status().isOk())
//			.andReturn()
//			.getResponse()
//			.getContentAsString();	
//		
//		boolean response = mapper.readValue(bodyAsJson, Boolean.class);
//		
//		assertTrue(response);
//		
//	}
//	
//	@Test
//	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
//			+ "VALUES (11121, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
//	@Sql(statements = {"DELETE FROM device", "DELETE FROM slot"}, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
//	public void addAssociationWithSlotThatDoesNotExist_shouldReturnStatus200AndFalse() throws UnsupportedEncodingException, Exception {
//		
//		String bodyAsJson = mvc.perform(
//				get("/api/device/11121/11112/addAssociationWithSlot")
//				.contentType(MediaType.APPLICATION_JSON)
//			)
//			.andExpect(status().isOk())
//			.andReturn()
//			.getResponse()
//			.getContentAsString();	
//		
//		boolean response = mapper.readValue(bodyAsJson, Boolean.class);
//		
//		assertFalse(response);
//		
//	}
//
//	
//	@Test
//	@Sql(statements = "INSERT INTO slot (id,deleted,number) VALUES (11112, false, 11111)", executionPhase = ExecutionPhase.AFTER_TEST_METHOD )
//	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, id_slot, deleted) "
//			+ "VALUES (11122, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', 11112, false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
//	@Sql(statements = {"DELETE FROM device", "DELETE FROM slot"}, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
//	public void deleteAssociationWithSlotThatExist_shouldReturnStatus200AndTrue() throws UnsupportedEncodingException, Exception {
//		
//		String bodyAsJson = mvc.perform(
//				get("/api/device/11122/deleteAssociationWithSlot")
//				.contentType(MediaType.APPLICATION_JSON)
//			)
//			.andExpect(status().isOk())
//			.andReturn()
//			.getResponse()
//			.getContentAsString();	
//		
//		boolean response = mapper.readValue(bodyAsJson, Boolean.class);
//		
//		assertTrue(response);
//		
//	}
//	
//	@Test
//	@Sql(statements = "INSERT INTO device (id, brand, chip, deposit, description, name, price_extra, price_ponctual, price_sub, rental_duration_in_hours, rules, state, state_comment, deleted) "
//			+ "VALUES (11123, 'dyson', 111111, 150, 'aspirateur trop stylé', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', false)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
//	@Sql(statements = {"DELETE FROM device", "DELETE FROM slot"}, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
//	public void deleteAssociationWithSlotThatDoesNotExist_shouldReturnStatus200AndFalse() throws UnsupportedEncodingException, Exception {
//		
//		String bodyAsJson = mvc.perform(
//				get("/api/device/11123/deleteAssociationWithSlot")
//				.contentType(MediaType.APPLICATION_JSON)
//			)
//			.andExpect(status().isOk())
//			.andReturn()
//			.getResponse()
//			.getContentAsString();	
//		
//		boolean response = mapper.readValue(bodyAsJson, Boolean.class);
//		
//		assertFalse(response);
//		
//	}
}	
