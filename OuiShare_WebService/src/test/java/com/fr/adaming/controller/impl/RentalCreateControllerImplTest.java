package com.fr.adaming.controller.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fr.adaming.OuiShareWebServiceApplicationTest;
import com.fr.adaming.constants.AbstractConstants;
import com.fr.adaming.dto.CustomerUpdateDto;
import com.fr.adaming.dto.DeviceDtoWithId;
import com.fr.adaming.dto.RentalDtoAvail;
import com.fr.adaming.dto.RentalDtoCreate;
import com.fr.adaming.dto.RentalDtoUpdate;

public class RentalCreateControllerImplTest extends OuiShareWebServiceApplicationTest {

	/***************************************************CREATE***************************************************************/
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)"
			}, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createValidRentalDto_shouldReturnRentalDto() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		DeviceDtoWithId device = new DeviceDtoWithId();
		device.setId(1L);
		
		
		CustomerUpdateDto customer = new CustomerUpdateDto();
		customer.setId(1L);
		
		
		LocalDateTime start = LocalDateTime.of(3000, 01, 01, 00, 00);
		LocalDateTime end = LocalDateTime.of(3000, 01, 01, 12, 00);
		RentalDtoCreate rental = new RentalDtoCreate();
		rental.setStartDate(start);
		rental.setEndDateTheoritical(end);
		rental.setSubscription(false);
		rental.setIdDevice(device.getId());
		rental.setIdCustomer(device.getId());
		
		
		String bodyAsJson = mvc.perform(
				post(AbstractConstants.Rental.CREATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(rental))
				)
				.andExpect(status().isOk())
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		RentalDtoCreate response = mapper.readValue(bodyAsJson, RentalDtoCreate.class);
				
				assertNotNull(response);
				assertNotNull(response.getStartDate());
				assertNotNull(response.getEndDateTheoritical());
				assertNotNull(response.getIdDevice());
				assertNotNull(response.getIdCustomer());
				assertEquals(response.isSubscription(), false);
				assertEquals(response.getStartDate(), LocalDateTime.of(3000, 01, 01, 00, 00));
				assertEquals(response.getEndDateTheoritical(), LocalDateTime.of(3000, 01, 01, 12, 00));
				assertEquals(response.getIdDevice(), 1L);
				assertEquals(response.getIdCustomer(), 1L);
			
		
	}
	
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 30000101, 30000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createRentalDtoWithNoAvaibility_shouldReturnError400() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		DeviceDtoWithId device = new DeviceDtoWithId();
		device.setId(1L);
		
		
		CustomerUpdateDto customer = new CustomerUpdateDto();
		customer.setId(1L);
		
		
		LocalDateTime start = LocalDateTime.of(3000, 01, 01, 00, 00);
		LocalDateTime end = LocalDateTime.of(3000, 01, 01, 12, 00);
		RentalDtoCreate rental = new RentalDtoCreate();
		rental.setStartDate(start);
		rental.setEndDateTheoritical(end);
		rental.setSubscription(false);
		rental.setIdDevice(device.getId());
		rental.setIdCustomer(customer.getId());
		
		
		String bodyAsJson = mvc.perform(
				post(AbstractConstants.Rental.CREATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(rental))
				)
				.andExpect(status().is(400))
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		
				
				assertEquals("", bodyAsJson);

			
		
	}
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			 }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createRentalDtoWithPastStartDate_shouldReturnError400() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		DeviceDtoWithId device = new DeviceDtoWithId();
		device.setId(1L);
		
		
		CustomerUpdateDto customer = new CustomerUpdateDto();
		customer.setId(1L);
		
		
		LocalDateTime start = LocalDateTime.of(2000, 01, 01, 00, 00);
		LocalDateTime end = LocalDateTime.of(3000, 01, 01, 12, 00);
		RentalDtoCreate rental = new RentalDtoCreate();
		rental.setStartDate(start);
		rental.setEndDateTheoritical(end);
		rental.setSubscription(false);
		rental.setIdDevice(device.getId());
		rental.setIdCustomer(customer.getId());
		
		
		String bodyAsJson = mvc.perform(
				post(AbstractConstants.Rental.CREATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(rental))
				)
				.andExpect(status().is(400))
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		
				
				assertEquals("", bodyAsJson);

			
		
	}
	
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			 }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createRentalDtoWithPastEndDateTheoritical_shouldReturnError400() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		DeviceDtoWithId device = new DeviceDtoWithId();
		device.setId(1L);
		
		
		CustomerUpdateDto customer = new CustomerUpdateDto();
		customer.setId(1L);
		
		
		LocalDateTime start = LocalDateTime.of(2020, 01, 01, 00, 00);
		LocalDateTime end = LocalDateTime.of(2000, 01, 01, 12, 00);
		RentalDtoCreate rental = new RentalDtoCreate();
		rental.setStartDate(start);
		rental.setEndDateTheoritical(end);
		rental.setSubscription(false);
		rental.setIdDevice(device.getId());
		rental.setIdCustomer(customer.getId());
		
		
		String bodyAsJson = mvc.perform(
				post(AbstractConstants.Rental.CREATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(rental))
				)
				.andExpect(status().is(400))
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		
				
				assertEquals("", bodyAsJson);

			
		
	}
	
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			 }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createRentalDtoWithNullStartDate_shouldReturnError400() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		DeviceDtoWithId device = new DeviceDtoWithId();
		device.setId(1L);
		
		
		CustomerUpdateDto customer = new CustomerUpdateDto();
		customer.setId(1L);
		
		
		
		LocalDateTime end = LocalDateTime.of(2000, 01, 01, 12, 00);
		RentalDtoCreate rental = new RentalDtoCreate();
		
		rental.setEndDateTheoritical(end);
		rental.setSubscription(false);
		rental.setIdDevice(device.getId());
		rental.setIdCustomer(customer.getId());
		
		
		String bodyAsJson = mvc.perform(
				post(AbstractConstants.Rental.CREATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(rental))
				)
				.andExpect(status().is(400))
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		
				
				assertEquals("", bodyAsJson);

			
		
	}
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			 }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createRentalDtoWithNullEndDateTheoritical_shouldReturnError400() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		DeviceDtoWithId device = new DeviceDtoWithId();
		device.setId(1L);
		
		
		CustomerUpdateDto customer = new CustomerUpdateDto();
		customer.setId(1L);
		
		
		LocalDateTime start = LocalDateTime.of(2020, 01, 01, 00, 00);
		
		RentalDtoCreate rental = new RentalDtoCreate();
		rental.setStartDate(start);
		
		rental.setSubscription(false);
		rental.setIdDevice(device.getId());
		rental.setIdCustomer(customer.getId());
		
		
		String bodyAsJson = mvc.perform(
				post(AbstractConstants.Rental.CREATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(rental))
				)
				.andExpect(status().is(400))
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		
				
				assertEquals("", bodyAsJson);

			
		
	}
	
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			 }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createRentalDtoWithNullIdDevice_shouldReturnError400() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		
		
		
		CustomerUpdateDto customer = new CustomerUpdateDto();
		customer.setId(1L);
		
		
		LocalDateTime start = LocalDateTime.of(3000, 01, 01, 00, 00);
		LocalDateTime end = LocalDateTime.of(3000, 01, 01, 12, 00);
		RentalDtoCreate rental = new RentalDtoCreate();
		rental.setStartDate(start);
		rental.setEndDateTheoritical(end);
		rental.setSubscription(false);
		
		rental.setIdCustomer(customer.getId());
		
		
		String bodyAsJson = mvc.perform(
				post(AbstractConstants.Rental.CREATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(rental))
				)
				.andExpect(status().is(400))
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		
				
				assertEquals("", bodyAsJson);

			
		
	}
	
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			 }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createRentalDtoWithNullIdCustomer_shouldReturnError400() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		
		DeviceDtoWithId device = new DeviceDtoWithId();
		device.setId(1L);
				
		LocalDateTime start = LocalDateTime.of(3000, 01, 01, 00, 00);
		LocalDateTime end = LocalDateTime.of(3000, 01, 01, 12, 00);
		RentalDtoCreate rental = new RentalDtoCreate();
		rental.setStartDate(start);
		rental.setEndDateTheoritical(end);
		rental.setSubscription(false);
		rental.setIdDevice(device.getId());
		
		
		
		String bodyAsJson = mvc.perform(
				post(AbstractConstants.Rental.CREATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(rental))
				)
				.andExpect(status().is(400))
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		
				
				assertEquals("", bodyAsJson);

			
		
	}
	
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)"
			}, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void createRentalDtoWithEndDateBeforeStartDate_shouldReturnError400() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		DeviceDtoWithId device = new DeviceDtoWithId();
		device.setId(1L);
		
		
		CustomerUpdateDto customer = new CustomerUpdateDto();
		customer.setId(1L);
		
		
		LocalDateTime start = LocalDateTime.of(3000, 01, 01, 00, 00);
		LocalDateTime end = LocalDateTime.of(1900, 01, 01, 12, 00);
		RentalDtoCreate rental = new RentalDtoCreate();
		rental.setStartDate(start);
		rental.setEndDateTheoritical(end);
		rental.setSubscription(false);
		rental.setIdDevice(device.getId());
		rental.setIdCustomer(device.getId());
		
		
		String bodyAsJson = mvc.perform(
				post(AbstractConstants.Rental.CREATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(rental))
				)
				.andExpect(status().is(400))
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		
				assertEquals("", bodyAsJson);
//				assertThat(response).hasFieldOrPropertyWithValue("rating", 2);
//				assertThat(response).hasFieldOrPropertyWithValue("comment", "fxgfggh");
//				assertThat(response).hasFieldOrPropertyWithValue("deleted", false);
			
		
	}
	
	
	/*****************************************************UPDATE
	 * @throws Exception 
	 * @throws JsonProcessingException 
	 * @throws UnsupportedEncodingException ********************************************/
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateWithValidRentalDto_shouldReturnUpdatedRentalNotNull() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		LocalDateTime start = LocalDateTime.of(3000, 01, 01, 00, 00);
		LocalDateTime end = LocalDateTime.of(3200, 01, 01, 12, 00);
		
		RentalDtoUpdate rental = new RentalDtoUpdate();
		rental.setId(1L);
		rental.setSubscription(true);
		rental.setStartDate(start);
		rental.setEndDateTheoritical(end);
		rental.setIdDevice(1L);
		rental.setIdCustomer(1L);

		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Rental.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(rental))
				)
				.andExpect(status().isOk())
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		RentalDtoUpdate response = mapper.readValue(bodyAsJson, RentalDtoUpdate.class);
				
				assertNotNull(response);
				assertNotNull(response.getId());
				assertNotNull(response.getStartDate());
				assertNotNull(response.getEndDateTheoritical());
				assertEquals(response.getStartDate(), LocalDateTime.of(3000, 01, 01, 00, 00));
				assertEquals(response.getEndDateTheoritical(), LocalDateTime.of(3200, 01, 01, 12, 00));
				assertEquals(response.isSubscription(), true);
				

	}
	
	
	
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateRentalDtoWithPastStartDate_shouldReturnError404() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		LocalDateTime start = LocalDateTime.of(2000, 01, 01, 00, 00);
		LocalDateTime end = LocalDateTime.of(3200, 01, 01, 12, 00);
		
		RentalDtoUpdate rental = new RentalDtoUpdate();
		rental.setId(1L);
		rental.setSubscription(true);
		rental.setStartDate(start);
		rental.setEndDateTheoritical(end);
		rental.setIdDevice(1L);
		rental.setIdCustomer(1L);

		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Rental.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(rental))
				)
				.andExpect(status().is(400))
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		assertEquals("", bodyAsJson);
				

	}
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateRentalDtoWithPastEndDateTheoritical_shouldReturnError404() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		LocalDateTime start = LocalDateTime.of(3000, 01, 01, 00, 00);
		LocalDateTime end = LocalDateTime.of(2000, 01, 01, 12, 00);
		
		RentalDtoUpdate rental = new RentalDtoUpdate();
		rental.setId(1L);
		rental.setSubscription(true);
		rental.setStartDate(start);
		rental.setEndDateTheoritical(end);
		rental.setIdDevice(1L);
		rental.setIdCustomer(1L);

		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Rental.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(rental))
				)
				.andExpect(status().is(400))
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		assertEquals("", bodyAsJson);
				

	}
	
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateRentalDtoWithEndDateTheoriticalBeforeStartDate_shouldReturnError404() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		LocalDateTime start = LocalDateTime.of(3000, 01, 01, 00, 00);
		LocalDateTime end = LocalDateTime.of(2500, 01, 01, 12, 00);
		
		RentalDtoUpdate rental = new RentalDtoUpdate();
		rental.setId(1L);
		rental.setSubscription(true);
		rental.setStartDate(start);
		rental.setEndDateTheoritical(end);
		rental.setIdDevice(1L);
		rental.setIdCustomer(1L);

		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Rental.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(rental))
				)
				.andExpect(status().is(400))
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		assertEquals("", bodyAsJson);
				

	}
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateRentalDtoWithNullIdCustomer_shouldReturnError404() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		LocalDateTime start = LocalDateTime.of(3000, 01, 01, 00, 00);
		LocalDateTime end = LocalDateTime.of(3200, 01, 01, 12, 00);
		
		RentalDtoUpdate rental = new RentalDtoUpdate();
		rental.setId(1L);
		rental.setSubscription(true);
		rental.setStartDate(start);
		rental.setEndDateTheoritical(end);
		rental.setIdDevice(1L);
		

		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Rental.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(rental))
				)
				.andExpect(status().is(400))
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		assertEquals("", bodyAsJson);
				

	}
	
	
	public void updateRentalDtoWithNullIdDevice_shouldReturnError404() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		LocalDateTime start = LocalDateTime.of(3000, 01, 01, 00, 00);
		LocalDateTime end = LocalDateTime.of(3200, 01, 01, 12, 00);
		
		RentalDtoUpdate rental = new RentalDtoUpdate();
		rental.setId(1L);
		rental.setSubscription(true);
		rental.setStartDate(start);
		rental.setEndDateTheoritical(end);
		rental.setIdCustomer(1L);
		

		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Rental.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(rental))
				)
				.andExpect(status().is(400))
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		assertEquals("", bodyAsJson);
				

	}
	
	public void updateRentalDtoWithNullStartDate_shouldReturnError404() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		
		LocalDateTime end = LocalDateTime.of(3200, 01, 01, 12, 00);
		
		RentalDtoUpdate rental = new RentalDtoUpdate();
		rental.setId(1L);
		rental.setSubscription(true);
		
		rental.setEndDateTheoritical(end);
		rental.setIdDevice(1L);
		rental.setIdCustomer(1L);

		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Rental.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(rental))
				)
				.andExpect(status().is(400))
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		assertEquals("", bodyAsJson);
				

	}
	
	public void updateRentalDtoWithNullEndDateTheoritical_shouldReturnError404() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
		LocalDateTime start = LocalDateTime.of(3000, 01, 01, 00, 00);
		
		
		RentalDtoUpdate rental = new RentalDtoUpdate();
		rental.setId(1L);
		rental.setSubscription(true);
		rental.setStartDate(start);
		
		rental.setIdDevice(1L);
		rental.setIdCustomer(1L);

		String bodyAsJson = mvc.perform(
				put(AbstractConstants.Rental.UPDATE)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(rental))
				)
				.andExpect(status().is(400))
				.andReturn()
				.getResponse()
				.getContentAsString();
				
		assertEquals("", bodyAsJson);
				

	}
	
	
	
	
	/*****************************************************AVAILABILITY********************************************/
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
							+ "values(1, 0, 20191125, 21201129, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE",
			"DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void availabilityWithRentalAfterNow_shouldReturnListWith1Entry() throws UnsupportedEncodingException, JsonProcessingException, Exception {
		
				
				
				String bodyAsJson2 = mvc.perform(get(AbstractConstants.Rental.AVAILABILITY, 1L).contentType(MediaType.APPLICATION_JSON))
						
				        .andExpect(content().contentType(MediaType.APPLICATION_JSON))

				        .andExpect(status().isOk())
						.andReturn()
						.getResponse()
						.getContentAsString();
						
						
						List<RentalDtoAvail> response2 = mapper.readValue(bodyAsJson2, new TypeReference <List<RentalDtoAvail>>() {});
					
						assertEquals(1, response2.size());
		
		

	}
	
	
	
}
