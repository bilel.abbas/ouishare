package com.fr.adaming.controller.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fr.adaming.OuiShareWebServiceApplicationTest;
import com.fr.adaming.constants.AbstractConstants;
import com.fr.adaming.dto.CustomerUpdateDto;
import com.fr.adaming.dto.DeviceDtoWithId;
import com.fr.adaming.dto.SubscriptionCreateDto;
import com.fr.adaming.dto.SubscriptionUpdateDto;

/**
 * @author Guillaume V. et Thibaud
 *
 */
public class SubscriptionControllerImplTest extends OuiShareWebServiceApplicationTest {

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20051214, 20060113, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void readAllSubscription_ShouldReturnEmptyList()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		String bodyAsJson = mvc.perform(get(AbstractConstants.Subscription.READ_ALL).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		List<SubscriptionUpdateDto> response = mapper.readValue(bodyAsJson,
				new TypeReference<List<SubscriptionUpdateDto>>() {
				});
		assertTrue(response.isEmpty());

	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20051214, 20060113, false, true)",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (6, 20051214, 20060113, false, true)",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (7, 20051214, 20060113, true, false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void readAllSubscription_ShouldReturnSize1()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		String bodyAsJson = mvc.perform(get(AbstractConstants.Subscription.READ_ALL).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		List<SubscriptionUpdateDto> response = mapper.readValue(bodyAsJson,
				new TypeReference<List<SubscriptionUpdateDto>>() {
				});
		assertEquals(response.size(), 2);
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20051214, 20060113, true, true)",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (7, 20051214, 20060113, true, false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void readAllRealSubscription_ShouldReturnSize2()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		String bodyAsJson = mvc.perform(get(AbstractConstants.Subscription.READ_ALL_REAL).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		List<SubscriptionUpdateDto> response = mapper.readValue(bodyAsJson,
				new TypeReference<List<SubscriptionUpdateDto>>() {
				});
		assertEquals(response.size(), 2);
	}

	@Test
	@Sql(statements = { "Delete FROM subscription", }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void readAllRealSubscription_ShouldReturnEmptyList()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		String bodyAsJson = mvc.perform(get(AbstractConstants.Subscription.READ_ALL_REAL).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		List<SubscriptionUpdateDto> response = mapper.readValue(bodyAsJson,
				new TypeReference<List<SubscriptionUpdateDto>>() {
				});
		assertTrue(response.isEmpty());
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20051214, 20060113, true, true)",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (6, 20051214, 20060113, false, true)",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (7, 20051214, 20060113, true, false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void readDeleteSubscription_ShouldReturnSize2()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		String bodyAsJson = mvc.perform(get(AbstractConstants.Subscription.READ_DELETE).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		List<SubscriptionUpdateDto> response = mapper.readValue(bodyAsJson,
				new TypeReference<List<SubscriptionUpdateDto>>() {
				});
		assertEquals(response.size(), 2);
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20051214, 20060113, true, true)",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (6, 20051214, 20060113, false, true)",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (7, 20051214, 20060113, true, false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void setDeleteTrueValidSubscription_ShouldReturnTrue()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		String bodyAsJson = mvc.perform(put(AbstractConstants.Subscription.SET_DELETED_TRUE, 6L).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("true", bodyAsJson);
	}

	@Test
	@Sql(statements = "delete from customer", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void testCreatingValidSubscription_shouldReturnStatus200WithContentNotNull()
			throws JsonProcessingException, Exception {
		SubscriptionCreateDto dto = new SubscriptionCreateDto();
		CustomerUpdateDto customerDto = new CustomerUpdateDto();
		DeviceDtoWithId deviceDto = new DeviceDtoWithId();

		dto.setStartDate(LocalDate.of(2018, 02, 14));
		dto.setCustomer(customerDto);
		customerDto.setEmail("a@a.fr");
		customerDto.setLastName("Murray");
		customerDto.setFirstName("billy");
		customerDto.setAdress("adresse");
		customerDto.setZipCode(123);
		customerDto.setCity("city");
		customerDto.setPwd("azerty");
		customerDto.setPhone("012345678");
		dto.setDevice(deviceDto);

		deviceDto.setBrand("dyson");
		deviceDto.setChip(150L);
		deviceDto.setDeposit(400D);
		deviceDto.setDescription("ceci est une description");
		deviceDto.setName("aspirateur");
		deviceDto.setPriceExtra(10D);
		deviceDto.setPricePonctual(10D);
		deviceDto.setPriceSub(5D);
		deviceDto.setRentalDurationInHours(1D);
		deviceDto.setRules("les regles");

		String bodyAsJson = mvc
				.perform(post(AbstractConstants.Subscription.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		System.out.println("DEBUG : " + bodyAsJson);

		SubscriptionCreateDto response = mapper.readValue(bodyAsJson, SubscriptionCreateDto.class);

		System.out.println("DEBUG : " + response);

		assertNotNull(response);
		assertThat(response).hasFieldOrPropertyWithValue("startDate", LocalDate.of(2018, 02, 14));
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20051214, 20060113, true, true)",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (6, 20051214, 20060113, false, true)",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (7, 20051214, 20060113, true, false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void setDeleteTrueUnknownSubscription_ShouldReturnFalse()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		String bodyAsJson = mvc.perform(put(AbstractConstants.Subscription.SET_DELETED_TRUE, 120L).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("false", bodyAsJson);
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20051214, 20060113, true, true)",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (6, 20051214, 20060113, false, true)",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (1515151, 20071214, 20080113, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void readByIdValidSubscription_ShouldReturnThisSubscription()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		String bodyAsJson = mvc.perform(get(AbstractConstants.Subscription.READ_ID, 1515151L).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		SubscriptionUpdateDto response = mapper.readValue(bodyAsJson, SubscriptionUpdateDto.class);

		assertNotNull(response);
		assertEquals(1515151, response.getId());
		assertEquals(true, response.isDeleted());
		assertEquals(LocalDate.of(2007, 12, 14), response.getStartDate());
		assertEquals(true, response.isRenew());
	}

	@Test
	@Sql(statements = "DELETE FROM subscription", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void readByIdUnknownSubscription_ShouldReturnNull()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		String bodyAsJson = mvc.perform(get(AbstractConstants.Subscription.READ_ID, 1515151L).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals(bodyAsJson, "");
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20051214, 20060113, true, true)",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (6, 20051214, 20060113, false, true)",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (1515151, 20071214, 20080113, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void deleteValidSubscription_ShouldReturnTrue()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		String bodyAsJson = mvc.perform(delete(AbstractConstants.Subscription.DELETE_ID, 6L).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("true", bodyAsJson);
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20051214, 20060113, true, true)",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (6, 20051214, 20060113, false, true)",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (1515151, 20071214, 20080113, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void deleteUnknownSubscription_ShouldReturnFalse()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		String bodyAsJson = mvc.perform(delete(AbstractConstants.Subscription.DELETE_ID, 25L).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("false", bodyAsJson);
	}

	public void testCreatingSubscriptionFuturStartDate_shouldReturnStatus400WithContentNull()
			throws JsonProcessingException, Exception {
		SubscriptionCreateDto dto = new SubscriptionCreateDto();
		CustomerUpdateDto customerDto = new CustomerUpdateDto();
		DeviceDtoWithId deviceDto = new DeviceDtoWithId();

		dto.setStartDate(LocalDate.of(2021, 02, 14));
		dto.setCustomer(customerDto);
		dto.setDevice(deviceDto);

		String bodyAsJson = mvc
				.perform(post(AbstractConstants.Subscription.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assert (bodyAsJson.isBlank());
	}

	@Test
	public void testCreatingSubscriptionNullStartDate_shouldReturnStatus400WithContentNull()
			throws JsonProcessingException, Exception {
		SubscriptionCreateDto dto = new SubscriptionCreateDto();
		CustomerUpdateDto customerDto = new CustomerUpdateDto();
		DeviceDtoWithId deviceDto = new DeviceDtoWithId();

		dto.setStartDate(null);
		dto.setCustomer(customerDto);
		dto.setDevice(deviceDto);

		String bodyAsJson = mvc
				.perform(post(AbstractConstants.Subscription.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assert (bodyAsJson.isBlank());
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20051214, 20060113, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void testUpateSubscriptionWithValidValues_shouldReturnStatus200ShouldReturnUpdatedSubscription()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		SubscriptionUpdateDto dto = new SubscriptionUpdateDto();
		dto.setStartDate(LocalDate.of(2002, 12, 8));
		dto.setEndDate(LocalDate.of(2003, 01, 13));
		dto.setId(5L);
		String bodyAsJson = mvc
				.perform(put(AbstractConstants.Subscription.UPDATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		SubscriptionUpdateDto response = mapper.readValue(bodyAsJson, SubscriptionUpdateDto.class);

		assertEquals(LocalDate.of(2002, 12, 8), response.getStartDate());
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20051214, 20060113, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void testUpateSubscriptionWithFutureStartDate_shouldReturnStatus400AndNull()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		SubscriptionUpdateDto dto = new SubscriptionUpdateDto();
		dto.setStartDate(LocalDate.of(2020, 12, 8));
		dto.setEndDate(LocalDate.of(2003, 01, 13));
		dto.setId(5L);
		String bodyAsJson = mvc
				.perform(put(AbstractConstants.Subscription.UPDATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assert (bodyAsJson.isBlank());
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20051214, 20060113, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void testUpateNotExistingSubscription_shouldReturnStatus400AndNull()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		SubscriptionUpdateDto dto = new SubscriptionUpdateDto();
		dto.setStartDate(LocalDate.of(2020, 12, 8));
		dto.setEndDate(LocalDate.of(2003, 01, 13));
		dto.setId(55L);
		String bodyAsJson = mvc
				.perform(put(AbstractConstants.Subscription.UPDATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

		assert (bodyAsJson.isBlank());
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20051214, 20060113, false, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void testDesactiveSubscriptionWithValidSubscription_shouldReturnStatus200ShouldReturnTrue()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		String bodyAsJson = mvc.perform(put(AbstractConstants.Subscription.DESACTIVE, 5L).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("true", bodyAsJson);
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20051214, 20060113, false, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void testDesactiveSubscriptionWithUnknownSubscription_shouldReturnStatus200ShouldReturnFalse()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		String bodyAsJson = mvc.perform(put(AbstractConstants.Subscription.DESACTIVE, 20L).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("false", bodyAsJson);
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20051214, 20060113, true, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void testDesactiveSubscriptionWithSubscriptionDeleted_shouldReturnStatus200ShouldReturnFalse()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		String bodyAsJson = mvc.perform(put(AbstractConstants.Subscription.DESACTIVE, 5L).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("false", bodyAsJson);
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20051214, 20060113, false, false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void testDesactiveSubscriptionWithSubscriptionRenewFalse_shouldReturnStatus200ShouldReturnFalse()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		String bodyAsJson = mvc.perform(put(AbstractConstants.Subscription.DESACTIVE, 5L).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("false", bodyAsJson);
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20051214, 20060113, false, false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void testReactiveSubscriptionWithValidSubscription_shouldReturnStatus200ShouldReturnTrue()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		String bodyAsJson = mvc.perform(put(AbstractConstants.Subscription.REACTIVE, 5L).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("true", bodyAsJson);
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20051214, 20060113, false, false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void testReactiveSubscriptionWithUnknownSubscription_shouldReturnStatus200ShouldReturnFalse()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		String bodyAsJson = mvc.perform(put(AbstractConstants.Subscription.REACTIVE, 20L).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("false", bodyAsJson);
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20051214, 20060113, true, false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void testReactiveSubscriptionWithSubscriptionDeleted_shouldReturnStatus200ShouldReturnFalse()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		String bodyAsJson = mvc.perform(put(AbstractConstants.Subscription.REACTIVE, 5L).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("false", bodyAsJson);
	}

	@Test
	@Sql(statements = { "Delete FROM subscription",
			"INSERT INTO subscription (id, start_date, end_date, deleted, renew) VALUES (5, 20051214, 20060113, false, true)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void testReactiveSubscriptionWithSubscriptionRenewTrue_shouldReturnStatus200ShouldReturnFalse()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		String bodyAsJson = mvc.perform(put(AbstractConstants.Subscription.REACTIVE, 5L).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("false", bodyAsJson);
	}

	@Test
	@Sql(statements = { "DELETE FROM subscription", "DELETE FROM customer",
			"Insert INTO customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 123, 'city', 'azerty', false)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew, id_customer) VALUES (3, 20170221, 20170321, false, true, 1)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (9, 20180314, 20180414, false, true)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew, id_customer) VALUES (5, 20170221, 20170321, true, true, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void findByCustomerWithValidCustomer_ShouldReturnSize2()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		CustomerUpdateDto customerDto = new CustomerUpdateDto();

		customerDto.setId(1L);
		customerDto.setEmail("a@a.fr");
		customerDto.setLastName("Murray");
		customerDto.setAdress("adresse");
		customerDto.setZipCode(123);
		customerDto.setCity("city");
		customerDto.setPwd("azerty");

		String bodyAsJson = mvc
				.perform(post(AbstractConstants.Subscription.FIND_CUSTOMER).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(customerDto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		List<SubscriptionUpdateDto> response = mapper.readValue(bodyAsJson,
				new TypeReference<List<SubscriptionUpdateDto>>() {
				});
		assertEquals(response.size(), 2);
	}

	@Test
	@Sql(statements = { "DELETE FROM subscription", "DELETE FROM customer",
			"Insert INTO customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 123, 'city', 'azerty', false)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew, id_customer) VALUES (3, 20170221, 20170321, false, true, 1)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (9, 20180314, 20180414, false, true)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew, id_customer) VALUES (5, 20170221, 20170321, true, true, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void findByCustomerWithUnknownCustomer_ShouldReturnNull()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		CustomerUpdateDto customerDto = new CustomerUpdateDto();

		customerDto.setId(3L);
		customerDto.setEmail("a@a.fr");
		customerDto.setLastName("Murray");
		customerDto.setAdress("adresse");
		customerDto.setZipCode(123);
		customerDto.setCity("city");
		customerDto.setPwd("azerty");

		String bodyAsJson = mvc
				.perform(post(AbstractConstants.Subscription.FIND_CUSTOMER).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(customerDto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		List<SubscriptionUpdateDto> response = mapper.readValue(bodyAsJson,
				new TypeReference<List<SubscriptionUpdateDto>>() {
				});
		
		assertTrue(response.isEmpty());
	}
	
	@Test
	@Sql(statements = { "DELETE FROM subscription", "DELETE FROM customer",
			"Insert INTO customer (id, email, last_name, adress, zip_code, city, pwd, deleted) values (1, 'a@a.fr','Murray', 'adresse', 123, 'city', 'azerty', false)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew, id_customer) VALUES (3, 20170221, 20170321, false, true, 1)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew) VALUES (9, 20180314, 20180414, false, true)",
			"Insert INTO subscription (id, start_date, end_date, deleted, renew, id_customer) VALUES (5, 20170221, 20170321, true, true, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void findByCustomerWithBadCustomer_ShouldReturnError4xx()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		CustomerUpdateDto customerDto = new CustomerUpdateDto();

		customerDto.setId(3L);
		

		String bodyAsJson = mvc
				.perform(get(AbstractConstants.Subscription.FIND_CUSTOMER).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(customerDto)))
				.andExpect(status().is4xxClientError()).andReturn().getResponse().getContentAsString();

	}

}
