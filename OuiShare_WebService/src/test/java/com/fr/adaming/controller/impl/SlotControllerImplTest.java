package com.fr.adaming.controller.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.fr.adaming.OuiShareWebServiceApplicationTest;
import com.fr.adaming.dto.SlotDto;


/**
 * @author Aurélien
 *
 */
public class SlotControllerImplTest extends OuiShareWebServiceApplicationTest {

	private static final String BASE_PATH = "/api/slot/";

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	@Sql(statements = "Delete from Slot", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void testCreatingValidSlot_shouldReturnStatus200WithContentNotNull()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		SlotDto dto = new SlotDto();

		dto.setDimensions("10x10x10");
		dto.setNumber(123L);
		dto.setDeleted(false);

		String bodyAsJson = mvc
				.perform(
						post(BASE_PATH).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		SlotDto response = mapper.readValue(bodyAsJson, SlotDto.class);

		assertNotNull(response);
		assertThat(response).hasFieldOrPropertyWithValue("dimensions", "10x10x10");
		assertThat(response).hasFieldOrPropertyWithValue("number", 123L);
	}

	@Test
	public void testCreatingSlotWithInvalidDimensions_shouldThrowException()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		thrown.expect(MismatchedInputException.class);

		SlotDto dto = new SlotDto();

		dto.setDimensions("10x10");
		dto.setNumber(123L);
		dto.setDeleted(false);

		String bodyAsJson = mvc
				.perform(
						post(BASE_PATH).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();
	}

	@Test
	@Sql(statements = { "Delete from Slot",
			"Insert into Slot (id, dimensions, number,deleted) values (20,'10x10x10',45676,false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void testUpdatingValidSlot_shouldReturnStatus200WithContentNotNull()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		SlotDto dto = new SlotDto(20L, 123465L, "20x20x20", true);

		String bodyAsJson = mvc
				.perform(put(BASE_PATH).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(200)).andReturn().getResponse().getContentAsString();

		SlotDto response = mapper.readValue(bodyAsJson, SlotDto.class);

		assertNotNull(response);
		assertThat(response).hasFieldOrPropertyWithValue("dimensions", "20x20x20");
		assertThat(response).hasFieldOrPropertyWithValue("number", 123465L);
		assertThat(response).hasFieldOrPropertyWithValue("deleted", true);
	}

	@Test
	@Sql(statements = { "Delete from Slot",
			"Insert into Slot (id, dimensions, number,deleted) values (20,'10x10x10',45676,false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void testUpdatingSlotWithInvalidDimensions_shouldThrowException()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {
		thrown.expect(MismatchedInputException.class);

		SlotDto dto = new SlotDto(20L, 123465L, "x20", true);

		String bodyAsJson = mvc
				.perform(put(BASE_PATH).contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();
	}

	@Test
	@Sql(statements = { "Delete from Slot",
			"Insert into Slot (id, dimensions, number,deleted) values (20,'10x10x10',45676,false),(21,'12x12x12',45677,false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void readSlots_ShouldReturnStatus200AndSlots()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		String bodyAsJson = mvc.perform(get(BASE_PATH)).andExpect(status().is(200)).andReturn().getResponse()
				.getContentAsString();

		List<SlotDto> response = mapper.readValue(bodyAsJson, new TypeReference<List<SlotDto>>() {
		});

		assertEquals("10x10x10",response.get(0).getDimensions());
		assertEquals(45676L,response.get(0).getNumber());
		assertEquals(false,response.get(0).isDeleted());

		assertEquals("12x12x12",response.get(1).getDimensions());
		assertEquals(45677L,response.get(1).getNumber());
		assertEquals(false,response.get(1).isDeleted());
	}

	@Test
	@Sql(statements = { "Delete from Slot",
			"Insert into Slot (id, dimensions, number,deleted) values (20,'10x10x10',45676,false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void readSlotByValidId_ShouldReturnStatus200AndSlots()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		String bodyAsJson = mvc.perform(get(BASE_PATH + "/20")).andExpect(status().is(200)).andReturn().getResponse()
				.getContentAsString();

		SlotDto response = mapper.readValue(bodyAsJson, SlotDto.class);

		assertEquals("10x10x10",response.getDimensions());
		assertEquals(45676L,response.getNumber());
		assertEquals(false,response.isDeleted());
	}

	@Test
	@Sql(statements = "Delete from Slot", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void readSlotByNotValidId_ShouldReturnStatus200AndNull()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		String bodyAsJson = mvc.perform(get(BASE_PATH + "/1")).andExpect(status().is(200)).andReturn().getResponse()
				.getContentAsString();

		assertEquals("",bodyAsJson);
	}

	@Test
	@Sql(statements = { "Delete from Slot",
			"Insert into Slot (id, dimensions, number,deleted) values (20,'10x10x10',45676,false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void deleteSlotByValidId_ShouldReturnStatus200AndTrue()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		String bodyAsJson = mvc.perform(delete(BASE_PATH + "/20")).andExpect(status().is(200)).andReturn().getResponse()
				.getContentAsString();

		assertEquals("true",bodyAsJson);
	}

	@Test
	@Sql(statements = { "Delete from Slot" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void deleteSlotByNotValidId_ShouldReturnStatus200AndFalse()
			throws UnsupportedEncodingException, JsonProcessingException, Exception {

		String bodyAsJson = mvc.perform(delete(BASE_PATH + "/1")).andExpect(status().is(200)).andReturn().getResponse()
				.getContentAsString();

		assertEquals("false",bodyAsJson);
	}
	
	@Test
	@Sql(statements = { "Delete from Slot",
	"Insert into Slot (id, dimensions, number,deleted) values (20,'10x10x10',45676,false)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void fakeDeleteSlotByValidId_ShouldReturnStatus200AndTrue() throws UnsupportedEncodingException, Exception{
		String bodyAsJson = mvc.perform(delete(BASE_PATH + "/fake/20")).andExpect(status().is(200)).andReturn().getResponse()
				.getContentAsString();
		
		assertEquals("true",bodyAsJson);
	}
	
	@Test
	@Sql(statements = "Delete from Slot", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	public void fakeDeleteSlotByNotValidId_ShouldReturnStatus200AndFalse() throws UnsupportedEncodingException, Exception{
		String bodyAsJson = mvc.perform(delete(BASE_PATH + "/fake/20")).andExpect(status().is(200)).andReturn().getResponse()
				.getContentAsString();
		
		assertEquals("false",bodyAsJson);
	}
}
