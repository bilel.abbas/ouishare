package com.fr.adaming.controller.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fr.adaming.OuiShareWebServiceApplicationTest;
import com.fr.adaming.constants.AbstractConstants;
import com.fr.adaming.dto.AdminDtoCreate;
import com.fr.adaming.dto.AdminDtoUpdate;

/**
 * @author bilel
 *
 */
public class AdminControllerImplTest extends OuiShareWebServiceApplicationTest {

//	METHODE CREATE
	@Test
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreatingValidAdmin_shouldReturnStatus200WithContentNotNull()
			throws JsonProcessingException, Exception {

		AdminDtoCreate dto = new AdminDtoCreate();
		dto.setEmail("bilel.abbas@gmail.com");
		dto.setPwd("azerty");
		dto.setUserName("bilel-abbas");

		String bodyAsJson = mvc
				.perform(post(AbstractConstants.Admin.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		AdminDtoCreate response = mapper.readValue(bodyAsJson, AdminDtoCreate.class);

		assertNotNull(response);
		assertEquals("bilel.abbas@gmail.com", response.getEmail());
		assertEquals("azerty", response.getPwd());
		assertEquals("bilel-abbas", response.getUserName());

	}

	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel.abbas@gmail.com','azerty',0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreatingAdminExisting_shouldReturnStatus400() throws JsonProcessingException, Exception {

		AdminDtoCreate dto = new AdminDtoCreate();
		dto.setEmail("bilel.abbas@gmail.com");

		String bodyAsJson = mvc.perform(post(AbstractConstants.Admin.CREATE).contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto))).andExpect(status().is(400)).andReturn().getResponse()
				.getContentAsString();
		
		assertEquals("",bodyAsJson);

	}
	
	
	
	@Test
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreatingInValidAdmin_withUserNameNull_shouldReturnStatus400()
			throws JsonProcessingException, Exception {

		AdminDtoCreate dto = new AdminDtoCreate();
		dto.setEmail("bilel.abbas@gmail.com");
		dto.setPwd("azerty");
		dto.setUserName(null);

				String bodyAsJson = mvc.perform(post(AbstractConstants.Admin.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();


				assertEquals("",bodyAsJson);

	}
	
	@Test
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testCreatingInValidAdmin_withPwdTooLong_shouldReturnStatus400()
			throws JsonProcessingException, Exception {

		AdminDtoCreate dto = new AdminDtoCreate();
		dto.setEmail("bilel.abbas@gmail.com");
		dto.setPwd("lemotdepasseesttroplong");
		dto.setUserName("bilel-abbas");

				String bodyAsJson = mvc.perform(post(AbstractConstants.Admin.CREATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().is(400)).andReturn().getResponse().getContentAsString();

				assertEquals("",bodyAsJson);


	}
	
	
	
	
	

//	METHODE UPDATE

	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel.abbas@gmail.com','azerty',0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testUpdateValidAdmin_shouldReturnStatus200WithContentNotNull()
			throws JsonProcessingException, Exception {

		AdminDtoUpdate dto = new AdminDtoUpdate(1L, "bilel-abbas", "bilel.abbas@gmail.com", "azerty", false);
		dto.setEmail("admin@adaming.fr");

		String bodyAsJson = mvc
				.perform(put(AbstractConstants.Admin.UPDATE).contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		AdminDtoUpdate response = mapper.readValue(bodyAsJson, AdminDtoUpdate.class);

		assertNotNull(response);
		assertEquals("admin@adaming.fr", response.getEmail());

	}
	

	

	@Test
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void UpdateAdminInExisting_shouldReturnStatus200ContentNull() throws JsonProcessingException, Exception {

		AdminDtoUpdate dto = new AdminDtoUpdate(1L, "bilel-abbas", "bilel.abbas@gmail.com", "azerty", false);

		String bodyAsJson = mvc.perform(put(AbstractConstants.Admin.UPDATE).contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto))).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();
		
		assertEquals("",bodyAsJson);


	}
	
	


	// Methode ReadById

	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel.abbas@gmail.com','azerty',1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testReadAdminByIdExisting_shouldReturnStatus200WithContentNotNull()
			throws JsonProcessingException, Exception {

//		AdminDtoUpdate dto = new AdminDtoUpdate();

		String bodyAsJson = mvc.perform(get(AbstractConstants.Admin.READ_ID, 1L).contentType(MediaType.APPLICATION_JSON))
//						.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		AdminDtoUpdate response = mapper.readValue(bodyAsJson, AdminDtoUpdate.class);

		assertNotNull(response);
		assertNotNull(response.getId());
		assertEquals(1l, response.getId());
		assertEquals("bilel-abbas", response.getUserName());
		assertEquals("bilel.abbas@gmail.com", response.getEmail());
		assertEquals("azerty", response.getPwd());
		assertTrue(response.isDeleted());
	}

	@Test
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testReadAdminByIdNotExisting_shouldReturnStatus200ContentNull() throws JsonProcessingException, Exception {

		AdminDtoUpdate dto = new AdminDtoUpdate();
	

		String bodyAsJson = mvc.perform(get(AbstractConstants.Admin.READ_ID, 1L).contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(dto))).andExpect(status().isOk()).andReturn().getResponse()
				.getContentAsString();
		
		assertEquals("",bodyAsJson);


	}

	// Methode ReadAllReal

	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel.abbas@gmail.com','azerty',1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testReadAllAdmin_shouldReturnStatus200WithContentNotNull() throws JsonProcessingException, Exception {

		String bodyAsJson = mvc.perform(get(AbstractConstants.Admin.READ_ALL_REAL).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
		System.out.println("Le message est " + bodyAsJson);
		
		List<AdminDtoUpdate> response = mapper.readValue(bodyAsJson, new TypeReference <List<AdminDtoUpdate>>() {});
		
		assertNotNull(response);
		assertEquals(1,response.get(0).getId().intValue());
		assertEquals("bilel-abbas",response.get(0).getUserName());
		assertEquals("bilel.abbas@gmail.com",response.get(0).getEmail());
		assertEquals("azerty",response.get(0).getPwd());
		

	}

//	Methode Delete

	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel.abbas@gmail.com','azerty',1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testdeleteByIdAdmin_shouldReturnStatus200andValueTrue() throws JsonProcessingException, Exception {

//		AdminDtoUpdate dto = new AdminDtoUpdate();


		String bodyAsJson = mvc.perform(delete(AbstractConstants.Admin.DELETE_ID, 1L).contentType(MediaType.APPLICATION_JSON))
//				.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("true", bodyAsJson);
	}

	@Test
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testdeleteByIdInexistAdmin_shouldReturn200andValueFalse() throws JsonProcessingException, Exception {

//		AdminDtoUpdate dto = new AdminDtoUpdate();


		String bodyAsJson = mvc.perform(delete(AbstractConstants.Admin.DELETE_ID, 1L).contentType(MediaType.APPLICATION_JSON))
//				.content(mapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("false", bodyAsJson);

	}
	
//	FindDeleteFalse

	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel.abbas@gmail.com','azerty',0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testFindDeleteFalse_shouldReturnStatus200WithContentNotNull()
			throws JsonProcessingException, Exception {

		String bodyAsJson = mvc.perform(get(AbstractConstants.Admin.READ_ALL).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
		System.out.println("Le message est " + bodyAsJson);
		
		List<AdminDtoUpdate> response = mapper.readValue(bodyAsJson, new TypeReference <List<AdminDtoUpdate>>() {});
		
		assertNotNull(response);
		assertEquals(1,response.get(0).getId().intValue());
		assertEquals("bilel-abbas",response.get(0).getUserName());
		assertEquals("bilel.abbas@gmail.com",response.get(0).getEmail());
		assertEquals("azerty",response.get(0).getPwd());

	}	

	
//	FindDeleteTrue

	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel.abbas@gmail.com','azerty',1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testFindDeleteTrue_shouldReturnStatus200WithContentNotNull() throws JsonProcessingException, Exception {

		String bodyAsJson = mvc.perform(get(AbstractConstants.Admin.READ_DELETE).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
		System.out.println("Le message est " + bodyAsJson);
		
		List<AdminDtoUpdate> response = mapper.readValue(bodyAsJson, new TypeReference <List<AdminDtoUpdate>>() {});
		
		assertNotNull(response);
		assertEquals(1,response.get(0).getId().intValue());
		assertEquals("bilel-abbas",response.get(0).getUserName());
		assertEquals("bilel.abbas@gmail.com",response.get(0).getEmail());
		assertEquals("azerty",response.get(0).getPwd());


	}

//	setDeleteTrue

	@Test
	@Sql(statements = { "delete from admin",
			"insert into admin (id, user_name, email, pwd, deleted) values(1, 'bilel-abbas', 'bilel.abbas@gmail.com','azerty',1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from admin", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void testSetDeleteTrue_shouldReturnStatus200WithContentTrue() throws JsonProcessingException, Exception {



		String bodyAsJson = mvc
				.perform(put(AbstractConstants.Admin.SET_DELETED_TRUE, 1L).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		assertEquals("true", bodyAsJson);

	}
	
	
}
