				#######		OuiShare	#######

***********	CONTEXTE	*************
   
Réalisation d'un site web java  
   
Délai : Back-End à réaliser avant le 28/11/2019 17h30

Description : OuiShare est une startup qui a pour vocation de mettre à disposition, dans les halls d'immeubles des casiers contenant des objets électroménagers et outils diverses pouvant être loués par les habitants de ces immeubles. 
	      L'application OuiShare doit donc permettre la location et la gestion des différents objets (gestions des objets, des abonnements, réservations, des clients...)


I. Rapport Technique 
	1.Cahier des charges
		a. Fonctionnalités
Grand Public : 
- Consulter le site de l'entreprise (concept, services proposés...)
- Consulter les informations pratiques
- S'inscrire comme nouveau client

Pour l'admin : 
- Se connecter
- Enregistrer, ajouter, modifier et afficher les objets
- Afficher toutes les locations, réservations, abonnements
- Enregistrer, ajouter, modifier et afficher les lockers et casiers
- Voir les avis des clients
- ...

Pour le client : 
- se connecter, mettre à jour son profil
- s'abonner à un ou plusieurs objets
- voir la liste de toutes les objets qu'il peut louer dans son immeuble
- réserver un objet
- demander un objet présent dans le catalogue
- afficher toutes ses réservations passées et en cours
- envoyer des demandes/ contacter l'entreprise




		b. Matériels et choix des technologies
Les technologies utilisées lors de ce projet ont été les suivantes : 
- SpringBootApp
- SpringWeb
- SpringData JPA
- Maven
- Lombok
- Git
- Log4J
- Swagger
- JUnit
- Concepts Objets - UML
- Gitlab
- H2
- MockMVC
- Sonar		
- JavaEE


	2. Recette
Recette fonctionnelle : 

Recette technique : 

Extension à implémenter : 
	participation des utilisateurs pour le choix de nouveaux objets disponibles dans leurs casiers.

	3. Difficultés rencontrées et solutions apportées
Une de nos principales difficultés a résidé dans la validation d'un diagramme de classe définitif. En effet celui ci a été modifié à plusieurs reprises.
Une autre grande difficulté a été le respect des délais pour la réalisation des différents sprint. En effet, les délais impartis étant courts et la quantité de travail importante, le temps fut en quelque sorte notre ennemi.

	4. Conclusion
Amélioration à effectuer :



II. Rapport de gestion du projet

	1. Découpage du projet

CONCEPTION
- Définition des fonctionnalités du site web
- Définition de l’architecture
- Définition et répartition des tâches

REALISATION (BACK-END)
- Création du projet commun (dépendances et versioning communs)
- Création des entités
- Mise en commun
- Réalisation des différentes fonctionnalités
- Réalisation des Tests Unitaires et Documentation Swagger(+Sonar)
- Réalisation des Tests Controller(MockMVC)
- Mise en commun


BILAN

- Retour de l’équipe
- Validation

	2. Tâches - Réalisation

La détermination des tâches et la répartition ont été indispensables.

Pour réaliser, l’application, les tâches suivantes étaient la base du projet :
- Réalisation des fonctionnalités/Customer
- Réalisation des fonctionnalités/Review
- Réalisation des fonctionnalités/Subscription
- Réalisation des fonctionnalités/Device
- Réalisation des fonctionnalités/Type
- Réalisation des fonctionnalités/Slot
- Réalisation des fonctionnalités/Locker
- Réalisation des fonctionnalités/Rental
- Réalisation des fonctionnalités/Admin


	3. Organisation de l'équipe


IV. Apport du projet
Ce projet constitue notre projet final marquant donc la fin de nos 3 mois de formation.
Il nous a permis de réaliser un projet concret, et de se rendre compte de la gestion réelle d'un projet en équipe via gitlab.

Avis général :



Développeurs :
	- Bilel Abbas 
	- Théo Corneloup
	- Thibaud Guilard
	- Jean-baptiste Jamet (Relation Client)
	- Aurélien Journet (Chef de Projet)
	- Julie Noterman (Scrum Master)
	- Ambroise Réné
	- Nicolas Ruffier
	- Dylan Salos
	- Guillaume Vittoz


Référent Technique : Guillaume Brias

Chef de Projet : Aurélien Journet

