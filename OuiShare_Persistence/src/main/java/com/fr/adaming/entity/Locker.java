package com.fr.adaming.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Ambroise RENE
 *
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Locker extends AbstractEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String adress;
	
	private Long zipCode;
	
	private String city;
	
	private Long slot;
	
	@Column(unique = true)
	private String name;
	
	@OneToMany
	@JoinColumn(name = "id_locker")
	private List<Slot> slots;
	
	@ManyToOne
	@JoinColumn(name = "id_admin")
	private Admin admins;

	public Locker(String adress, Long zipCode, String city, Long slot, String name,boolean deleted) {
		super();
		this.adress = adress;
		this.zipCode = zipCode;
		this.city = city;
		this.slot = slot;
		this.name = name;
		this.deleted = deleted;
	}

	public Locker(Long id, String adress, Long zipCode, String city, Long slot, String name,boolean deleted) {
		super();
		this.id = id;
		this.adress = adress;
		this.zipCode = zipCode;
		this.city = city;
		this.slot = slot;
		this.name = name;
		this.deleted = deleted;
	}
	
	
}
