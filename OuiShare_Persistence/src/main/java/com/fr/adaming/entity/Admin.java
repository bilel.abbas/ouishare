package com.fr.adaming.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author bilel
 *
 */
@Entity
@Getter @Setter @NoArgsConstructor @AllArgsConstructor @ToString
public class Admin extends AbstractEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false)
	private String userName;
	
	@Column(nullable = false, unique = true)
	private String email;
	
	@Column(nullable = false)
	private String pwd;
	
	@OneToMany(mappedBy = "admins",fetch = FetchType.EAGER)
	private List<Locker> lockers;

	public Admin(Long id, String userName, String email, String pwd) {
		super();
		this.id = id;
		this.userName = userName;
		this.email = email;
		this.pwd = pwd;
	}

	public Admin(Long id) {
		super();
		this.id = id;
	}
	
	
	
	

}
