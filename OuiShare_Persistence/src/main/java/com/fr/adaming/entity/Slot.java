package com.fr.adaming.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Aurélien
 *
 */

@Entity
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class Slot extends AbstractEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private Long number;
	
	private String dimensions;
	
	@ManyToOne
	@JoinColumn(name="id_locker")
	private Locker locker;
	
	@OneToOne
	@JoinColumn(name="id_device")
	private Device device;
	
	public Slot(Long number,String dimensions) {
		this.number=number;
		this.dimensions=dimensions;
	}
	
	public Slot(Long id,Long number,String dimensions,boolean deleted) {
		this.id=id;
		this.number=number;
		this.dimensions=dimensions;
		this.deleted=deleted;
	}
}
