package com.fr.adaming.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Aurélien et Bilel
 *
 */
@Entity
@Getter
@Setter
@AllArgsConstructor @NoArgsConstructor
public class Type extends AbstractEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false, unique = true)
	private String name;

	private int stock;
	
	public Type(Long id,String name,int stock,boolean deleted) {
		this.id=id;
		this.name=name;
		this.stock=stock;
		this.deleted=deleted;
	}
}
