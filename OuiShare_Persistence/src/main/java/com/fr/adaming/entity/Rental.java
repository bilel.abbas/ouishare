package com.fr.adaming.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Nicolas RUFFIER
 *
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Rental extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable = false)
	private LocalDateTime startDate;
	@Column(nullable = false)
	private LocalDateTime endDateTheoritical;
	private LocalDateTime endDateReal;
	private boolean subscription;

	@ManyToOne(optional = false)
	private Device device;

	@ManyToOne(optional = false)
	private Customer customer;

	@OneToOne
	private Review review;

	public Rental(LocalDateTime startDate, LocalDateTime endDateTheoritical, boolean subscription, Device device,
			Customer customer) {
		super();
		this.startDate = startDate;
		this.endDateTheoritical = endDateTheoritical;
		this.subscription = subscription;
		this.device = device;
		this.customer = customer;
	}

	public Rental(Long id, LocalDateTime startDate, LocalDateTime endDateTheoritical, LocalDateTime endDateReal,
			boolean subscription, Device device, Customer customer) {
		super();
		this.id = id;
		this.startDate = startDate;
		this.endDateTheoritical = endDateTheoritical;
		this.endDateReal = endDateReal;
		this.subscription = subscription;
		this.device = device;
		this.customer = customer;
	}

	public Rental(Long id, LocalDateTime startDate, LocalDateTime endDateTheoritical, boolean subscription, Device device,
			Customer customer) {
		super();
		this.id = id;
		this.startDate = startDate;
		this.endDateTheoritical = endDateTheoritical;
		this.subscription = subscription;
		this.device = device;
		this.customer = customer;
	}

}
