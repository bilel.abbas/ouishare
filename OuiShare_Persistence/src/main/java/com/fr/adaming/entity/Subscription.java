package com.fr.adaming.entity;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Thibaud
 *
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Subscription extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable = false)
	private LocalDate startDate;
	private LocalDate endDate;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_customer")
	private Customer customer;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_device")
	private Device device;
	private boolean renew = true;

	public Subscription(LocalDate startDate, Customer customer, Device device) {
		this.startDate = startDate;
		this.customer = customer;
		this.device = device;
		this.deleted = false;
		this.renew = true;
	}

	public Subscription(long id, LocalDate startDate, Customer customer, Device device) {
		super();
		this.id = id;
		this.startDate = startDate;
		this.customer = customer;
		this.device = device;
		this.deleted = false;
		this.renew = true;
	}
	
	@PrePersist
	public void setUpEndDate() {
		this.setEndDate(this.getStartDate().plusMonths(1));
	}

}
