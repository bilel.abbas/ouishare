package com.fr.adaming.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity @Getter @Setter @NoArgsConstructor @ToString
public class Review extends AbstractEntity{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private int rating;
	
	private String comment;
	
	@OneToOne
	private Rental rental;
	
	

	public Review(Long id, int rating, String comment, boolean deleted) {
		super();
		this.id = id;
		this.rating = rating;
		this.comment = comment;
		this.deleted = deleted;
	}

	public Review(int rating, String comment, boolean deleted) {
		super();
		this.rating = rating;
		this.comment = comment;
		this.deleted = deleted;
	}
	
	
	
	

}
