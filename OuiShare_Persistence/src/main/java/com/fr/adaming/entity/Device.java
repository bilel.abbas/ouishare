package com.fr.adaming.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Julie Noterman
 *
 */

@Entity
@Getter @Setter @NoArgsConstructor
public class Device extends AbstractEntity{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;
	
	@Column(nullable = false)
	protected String name;
	
	protected String brand;
	
	protected String description;
	
	@Column(nullable = false)
	protected Double priceSub;
	
	@Column(nullable = false)
	protected Double pricePonctual;
	
	@Column(nullable = false)
	protected Double deposit;
	
	@Column(nullable = false)
	protected Double priceExtra;
	
	protected String state;
	
	protected String stateComment;
	
	@Column(nullable = false)
	protected Long chip;
	
	@Column(nullable = false)
	protected Double rentalDurationInHours;
	
	protected String rules;
	
	protected String urlImage;
	
	@OneToOne
	@JoinColumn(name = "id_slot")
	protected Slot slot;
	
	@OneToOne
	@JoinColumn(name = "id_type")
	protected Type type;
	
	

	public Device(String name, String brand, String description, Double priceSub, Double pricePonctual, Double deposit,
			Double priceExtra, String state, String stateComment, Long chip, Double rentalDurationinHours, String rules) {
		super();
		this.name = name;
		this.brand = brand;
		this.description = description;
		this.priceSub = priceSub;
		this.pricePonctual = pricePonctual;
		this.deposit = deposit;
		this.priceExtra = priceExtra;
		this.state = state;
		this.stateComment = stateComment;
		this.chip = chip;
		this.rentalDurationInHours = rentalDurationinHours;
		this.rules = rules;
	}

	public Device(Long id, String name, String brand, String description, Double priceSub, Double pricePonctual,
			Double deposit, Double priceExtra, String state, String stateComment, Long chip, Double rentalDurationInHours,
			String rules) {
		super();
		this.id = id;
		this.name = name;
		this.brand = brand;
		this.description = description;
		this.priceSub = priceSub;
		this.pricePonctual = pricePonctual;
		this.deposit = deposit;
		this.priceExtra = priceExtra;
		this.state = state;
		this.stateComment = stateComment;
		this.chip = chip;
		this.rentalDurationInHours = rentalDurationInHours;
		this.rules = rules;
	}
	
	
	
	

}
