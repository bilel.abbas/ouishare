package com.fr.adaming.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author CORNELOUP Theo
 *
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Customer extends AbstractEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;
	
	@Column(unique = true, nullable = false)
	protected String email;
	
	@Column(nullable = false)
	protected String lastName;
	
	protected String firstName;
	
	@Column(nullable = false)
	protected String adress;
	
	@Column(nullable = false)
	protected Integer zipCode;
	
	@Column(nullable = false)
	protected String city;
	
	@Column(nullable = false)
	protected String pwd;
	
	protected String phone;
	
	@OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
	protected List<Subscription> subscription;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_locker")
	protected Locker locker;

	public Customer(Long id, String email, String lastName, String adress, Integer zipCode, String city, String pwd) {
		super();
		this.id = id;
		this.email = email;
		this.lastName = lastName;
		this.adress = adress;
		this.zipCode = zipCode;
		this.city = city;
		this.pwd = pwd;
		
	}

	public Customer(String email, String lastName, String adress, Integer zipCode, String city, String pwd) {
		super();
		this.email = email;
		this.lastName = lastName;
		this.adress = adress;
		this.zipCode = zipCode;
		this.city = city;
		this.pwd = pwd;
		
	}
	
	

	

	
	
	
}
