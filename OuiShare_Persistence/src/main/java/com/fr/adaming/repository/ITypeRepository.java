package com.fr.adaming.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fr.adaming.entity.Type;

/**
 * @author Aurélien
 *
 */
@Repository
public interface ITypeRepository extends JpaRepository<Type, Long>,IAbstractRepository<Type, Long> {

}