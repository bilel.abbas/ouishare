package com.fr.adaming.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fr.adaming.entity.Locker;

/**
 * @author Ambroise RENE
 *
 */
@Repository
public interface ILockerRepository extends JpaRepository<Locker, Long>, IAbstractRepository<Locker, Long>{

	@Query(value = "Select * from locker where name like :name" ,nativeQuery = true)
	public Locker existsByName(@Param(value = "name")String name);
	
	@Query(value = "Select distinct type.name from type,device,slot,locker where type.id=device.id_type and device.id=slot.id_device and slot.id_locker = locker.id and locker.id =:id " ,nativeQuery = true)
	public List<String> typeList(@Param(value = "id")Long idLocker);
	
}
