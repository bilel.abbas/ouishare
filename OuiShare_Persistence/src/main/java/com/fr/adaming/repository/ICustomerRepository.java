package com.fr.adaming.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fr.adaming.entity.Customer;

/**
 * @author CORNELOUP Theo
 *
 */
@Repository
public interface ICustomerRepository extends JpaRepository<Customer, Long>, IAbstractRepository<Customer, Long>{
 
	public List<Customer> findByAdress(String adress);
	public List<Customer> findByLastName(String lastName);
	public List<Customer> findByZipCode(Integer zipCode);
	public List<Customer> findByCity(String city);
	public Customer findByEmailAndPwd(String email, String pwd);
	public boolean existsByEmail(String email);
	public Customer findByEmail(String email);

	
	
}
