package com.fr.adaming.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fr.adaming.entity.Slot;

/**
 * @author Aurélien
 *
 */
public interface ISlotRepository extends JpaRepository<Slot, Long>,IAbstractRepository<Slot, Long>{
	
	public Slot findByNumber(Long number);

	
}
