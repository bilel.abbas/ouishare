package com.fr.adaming.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fr.adaming.entity.Customer;
import com.fr.adaming.entity.Subscription;

/**
 * @author Thibaud
 *
 */
@Repository
public interface ISubscriptionRepository extends JpaRepository<Subscription, Long>, IAbstractRepository<Subscription, Long>{
	
	@Query(name = "select * from subscription where customer like :customer")
	public List<Subscription> findByCustomer(Customer customer);
	
}
