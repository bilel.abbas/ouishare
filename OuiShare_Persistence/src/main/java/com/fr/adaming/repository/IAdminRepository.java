package com.fr.adaming.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.fr.adaming.entity.Admin;

/**
 * @author bilel
 *
 */
@Repository
public interface IAdminRepository extends IAbstractRepository<Admin, Long>, JpaRepository<Admin, Long> {

	public Admin readByEmailAndPwd(String email, String pwd);

}
