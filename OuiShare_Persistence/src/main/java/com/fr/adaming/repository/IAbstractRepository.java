package com.fr.adaming.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.fr.adaming.entity.AbstractEntity;

public interface IAbstractRepository<E extends AbstractEntity, ID> {

	public List<E> findByDeletedTrue();

	public List<E> findByDeletedFalse();	
	/**
	 * this method will mark an entity as deleted (update deleted to TRUE)
	 * 
	 * @param id identifier of the entity
	 * @return the number of affected rows in DB
	 */

	@Transactional
	@Modifying
	@Query(value = "UPDATE #{#entityName} SET deleted = true WHERE id = :id")
	public int fakeDelete(ID id);

}
