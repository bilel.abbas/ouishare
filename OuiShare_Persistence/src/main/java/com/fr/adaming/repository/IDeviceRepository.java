package com.fr.adaming.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fr.adaming.entity.Device;



/**
 * @author Julie Noterman and Aurélien Journet
 *
 */
@Repository
public interface IDeviceRepository extends JpaRepository<Device, Long>, IAbstractRepository<Device, Long> {
	

	@Query(value = "SELECT * FROM device WHERE id_slot is null", nativeQuery = true)
	public List<Device> readAllCatalog();

	
	@Query(value="select * from device,locker,slot where "
			+ "locker.id=slot.id_locker "
			+ "and slot.id=device.id_slot "
			+ "and locker.id=:id and device.deleted=false",nativeQuery=true)
	public List<Device> getDevicesInLockerById(@Param(value = "id")Long id);

	@Query(value="select * from device,locker,slot,customer where "
			+ "locker.id=slot.id_locker "
			+ "and slot.id=device.id_slot "
			+ "and customer.id_locker=locker.id "
			+ "and customer.id=:id and device.deleted=false",nativeQuery=true)
	public List<Device> getDevicesInCustomerById(@Param(value = "id")Long id);
	
	
	@Query(value="select * from device,subscription,customer where "
			+ "subscription.id_customer=customer.id "
			+ "and subscription.id_device=device.id "
			+ "and subscription.renew=true "
			+ "and customer.id=:id and device.deleted=false",nativeQuery=true)
	public List<Device> getDevicesInSubscriptionsByCustomerId(@Param(value = "id")Long id);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE device SET id_slot = :idSlot WHERE id = :idDevice", nativeQuery = true )
	public void addSlot(Long idDevice, Long idSlot);

	
	
}
