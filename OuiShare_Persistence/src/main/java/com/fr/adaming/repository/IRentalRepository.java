package com.fr.adaming.repository;

import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fr.adaming.entity.Device;
import com.fr.adaming.entity.Rental;


/**
 * @author Nicolas RUFFIER
 *
 */
@Repository
public interface IRentalRepository extends JpaRepository<Rental, Long>, IAbstractRepository<Rental, Long>{

	@Query(value = "SELECT * FROM rental WHERE device_id = :idDevice AND "
			+ "((start_date BETWEEN :startDate AND :endDateTheoritical) "
			+ "OR (end_date_theoritical BETWEEN :startDate AND :endDateTheoritical)"
			+ "OR ((start_date < :startDate) AND (end_date_theoritical > :endDateTheoritical)))", nativeQuery = true)
	public List<Rental> available(@Param(value = "idDevice") Long idDevice, @Param(value = "startDate") LocalDateTime startDate, @Param(value = "endDateTheoritical") LocalDateTime endDateTheoritical);

	public List<Rental> findByDeviceAndStartDateBetweenOrEndDateTheoriticalBetween(Device d,LocalDateTime start,LocalDateTime end, LocalDateTime x, LocalDateTime y);
	
	public List<Rental> findByDeviceAndEndDateTheoriticalAfter(Device device, LocalDateTime now);

	@Transactional
	@Modifying
	@Query(value = "update rental set review_id = :idReview where id = :idRental", nativeQuery = true)
	public void addReview(Long idRental, Long idReview);

	@Transactional
	@Modifying
	@Query(value = "update rental set end_date_real = :endDate where id = :id", nativeQuery = true)
	public void addEndDateReal(Long id, LocalDateTime endDate);
	
}
