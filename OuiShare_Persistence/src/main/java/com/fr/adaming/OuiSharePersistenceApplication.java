package com.fr.adaming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OuiSharePersistenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OuiSharePersistenceApplication.class, args);
	}
}
