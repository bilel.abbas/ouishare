package com.fr.adaming.repository;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.fr.adaming.OuiSharePersistenceApplication;
import com.fr.adaming.entity.Device;
import com.fr.adaming.entity.Rental;
import com.fr.adaming.repository.IRentalRepository;

/**
 * @author Nicolas RUFFIER
 *
 */
@SpringBootTest(classes = OuiSharePersistenceApplication.class)
public class IRentalRepositoryTest {

	@Autowired
	private IRentalRepository repo;

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void availableSQLOk_shouldReturnListEmpty() {
		List<Rental> list = repo.available(1L, LocalDateTime.of(2000, 01, 01, 00, 00),
				LocalDateTime.of(2000, 01, 01, 02, 00));
		assertTrue(list.isEmpty());
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void availableOk_shouldReturnListEmpty() {

		Device device = new Device();
		device.setId(1L);
		device.setName("a");
		device.setPriceSub(1D);
		device.setPricePonctual(1D);
		device.setPriceExtra(1D);
		device.setDeposit(1D);
		device.setChip(1L);
		device.setRentalDurationInHours(1D);
		List<Rental> list = repo.findByDeviceAndStartDateBetweenOrEndDateTheoriticalBetween(device,
				LocalDateTime.of(2000, 01, 01, 00, 00), LocalDateTime.of(2000, 01, 01, 02, 00),
				LocalDateTime.of(2000, 01, 01, 00, 00), LocalDateTime.of(2000, 01, 01, 02, 00));
		assertTrue(list.isEmpty());
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void availableSQLWithDateDebutInvalid_shouldReturnListNotEmpty() {
		List<Rental> list = repo.available(1L, LocalDateTime.of(2000, 01, 25, 00, 00),
				LocalDateTime.of(2001, 01, 01, 02, 00));
		assertFalse(list.isEmpty());
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1 ,1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void availableWithDateDebutInvalid_shouldReturnListNotEmpty() {

		Device device = new Device();
		device.setId(1L);
		device.setName("a");
		device.setPriceSub(1D);
		device.setPricePonctual(1D);
		device.setPriceExtra(1D);
		device.setDeposit(1D);
		device.setChip(1L);
		device.setRentalDurationInHours(1D);
		List<Rental> list = repo.findByDeviceAndStartDateBetweenOrEndDateTheoriticalBetween(device,
				LocalDateTime.of(2000, 01, 25, 00, 00), LocalDateTime.of(2001, 01, 25, 00, 00),
				LocalDateTime.of(2000, 01, 25, 00, 00), LocalDateTime.of(2001, 01, 25, 00, 00));
		assertFalse(list.isEmpty());
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void availableSQLWithDateEndInvalid_shouldReturnListNotEmpty() {
		List<Rental> list = repo.available(1L, LocalDateTime.of(1999, 01, 25, 00, 00),
				LocalDateTime.of(2000, 01, 25, 00, 00));
		assertFalse(list.isEmpty());
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void availableWithDateEndInvalid_shouldReturnListNotEmpty() {

		Device device = new Device();
		device.setId(1L);
		device.setName("a");
		device.setPriceSub(1D);
		device.setPricePonctual(1D);
		device.setPriceExtra(1D);
		device.setDeposit(1D);
		device.setChip(1L);
		device.setRentalDurationInHours(1D);
		List<Rental> list = repo.findByDeviceAndStartDateBetweenOrEndDateTheoriticalBetween(device,
				LocalDateTime.of(1999, 01, 25, 00, 00), LocalDateTime.of(2000, 01, 25, 00, 00),
				LocalDateTime.of(1999, 01, 25, 00, 00), LocalDateTime.of(2000, 01, 25, 00, 00));
		assertFalse(list.isEmpty());
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (2,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000102, 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(2, 0, 20000101, 20000131, 0, 2, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void availableSQLWithOtherDeviceUnavaiableBothDate_shouldReturnListEmpty() {
		List<Rental> list = repo.available(1L, LocalDateTime.of(2000, 01, 24, 00, 00),
				LocalDateTime.of(2000, 01, 25, 00, 00));
		assertTrue(list.isEmpty());
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (2,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(2, 0, 20000101, 20000131, 0, 2, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void availableWithOtherDeviceUnavaiableBothDate_shouldReturnListEmpty() {

		Device device = new Device();
		device.setId(1L);
		device.setName("a");
		device.setPriceSub(1D);
		device.setPricePonctual(1D);
		device.setPriceExtra(1D);
		device.setDeposit(1D);
		device.setChip(1L);
		device.setRentalDurationInHours(1D);
		List<Rental> list = repo.findByDeviceAndStartDateBetweenOrEndDateTheoriticalBetween(device,
				LocalDateTime.of(2000, 01, 24, 00, 00), LocalDateTime.of(2000, 01, 25, 00, 00),
				LocalDateTime.of(2000, 01, 24, 00, 00), LocalDateTime.of(2000, 01, 25, 00, 00));
		assertTrue(list.isEmpty());
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (2,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 19990101, 19990102, 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(2, 0, 20000101, 20000131, 0, 2, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void availableSQLWithOtherDeviceUnavaiableStartDate_shouldReturnListEmpty() {
		List<Rental> list = repo.available(1L, LocalDateTime.of(2000, 01, 24, 00, 00),
				LocalDateTime.of(2001, 01, 25, 00, 00));
		assertTrue(list.isEmpty());
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (2,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(2, 0, 20000101, 20000131, 0, 2, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void availableWithOtherDeviceUnavaiableStartDate_shouldReturnListEmpty() {

		Device device = new Device();
		device.setId(1L);
		device.setName("a");
		device.setPriceSub(1D);
		device.setPricePonctual(1D);
		device.setPriceExtra(1D);
		device.setDeposit(1D);
		device.setChip(1L);
		device.setRentalDurationInHours(1D);
		List<Rental> list = repo.findByDeviceAndStartDateBetweenOrEndDateTheoriticalBetween(device,
				LocalDateTime.of(2001, 01, 24, 00, 00), LocalDateTime.of(2000, 01, 25, 00, 00),
				LocalDateTime.of(2000, 01, 24, 00, 00), LocalDateTime.of(2000, 01, 25, 00, 00));
		assertTrue(list.isEmpty());
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (2,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 19990101, 19990102, 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(2, 0, 20000101, 20000131, 0, 2, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void availableSQLWithOtherDeviceUnavaiableEndDate_shouldReturnListEmpty() {
		List<Rental> list = repo.available(1L, LocalDateTime.of(1999, 01, 24, 00, 00),
				LocalDateTime.of(2000, 01, 25, 00, 00));
		assertTrue(list.isEmpty());
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (2,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(2, 0, 20000101, 20000131, 0, 2, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void availableWithOtherDeviceUnavaiableEndDate_shouldReturnListEmpty() {

		Device device = new Device();
		device.setId(1L);
		device.setName("a");
		device.setPriceSub(1D);
		device.setPricePonctual(1D);
		device.setPriceExtra(1D);
		device.setDeposit(1D);
		device.setChip(1L);
		device.setRentalDurationInHours(1D);
		List<Rental> list = repo.findByDeviceAndStartDateBetweenOrEndDateTheoriticalBetween(device,
				LocalDateTime.of(1999, 01, 24, 00, 00), LocalDateTime.of(2000, 01, 25, 00, 00),
				LocalDateTime.of(1999, 01, 24, 00, 00), LocalDateTime.of(2000, 01, 25, 00, 00));
		// Doit retourner un objet avec un autre device
		assertFalse(list.isEmpty());
	}
	
	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, 20000101, 20000131, 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void availableSQLWithBothDateOutsideRental_shouldReturnListNotEmpty() {
		List<Rental> list = repo.available(1L,
				LocalDateTime.of(2000, 01, 24, 00, 00), LocalDateTime.of(2000, 01, 25, 00, 00));
		assertFalse(list.isEmpty());
	}
	
	
	
	/********************************* Avaiability ***************************/

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, '20000101 00:00', '20000101 01:00', 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(2, 0, '20000101 01:15', '20000131 02:15', 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(3, 0, '20000101 02:30', '20000131 03:30', 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void checkDispoWithOneDevice_shouldReturnListWith3Entity() {
		Device device = new Device();
		device.setId(1L);
		device.setName("a");
		device.setPriceSub(1D);
		device.setPricePonctual(1D);
		device.setPriceExtra(1D);
		device.setDeposit(1D);
		device.setChip(1L);
		device.setRentalDurationInHours(1D);
		List<Rental> list = repo.findByDeviceAndEndDateTheoriticalAfter(device, LocalDateTime.of(1999, 01, 01, 00, 00));

		assertSame(3, list.size());
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, '19990101 00:00', '19990101 01:00', 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(2, 0, '20000101 00:00', '20000101 01:00', 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(3, 0, '20000101 01:15', '20000131 02:15', 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(4, 0, '20000101 02:30', '20000131 03:30', 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void checkDispoWithOneDeviceAndRentalBeforeNow_shouldReturnListWith3Entity() {
		Device device = new Device();
		device.setId(1L);
		device.setName("a");
		device.setPriceSub(1D);
		device.setPricePonctual(1D);
		device.setPriceExtra(1D);
		device.setDeposit(1D);
		device.setChip(1L);
		device.setRentalDurationInHours(1D);
		List<Rental> list = repo.findByDeviceAndEndDateTheoriticalAfter(device, LocalDateTime.of(2000, 01, 01, 00, 00));

		assertSame(3, list.size());
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, '19990101 00:00', '19990101 01:00', 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(2, 0, '19991231 23:00', '20000101 00:00', 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(3, 0, '20000101 00:00', '20000101 01:00', 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(4, 0, '20000101 01:15', '20000131 02:15', 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(5, 0, '20000101 02:30', '20000131 03:30', 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void checkDispoWithOneDeviceAndRentalBeforeNowAndEndNow_shouldReturnListWith3Entity() {
		Device device = new Device();
		device.setId(1L);
		device.setName("a");
		device.setPriceSub(1D);
		device.setPricePonctual(1D);
		device.setPriceExtra(1D);
		device.setDeposit(1D);
		device.setChip(1L);
		device.setRentalDurationInHours(1D);
		List<Rental> list = repo.findByDeviceAndEndDateTheoriticalAfter(device, LocalDateTime.of(2000, 01, 01, 00, 00));

		assertSame(3, list.size());
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(2, 0, '19991231 23:00', '20000101 00:01', 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(3, 0, '20000101 01:00', '20000101 02:00', 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(4, 0, '20000101 01:15', '20000131 02:15', 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(5, 0, '20000101 02:30', '20000131 03:30', 0, 1, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void checkDispoWithOneDeviceAndRentalStartBeforeNowAndEndAfterNow_shouldReturnListWith4Entity() {
		Device device = new Device();
		device.setId(1L);
		device.setName("a");
		device.setPriceSub(1D);
		device.setPricePonctual(1D);
		device.setPriceExtra(1D);
		device.setDeposit(1D);
		device.setChip(1L);
		device.setRentalDurationInHours(1D);
		List<Rental> list = repo.findByDeviceAndEndDateTheoriticalAfter(device, LocalDateTime.of(2000, 01, 01, 00, 00));

		assertSame(4, list.size());
	}

	@Test
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER",
			"insert into customer (id, email, last_name, adress, zip_code, city, pwd, phone, deleted)"
					+ " values (1,'a','a', 'a', 1, 'a', 'a', 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (1,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (2,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into device (id, name, price_sub, price_ponctual, price_extra, deposit, chip, rental_duration_in_hours, deleted)"
					+ " values (3,'a', 1, 1, 1, 1, 1, 1, 0)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(1, 0, '20000101 00:00', '20000101 01:00', 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(2, 0, '20000101 01:15', '20000131 02:15', 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(3, 0, '20000101 02:30', '20000131 03:30', 0, 1, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(4, 0, '20000101 00:00', '20000101 01:00', 0, 2, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(5, 0, '20000101 01:15', '20000131 02:15', 0, 2, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(6, 0, '20000101 02:30', '20000131 03:30', 0, 2, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(7, 0, '20000101 00:00', '20000101 01:00', 0, 3, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(8, 0, '20000101 01:15', '20000131 02:15', 0, 3, 1)",
			"insert into rental (id, deleted, start_date, end_date_theoritical, subscription, device_id, customer_id) "
					+ "values(9, 0, '20000101 02:30', '20000131 03:30', 0, 3, 1)" }, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = { "DELETE FROM RENTAL", "DELETE FROM DEVICE", "DELETE FROM CUSTOMER" }, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void checkDispoWithManyDevice_shouldReturnListWith3Entity() {
		Device device = new Device();
		device.setId(1L);
		device.setName("a");
		device.setPriceSub(1D);
		device.setPricePonctual(1D);
		device.setPriceExtra(1D);
		device.setDeposit(1D);
		device.setChip(1L);
		device.setRentalDurationInHours(1D);
		List<Rental> list = repo.findByDeviceAndEndDateTheoriticalAfter(device, LocalDateTime.of(1999, 01, 01, 00, 00));

		assertSame(3, list.size());
	}

}
